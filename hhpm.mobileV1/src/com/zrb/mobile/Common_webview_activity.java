package com.zrb.mobile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.CenterProgressWebView.IOnJsPrompt;
import com.zrb.mobile.utility.Constants;

public class Common_webview_activity extends BaseActivity implements OnClickListener, IOnJsPrompt {
 
	CenterProgressWebView mWebView;
     String mtitle,murl;
  
     public static void launch (Context mActivity,String url,String title){
 		
		 if(mActivity==null) return;
		 Intent it = new Intent(mActivity, Common_webview_activity.class);
		 it.putExtra("url", url);
		 it.putExtra("title", title);
		 mActivity.startActivity(it);
	}
     
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
 	  
		setContentView(R.layout.common_webview_layout);
		if (bundle != null)
			restoreSelf(bundle);
		else {
			murl = this.getIntent().getStringExtra("url");
			mtitle = this.getIntent().getStringExtra("title");
		}
 		initView();
 	}
 
	@Override
	protected  void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("url", murl);
        outState.putString("title", mtitle);
    }

    private void restoreSelf(Bundle savedInstanceState)
    {
    	murl = savedInstanceState.getString("url");
    	mtitle = savedInstanceState.getString("title");
    }

	private void initView() {
 		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
       	
		this.findViewById(R.id.txt_title).setVisibility(View.GONE) ;
		
 	    mWebView = (CenterProgressWebView) this.findViewById(R.id.webView1);
 	    mWebView.setOnJsPromptListener(this);
 	 
		 mWebView.loadUrl(murl);
 		//mWebView.loadUrl("http://www.baidu.com");

	}
 	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.btn_project_call:
 		     Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +Constants.serverPhone));
			 this.startActivity(intent);
			 break;
 
			 
//		 case R.id.write_comments_layout:
//			 goWritecomment(Project_commentActivity.class,102);
// 			 break;
//		 case R.id.rightbutton_comment_list:
//			 Intent it4 = new Intent(Project_info_lience.this, Project_commentlist_Activity.class);
//			 it4.putExtra("projectId", projectId);
//		     startActivity(it4); 
// 			 break;
	 	 case R.id.title_left_root:
			this.finish();
			break;
 
		}
 	}
	
    
	 
	
	
	@Override
	public void PromptInfo(String message, String defaultValue) {
		// TODO Auto-generated method stub
		Log.e("PromptInfo", message);//id
		if(TextUtils.isEmpty(message)) return;
		if(!message.contains(",")) return;
		//CommonUtil.showToast(message+"|"+defaultValue, this);
		 String[] tmpArr=message.split(",");
 
	      if( "1".equals(tmpArr[0])){
	    	  if(tmpArr[1].equals("0")) return;
 	  	  	  Intent it = new Intent(this, BaiduMapActivity.class);
 		 	  it.putExtra("latitude", Double.parseDouble(tmpArr[2]));
		 	  it.putExtra("longitude", Double.parseDouble(tmpArr[1]));
		 	  it.putExtra("address", "");
		 	  startActivity(it);
 	      }
 
	}

	
	 
	@Override
	public void ReceivedTitle(String title) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void	onActivityResult(int requestCode, int resultCode, Intent data){
 	    if(resultCode==RESULT_OK){
		  switch (requestCode) { // resultCodeΪ�ش��ı�ǣ�����B�лش�����RESULT_OK
 		    case 105:
		    //	startChat();
		    	break;
		 
		   default:
			break;
		  }
 	    }
 	    super.onActivityResult(requestCode, resultCode, data);
   } 
 
	
}
