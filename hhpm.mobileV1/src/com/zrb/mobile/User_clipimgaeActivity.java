package com.zrb.mobile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

 
 
import com.zrb.applib.utils.FileUtil;
import com.zrb.mobile.common.LoadUserAvatar;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.ClipImageLayout;
import com.zrb.mobile.utility.CommonUtil;

import android.media.ExifInterface;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.TextView;

public class User_clipimgaeActivity extends BaseActivity implements OnClickListener {

	private ClipImageLayout mClipImageLayout;
    private String mfilename;
    private String mImagename;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.u_center_clip_image);

		mClipImageLayout = (ClipImageLayout) findViewById(R.id.id_clipImageLayout);
		mfilename=  this.getIntent().getStringExtra("filename");
		mImagename=  this.getIntent().getStringExtra("imagename");
		initView();
		if(!TextUtils.isEmpty(mfilename)){
		    //Bitmap bitmap = BitmapFactory.decodeFile(LoadUserAvatar.getAvatarpath()+"/" + mfilename);
		    
		    //Bitmap bitmap =  CommonUtil.getImage(LoadUserAvatar.getAvatarpath()+"/" + mfilename);
			
			int curDegree=getBitmapDegree(mfilename);
			Bitmap  bitmap =  CommonUtil.getImage(mfilename);
			if(curDegree==0) 
				mClipImageLayout.setImageBitmap(bitmap);
			else
		 	 
				mClipImageLayout.setImageBitmap(rotateBitmapByDegree(bitmap,curDegree));
		 
			
		}
	}

	public static Bitmap rotateBitmapByDegree(Bitmap bm, int degree) {
	    Bitmap returnBm = null;
	  
	    // 根据旋转角度，生成旋转矩阵
	    Matrix matrix = new Matrix();
	    matrix.postRotate(degree);
	    try {
	        // 将原始图片按照旋转矩阵进行旋转，并得到新的图片
	        returnBm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
	    } catch (OutOfMemoryError e) {
	    }
	    if (returnBm == null) {
	        returnBm = bm;
	    }
	    if (bm != returnBm) {
	        bm.recycle();
	    }
	    return returnBm;
	}
	
	private int getBitmapDegree(String path) {
	    int degree = 0;
	    try {
	        // 从指定路径下读取图片，并获取其EXIF信息
	        ExifInterface exifInterface = new ExifInterface(path);
	        // 获取图片的旋转信息
	        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
	                ExifInterface.ORIENTATION_NORMAL);
	        switch (orientation) {
	        case ExifInterface.ORIENTATION_ROTATE_90:
	            degree = 90;
	            break;
	        case ExifInterface.ORIENTATION_ROTATE_180:
	            degree = 180;
	            break;
	        case ExifInterface.ORIENTATION_ROTATE_270:
	            degree = 270;
	            break;
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return degree;
	}
    private void initView(){
	  
	    TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("头像裁剪");
		
		TextView rightHite=	(TextView)this.findViewById(R.id.right_title);
		rightHite.setVisibility(View.VISIBLE);
		rightHite.setText("完成");
		rightHite.setOnClickListener(this);
		
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
 
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		   case R.id.title_left_root:
			 this.finish();
			 break;
		   case R.id.right_title:
			   saveBitmap();
			   break;
 		}
 	}

	//LoadUserAvatar.getAvatarpath(), imageName)
	private void saveBitmap(){
		Bitmap bitmap = mClipImageLayout.clip();
		FileUtil.saveBitmap(LoadUserAvatar.getAvatardir(),mImagename,getsmallBitmap(bitmap));
		//this.setr
		 
		 
		this.setResult(android.app.Activity.RESULT_OK);
    	this.finish();
	}
	
	 private  Bitmap getsmallBitmap(Bitmap bitmap) {
		 
		  int w = bitmap.getWidth();  
	      float hh = 80f;//这里设置高度为800f  
		  Matrix matrix = new Matrix(); 
		  matrix.postScale(hh/w,hh/w); //长和宽放大缩小的比例
		  Bitmap resizeBmp = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
		  return resizeBmp;
		 }
	
	/*@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.id_action_clip:
			Bitmap bitmap = mClipImageLayout.clip();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] datas = baos.toByteArray();
			
			Intent intent = new Intent(this, ShowImageActivity.class);
			intent.putExtra("bitmap", datas);
			startActivity(intent);

			break;
		}
		return super.onOptionsItemSelected(item);
	}*/
}
