package com.zrb.mobile;

import com.zrb.mobile.ui.CenterProgressWebView;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.Window;
import android.webkit.WebSettings;

public class TestHomeActivity extends Activity {

	CenterProgressWebView mWebView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		
		setContentView(R.layout.activity_home);
	
		mWebView=(CenterProgressWebView)this.findViewById(R.id.webView1);
		
		String url="file:///android_asset/www/index.html";

		mWebView.loadUrl(url);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

}
