package com.zrb.mobile;

import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.utility.AlertDialog;
import com.zrb.mobile.utility.DialogBuilder;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class BaseCheckloginActivity extends BaseActivity {

	Dialog PopDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	public void showLoginDialog() {
		 
		
		 new AlertDialog(BaseCheckloginActivity.this).builder().setTitle("登录提示")
		   .setMsg("登录后才可以使用该功能")
		   .setPositiveButton("马上登录", new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent1 = new Intent(BaseCheckloginActivity.this, User_phone_login.class);
				startActivity(intent1);
			}
		    }).setNegativeButton("取消").show();

	}

}
