package com.zrb.mobile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.hhpm.lib.PageIndicator.TabPageIndicator;
import com.zrb.mobile.adapter.NewsFragmentPagerAdapter;
import com.zrb.mobile.adapter.model.NewTypeDto;
import com.zrb.mobile.adapter.model.NewTypeDtos;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.applib.utils.FileUtil;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class News_Main2Fragment extends Fragment implements OnClickListener
{
//	private List<Fragment> mTabContents = new ArrayList<Fragment>();
	private FragmentPagerAdapter mAdapter;
	private ViewPager mViewPager;
	List<NewTypeDto> mDatas=null;
	// private List<String> mDatas = Arrays.asList( "ȫ������","�����ȵ�","�۵���","Ͷ�ʶ�̬","���߷���");
	 //private int[] mDataIds ={-1,20,32,28,14};
//	private List<String> mDatas = Arrays.asList("����", "�ղ�", "�Ƽ�");
	//private String[] mDatas={ "ȫ������","�����ȵ�","�۵���","Ͷ�ʶ�̬","���߷���"};
	boolean isFirst=true;
	//private ViewPagerIndicator mIndicator;
	TabPageIndicator  mIndicator;
	int currentPageIdx=0;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
 	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
		View view = inflater.inflate(R.layout.news_home_fragment, container, false);

		//InitView();
 		TextView mtitle=(TextView)view.findViewById(R.id.titleText);
 		mtitle.setOnClickListener(this);
		mViewPager = (ViewPager) view.findViewById(R.id.id_pager);
		//mIndicator = (ViewPagerIndicator) view.findViewById(R.id.id_indicator);
		mIndicator = (TabPageIndicator) view.findViewById(R.id.id_indicator);
		
		view.findViewById(R.id.searchButton).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent it = new Intent(News_Main2Fragment.this.getActivity(), Search_hottag_Activity.class);
				News_Main2Fragment.this.getActivity().startActivity(it);
			}});
		
 		return view;
	}
 	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
 		 super.onActivityCreated(savedInstanceState);
 		//if (this.getUserVisibleHint()) firstLoad();
	}
	
	@Override
	public void onResume()
	{
 		super.onResume();
 		 if (this.getUserVisibleHint()) firstLoad();
   		//super.setUserVisibleHint(isVisibleToUser)
	}
 
	private void firstLoad(){
		
		if(isFirst) { 
			isFirst=false;
			//LoadCategory();
		    try {
				 String results=	CommonUtil.loadFileAsString(FileUtil.NewTypeName);
	 	         Gson gson = new Gson() ;  
		            
		         NewTypeDtos tmpDto=	gson.fromJson(results, NewTypeDtos.class);
		         NewTypeDto firstDto=new NewTypeDto();
		         firstDto.id=-1;
		         firstDto.type="全部资讯";
		         tmpDto.data.add(0,firstDto);
		         
		         mDatas=tmpDto.data;
		          LoadData();
		        // initFragment();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
 		}
 	}
   	
	private ArrayList<Fragment> fragments = new ArrayList<Fragment>();
	/** 
	 *  ��ʼ��Fragment
	 * */
	private void initFragment() {
	
		fragments.clear();//���
		int count =  mDatas.size();
		for(int i = 0; i< count;i++){
			Bundle data = new Bundle();
    		data.putString("text", mDatas.get(i).type);
    		data.putInt("id", mDatas.get(i).id);
			NewsFragment newfragment = new NewsFragment();
			newfragment.setArguments(data);
			fragments.add(newfragment);
		}
		NewsFragmentPagerAdapter mAdapetr = new NewsFragmentPagerAdapter(this.getChildFragmentManager(), fragments,mDatas);
//		mViewPager.setOffscreenPageLimit(0);
		mViewPager.setAdapter(mAdapetr);
		mIndicator.setViewPager(mViewPager,0);
		 
	}
	
	public void LoadData()
    {
    	initDatas();
		//����Tab�ϵı���
 		mViewPager.setAdapter(mAdapter);
		//���ù�����ViewPager
		mIndicator.setViewPager(mViewPager,0);
    }
	
	private void initDatas()
	{
		 
  		mAdapter = new FragmentPagerAdapter(this.getChildFragmentManager()){
  		
			@Override
			public int getCount(){ return mDatas.size(); }
 			
			@Override
			public int getItemPosition(Object object) {
				return POSITION_NONE;
			}
 			
			@Override
			public Fragment getItem(int position) {
 				  News_tablist_Fragment fragment =  News_tablist_Fragment.newInstance(mDatas.get(position).id);
  			      return fragment;
  			}
			
			@Override
			public CharSequence getPageTitle(int position) {
				return mDatas.get(position).type;
 			}
			
			@Override
			public Object instantiateItem(ViewGroup container, final int position) {
				Object obj = super.instantiateItem(container, position);
				return obj;
			}
 	   };
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Fragment fragment =this.getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.id_pager + ":" + mViewPager.getCurrentItem());
		((News_tablist_Fragment)fragment).notifyRefresh();
		
		//CommonUtil.showToast(fragment==null ? "null":"not null", this.getActivity());
	}

 
}