package com.zrb.mobile;

import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.ObserveScrollView;
import com.zrb.mobile.ui.ObserveScrollView.OnScrollListener;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class oldUser_info_Activity extends Activity implements OnClickListener, OnScrollListener {

	CenterProgressWebView mWebView;
	String mtitle, mUrl;
	ObserveScrollView mscrollview;
	RelativeLayout titlebar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//setContentView(R.layout.preferences_activity);
		setContentView(R.layout.u_center);
		//mUrl = this.getIntent().getStringExtra("url");
		//mtitle = this.getIntent().getStringExtra("title");
		 initView();
	

	}
	
	private void initView() {
		
		
		this.findViewById(R.id.title_left_root).setOnClickListener(this);
		this.findViewById(R.id.project_container).setOnClickListener(this);
		this.findViewById(R.id.login_icon).setOnClickListener(this);
		mscrollview	=(ObserveScrollView)this.findViewById(R.id.myscroll_container);
	 	//mscrollview.setOnScrollListener(this);
		 mscrollview.setOnTouchListener(scrollTouch);
		fadingHeight=300;
		//LinearLayout tmpliner=	(LinearLayout)this.findViewById(R.id.testlayout);
		RelativeLayout tmpliner=	(RelativeLayout)this.findViewById(R.id.title_container);
		titlebar=tmpliner;
		//tmpliner.setBackgroundResource(R.drawable.qihoo_accounts_btn_second_normal);
		 ColorDrawable bgDrawable=new ColorDrawable(getResources().getColor(R.color.white));
		mBgDrawable=this.getApplication().getResources().getDrawable(R.drawable.title_bar);
		mBgDrawable.setAlpha(0);
		tmpliner.setBackgroundDrawable(mBgDrawable);
		/*titlebar=(RelativeLayout)this.findViewById(R.id.title_container);
		mBgDrawable=this.getApplication().getResources().getDrawable(R.drawable.title_bar);
		
		
		   ColorDrawable bgDrawable=new ColorDrawable(getResources().getColor(R.color.white));
		//mBgDrawable.setAlpha(0);
		titlebar.setBackgroundDrawable(mBgDrawable);
		//updateActionBarAlpha(0);
	
		fadingHeight=100;*/
	}

	
 
	 private Drawable mBgDrawable;
    private int fadingHeight;   //可隐藏的控件高度
	 private int oldY;
	 private int fadingOffset;

	public static final int ALPHA_START=0;
	 public static final int ALPHA_END=255;
	  private static String TAG = "FadingScrollView";
    private	OnTouchListener scrollTouch=new OnTouchListener()
	{

		@Override
		public boolean onTouch(View v, MotionEvent ev) {
			// TODO Auto-generated method stub
			  switch (ev.getAction()) {
	          case MotionEvent.ACTION_DOWN:
	              oldY = (int) ev.getY();
	              break;
	          case MotionEvent.ACTION_MOVE:
	              int scrollY =mscrollview.getScrollY();

	              Log.i(TAG, "scrollY:" + scrollY + " ;-fadingHeight" + fadingHeight);
	              int y = (int) ev.getY();
	              int deltaY = y - oldY;

	              int willScrollY = scrollY - deltaY;

	              if (willScrollY > fadingHeight) {
	                  willScrollY = fadingHeight;
	              }

	              if (willScrollY < 0) {
	                  willScrollY = 0;
	              }

	              mscrollview.scrollTo(0, willScrollY);
	              updateActionBarAlpha(willScrollY*(ALPHA_END-ALPHA_START)/fadingHeight+ALPHA_START);
	              oldY = y;

	              break;
	          case MotionEvent.ACTION_UP:
	              break;
	      }

	      return true;
		}
		
		
	};
	
	  public void setActionBarAlpha(int alpha) throws Exception{
	        if(titlebar==null||mBgDrawable==null){
	            throw new Exception("acitonBar is not binding or bgDrawable is not set.");
	        }
	        mBgDrawable.setAlpha(alpha);
	        titlebar.setBackgroundDrawable(mBgDrawable);
	    }
	    
    void updateActionBarAlpha(int alpha){
	        try {
	            setActionBarAlpha(alpha);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        
	    }
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId())
		{
		   case R.id.title_left_root:
				 this.finish();
			break;
		   case R.id.login_icon:
			    Intent it = new Intent(this,oldUser_info_modifyActivity.class);
				startActivity(it); 
			   break;
		   case R.id.project_container:
			    Intent it2 = new Intent(this,oldUser_project_Activity.class);
				startActivity(it2); 
			   break;  
 		}
		
	}

	@Override
	public void onScroll(int scrollY) {
		// TODO Auto-generated method stub
		  if(scrollY<fadingHeight)
		   updateActionBarAlpha(scrollY*(ALPHA_END-ALPHA_START)/fadingHeight+ALPHA_START);
	}
}
