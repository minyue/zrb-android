package com.zrb.mobile;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.zrb.mobile.ucenter.User_OwnProjectFragment;
import com.zrb.mobile.utility.Constants;

public class Choiceness_MainFragment  extends Fragment implements OnClickListener {

	int curPostcode=0;	
	int curIndustryCode=Constants.ALLIndustryCode;//全部行业
	View RootView;
	Handler myhandler = new Handler();
	ViewPager mviewpager;
	private List<Fragment> fragments = new ArrayList<Fragment>();
	private RadioButton seltextView;
	TextView titleText,txt_locationBtn,txt_industryBtn ;
	FragmentPagerAdapter mAdapter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// return inflater.inflate(R.layout.u_center_registerstep1, null);

		View view = inflater.inflate(R.layout.choiceness_main_fragment, container, false);
		RootView=view;
 		initView();
		return view;
	}
	
	private void initView() {
 		checkListener checkradio = new checkListener();
 		seltextView = (RadioButton) RootView.findViewById(R.id.mymsg_chk0);
		seltextView.setOnClickListener(checkradio);

		RootView.findViewById(R.id.sysmsg_chk1).setOnClickListener(checkradio);
 		mviewpager=(ViewPager)RootView.findViewById(R.id.viewpager1);
 		mviewpager.setOnPageChangeListener(new OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				if(arg0==0) {
					if(seltextView!=null){
						seltextView.setChecked(false);
					}
					seltextView= (RadioButton) RootView.findViewById(R.id.mymsg_chk0);
					seltextView.setChecked(true);
				}
				else
				{
					if(seltextView!=null){
						seltextView.setChecked(false);
					}
					seltextView= (RadioButton) RootView.findViewById(R.id.sysmsg_chk1);
					seltextView.setChecked(true);
 				}
				
			}});
 		
 		txt_locationBtn=(TextView)RootView.findViewById(R.id.txt_btn_location);
		txt_locationBtn.setOnClickListener(this);
		
		txt_industryBtn=(TextView)RootView.findViewById(R.id.txt_btn_industry);
		txt_industryBtn.setOnClickListener(this);
  	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		fragments.add(new FindProject_MainFragment());
		fragments.add(new FindMoney_MainFragment());
		initDatasAdapter();
	}
	
	private void initDatasAdapter()
	{
		 
  		mAdapter = new FragmentPagerAdapter(this.getChildFragmentManager()){
  		
			@Override
			public int getCount(){ return fragments.size(); }
 			
			@Override
			public int getItemPosition(Object object) {
				return POSITION_NONE;
			}
 			
			@Override
			public Fragment getItem(int position) {
 				 return fragments.get(position);
  			}
	 		
			@Override
			public Object instantiateItem(ViewGroup container, final int position) {
				Object obj = super.instantiateItem(container, position);
				return obj;
			}
 	   };
 	   
 	  mviewpager.setAdapter(mAdapter);
 	  mviewpager.setCurrentItem(0);
	}

	public class checkListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (seltextView.getId() == v.getId())
				return;

		/*	if (v.getId() != -1) {
				((RadioButton) v).setChecked(true);
				seltextView.setChecked(false);
				seltextView = (RadioButton) v;
			}*/
			switch (v.getId()) {
			case R.id.mymsg_chk0:
				mviewpager.setCurrentItem(0,false);
 				break;
			case R.id.sysmsg_chk1:
				mviewpager.setCurrentItem(1,false);
				break;
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
        switch(v.getId()){
           case R.id.txt_btn_location:
        	 Intent intent2 = new Intent(this.getActivity(),Project_popup_city.class);
       		intent2.putExtra("code", curPostcode);
       		intent2.putExtra("name", txt_locationBtn.getText().toString());
       		this.startActivityForResult(intent2, 101);
       		Choiceness_MainFragment.this.getActivity().overridePendingTransition(R.anim.slide_up_enter,R.anim.slide_up_exit);
         	  break;
           case R.id.txt_btn_industry:
        		Intent intent = new Intent(this.getActivity(),Project_popup_industry.class);
        		intent.putExtra("industry", curIndustryCode);
        		
        		this.startActivityForResult(intent, 100);
        		Choiceness_MainFragment.this.getActivity().overridePendingTransition(R.anim.slide_up_enter,R.anim.slide_up_exit);
         	  break;
            }
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode ==Activity.RESULT_OK) {
			switch (requestCode) {
 			case 100:
				if (data != null) {
/* 					curIndustryCode = data.getIntExtra("code", 0) ;
					if(emptylayout.isShow())  emptylayout.fadehide(); 
					msearchResults.setRefreshing(true);*/
				}
				break;
 			case 101:
				if (data != null) {
					curPostcode = data.getIntExtra("code", 0) ;
					txt_locationBtn.setText(data.getStringExtra("name"));
				/*	if(emptylayout.isShow())  emptylayout.fadehide(); 
					msearchResults.setRefreshing(true);*/
				}
				break;
			default:
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);

	}
}
