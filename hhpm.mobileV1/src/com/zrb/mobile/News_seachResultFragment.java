package com.zrb.mobile;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.Header;

 
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
 
import com.zrb.mobile.XList.IXListViewLoadMore;
import com.zrb.mobile.XList.IXListViewRefreshListener;
import com.zrb.mobile.XList.XListView;
import com.zrb.mobile.adapter.News_searchResultAdapter;
import com.zrb.mobile.adapter.NewslistItemAdapter;
import com.zrb.mobile.adapter.model.Article2Dto;
import com.zrb.mobile.adapter.model.Article2Dtos;
import com.zrb.mobile.adapter.model.ArticleDtos;
import com.zrb.mobile.adapter.model.NewsDto;
 
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.NetUtil;
import com.zrb.mobile.utility.ZrbRestClient;
 
 

@SuppressLint("ValidFragment")
public class News_seachResultFragment extends Fragment implements IXListViewRefreshListener, IXListViewLoadMore
{
	private static final int LOAD_MORE = 0x110;
	private static final int LOAD_REFREASH = 0x111;

	private static final int TIP_ERROR_NO_NETWORK = 0X112;
	private static final int TIP_ERROR_SERVER = 0X113;

	/**
	 * 是否是第一次进入
	 */
	private boolean isFirstIn = true;

	/**
	 * 是否连接网络
	 */
	private boolean isConnNet = false;

	/**
	 * 当前数据是否是从网络中获取的
	 */
	private boolean isLoadingDataFromNetWork;

	/**
	 * 默认的newType
	 */
	private int newsType =-1;
	/**
	 * 当前页面
	 */
	private int currentPage = 1;
	/**
	 * 处理新闻的业务类
	 */
	//private NewsItemBiz mNewsItemBiz;

	/**
	 * 与数据库交互
	 */
	//private NewsItemDao mNewsItemDao;

	/**
	 * 扩展的ListView
	 */
	private XListView mXListView;
	/**
	 * 数据适配器
	 */
	private News_searchResultAdapter mAdapter;

	/**
	 * 数据
	 */
	private List<NewsDto> mDatas = new ArrayList<NewsDto>();

	/**
	 * 获得newType
	 * 
	 * @param newsType
	 */
	CustomloadingView loadingview;
	
	public News_seachResultFragment(int newsType)
	{
		this.newsType = newsType;
		//Logger.e(newsType + "newsType");
		//mNewsItemBiz = new NewsItemBiz();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.news_item_searchresult, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		/*//mNewsItemDao = new NewsItemDao(getActivity());
		mAdapter = new News_searchResultAdapter(getActivity(), mDatas);
	 
	 
		mXListView = (XListView) getView().findViewById(R.id.id_xlistView);
		loadingview = (CustomloadingView) getView().findViewById(R.id.list_emptyview);
		
		mXListView.setAdapter(mAdapter);
		mXListView.setPullRefreshEnable(this);
		mXListView.setPullLoadEnable(this);
	
 		// mXListView.NotRefreshAtBegin();

		mXListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				//String url="http://61.183.87.134:8090/newsCenter/third.jspx?newsId=1f4bed60-0025-46ea-af94-73689a6f8980";
				Article2Dto newsItem = mDatas.get(position-2);
				String url=AppSetting.BASE_URL.substring(0,AppSetting.BASE_URL.indexOf("mobile"))+"/newsCenter/third.jspx?newsId="+newsItem.id;
				
				Intent intent = new Intent(getActivity(), News_ContentActivity.class);
				intent.putExtra("title", newsItem.title);
				intent.putExtra("url",url);
				startActivity(intent);
			}

		});
		if (isFirstIn)
		{
			*//**
			 * 进来时直接刷新
			 *//*
			mXListView.startRefresh();
			isFirstIn = false;
		} else
		{
			mXListView.NotRefreshAtBegin();
		}*/
	}

	@Override
	public void onRefresh()
	{
		//new LoadDatasTask().execute(LOAD_REFREASH);
		currentPage=1;
		LoadDate();
	}

	@Override
	public void onLoadMore()
	{
		currentPage++;
		LoadDate();
	}
 	
	private void LoadDate()
	{
		/*ZrbRestClient.getRoot("/information/informationList.jspx?pageSize=10&pageNum="+currentPage, null,  new AsyncHttpResponseHandler()
    	{
 			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				loadingview.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				loadingview.Hide();
 				String results=new String(arg2);
				try 
				{
					GsonBuilder gsonb = new GsonBuilder();  
			        Gson gson = gsonb.create();  
			            
			        Article2Dtos tmpDto=	gson.fromJson(results, Article2Dtos.class);
			        if(currentPage==1)  mAdapter.setDatas(tmpDto.results);
			        else
			        	mAdapter.addAll(tmpDto.results);
  	 
					mAdapter.notifyDataSetChanged();
 			
     			}
				catch(Exception e){
					loadingview.HideText(e.getMessage()).ShowRefleshBtn(true).ShowError();
				}
			 
				mXListView.stopRefresh();
				mXListView.stopLoadMore();
 			}});
  	*/
	}
	
 
}
