package com.zrb.mobile;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.hhpm.lib.PageIndicator.RingPagerIndicator;
import com.hhpm.lib.PageIndicator.RingPagerIndicator.onPageSelectListener;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
 
import com.zrb.matching.MatchingActivity;
import com.zrb.mobile.adapter.GuidePageAdapter;
import com.zrb.mobile.common.LoadUserAvatar;
import com.zrb.mobile.expandmenu.ExpandableButtonMenu;
import com.zrb.mobile.expandmenu.ExpandableMenuOverlay;
import com.zrb.mobile.expandmenu.ExpandableButtonMenu.MenuButton;
import com.zrb.applib.utils.LocalUserInfo;
import com.zrb.applib.utils.AppSetting;
import com.zrb.applib.utils.FileUtil;
import com.zrb.discover.BannerSplashActivity;
import com.zrb.discover.Overlay;
import com.zrb.discover.UserCardDetailActivity;
import com.zrb.discover.testscrolllayout;
import com.zrb.mobile.register.UserRegister;
import com.zrb.mobile.register.UserRegisterFinish;
import com.zrb.mobile.transformer.RotateDownTransformer;
import com.zrb.mobile.ui.ActionAnimView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;

import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

public class Splash_Activity extends  Activity implements OnClickListener, onPageSelectListener {

    private final  Handler _handler = new Handler();
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
 	    boolean hasGuide=	LocalUserInfo.getInstance(Splash_Activity.this).hasGuide();
 	  // hasGuide=false;
 	   setContentView(!hasGuide ? R.layout.activity_guide:R.layout.splash);
     
       /*
    
      
  
      _handler.postDelayed(new Runnable() {
 			@Override
			public void run() {
				// TODO Auto-generated method stub
 				  Intent it = new Intent(Splash_Activity.this, UserRegisterFinish.class);
 				  //	Intent it = new Intent(Splash_Activity.this, HomeActivity.class);
 				    it.putExtra("postCode", 421182);
 					 startActivity(it);
 					 Splash_Activity.this.finish(); 
			}}, 1000);  */
 	  
        if(hasGuide){
  	    	checkfiledir();
 	 		checkLogin();  
 	     }
 	     else {
  	    	initGuidePage();
 	     }   
     }
 	
	 
	RingPagerIndicator tmpind;
	View maction_layout;
	private void initGuidePage(){
		ViewPager mviewPager = (ViewPager) this.findViewById(R.id.guidePages);
		
		maction_layout=this.findViewById(R.id.action_layout);
		LayoutInflater inflater = getLayoutInflater();
	    ArrayList<View> pageViews= new ArrayList<View>();
	 
		pageViews.add(inflater.inflate(R.layout.viewpager_page1, null));
		pageViews.add(inflater.inflate(R.layout.viewpager_page2, null));
		pageViews.add(inflater.inflate(R.layout.viewpager_page3, null));
		pageViews.add(inflater.inflate(R.layout.viewpager_page4, null));
 
		mviewPager.setAdapter(new GuidePageAdapter(pageViews));
   	this.findViewById(R.id.regin_btn).setOnClickListener(this);
		this.findViewById(R.id.login_btn).setOnClickListener(this);
	 
		 
		mviewPager.setPageTransformer(true, new RotateDownTransformer());  
		
	    tmpind=(RingPagerIndicator)this.findViewById(R.id.custom_indicator);
		tmpind.setViewPager(mviewPager); 
		tmpind.setPageSelectListener(this);
		tmpind.redraw();
 	}
	 
	@Override
	public void onPageSelected(int postion) {
		// TODO Auto-generated method stub
		 if(postion==3) {
			 tmpind.setVisibility(View.GONE);
			 Animation anim = AnimationUtils.loadAnimation(Splash_Activity.this, R.anim.fade_in);
			 maction_layout.clearAnimation();
			 maction_layout.startAnimation(anim);
			 maction_layout.setVisibility(View.VISIBLE);
 		 }
		 else{
			 tmpind.setVisibility(View.VISIBLE);
			 maction_layout.setVisibility(View.GONE);
		 }
 	}
	
	private void checkfiledir(){
  		  File dir =new File(FileUtil.filePath+LoadUserAvatar.AvatarDir);
	         if(!dir.exists()){
	             dir.mkdirs();
	     }
	}
	
	private void checkLogin(){
		
 		if (LocalUserInfo.getInstance(Splash_Activity.this).isLogined()) {
  			LocalUserInfo.getInstance(Splash_Activity.this).restoreUserInfo();
  	 	    Refleshtoken();
 		}
		else showMain(); 
  	}
 	
	private void Refleshtoken(){
 		RequestParams param = new RequestParams();
		param.put("uid", AppSetting.curUser.uid);
		param.put("password", AppSetting.curUser.pwd);
		CommonUtil.InfoLog("Refleshtoken", param.toString());
		ZrbRestClient.post("doRefreshLogin", param,
				new AsyncHttpResponseHandler() {
 					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
   						//LoadCategory();
 						showMain();
 					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) { 
						//LoadCategory();
						    String results = new String(arg2);
 	 						JSONObject jsonObject2;
							try {
								jsonObject2 = new JSONObject(results);
								int retCode= jsonObject2.getInt("res");
								if(retCode==1){
									JSONObject tmpObj= jsonObject2.getJSONObject("data");
									AppSetting.curUser.zrb_front_ut=tmpObj.getString("zrb_front_ut");
								}
							   
							} catch (JSONException e) {
								//e.printStackTrace();
							}
						 showMain();
					}
				});
 	}
     	
	private void showMain(){
	 	_handler.postDelayed(new Runnable() {
 			@Override
			public void run() {
				// TODO Auto-generated method stub
			 
// 				Intent it = new Intent(Splash_Activity.this, MatchingActivity.class);
			     Intent it = new Intent(Splash_Activity.this, MainTabActivity.class);
//			     Intent it = new Intent(Splash_Activity.this, Project_cityInfoActivity.class);
 			  //	Intent it = new Intent(Splash_Activity.this, HomeActivity.class);
 				 startActivity(it);
				 Splash_Activity.this.finish();
			}
		}
 		, 2000);    
	 }
  
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		 
		 switch(v.getId()){
			case R.id.regin_btn:
			/*	  Intent it2 = new Intent(Splash_Activity.this, User_phone_login.class);
				  it2.putExtra("guidelogin", true);
 	 			  startActivity(it2);
				  Splash_Activity.this.finish();*/
				
				break;
			case R.id.login_btn:
				  Intent it = new Intent(Splash_Activity.this, MainTabActivity.class);
	 	 			  startActivity(it);
				  Splash_Activity.this.finish();
				break;
		 
			
		} 
	}




  
}
