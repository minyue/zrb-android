/*
 * Copyright (C) 2013 Manuel Peinado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zrb.mobile;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
 
import com.zrb.mobile.fadingactionbar.SherlockFadingActionBarHelper;
import com.zrb.mobile.utility.CommonUtil;

public class ScrollViewActivity extends SherlockActivity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SherlockFadingActionBarHelper helper = new SherlockFadingActionBarHelper()
            .actionBarBackground(R.drawable.app_bg2)
            .headerLayout(R.layout.header)
            .contentLayout(R.layout.activity_scrollview);;
        setContentView(helper.createView(this));
        helper.initActionBar(this); 
        
        
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
       // this.findViewById(R.id.title_left_root).setOnClickListener(this);
        //setContentView(R.layout.activity_scrollview);
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		CommonUtil.showToast("dsfsdf", this);
	}

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.activity_menu, menu);
        return true;
    }*/
}
