package com.zrb.mobile;

import org.apache.http.Header;

import com.amap.api.location.f;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.umeng.socialize.controller.impl.InitializeController;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.adapter.model.IndustrydistributDto;
import com.zrb.mobile.anim.Techniques;
import com.zrb.mobile.anim.YoYo;
import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.ui.NumEditText;
import com.zrb.mobile.ui.NumEditText.OnNumslistener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PushLiuYanActivity extends RegisterBaseActivity implements OnClickListener, OnNumslistener {
	private TextView txt_title;
	private LinearLayout title_left_root;
	private TextView txtrightTitle;
	private Tip progTip;
	private NumEditText editText1;
	private String content;
	public long intentionId;
 
	private int position;
	private String from;
	private TextView txtNum;
	private boolean isTiping = false;
 
	boolean isneedSend = true;
	Handler mhandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_push_liu_yan);
		 
		from = getIntent().getStringExtra("from");
		intentionId = getIntent().getLongExtra("id", 0);
		position = getIntent().getIntExtra("position", 0);
		isneedSend = getIntent().getBooleanExtra("needsend", true);
		initView();
		 
	}

	private void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在发布中...", true,
					true);
		else
			progTip.Show("正在发布中...");
	}


	protected void initView() {
		// this.findViewById(R.id.title_left_root).setOnClickListener(this);
		super.initView();
  
		txt_title = (TextView) findViewById(R.id.txt_title);
		title_left_root = (LinearLayout) findViewById(R.id.title_left_root);
		txtrightTitle = (TextView) this.findViewById(R.id.right_title);
		editText1 = (NumEditText) findViewById(R.id.editText1);
		editText1.setMaxNums(200);
		editText1.setOnNumslistener(this);
		txtNum = (TextView) findViewById(R.id.txtNum);
		 
		txtrightTitle.setText("发布");
		txtrightTitle.setVisibility(View.VISIBLE);
		txt_title.setText("发表留言");
		title_left_root.setOnClickListener(this);
		txtrightTitle.setOnClickListener(this);
		 
	}
  
	@Override
	public void onClick(View v) {
		Intent intent;
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.title_left_root:
			onBackPressed();

			break;
		case R.id.right_title:
			// 右键
			if (AppSetting.curUser == null) {

				Toast.makeText(PushLiuYanActivity.this, "请先登录", 0).show();
				intent = new Intent(this, User_phone_login.class);
				startActivity(intent);
			} else {
				
 				saveData();
			}
			break;
		default:
			break;
		}
	}

	ZrbRestClient mZrbRestClient;
	private void saveData() {
		// 发布项目进行非空的判断
		content = editText1.getText().toString();
		if ("".equals(content)) {
			initNotify("请填写留言内容");
			// Toast.makeText(PushLiuYanActivity.this, "请填写留言内容", 0).show();
			return;
		}
		 
		showloading();
		RequestParams param = new RequestParams();
	 
		param.put("content", content);
		param.put("intentionId", intentionId);
 
		CommonUtil.InfoLog("param", param.toString());
        if(null==mZrbRestClient){
        	mZrbRestClient=new ZrbRestClient(this);
        	mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					progTip.Dismiss();
 					initNotify(PushLiuYanActivity.this.getString(arg0));
				}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					String results = new String(arg2);
					progTip.Dismiss();
					try {
						Gson gson = new Gson();
						IndustrydistributDto tmpDto = gson.fromJson(
								results, IndustrydistributDto.class);
						if (tmpDto.res == 1) {
							CommonUtil.showToast(tmpDto.msg,
									PushLiuYanActivity.this);
							PushLiuYanActivity.this.finish();
							if (isneedSend) {
								if ("1".equals(from)) {
									Intent intent1 = new Intent(
											Constants.FocusNum_Find_mainlist);
									intent1.putExtra("position", position);
									intent1.putExtra("from", "2");
									sendBroadcast(intent1);
								} else if ("2".equals(from)) {
									Intent intent1 = new Intent(
											Constants.FocusNum_PUBLISE_mainlist);
									intent1.putExtra("position", position);
									intent1.putExtra("from", "2");
									sendBroadcast(intent1);
								} else if ("3".equals(from)) {
									Intent intent1 = new Intent(
											Constants.FocusNum_ATTENTION_mainlist);
									intent1.putExtra("position", position);
									intent1.putExtra("from", "2");
									sendBroadcast(intent1);
								}
								Intent intent2 = new Intent(Constants.FocusNum_FUNDETIAL_mainlist);
								sendBroadcast(intent2);
							}

						} else {
							initNotify(tmpDto.msg);
						}
					} catch (Exception e) {
						initNotify(PushLiuYanActivity.this
								.getString(R.string.error_serverdata_loadfail));
					}

				}
			});
         }
		mZrbRestClient.post("intentionMessage/add", param);
	}

	 

	@Override
	public void showtextNum(int number) {
		// TODO Auto-generated method stub
		txtNum.setText((200-number)+""+"/200");
		 
	}
}
