package com.zrb.mobile.adapter;

import java.util.List;

import com.zrb.mobile.adapter.model.FindMoneyDto;

import android.widget.BaseAdapter;

public abstract class MatchingBaseAdapter  extends BaseAdapter {
	
	public abstract void setJsonDatas(String tmpDtos,boolean isAppend)  ;
	public abstract int getCurpageSize()  ;

}
