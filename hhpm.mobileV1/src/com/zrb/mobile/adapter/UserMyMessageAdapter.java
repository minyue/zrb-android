package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.MyMessageDto;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class UserMyMessageAdapter extends BaseAdapter {
	private Context context;
	// private List<Map<String, String>> list;
	public List<MyMessageDto> list1 = new ArrayList<MyMessageDto>();
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");//

	// 重构
	public UserMyMessageAdapter(Context context, List<MyMessageDto> list1) {
		this.context = context;
		this.list1 = list1;
	}

	public long getMinId() {
		if (list1 != null && list1.size() > 0)
			return list1.get(list1.size() - 1).id;
		return -1;
	}

	// public long getMinPushTime() {
	// if (list1 != null && list1.size() > 0)
	// return list1.get(list1.size() - 1).pushTime;
	// return -1;
	// }

	public void setCurDatas(List<MyMessageDto> tmpDtos) {
		list1.clear();
		list1.addAll(tmpDtos);
	}

	public void addAll(List<MyMessageDto> tmpDtos) {
		list1.addAll(tmpDtos);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list1.size() > 0 ? list1.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list1.get(position) != null ? list1.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position > 0 ? position : 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.u_my_message, parent, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		MyMessageDto intention = list1.get(position);
		if (null != intention) {
			if (!TextUtils.isEmpty(intention.messageInfo.bizType)) {
				if ("pro_fund_pulished".equals(intention.messageInfo.bizType)) {
					holder.tv_titile.setText("发布成功");
				} else if ("pro_fund_collected"
						.equals(intention.messageInfo.bizType)) {
					holder.tv_titile.setText("关注通知");
				} else if ("pro_fund_changed"
						.equals(intention.messageInfo.bizType)) {
					holder.tv_titile.setText("变更通知");
				}
			}

			if (intention.createTime != null) {
				holder.tv_time.setText(sdf.format(intention.createTime));
			}
			if (!TextUtils.isEmpty(intention.messageInfo.content)) {
				holder.tv_my_message_content_tv
						.setText(intention.messageInfo.content.toString());
			}
			holder.iv_my_message_isread_iv
					.setImageResource(intention.read == 0 ? R.drawable.ico_message_unread
							: R.drawable.ico_message_read);
		}

		return convertView;
	}

	private class ViewHolder {
		TextView tv_titile, tv_time, tv_my_message_content_tv;
		ImageView iv_my_message_isread_iv;

		public ViewHolder(View v) {
			tv_titile = (TextView) v.findViewById(R.id.tv_message_title);
			tv_time = (TextView) v.findViewById(R.id.at_date);
			tv_my_message_content_tv = (TextView) v
					.findViewById(R.id.tv_my_message_content_tv);
			iv_my_message_isread_iv = (ImageView) v
					.findViewById(R.id.iv_my_message_isread_iv);

		}
	}

}