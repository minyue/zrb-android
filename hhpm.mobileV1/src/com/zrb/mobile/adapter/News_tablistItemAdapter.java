package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.mobile.R;
import com.zrb.mobile.Search_resultActivity;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.TagsDto;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
 

public class News_tablistItemAdapter extends BaseAdapter
{

	private LayoutInflater mInflater;
	private List<NewsDto> mDatas;

	public final static HashMap FavoriteMap=new HashMap();
 	/**
	 * 使用了github开源的ImageLoad进行了数据加载
	 */
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private int hotColor=Color.rgb(221, 85, 68);
	private int newColor=Color.rgb(29, 167, 201);
	private SimpleDateFormat sdf= new SimpleDateFormat("MM.dd");//yyyy.
	private int newType;
	Map map;
	Drawable onfavitor,offfavitor,addfavitor;
	/** 弹出的更多选择框  */
	private PopupWindow popupWindow;
	popAction onPopAction;
	
	OnFavoriteListener mCollectionListener;
	public void setOnFavoriteListener(OnFavoriteListener curCollectionListener){
 		mCollectionListener=curCollectionListener;
	}
 	
	public void setNewType(int curnewType){
		newType=curnewType;
	 
		if(newType==-1)
		{ 
			map= new HashMap();
		    map.put(20, "新闻热点");
		    map.put(32, "观点解读");
		    map.put(28, "投资动态");
		    map.put(14, "政策法规");
		}
 	}
	
	public NewsDto getCurItem(int position)
	{
		return mDatas.get(position);
	}
	
	public News_tablistItemAdapter(Context context, List<NewsDto> datas)
	{
		this.mDatas = datas;
		mInflater = LayoutInflater.from(context);
		//imageLoader = ImageLoader.getInstance();
  		//.showStubImage(R.drawable.images)
		options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.loadpic)
				.showImageForEmptyUri(R.drawable.loadpic).showImageOnFail(R.drawable.loadpic).cacheInMemory(false)
				.cacheOnDisc(true).build();
		onfavitor=context.getResources().getDrawable(R.drawable.news_list_follow_on);
		offfavitor=context.getResources().getDrawable(R.drawable.news_list_follow_off);
		addfavitor=context.getResources().getDrawable(R.drawable.news_list_follow_add);
		initPopWindow();
		onPopAction=new popAction(1);
   }

	public void setImgLoader(ImageLoader curImgLoad) {
 		imageLoader = curImgLoad;
 	}
	
	public void addAll(List<NewsDto> mDatas)
	{
		this.mDatas.addAll(mDatas);
	}

	public int getMaxId(){
		
		if(mDatas==null||mDatas.size()<=0) return 1;
		else
			return mDatas.get(0).id;
	}
	
	public int getMinId(){
		if(mDatas==null||mDatas.size()<=0) return 1;
		else
			return mDatas.get(mDatas.size()-1).id;
	}
	
	public void setDatas(List<NewsDto> mDatas)
	{
		this.mDatas.clear();
		this.mDatas.addAll(mDatas);
	}
	
	public void insertForward(List<NewsDto> mDatas){
		//this.mDatas.
		this.mDatas.addAll(0, mDatas);//.addAll(mDatas);
	}

	@Override
	public int getCount()
	{
		return mDatas.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}
 
	private void setTagEvent(LinearLayout tagLayout){
		
      for(int i=0;i<3;i++){
 			TextView tmpview=(TextView)tagLayout.getChildAt(i);
 			tmpview.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(v.getTag()!=null&&v.getTag().toString().contains(",")){
						
						  String message=v.getTag().toString();
						  String[] tmpArr=message.split(",");
					 	  Intent it = new Intent(v.getContext(), Search_resultActivity.class);
						  it.putExtra("tag", tmpArr[1]+",");
						  it.putExtra("value", Integer.parseInt(tmpArr[0]));
						  v.getContext().startActivity(it);
					}
				}
 				
 			} );
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder = null;
		NewsDto tmpDto=mDatas.get(position);
 		if (convertView == null){
			
			//convertView = mInflater.inflate(R.layout.news_list_item_zrb, null);
 			convertView = mInflater.inflate(R.layout.news_list_item_zrb, null);
 			
			holder = new ViewHolder();
 		
			holder.newstype = (TextView) convertView.findViewById(R.id.id_newstype);
			holder.mContent = (TextView) convertView.findViewById(R.id.id_content);
			holder.mTitle = (TextView) convertView.findViewById(R.id.id_title);
			holder.mDate = (TextView) convertView.findViewById(R.id.id_date);
			holder.mImg = (ImageView) convertView.findViewById(R.id.id_newsImg);
			holder.msource=(TextView) convertView.findViewById(R.id.news_source);
			holder.favitorNum=(TextView) convertView.findViewById(R.id.news_favitorcount);
			holder.tagContainer=(LinearLayout)convertView.findViewById(R.id.tag_container);
			
			setTagEvent(holder.tagContainer);
			
			convertView.setTag(holder);
			holder.favitorNum.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
  				     int positon=Integer.parseInt(v.getTag().toString());
					 
					 NewsDto tmpDto2= mDatas.get(positon);
					// tmpDto2.collectionCount++;
					// ((TextView)v).setText(String.format("%1$d人收藏",tmpDto2.collectionCount ));
 					 if(AppSetting.curUser==null){
						 if(mCollectionListener!=null)
								mCollectionListener.onClickCollect(tmpDto2.id,true);
	 				 }
					 else{
						boolean ishave= FavoriteMap.containsKey(tmpDto2.id);
						if(ishave)	{
							onPopAction.setNewsId(tmpDto2.id);
							onPopAction.onClick(v);
					    }
						else{
							if(mCollectionListener!=null){
								 mCollectionListener.onClickCollect(tmpDto2.id,true);
								 TextView tmp=(TextView)v;
								 if(tmp!=null)
									 tmp.setCompoundDrawablesWithIntrinsicBounds(offfavitor,null,null,null);
							}
							  
						}
					 }
 				}});
  		}
		else
			holder = (ViewHolder) convertView.getTag();
		 
		settag(holder.tagContainer,tmpDto.tagsInfoList);
	    if(newType!=-1)	holder.newstype.setVisibility(View.GONE);
	    else {
	    	
	    	int Rid=-1;
	    	if(tmpDto.category==20) Rid=R.drawable.news_photo_hotspot;
	    	else if(tmpDto.category==32) Rid=R.drawable.news_photo_view;  
	    	else if(tmpDto.category==28) Rid=R.drawable.news_photo_invest;  
	    	else if(tmpDto.category==14) Rid=R.drawable.news_photo_olicy;  
	    	else Rid=R.drawable.news_photo_hotspot;  
	    	
	    	Drawable left= convertView.getContext().getResources().getDrawable(Rid);
 	    	holder.newstype.setCompoundDrawablesWithIntrinsicBounds( left,null, null, null);
	    	holder.newstype.setText(map.get(tmpDto.category)==null ? "":map.get(tmpDto.category).toString());
	    }
		//newType
		
	    String tmpflag=isRecent(tmpDto.createTime) ? "最新":"";
 	    if(!TextUtils.isEmpty(tmpDto.hotPointId)) tmpflag="热点";
		 
 	    if(!TextUtils.isEmpty(tmpflag))
 	    {
 	 	   String msource=tmpflag+"｜"+tmpDto.channel;
 		   SpannableStringBuilder styled = new SpannableStringBuilder(msource);
           int idx=msource.indexOf("｜");
    	 
 		   styled.setSpan(new ForegroundColorSpan(tmpflag=="热点" ? hotColor:newColor), 0,idx,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// i 未起始字符索引，j 为结束字符索引
 		    //msource.setText("新浪网｜"+Html.fromHtml("<font color=\"#dd5544\">热点</font>"));
           holder.msource.setText(styled); 
 	    }
 	    else
 	    	holder.msource.setText(tmpDto.channel); 
    	
    	boolean isbankSummary=TextUtils.isEmpty(tmpDto.summary) ;
    	holder.mTitle.setText(isbankSummary ? tmpDto.title:tmpDto.shortTitle);
    	holder.mTitle.setMaxLines(isbankSummary? 2:1);
    	holder.mTitle.setSingleLine(!isbankSummary);
    	holder.mContent.setText(tmpDto.summary);
    	
    	holder.mDate.setText(CommonUtil.formatlongDate(tmpDto.createTime,sdf));
    	holder.favitorNum.setText(String.format("%1$d关注",tmpDto.collectionCount ));
    	holder.favitorNum.setTag(position);
    	  
    	
    	if(AppSetting.curUser!=null) {
     		boolean isHave=FavoriteMap.containsKey(tmpDto.id);
      		//onfavitor,offfavitor,addfavitor;
     		holder.favitorNum.setCompoundDrawablesWithIntrinsicBounds(isHave? offfavitor:addfavitor,null,null,null);
    	}
    	else
    		holder.favitorNum.setCompoundDrawablesWithIntrinsicBounds(addfavitor,null,null,null);
     	
        if(!TextUtils.isEmpty(tmpDto.shortImgPath))
        	imageLoader.displayImage(AppSetting.mediaServer+tmpDto.shortImgPath, holder.mImg);
        else
        	holder.mImg.setImageResource(R.drawable.cszt_img03); 
  		return convertView;
 	}
	
	private boolean isRecent(long date){
 		java.util.Date nowdate=new java.util.Date();
		return CommonUtil.formatlongDate(date,sdf).equals(sdf.format(nowdate));
 	}

	private void settag(LinearLayout tagLayout,List<TagsDto> tags)
	{
 		int tmplen=tags==null ? 0:tags.size();
		for(int i=0;i<3;i++){
			
			TextView tmpview=(TextView)tagLayout.getChildAt(i);
			if(i<tmplen) { 
				tmpview.setText(tags.get(i).labelName);
				tmpview.setVisibility(View.VISIBLE);
				tmpview.setTag(tags.get(i).id+","+tags.get(i).labelName);
			}
			else{
				tmpview.setVisibility(View.GONE);
			}
		}
 	}
	
	private final class ViewHolder
	{
		TextView mTitle;
		TextView mContent;
		ImageView mImg;
		TextView mDate;
		TextView favitorNum;
		TextView msource;
		TextView newstype;
		LinearLayout tagContainer;
	}
	
	public interface OnFavoriteListener{
		
		void onClickCollect(int newsId,boolean isAdd);
	}

	/** 
	 * 每个ITEM中more按钮对应的点击动作
	 * */
	public class popAction implements OnClickListener{
		int position;
		public popAction(int position){
			this.position = position;
		}
		
		public void setNewsId(int newsId){
 			position=newsId;
		}
		
		@Override
		public void onClick(View v) {
			int[] arrayOfInt = new int[2];
			//获取点击按钮的坐标
			v.getLocationOnScreen(arrayOfInt);
	        int x = arrayOfInt[0];
	        int y = arrayOfInt[1];
	        showPop(v, x , y, position);
		}
	}
 
	private RelativeLayout btn_pop_close;
 	/**
	 * 初始化弹出的pop
	 * */
	private void initPopWindow() {
		View popView = mInflater.inflate(R.layout.list_pop_item, null);
		popupWindow = new PopupWindow(popView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new ColorDrawable(0));
		//设置popwindow出现和消失动画
		popupWindow.setAnimationStyle(R.style.PopMenuAnimation2);//PopMenuAnimation
		btn_pop_close = (RelativeLayout) popView.findViewById(R.id.root_view);
	}
	
	/** 
	 * 显示popWindow
	 * */
	public void showPop(final View parent, int x, int y,final int newsId) {
		//设置popwindow显示位置
		
		x+=CommonUtil.dip2px(parent.getContext(), 20);
 	    popupWindow.showAtLocation(parent, 0, x, y);
		//获取popwindow焦点
		popupWindow.setFocusable(true);
		//设置popwindow如果点击外面区域，便关闭。
		popupWindow.setOutsideTouchable(true);
		popupWindow.update();
		if (popupWindow.isShowing()) {
			
		}
		btn_pop_close.setOnClickListener(new OnClickListener() {
			public void onClick(View paramView) {
				popupWindow.dismiss();
				if(mCollectionListener!=null){
					 TextView tmp=(TextView)parent;
					 if(tmp!=null)
						 tmp.setCompoundDrawablesWithIntrinsicBounds(addfavitor,null,null,null);
					 mCollectionListener.onClickCollect(newsId,false);
				}
					  
			}
		});
	}
}
