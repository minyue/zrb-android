package com.zrb.mobile.adapter.model;

 

	public class ProjectSurveyDto {
		
		public int autoId;
 		public Long userId; 	    /**用户id**/
 		public int projectId;  	             /**项目id**/
 		public String projectName;
 		public long inspectTime;       /**看地时间**/
 		public String contact;                   /**联系人**/
 		public int personNum;           	 /**同行人数**/
 		public String telphone;                  /**联系电话**/
 	
		public String companyName;             	 /**公司名称**/
 		
		public long insertTime;        /**数据录入时间**/

 		public long inspectTimeActual; /**实际勘察时间**/
 		public String personNumActual;              /**实际勘察人数**/
 
	 	public String accomPersons;              /**招商部陪同人员**/
  		public long updateTime;       /**数据更新时间**/
 		public int inspectStatus;              /**勘察状态，0：取消，1：申请；2、确认；3、完成**/
 		
 		public String projectPicUrl;
 		
 		public String cancelReason;
 		public String reasonTypeZH;
	 
	}
 
