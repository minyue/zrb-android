package com.zrb.mobile.adapter.model;

 

 
public class DynamicDto{
		
		public static final int TYPE_DESCRIPTION = 0;
		public static final int TYPE_TIMELINE = 1;
 		public int id;
		public long userId;
		public long createTime;
		public int type;
		public String target;
		public String title;
		public String extra;
		public  ExtraInfo extraInfo;
		public int showType;
		
		public DynamicDto(){}
		
		public boolean isDescription(){
			return showType == TYPE_DESCRIPTION;
		}
		
		public boolean isTimeline(){
			return showType == TYPE_TIMELINE;
		}
      
		
		public	 class ExtraInfo{
			public String realname;
			public String org;
			public String avatar;
			public String industryName;
			public String positionName;
		}
}