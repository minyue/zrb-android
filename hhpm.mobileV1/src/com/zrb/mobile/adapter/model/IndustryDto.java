package com.zrb.mobile.adapter.model;

import java.io.Serializable;
import java.util.List;
 

	public class IndustryDto  {  
	 
		public String name;  //apk名字  
		public String value;   //apk更新日志  
		public List<String> company;
		
		
		public IndustryDto(String curName,String industryValue){
			name=curName;
			value=industryValue;
		}
		
	   public int getChildSize(){  
		        return company.size();  
	   }  
	   
	   public String getChild(int index){  
	        return company.get(index);  
	    }  
	}  
	
 

