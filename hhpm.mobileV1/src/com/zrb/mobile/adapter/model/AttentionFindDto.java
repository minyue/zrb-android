package com.zrb.mobile.adapter.model;

import android.R.integer;

public class AttentionFindDto {

	public long createTime;
	public long id;
	public String title;
	public int type;
	public String typeName;
	public String userName;
	public String province;
	public String total;

	public long collectNum;
	public int messageNum;

}
