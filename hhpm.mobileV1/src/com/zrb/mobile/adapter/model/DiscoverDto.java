package com.zrb.mobile.adapter.model;

public class DiscoverDto {
	public static final int TYPE_DESCRIPTION = 0;
	public static final int TYPE_TIMELINE = 1;
/*	public static final int TYPE_PROJECT = 101;
	public static final int TYPE_FUND = 102;*/
	public long id;
	public String intro;
	public String industry;
	public String district;
	public long amount;
	public int mode;
	public int type;
	public int userId;
	public long pushTime;
	public int isValid;
	public String modeName;
	public String typeName;
	public String districtName;
	public String industryName;
	public int messageAmount;
	public String content;
	public long attentionTimes;
	public String userName;
	public String avatar;
	public int attention;
	public String authName;
	public String org;
	public Long projectId;
	public int showType;
	public String position;
	public int auth;
	public String userIndustryName;
	
	public boolean isDescription(){
		return showType == TYPE_DESCRIPTION;
	}
	
	public boolean isTimeline(){
		return showType == TYPE_TIMELINE;
	}
	
}

// ArticleDto.java

// "id":95,"intro":"testintro正在提交中中39","industry":"104","district":"330782","amount":49,"mode":301,"type":102,"userId":1000000036,"pushTime":1442906092000,"isValid":1,"content":"content","handlePerson":null,"handleTime":null,"status":null,"note":null,"projectId":null,"attentionTimes":null,"avatar":null,"userName":null