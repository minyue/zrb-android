package com.zrb.mobile.adapter.model;

import java.util.List;

import android.R.integer;

public class PersonNumberDto {

	public int res;
	public String msg;
	public PersonNumberDtos data;

	public class PersonNumberDtos {
		public long collectedNumber;
		public long collectFund;
		public long collectProject;
	}
}
// {"res":1,"msg":null,"data":true}