package com.zrb.mobile.adapter.model;

public class ProjectsDto {
	
	public long autoId;          //项目id
	public String projectId;    //项目编号
	public int pppType;      //PPP项目分类，1：非经营性、2：经营性、3：准经营性
 	public String pppTypeZH;
 	
 	public String assetType;
 	public String assetTypeZH;
 	
	public String purposeZH;
 	
	public String projectName;
	public int projectType;      //项目类型，1：ppp投资权、2：土地使用权、3：资产
	public String projectTypeZH;
 	public int industryId;       //项目行业
 	public int provincePostcode;
 	public String province;
 	public int cityPostcode;
 	public String city;
 	public int countyPostcode;
 	public String county;
 	public long insertTime;
 	public long updateTime;
 	public long pushTime;
 	public String pic;
 	public String bigPic;
 	public String intro;        //项目简介
 	public String investmentStr;  //项目金额
 	public String keywords;     //项目标签
 	public String hotValue;     //项目当前热度
 	public int isValidate;      //认证状态，0：未认证，1：电话认证，2：现场认证，2：授权项目
 	public String isValidateZH;
 	public int projectStatus;   //项目状态
 	public int isCase;          //是否属于案例，0：否，1：是
 	public double area;         //土地面积
 	public long tradeDate;      //项目发布时间
 	public int isRecommend;      //是否属于推介项目
 	
 	public String geoCoor;
 	
 	/**招商状态**/
 	public int investmentStatus; //1401招商中 1402公告期
	
	/**地址描述**/
 	public String depict;//地址描述
 
}



//{"autoId":2051,"projectId":null,"pppType":null,"projectName":"马鞍山市垃圾焚烧发电","projectType":1,"industryId":20,"provincePostcode":"340000","province":"安徽省","cityPostcode":"340500",
//"city":"马鞍山市","countyPostcode":"340503","county":"花山区","insertTime":1435930294000,"updateTime":1436426030000,"pic":null,"bigPic":null,
//"intro":"项目建设分两期完成，一期设计规模为800吨/日，年度处理垃圾26.66万吨；二期建成后达到1200吨/日。本项目的垃圾处理的服务范围包括马鞍山市市区（花山区、雨山区、博望区）和当涂县。 "
//,"investment":51900.0000,"keywords":null,"hotValue":null,"isValidate":0,"nameMd5":"18a17a2952f4f94fc156861ce6b3c8fd","projectStatus":1,"isCase":0,"area":0.0,
//"tradeDate":1438358400000,"isRecommend":0}