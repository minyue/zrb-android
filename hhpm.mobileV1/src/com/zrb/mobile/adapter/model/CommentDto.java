package com.zrb.mobile.adapter.model;

public class CommentDto {
	
	public String username;
	public String avatar;
	public String content;
	public long commentTime; 

}
