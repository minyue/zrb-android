package com.zrb.mobile.adapter.model;

import java.util.List;

public class NewsDto {

	public String summary;
	public String shortImgPath;
	public long createTime;
	public String author;
	public String imgPath;
	public String channel;//来源源
	public int id;
	public String shortTitle;
	public int category;//分类
	public String title;
	public List<TagsDto> tagsInfoList;//标签
	public int collectionCount;//收藏 
	public String hotPointId; //null,不是热点
	
	//collectionCount":0,"id":10,"category":14,"hotPointId":null
}
