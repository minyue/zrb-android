package com.zrb.mobile.adapter.model;

public class ProjectBannerDto {
	
	public String createBy;
	public long createTime;
	public int id;
	public int informationId;
	public String name;
	//public String param;
 
	
	public String picUrl;
	public String positionId;
	public String projectId;
	public String projectName;

	public String shortName;
	public String sortNo;
	public String title;
//	public int orderId;
}
