package com.zrb.mobile.adapter.model;

public class TagsDto {

	public int id;
 	public int sequence;/**排序**/
 	public String labelName;
 	public long  createTime;
 	public int delFlag;
 	public int searchLabel ;/**是否为搜索标签,0-否，1-是**/

  }
//{"id":18,"sequence":4,"labelName":"旧城改造","createTime":1433257211000,"delFlag":0,"searchLabel":0}