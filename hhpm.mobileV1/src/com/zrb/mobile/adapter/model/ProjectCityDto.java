package com.zrb.mobile.adapter.model;

import java.util.List;

public class ProjectCityDto {
	
	public String province;
	public String city;
	public String keyWords;
	public float gdp;
	public float population;
	public float pcdi;
	public float urbanRate;
	public String intro;
	public float primaryIndustry;
	public float secondIndustry;
	public float tertiaryIndustry;
	public List<CityStatisDto> cityStatistics;
//	public int orderId;
}


//"population":0.0,"pcdi":55000.0,"urbanRate":0.0,"intro":"测试用的城市介绍测试用的城市介绍测试用的城市介绍测试用的城市介绍","primaryIndustry":900.0,"secondIndustry":1200.0,"tertiaryIndustry":1600.0,"cityStatistics":[{"date":"2010","gdp":8004.0,"gdpRate":7.0},{"date":"2011","gdp":8265.0,"gdpRate":7.5},{"date":"2012","gdp":8478.0,"gdpRate":7.1},{"date":"2013","gdp":8880.0,"gdpRate":7.8},{"date":"2014","gdp":9158.0,"gdpRate":7.8}]}