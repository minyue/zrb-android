package com.zrb.mobile.adapter.model;

import java.util.List;

public class FocusDiscoverDto {
	public int res;
	public String msg;
	public Data data;

	public class Data {
		public boolean isCollected;
	}
}
