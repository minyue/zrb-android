package com.zrb.mobile.adapter.model;

public class MyMessageDto {

	/** 主键,自增长 **/
	public Long id;
	public long receiverUid;
	public String type;
	public String messageInfoId;
	public int status;
	/** 意向发布时间 **/
	public Long createTime;
	public Messages messageInfo;
	/** 是否已读 **/
	public int read;

	public class Messages {
		// 消息id
		public long id;
		/** 简介/标题 **/
		public String title;

		/** 意向发布时间 **/
		public Long createTime;

		/** 关注次数 **/
		public Long collectNum;

		/** 咨询次数 **/
		public Long queryNum;

		/** 通知名字 **/
		public String typeName;

		/** 发送人的名称 **/
		public String senderUname;
		// 消息的内容
		public String content;
		// 枚举跳转 101项目，102资金
		public String bizKey;
		public String bizType;
	}

}
