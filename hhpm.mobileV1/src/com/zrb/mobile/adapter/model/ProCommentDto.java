package com.zrb.mobile.adapter.model;

public class ProCommentDto {
	/**评论主键id，自增**/
	private Long id;

	/**项目id**/
	private Long pid;

	/**用户id**/
	private Long uid;

	/**用户名字**/
	private String uname;

	/**评论内容**/
	private String content;

	/**目标id**/
	private Long targetId;

	/**目标类型**/
	private String targetType;

	/**目标名字**/
	private String targetName;

	/**评论状态**/
	private Integer status;

	/**创建时间**/
	private long ctime;

	/**更新时间**/
	private String uptime;

	/**删除时间**/
	private String detime;
	
	/**评论问题类型**/
	private Integer questionType;

	private String picUrl;
	
	
	
	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public long getCtime() {
		return ctime;
	}

	public void setCtime(long ctime) {
		this.ctime = ctime;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public String getDetime() {
		return detime;
	}

	public void setDetime(String detime) {
		this.detime = detime;
	}

	public Integer getQuestionType() {
		return questionType;
	}

	public void setQuestionType(Integer questionType) {
		this.questionType = questionType;
	}

	
}
