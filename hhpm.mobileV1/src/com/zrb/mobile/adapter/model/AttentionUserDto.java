package com.zrb.mobile.adapter.model;

public class AttentionUserDto extends BaseDto {
	
	public int id;
	public Long fromUserId;
	public Long toUserId;
	public Long updateTime;
	public int  status;
 	public int relation;
 	public FollowUserDto fromUser;
 	public FollowUserDto toUser;
 
}
//{"id":257,"bizid":-1,"cover":null,"sort":null,"url":"group1/M00/00/05/rBEg_VZULcmAb2cAAAAVZ-ZPBj4059.jpg","name":"test.png","remark":"12","status":0,"type":10,"addtime":1448357495000,"uptime":1448357522000}