package com.zrb.mobile.adapter.model;

public class AdsDto {
	
	public int id;
	public int bizid;
	public String cover;
//	public int orderId;
	public int sort;
	public String url;
	public String name;
	public String remark;
	public int type;
	public String actionUrl;
	 
	public AdsDto(int key,String tmpurl,String des){
		
		id=key;
		url=tmpurl;
		remark=des;
	}
 
}
//{"id":257,"bizid":-1,"cover":null,"sort":null,"url":"group1/M00/00/05/rBEg_VZULcmAb2cAAAAVZ-ZPBj4059.jpg","name":"test.png","remark":"12","status":0,"type":10,"addtime":1448357495000,"uptime":1448357522000}