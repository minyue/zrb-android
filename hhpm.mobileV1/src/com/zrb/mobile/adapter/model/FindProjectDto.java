package com.zrb.mobile.adapter.model;

import java.util.List;

public class FindProjectDto {

	public long id;
	public String projectName;
	public int industryId;
	public String industryStr;
	public String provincePostcode;
	public String province;
	public String cityPostcode;
  	public String city;
  	
	public long insertTime;
	public long updateTime;
	public String pic;
	//融资
	public double total;
	public int projectStatus;
	public String projectStatusStr;
	public long userId;
	public String userName;
	public long pushtime;
	public int mode;
	public String modeStr;
	//
	public int auth;
	public String authStr;
	public long intentionId;
	public String intentionStr;
	public String source;
	public String projectNo;
	public String pusher;
	public String phone;
	public String pusherName;
	public String pushtimeStr;
	public String insertTimeStr;
	public String industryColor;
/*	public String qryStartDate;
	public String qryEndDate;
	public String qryKeyWord;*/
}

//{"id":32414," ", , ,"auth":501," ,"pusher":"1000000039","pusherName":null,"phone":"18164005008","pushtimeStr":null,"insertTimeStr":null,"qryStartDate":null,"qryEndDate":null,"qryKeyWord":null} 
// {"code":110000,"name":"������","isCapital":null,"pycode":"bjs","id":1}