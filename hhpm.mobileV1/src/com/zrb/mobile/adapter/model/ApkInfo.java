package com.zrb.mobile.adapter.model;

import java.io.Serializable;
 

	public class ApkInfo  {  
		public String downloadUrl;  //下载地址  
		public String apkVersion;  //apk版本  
		public String apkSize;    //apk文件大小  
		public int apkCode;   //apk版本号(更新必备)  
		public String apkName;  //apk名字  
		public String apkLog;   //apk更新日志  
	}  
	
 

