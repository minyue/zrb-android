package com.zrb.mobile.adapter.model;

public class ChoiceGenera_CardDto extends BaseDto {
	
	public int choiceType;
	public long bizId;
	public String title;
	
	public long publishTime;
	public String pic;
//	public int orderId;
	public int auth;
	public int collectionCount;
	public int advisoryCount;
	public boolean showTime;
 
}
//{"id":257,"bizid":-1,"cover":null,"sort":null,"url":"group1/M00/00/05/rBEg_VZULcmAb2cAAAAVZ-ZPBj4059.jpg","name":"test.png","remark":"12","status":0,"type":10,"addtime":1448357495000,"uptime":1448357522000}