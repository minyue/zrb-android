package com.zrb.mobile.adapter.model;

 

	public class CityDto {
		
		public int id;
		public String cityName;
		public int  postcode;
		public String areacode;
		public int cityLevel;
		public int topCityId;
		public String img;
		
		public String hotValue;
		public int isRecommend;
		public int recommentRank;
		public int isTopics;
		public String topCity;
		
  	}
 
	
	//{"id":1,"cityName":"北京市","postcode":"110000","areacode":"（+86）010","cityLevel":1,"topCityId":0,"img":"[\"\"]","hotValue":null,"isRecommend":1,"recommentRank":0,"isTopics":1,"topCity":null}
/*	public int  code;
	public String name;
	public String isCapital;
	public String pycode;
	public int id;*/
	
	//{"code":110000,"name":"北京市","isCapital":null,"pycode":"bjs","id":1}