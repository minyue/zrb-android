package com.zrb.mobile.adapter.model;

import java.util.List;

public class AdsDtoResult {
	
	  public int res;
	  public String msg;
	  public List<AdsDto> data;
}
