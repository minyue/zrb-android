package com.zrb.mobile.adapter.model;

import java.util.List;

public class AttentionProjectDtos  {
	
	  public int res;
	  public String msg;
	  public List<AttentionProjectDto> data;    
  
}



//{"autoId":2051,"projectId":null,"pppType":null,"projectName":"马鞍山市垃圾焚烧发电","projectType":1,"industryId":20,"provincePostcode":"340000","province":"安徽省","cityPostcode":"340500",
//"city":"马鞍山市","countyPostcode":"340503","county":"花山区","insertTime":1435930294000,"updateTime":1436426030000,"pic":null,"bigPic":null,
//"intro":"项目建设分两期完成，一期设计规模为800吨/日，年度处理垃圾26.66万吨；二期建成后达到1200吨/日。本项目的垃圾处理的服务范围包括马鞍山市市区（花山区、雨山区、博望区）和当涂县。 "
//,"investment":51900.0000,"keywords":null,"hotValue":null,"isValidate":0,"nameMd5":"18a17a2952f4f94fc156861ce6b3c8fd","projectStatus":1,"isCase":0,"area":0.0,
//"tradeDate":1438358400000,"isRecommend":0}