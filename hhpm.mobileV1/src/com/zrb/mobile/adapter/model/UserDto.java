package com.zrb.mobile.adapter.model;

public class UserDto {

	public long uid;
	public String uname;
	public String zrb_front_ut;
	public Boolean auth;
	public String org;
	//public String userTypeName;
	//public String userType;
	public String avatar;
	public String orgId;
	public String uemail;
	
	//职务
	public String position;
	//行业
	public String industry;
	//行业id
	public String industryId;
	public String pwd;
	
	public int collectProject;
	public int collectedNumber;
	public int collectFund;
	/*public String pwd;
	public String huanxinImPassword;
	public String huanxinImUsername;*/
  
}

//{"res":1,"msg":null,"data":{"huanxinImPassword":"4AADCB9261355F31504728CC234D5872","uname":"qwee","zrb_front_ut":"32ffdc4e6cbec314c892a57fc45029a8","auth":false,"org":null,"avatar":"/contentImg/20150810172904300.png","uemail":"503973663@qq.com","orgId":"","uid":2000000332,"userTypeName":"政府","userType":"2","huanxinImNickname":"qwee","huanxinImUsername":"test_2000000332"},"bannerData":null}
 