package com.zrb.mobile.adapter.model;

public class IntentionDto {

	/** 主键,自增长 **/
	public Long id;

	/** 简介/标题 **/
	public String intro;

	/** 行业id,多个以#号隔开,来源于industry表 **/
	public String industry;

	/** 投资区域,来源于city_info **/
	public String district;

	/** 投资/融资总额 **/
	public java.math.BigDecimal amount;

	/** 投资/融资方式，值见数据字典 **/
	public Integer mode;

	/** 意向类型,值见数据字典表 **/
	public Integer type;

	/** 意向发布人id **/
	public Long userId;

	/** 意向发布时间 **/
	public Long pushTime;

	/** 是否有效 0，无效1，有效 **/
	public Integer isValid;

	/** 项目/资金详情 **/
	public String content;

	/** 处理人 **/
	public String handlePerson;

	/** 处理时间 **/
	public Long handleTime;

	/** 是否已处理，0，未处理 1，已处理 **/
	public Integer status;

	/** 备注 **/
	public String note;

	/** 关联项目id **/
	public Long projectId;
	
	public Long attentionTimes;
	
	public String avatar;
	
	public String userName;
	
	public Integer messageAmount;
	
	public String org;
	
	public String modeName;
	
	public String typeName;
	
	public String districtName;

	public String industryName;
	public int attention;
	
}
