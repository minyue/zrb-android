package com.zrb.mobile.adapter.model;

import java.util.List;

public class OwnProjectDetailDto {
	
	public String projectName;          
	public String period;    
	public String pic;      
	public String investment;
	public int inspectNum;       
	public int commentNum;
	public int inviteNum;
	public int viewNum;
	public List<CommentStatisticDto> commentStatistics;
	public List<inspectStatisticDto>   inspectStatistics;
	public List<inspectStatisticDto>   collectionStatistics;
	public List<inspectStatisticDto>    shareStatistic;
  
 	 
}

 //"projectName":"永昌县和顺佳苑棚户区改造项目","period":"132天23小时","pic":"","investment":2165.0000,"inspectNum":0,"commentNum":16