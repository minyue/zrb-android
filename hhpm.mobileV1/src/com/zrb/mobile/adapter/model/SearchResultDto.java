package com.zrb.mobile.adapter.model;

import java.util.List;

 

	public class SearchResultDto {
		
		public int pageNum;
		public int pageSize;
		
		public int startRow;
		public int endRow;
		public int  total;
		public int pages;
		
	    public List<NewsDto> list;
	 
	}
 
