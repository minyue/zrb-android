package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.AttentionFindDto;

import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;

public class AttentionFindAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public TextView source;
		public TextView date;

	}

	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd"); // ʱ���ʽ

	public List<AttentionFindDto> CurrentDatas;
	private LayoutInflater mInflater;
	private int curAttionType = -1;

	public void setCurAttionType(int curType) {
		curAttionType = curType;
	}

	public AttentionFindAdapter(Context context,
			ArrayList<AttentionFindDto> mDatas) {
		this.mInflater = LayoutInflater.from(context);
		CurrentDatas = mDatas;

	}

	public void setAll(List<AttentionFindDto> items) {
		CurrentDatas.clear();
		CurrentDatas.addAll(items);
	}

	public void addAll(List<AttentionFindDto> items) {
		CurrentDatas.addAll(items);
	}

	public void setCurrentDatas(List<AttentionFindDto> currentDatas) {
		CurrentDatas = currentDatas;
	}

	public void removeAt(int pos) {
		CurrentDatas.remove(pos);
	}

	public AttentionFindDto getAt(int pos) {
		return CurrentDatas.get(pos);
	}

	public long getMinTime() {
		if (CurrentDatas != null && CurrentDatas.size() > 0)
			return CurrentDatas.get(CurrentDatas.size() - 1).createTime;
		return -1;
	}

	public int getCount() {

		return CurrentDatas.size();

	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		AttentionFindDto tmpDto = CurrentDatas.get(position);
		if (convertView == null) {
			// convertView = mInflater.inflate(R.layout.attentionfind_list_item,
			// parent,false);
			convertView = mInflater.inflate(R.layout.attentionfind_list_item,
					null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView
					.findViewById(R.id.tv_message_title);
			holder.source = (TextView) convertView
					.findViewById(R.id.tv_message_source);
			holder.date = (TextView) convertView
					.findViewById(R.id.tv_message_time);
			//
			convertView.setTag(holder);//
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.title.setText(tmpDto.title);

		switch (curAttionType) {
		case Constants.FocusIntent:
			holder.source.setText(TextUtils.isEmpty(tmpDto.userName)?tmpDto.typeName + " " +"匿名": tmpDto.typeName + " " + tmpDto.userName);
			break;
		case Constants.FocusProject:
			holder.source.setText(tmpDto.province + " ·￥ " + tmpDto.total);
			break;
		default:
			holder.source.setText(tmpDto.province + " ·￥ " + tmpDto.total);
			break;
		}

		holder.date.setText(dateformat.format(tmpDto.createTime));
		return convertView;
	}

}
