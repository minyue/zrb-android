package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
 
import com.zrb.mobile.MyFindMoney;
import com.zrb.mobile.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class UserMyFindMoneyAdapter extends BaseAdapter {
	private Context context;
//	private List<Map<String, String>> list;
	public List<MyFindMoney> list1 = new ArrayList<MyFindMoney>();
	private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");
	
	//重构
	public UserMyFindMoneyAdapter(Context context,List<MyFindMoney> list1){
	      this.context = context;
	      this.list1 = list1;
	}

	public long getMinId(){
	 	  if (list1 != null&&list1.size()>0)
				return list1.get(list1.size()-1).id;
			return -1;
	}
	
	public long getMinPushTime(){
	 	  if (list1 != null&&list1.size()>0)
				return list1.get(list1.size()-1).pushTime;
			return -1;
	}
	
	public void setCurDatas(List<MyFindMoney> tmpDtos){
		list1.clear();
		list1.addAll(tmpDtos);
	}
	
	public void addAll(List<MyFindMoney> tmpDtos){
		list1.addAll(tmpDtos);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list1.size() > 0 ? list1.size():0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list1.get(position) != null ? list1.get(position) :null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position > 0 ? position : 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
        	convertView = LayoutInflater.from(context).inflate(R.layout.u_my_finemoney, parent, false);
        	holder = new ViewHolder(convertView);
        	convertView.setTag(holder);
        }else{
        	holder = (ViewHolder) convertView.getTag();
        }
        
        MyFindMoney  intention = list1.get(position);
        if( null != intention){
        	 if( null != intention.title && !"".equals(intention.title)){
        		 holder.tv_titile.setText(intention.title.toString());
             }
        	 if( null != intention.collectNum && !"".equals(intention.collectNum)){
        		 holder.tv_guanzhu.setText("" +intention.collectNum);
             }
        	 if( null != intention.queryNum && !"".equals(intention.queryNum)){
        		 holder.tv_messagecount.setText("" +intention.queryNum);
             }
        	 if( null != intention.pushTime && !"".equals(intention.pushTime)){
        		 holder.tv_time.setText(sdf.format(intention.pushTime));
             }
        }
		
		return convertView;
	}
	private class ViewHolder{
		TextView tv_titile,tv_guanzhu,tv_messagecount,tv_time;
		public ViewHolder(View v) {
			tv_titile = (TextView) v.findViewById(R.id.tv_message_title);
			tv_guanzhu = (TextView) v.findViewById(R.id.et_account);
			tv_messagecount = (TextView) v.findViewById(R.id.at_account);
			tv_time = (TextView) v.findViewById(R.id.at_date);
			
		}
	}
	
	
}