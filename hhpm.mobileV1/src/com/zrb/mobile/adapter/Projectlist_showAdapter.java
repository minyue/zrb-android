package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.*;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.ListView;

import android.widget.ImageView;

import android.widget.TextView;

public class Projectlist_showAdapter extends MatchingBaseAdapter {

	private LayoutInflater mInflater;

	DisplayImageOptions options;
	public List<FindProjectDto> CurrentDatas = new ArrayList<FindProjectDto>();

	private ImageLoader imageLoader;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");// yyyy.

	 
	Drawable defaultpic,localAuthpic,infoAuthpic;
	Context mcontext;

	private class ViewHolder {
		public TextView title; 
 		public TextView pppType;

		public ImageView image;
  		public TextView publishDate;
 		public TextView project_address;
		public TextView project_auth_status;
		public TextView project_price;
		public ImageView mlefticon;
		//public GradientDrawable iconBg;
	}

	public void setImgLoader(ImageLoader curImgLoad) {
		imageLoader = curImgLoad;

	}

	public Projectlist_showAdapter(Context context) {

		this.mInflater = LayoutInflater.from(context);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.loadpic)
				.showImageForEmptyUri(R.drawable.loadpic)
				.showImageOnFail(R.drawable.loadpic).cacheInMemory(false)
/*			    .displayer(new FadeInBitmapDisplayer(300)) */
				.cacheOnDisc(true).build();
  
		mcontext=context;
		defaultpic = context.getResources().getDrawable(R.drawable.loadpic3);
		localAuthpic= context.getResources().getDrawable(R.drawable.sdrzx);
		infoAuthpic= context.getResources().getDrawable(R.drawable.zlrz);

	}
	
	 
	public void setDatas(List<FindProjectDto> tmpDtos,boolean isAppend) {
		if(!isAppend) CurrentDatas.clear();
		
		CurrentDatas.addAll(tmpDtos);
	}
 
	@Override
	public void setJsonDatas(String tmpDtos, boolean isAppend) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		FindProjectDtos tmpDto = gson.fromJson(tmpDtos,FindProjectDtos.class);
		setDatas(tmpDto.data,isAppend);
	}
	
	@Override
	public int getCurpageSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public long getMinId() {
		if (CurrentDatas != null && CurrentDatas.size() > 0)
			return CurrentDatas.get(CurrentDatas.size() - 1).id;
		return -1;
	}

	public String getMinTime() {
		if (CurrentDatas != null && CurrentDatas.size() > 0)
			return CommonUtil.formatlongDate(CurrentDatas.get(CurrentDatas.size() - 1).pushtime
					                        ,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		return CommonUtil.formateDate(new Date());
	}

	@Override
	public int getCount() {
		if (CurrentDatas != null) {
			return CurrentDatas.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {

		// TODO Auto-generated method stub

		return CurrentDatas.get(position);

	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;

	}

	private void setprojectType(int code, ImageView projectType) {

		switch (code) {
		case Constants.pppType:// "pppͶ��Ȩ"
			projectType.setImageResource(R.drawable.project_ppp_plabel);
			break;
		case Constants.purposeType:// ����ʹ��Ȩ
			projectType.setImageResource(R.drawable.project_place_plable);
			break;
		case Constants.assetType:// "�ʲ���"
			projectType.setImageResource(R.drawable.project_assets_plable);
			break;
		default:
			;
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;
		FindProjectDto curDto = CurrentDatas.get(position);
		ViewHolder holder;
		if (convertView == null) {

			view = mInflater.inflate(R.layout.projects_list_item_zrb, null);
			holder = new ViewHolder();
			holder.title = (TextView) view.findViewById(R.id.id_project_title);
			holder.image = (ImageView) view.findViewById(R.id.id_projectsImg);
			//holder.projectType = (ImageView) view.findViewById(R.id.id_projectstype);
			//holder.txt_project_pppType = (TextView) view.findViewById(R.id.txt_project_pppType);
			//holder.project_date = (TextView) view.findViewById(R.id.project_date);
			holder.project_address = (TextView) view.findViewById(R.id.project_address);
			holder.project_auth_status = (TextView) view.findViewById(R.id.project_auth_status);
			holder.project_price = (TextView) view.findViewById(R.id.project_price);
			holder.mlefticon = (ImageView) view.findViewById(R.id.left_icon);
 
			holder.pppType = (TextView) view.findViewById(R.id.txt_project_pppType);
			//holder.price = (TextView) view.findViewById(R.id.project_price);
			holder.publishDate = (TextView) view.findViewById(R.id.project_date);
			
/*			GradientDrawable drawable = new GradientDrawable();
			drawable.setShape(GradientDrawable.RECTANGLE);
			drawable.setCornerRadii(new float[] { 5, 5, 5, 5, 5, 5, 5, 5 });
			holder.iconBg=drawable;*/
			
			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.title.setText(curDto.projectName);
		
		if(holder.image.getTag()!=null&&holder.image.getTag().toString().equals(String.valueOf(position))){
 			
		}
		else {
			if (!TextUtils.isEmpty(curDto.pic)) {
				ImageLoader.getInstance().displayImage( AppSetting.mediaServer + curDto.pic, holder.image, options);
			} else
				holder.image.setBackgroundDrawable(defaultpic);
 		}
 		
  		holder.image.setTag(position);
  	/*	if(holder.image.getTag()!=null)
  			CommonUtil.InfoLog("image.getTag()", holder.image.getTag().toString());*/
  		
		holder.pppType.setText("不限".equals(curDto.modeStr) ? "方式"+curDto.modeStr:curDto.modeStr);
 		holder.publishDate.setText(CommonUtil.formatlongDate(curDto.pushtime,sdf));
 		holder.project_address.setText(TextUtils.isEmpty(curDto.province) ?"全国":curDto.province.replace("省", "").replace("市", ""));
 		holder.project_auth_status.setText("所属行业: "+curDto.industryStr);
		
		if (Constants.PROJECT_SCENE_AUTH == curDto.auth) {
 			holder.mlefticon.setImageDrawable(localAuthpic);
			holder.mlefticon.setVisibility(View.VISIBLE);
 		} 
		else if (Constants.PROJECT_MATERIALS_AUTH == curDto.auth) {
 			holder.mlefticon.setImageDrawable(infoAuthpic);
			holder.mlefticon.setVisibility(View.VISIBLE);
 		}else {
			holder.mlefticon.setVisibility(View.GONE);
		}

		holder.project_price.setText((int)curDto.total + "万");//"估值: "
		return view;
	}
 

	private String getfisrtChar(String name) {
		if (TextUtils.isEmpty(name))
			return "";
		else
			return name.substring(0, 1);
	}

	public void updateSingleRow(ListView listView, long id) {

		 if (listView != null) {
			int start = listView.getFirstVisiblePosition();
			for (int i = start, j = listView.getLastVisiblePosition(); i <= j; i++)
				if (id == ((FindProjectDto) listView.getItemAtPosition(i)).id) {
					View view = listView.getChildAt(i - start);
					getView(i, view, listView);
					break;
				}
		} 
		
	 
	}


	
}
