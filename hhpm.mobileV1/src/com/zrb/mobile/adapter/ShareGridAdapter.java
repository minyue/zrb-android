package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.ProjectDto;
import com.zrb.mobile.common.NightConfig;

public  class ShareGridAdapter extends BaseAdapter {

 
	public List<RadioItemEx> CurrentDatas = new ArrayList<RadioItemEx>();
 	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
 	private boolean isNight=false;
	public ShareGridAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
		if(NightConfig.getInstance(context).getNightModeSwitch())   isNight=true;
 		 
	}

	@Override
	public int getCount() {

		return CurrentDatas.size();// 返回数组的长度

	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
	 

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.detail_more_item, null);
 		} else {
			 
		}
		
		/* 得到各个控件的对象 */
		TextView title = (TextView) convertView.findViewById(R.id.text);
		ImageView imgview = (ImageView) convertView.findViewById(R.id.icon);
		
		title.setText(CurrentDatas.get(position).Name);
		imgview.setImageResource(CurrentDatas.get(position).Rid) ;
		//holder.text.setText("用途："+CurrentDatas.get(position).desc1+" | 出让时间："+CurrentDatas.get(position).desc2);
      
		title.setTextColor(convertView.getContext().getResources().getColor(
					 isNight ? R.color.detail_more_title_item_text_night
							: R.color.detail_more_title_item_text));
		return convertView;
	}/* 存放控件 */

}