package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.HotRankDto;
import com.zrb.mobile.utility.CommonUtil;

 
public  class AppSharelistAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public ImageView lefticon;
		public ImageView righticon;
	}
	
	private int _selIndex = 0;
 	public List<RadioItemEx> CurrentDatas = new ArrayList<RadioItemEx>();

	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
 	
	
	public AppSharelistAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
	}

	public void setDatalist(List<RadioItemEx> curDatas){
		CurrentDatas.clear();
		CurrentDatas.addAll(curDatas);
	}
	
	@Override
	public int getCount() {
 		return CurrentDatas.size();// 返回数组的长度
 	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.app_share_list_item, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.remind_text);
			holder.lefticon = (ImageView) convertView.findViewById(R.id.left_icon);
			holder.righticon=(ImageView) convertView.findViewById(R.id.add_search);
			 
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
		}
  
		holder.lefticon.setImageResource(CurrentDatas.get(position).Rid);
	 
		holder.righticon.setTag(position);
		holder.title.setText(CurrentDatas.get(position).Name);

			 
		return convertView;
	}/* 存放控件 */
	
	
 


}