package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.ProCommentDto;
import com.zrb.mobile.ui.CircleImageView;
import com.zrb.applib.utils.AppSetting;

public class Project_comments_listAdapter extends BaseAdapter {
	
	public interface onCommentListener{
		
		public void onReply(int postion,int type);//type 1,del 2 reply
	}
	Long uid;
	ProCommentDto curDtos;
	private LayoutInflater mInflater;
	private List<ProCommentDto> mDatas;
	DisplayImageOptions options;
	private SimpleDateFormat sdf= new SimpleDateFormat("MM/dd");//yyyy.
 	int norColor=Color.rgb(95, 95, 95);
 	Context context;
 	onCommentListener mOnCommentlistener;
 
 	public void setOnCommentListener(onCommentListener Commentlistener ){
 		mOnCommentlistener=Commentlistener;
 	}
 	
	private class ViewHolder {
		public TextView title;//id_projectstype
		public TextView content;
     	public CircleImageView image;
		public TextView commentTime;
		public TextView delete;
		public TextView digg_count;
	}
 

	public Project_comments_listAdapter(Context context,ArrayList<ProCommentDto> arrayList) {
		this.context = context;
		this.mDatas = arrayList;
		this.mInflater = LayoutInflater.from(context);
 	     options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.loadpic)
		.showImageForEmptyUri(R.drawable.loadpic)
		.showImageOnFail(R.drawable.loadpic).cacheInMemory(true)
		.cacheOnDisc(true)
		.build();
 	}
 	
	public void setDatas(List<ProCommentDto> tmpDtos){
 		mDatas.clear();
		mDatas.addAll(tmpDtos);
	}
	
	public void addAll(List<ProCommentDto> tmpDtos){
 		mDatas.addAll(tmpDtos);
	}
	
	public void removeAtIndex(int postion){
		
		mDatas.remove(postion);
		this.notifyDataSetChanged();
	}
	
	public int getMinId(){
		
		if(mDatas==null||mDatas.size()<=0) return 1;
		else{
			long il = mDatas.get(mDatas.size()-1).getId();
			return (int)il;
		}
	}	
	
	public ProCommentDto getCurItem(int position){
 		return mDatas.get(position);
	}
	
	@Override
	public int getCount() {
		if (mDatas != null) {
			return mDatas.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;

	}

	@Override
	public long getItemId(int position) {
 		// TODO Auto-generated method stub
 		return position;

	}
  
	private void setTitleHightlight(final String tmpStrTitle,TextView mTitle){
		
		String tagwords="说:";
 	 	int idx2=tmpStrTitle.indexOf(tagwords);
 		if(idx2>-1){
 			SpannableStringBuilder styled2 = new SpannableStringBuilder(tmpStrTitle);
			styled2.setSpan(new ForegroundColorSpan(norColor), idx2,idx2+ tagwords.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// i 未起始字符索引，j 为结束字符索引
	    	mTitle.setText(styled2); 
		}
		else
			mTitle.setText(tmpStrTitle);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	 
		View view = convertView;
		curDtos = mDatas.get(position);
		final ViewHolder holder;
		uid = curDtos.getUid();
		if (convertView == null) {

			view = mInflater.inflate(R.layout.project_comment_list_item,null);
			holder = new ViewHolder();
		 	holder.title = (TextView) view.findViewById(R.id.ss_user);
 			holder.image = (CircleImageView) view.findViewById(R.id.ss_avatar);
 			holder.content = (TextView) view.findViewById(R.id.ss_content);
 			holder.commentTime= (TextView) view.findViewById(R.id.comment_time);
 			holder.delete = (TextView) view.findViewById(R.id.delete);
 			holder.digg_count = (TextView) view.findViewById(R.id.digg_count);
 			holder.delete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(v.getTag()!=null)
						if(mOnCommentlistener!=null)
							mOnCommentlistener.onReply(Integer.parseInt(v.getTag().toString()),1);//type 1,del 2 reply
				}
			});
 			
 			holder.digg_count.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(v.getTag()!=null)
						if(mOnCommentlistener!=null)
							mOnCommentlistener.onReply(Integer.parseInt(v.getTag().toString()),2);
				}
			});
 			
			view.setTag(holder);

		} else {
 			holder = (ViewHolder) view.getTag();
 		}
  		 
		if( null == curDtos.getTargetId() || 0 == curDtos.getTargetId()){
			setTitleHightlight("用户 "+curDtos.getUname()+"  说:",holder.title);
		}
		
		if( null != curDtos.getTargetId() && 0 != curDtos.getTargetId()){
			setTitleHightlight("用户 "+curDtos.getUname()+"  说: @"+curDtos.getTargetName()+"",holder.title);
		}
 	 
		if(null!=AppSetting.curUser)
		   holder.delete.setVisibility(curDtos.getUid() == AppSetting.curUser.uid ?View.VISIBLE:View.GONE);
		
		holder.delete.setTag(position);
		holder.digg_count.setTag(position);
 	    holder.commentTime.setText(new java.text.SimpleDateFormat("yyyy/MM/dd").format(curDtos.getCtime()));
		holder.content.setText(curDtos.getContent());
		
		
		if(!TextUtils.isEmpty(curDtos.getPicUrl())){
		   ImageLoader.getInstance().displayImage(AppSetting.BASE_URL+"contentImg/"+curDtos.getPicUrl(), holder.image,options);
		}
		else
			holder.image.setImageResource(R.drawable.my_center_head);
    	return view;

	}
	
	
//	public void geturl(){
//		RequestParams param = new RequestParams();
//		param.put("uid",uid);
//		System.out.println("uid:--------" + uid);
//		ZrbRestClient.post("projectApi/getUpic" , param,
//				new AsyncHttpResponseHandler() {
//
//					@Override
//					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
//							Throwable arg3) {
//						CommonUtil.showToast("网络超时,请重新连接", context);
//					}
//
//					@Override
//					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
//						String results = new String(arg2);
//						
//						CommonUtil.InfoLog("test", results);
//						try {
//							Gson gson = new Gson();
//							RegisterDto tmpDto = gson.fromJson(results,RegisterDto.class);
//							if( tmpDto.res == 1){
//								url = tmpDto.data;
//							}else{
//								url = null;
//							}
//					} catch (Exception e) {
//
//							CommonUtil.showToast( "fail:"+e.getMessage(),context);
//							System.out.println("eeee!!!!" + e.getMessage());
//
//						}
//
//					}
//
//				});
//	}
	
}
