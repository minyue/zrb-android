package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.*;
import com.zrb.applib.utils.AppSetting;
 
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;

import android.widget.BaseAdapter;

import android.widget.ImageView;

import android.widget.TextView;

public class Projectlist_tradingAdapter extends BaseAdapter {

	private LayoutInflater mInflater;

	DisplayImageOptions options;
	public List<ProjectTradingDto> CurrentDatas = new ArrayList<ProjectTradingDto>();
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private ImageLoader imageLoader;
 	
	private class ViewHolder {
		public TextView title;
		public TextView belongArea;
		public ImageView image;
		public TextView bussiness;
		public TextView transferDate;
		public TextView totalmoney;
		public TextView publishDate;
	}
 
	public void setImgLoader(ImageLoader curImgLoad) {

		imageLoader = curImgLoad;

	}

	public Projectlist_tradingAdapter(Context context) {

		this.mInflater = LayoutInflater.from(context);

		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.guess_like_img)
		.showImageForEmptyUri(R.drawable.guess_like_img)
		.showImageOnFail(R.drawable.guess_like_img).cacheInMemory(true)
		.cacheOnDisc(true)
		.build();

	}

	@Override
	public int getCount() {
		if (CurrentDatas != null) {
			return CurrentDatas.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {

		// TODO Auto-generated method stub

		return null;

	}

	@Override
	public long getItemId(int position) {

		// TODO Auto-generated method stub

		return position;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;
		final ViewHolder holder;
		if (convertView == null) {

			view = mInflater.inflate(R.layout.project_list_item_csdn,null);
			holder = new ViewHolder();
			holder.title = (TextView) view.findViewById(R.id.id_title);
 			holder.image = (ImageView) view.findViewById(R.id.id_newsImg);
 			holder.belongArea = (TextView) view.findViewById(R.id.id_belongArea);
 			holder.bussiness= (TextView) view.findViewById(R.id.id_bussiness);
 			holder.transferDate= (TextView) view.findViewById(R.id.id_returerate);
 			holder.totalmoney= (TextView) view.findViewById(R.id.id_totalmoney);
 			holder.publishDate=(TextView)view.findViewById(R.id.id_date);
			view.setTag(holder);

		} else {

			holder = (ViewHolder) view.getTag();

		}

		holder.title.setText(CurrentDatas.get(position).title);
		holder.belongArea.setText(CurrentDatas.get(position).belongArea.replace("所在地区:", ""));
		//holder.bussiness.setText(CurrentDatas.get(position).belongArea);
		//性质: 批发零售    | 资产招商
		holder.bussiness.setText("性质：" + CurrentDatas.get(position).transferWay.replace("出让方式:", "")	+ " | " + CurrentDatas.get(position).Landuse.replace("土地用途:", ""));
		
		
		holder.transferDate.setText(""+CurrentDatas.get(position).transferDate);
		holder.totalmoney.setText(CurrentDatas.get(position).totalmoney+"万");

	 	String url =CurrentDatas.get(position).img ;//AppSetting.mediaServer+
		imageLoader.displayImage(url, holder.image, options,animateFirstListener);
		holder.publishDate.setText(CurrentDatas.get(position).publishdate);
		return view;

	}
	
	private String formatdate(String curDate)
	{
		if(TextUtils.isEmpty(curDate)) return "";
		if(curDate.contains(" "))
			return curDate.split(" ")[0];
		else
			return curDate;
	}

	public void displayedImageclear() {

		AnimateFirstDisplayListener.displayedImages.clear();

	}

	private static class AnimateFirstDisplayListener extends

	SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
		.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,Bitmap loadedImage) {

			if (loadedImage != null) {

				ImageView imageView = (ImageView) view;

				boolean firstDisplay = !displayedImages.contains(imageUri);

				if (firstDisplay) {

					FadeInBitmapDisplayer.animate(imageView, 500);

					displayedImages.add(imageUri);

				}

			}

		}

	}

}
