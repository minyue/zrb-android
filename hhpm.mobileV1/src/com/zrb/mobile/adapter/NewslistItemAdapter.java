package com.zrb.mobile.adapter;

import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.Article2Dto;
import com.zrb.applib.utils.AppSetting;
 

public class NewslistItemAdapter extends BaseAdapter
{

	private LayoutInflater mInflater;
	private List<Article2Dto> mDatas;

	/**
	 * 使用了github开源的ImageLoad进行了数据加载
	 */
	private ImageLoader imageLoader;
	private DisplayImageOptions options;

	public NewslistItemAdapter(Context context, List<Article2Dto> datas)
	{
		this.mDatas = datas;
		mInflater = LayoutInflater.from(context);
		//imageLoader = ImageLoader.getInstance();
  		//.showStubImage(R.drawable.images)
		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.images).showImageOnFail(R.drawable.images).cacheInMemory(true)
				.cacheOnDisc(true).displayer(new FadeInBitmapDisplayer(300))
				.build();
   }

	public void setImgLoader(ImageLoader curImgLoad) {
 		imageLoader = curImgLoad;
 	}
	
	public void addAll(List<Article2Dto> mDatas)
	{
		this.mDatas.addAll(mDatas);
	}

	public void setDatas(List<Article2Dto> mDatas)
	{
		this.mDatas.clear();
		this.mDatas.addAll(mDatas);
	}

	@Override
	public int getCount()
	{
		return mDatas.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder = null;
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.news_list_item_csdn, null);
			holder = new ViewHolder();

			holder.mContent = (TextView) convertView.findViewById(R.id.id_content);
			holder.mTitle = (TextView) convertView.findViewById(R.id.id_title);
			holder.mDate = (TextView) convertView.findViewById(R.id.id_date);
			holder.mImg = (ImageView) convertView.findViewById(R.id.id_newsImg);

			convertView.setTag(holder);
		} else
		{
			holder = (ViewHolder) convertView.getTag();
		}
		Article2Dto newsItem = mDatas.get(position);
		holder.mTitle.setText(newsItem.title);//.getTitle()
		holder.mContent.setText(newsItem.content);//.getContent()
		holder.mDate.setText(newsItem.pubTime);//.getDate()
		if (!TextUtils.isEmpty(newsItem.img))                //.getImgLink()
		{
			holder.mImg.setVisibility(View.VISIBLE);
			imageLoader.displayImage(AppSetting.mediaServer+newsItem.img, holder.mImg, options);
		} else
		{
			holder.mImg.setVisibility(View.GONE);
		}

		return convertView;
	}

	private final class ViewHolder
	{
		TextView mTitle;
		TextView mContent;
		ImageView mImg;
		TextView mDate;
	}

}
