package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.HotRankDto;
import com.zrb.mobile.utility.CommonUtil;

 
public  class HotRangeResultAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public TextView ranktxt;
		public ImageView lefticon;
		public ImageView righticon;
		public RelativeLayout view;
	}
	
	private int _selIndex = 0;
 	public List<HotRankDto> CurrentDatas = new ArrayList<HotRankDto>();

	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
 	
	
	public HotRangeResultAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
	}

	public void setDatalist(List<HotRankDto> curDatas){
		CurrentDatas.clear();
		CurrentDatas.addAll(curDatas);
	}
	
	@Override
	public int getCount() {
 		return CurrentDatas.size();// 返回数组的长度
 	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.search_hotrange_list_item, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.remind_text);
			holder.lefticon = (ImageView) convertView.findViewById(R.id.left_imgicon);
			holder.righticon=(ImageView) convertView.findViewById(R.id.add_search);
			holder.ranktxt = (TextView) convertView.findViewById(R.id.left_ranktxt);
			holder.view = (RelativeLayout) convertView.findViewById(R.id.lineView);
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
			/* 设置TextView显示的内容，即我们存放在动态数组中的数据 */
			// holder.title.setText(getDate().get(position).get("ItemTitle").toString());
			// holder.text.setText(getDate().get(position).get("ItemText").toString());

			/* 为Button添加点击事件 */
			/*
			 * holder.bt.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override publicvoid onClick(View v) { Log.v("MyListViewBase",
			 * "你点击了按钮" + position); //打印Button的点击信息 } });
			 */

		}
		holder.righticon.setTag(position);
		holder.title.setText(CurrentDatas.get(position).title);
		holder.ranktxt.setText((position+1)+".");
//		if(position<3){      
//		 
//			holder.ranktxt.setTextColor(convertView.getContext().getResources().getColor(R.color.hot_rank_color));
// 			//holder.lefticon.setImageResource(R.drawable.rank1);
// 			//holder.lefticon.setVisibility(View.VISIBLE);
//		}
// 		else{
             //holder.lefticon.setVisibility(View.GONE);
			// holder.ranktxt.setText((position+1)+"");
			 holder.ranktxt.setTextColor(convertView.getContext().getResources().getColor(R.color.hot_rank_fontcolor));
//		}
		if( position % 2 !=0){
			holder.view.setBackgroundColor(convertView.getContext().getResources().getColor(R.color.search_hot_popular));
		}
			 
		return convertView;
	}/* 存放控件 */
	
	
 


}