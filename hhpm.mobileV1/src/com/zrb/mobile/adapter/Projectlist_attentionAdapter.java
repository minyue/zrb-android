package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

 
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.*;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
 
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;

import android.widget.BaseAdapter;

import android.widget.ImageView;

import android.widget.TextView;

public class Projectlist_attentionAdapter extends BaseAdapter {

	private LayoutInflater mInflater;

	DisplayImageOptions options;
	public List<AttentionProjectDto> CurrentDatas=new ArrayList<AttentionProjectDto>();
 
	private ImageLoader imageLoader;
	private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");//yyyy.
   	
	private class ViewHolder {
		public TextView title;//id_projectstype
		public ImageView projectType;
		public TextView pppType;
		
		public ImageView image;
 		public TextView price;
  		public TextView publishDate;
	}
 
	public void setImgLoader(ImageLoader curImgLoad) {
 		imageLoader = curImgLoad;
	}

	public Projectlist_attentionAdapter(Context context) {

		this.mInflater = LayoutInflater.from(context);
 	     options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.loadpic)
		.showImageForEmptyUri(R.drawable.loadpic)
		.showImageOnFail(R.drawable.loadpic).cacheInMemory(true)
	//	.displayer(new FadeInBitmapDisplayer(300))
		.cacheOnDisc(true)
		.build();
  	}
 	
	public void setDatas(List<AttentionProjectDto> tmpDtos){
 		CurrentDatas.clear();
		CurrentDatas.addAll(tmpDtos);
	}
	
	public void addAll(List<AttentionProjectDto> tmpDtos){
 		CurrentDatas.addAll(tmpDtos);
	}
	
	public String getMinCollection()
	{
		if (CurrentDatas != null&&CurrentDatas.size()>0)
			return CommonUtil.formatlongDate(CurrentDatas.get(CurrentDatas.size()-1).collectionTime,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		return "";
	}
  
	@Override
	public int getCount() {
		if (CurrentDatas != null) {
			return CurrentDatas.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {

		// TODO Auto-generated method stub

		return null;

	}

	@Override
	public long getItemId(int position) {
 		// TODO Auto-generated method stub
 		return position;

	}
  	
	private String replace(String tmpStr,String replaceStr){
	 	if(TextUtils.isEmpty(tmpStr)) return "";
		else
			return tmpStr.replace(replaceStr, "");
	}
	
	 
    private void setprojectType(int code,ImageView projectType){
	
	 switch (code) {
	  case 1://"ppp投资权"
		  projectType.setImageResource(R.drawable.project_ppp_plabel);
		  break;
 	  case 2://土地使用权
 		 projectType.setImageResource(R.drawable.project_place_plable);
 		  break;
	  case 3://"资产交易"
		  projectType.setImageResource(R.drawable.project_assets_plable);
		  break;
	  default:
		;
	  }
    }	
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;
		ProjectsDto curDto=CurrentDatas.get(position);
		final ViewHolder holder;
		if (convertView == null) {

			view = mInflater.inflate(R.layout.projects_attentionlist_item,null);
			holder = new ViewHolder();
		 	holder.title = (TextView) view.findViewById(R.id.id_project_title);
 			holder.image = (ImageView) view.findViewById(R.id.id_projectsImg);
 			holder.projectType = (ImageView) view.findViewById(R.id.id_projectstype);
 			
 			holder.pppType= (TextView) view.findViewById(R.id.txt_project_pppType);
 			holder.price= (TextView) view.findViewById(R.id.project_price);
 			holder.publishDate=(TextView)view.findViewById(R.id.project_date); 
			view.setTag(holder);

		} else {
 			holder = (ViewHolder) view.getTag();
 		}
  		
	  	holder.title.setText(curDto.projectName);
 	 	//setprojectType(curDto.projectType,holder.projectType);
  	 	//holder.pppType.setText(curDto.projectTypeZH+" ｜ ");
  		if(curDto.projectType==Constants.purposeType)  holder.pppType.setText(curDto.purposeZH+" ｜ ");//土地
  		else if(curDto.projectType==Constants.pppType)      holder.pppType.setText(curDto.pppTypeZH+" ｜ ");//
  		else if(curDto.projectType==Constants.assetType)    holder.pppType.setText(curDto.assetTypeZH+" ｜ ");
  	 	
     	holder.price.setText("估值："+curDto.investmentStr+"万");
 		holder.publishDate.setText(CommonUtil.formatlongDate(curDto.tradeDate,sdf)); 
  	 
		if(!TextUtils.isEmpty(curDto.pic))
	    	imageLoader.displayImage(AppSetting.mediaServer+curDto.pic, holder.image, options);  
    	return view;

	}
	
	private String formatdate(String curDate)
	{
		if(TextUtils.isEmpty(curDate)) return "";
		if(curDate.contains(" "))
			return curDate.split(" ")[0];
		else
			return curDate;
	}
 

}
