package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

 
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.CityDto;
import com.zrb.mobile.adapter.model.HotRankDto;
import com.zrb.mobile.adapter.model.MsgNoticeDto;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.MsgformatUtil;

 
public  class MsgNoticelistAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public TextView time;
		public TextView message;
		public ImageView avatar;
	}
	int norColor=Color.parseColor("#ea8010");
	private int _selIndex = 0;
	private List<MsgNoticeDto> CurrentDatas = new ArrayList<MsgNoticeDto>();

	private LayoutInflater mInflater;// �õ�һ��LayoutInfalter�����������벼�� /*���캯��*/
 	
	
	
	
	public MsgNoticelistAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
	}

	public void setDatalist(List<MsgNoticeDto> curDatas){
		CurrentDatas.clear();
		CurrentDatas.addAll(curDatas);
	}
	
    public MsgNoticeDto getCurItem(int pos) {
     	return CurrentDatas.get(pos);
    }
	
	public void addAll(List<MsgNoticeDto> curDatas){
 		CurrentDatas.addAll(curDatas);
	}
	
	public int getMinId(){
 		return CurrentDatas.get(CurrentDatas.size()-1).id;
	}
	
	@Override
	public int getCount() {
 		return CurrentDatas.size();// ��������ĳ���
 	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* ������ϸ���͸÷��� */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		MsgNoticeDto curDto=CurrentDatas.get(position);
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.message_notice_list_item, parent, false);
			holder = new ViewHolder();
			/* �õ������ؼ��Ķ��� */
			holder.title = (TextView) convertView.findViewById(R.id.msgname);
			holder.time = (TextView) convertView.findViewById(R.id.msgtime);
			holder.message=(TextView) convertView.findViewById(R.id.message_summary);
			holder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// ��ViewHolder����
		} else {
			holder = (ViewHolder) convertView.getTag();// ȡ��ViewHolder���� }
		}
  
		//holder.lefticon.setImageResource(CurrentDatas.get(position).Rid);
	 
	 
		//holder.title.setText(curDto.messageInfo.senderUname);
		if(curDto.read == 0){
			holder.avatar.setEnabled(true);
		}else{
			holder.avatar.setEnabled(false);
		}
 		
		holder.time.setText(new java.text.SimpleDateFormat("MM-dd").format(curDto.messageInfo.createTime));
		//MsgformatUtil.formatSummary(holder.message,holder.title ,curDto);
	 		 
		return convertView;
	}/* ��ſؼ� */
	
	public void setReadStatus(View curView){
		ImageView curImg=	(ImageView)	curView.findViewById(R.id.avatar);
		if(curImg!=null) curImg.setEnabled(false);
	}
 


}