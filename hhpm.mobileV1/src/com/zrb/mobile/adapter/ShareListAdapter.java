package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.ProjectDto;
import com.zrb.mobile.common.AppInfo;

public  class ShareListAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public ImageView icon;
		 
	}

	public List<AppInfo> CurrentDatas = new ArrayList<AppInfo>();

	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/

	
	
	public ShareListAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {

		return CurrentDatas.size();// 返回数组的长度

	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.share_dialog_item, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.text_weixin);
			holder.icon = (ImageView) convertView.findViewById(R.id.weixin_more);
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
 

		}
		holder.title.setText(CurrentDatas.get(position).appName);
		holder.icon.setImageDrawable(CurrentDatas.get(position).appIcon) ;
		//holder.text.setText("用途："+CurrentDatas.get(position).desc1+" | 出让时间："+CurrentDatas.get(position).desc2);

		return convertView;
	}/* 存放控件 */

}