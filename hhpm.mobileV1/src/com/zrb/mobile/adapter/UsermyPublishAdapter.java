package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
 
import com.zrb.matching.MatchingActivity;
import com.zrb.mobile.DiscoverDetailOneActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.User_my_publish;
import com.zrb.mobile.adapter.model.AttentionFindDto;
import com.zrb.mobile.adapter.model.IntentionDto;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class UsermyPublishAdapter extends BaseAdapter {
	private Context context;
//	private List<Map<String, String>> list;
	public List<IntentionDto> list1 = new ArrayList<IntentionDto>();
	private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");
	
	//重构
	public UsermyPublishAdapter(Context context,List<IntentionDto> list1){
	      this.context = context;
	      this.list1 = list1;
	}

	public IntentionDto getAt(int pos) {
		return list1.get(pos);
	}
	
	public void removeAt(int pos) {
		list1.remove(pos);
	}
	public long getMinId(){
	 	  if (list1 != null&&list1.size()>0)
				return list1.get(list1.size()-1).id;
			return -1;
	}
	
	public void setCurDatas(List<IntentionDto> tmpDtos,boolean isAppend){
		if(!isAppend) list1.clear();
		list1.addAll(tmpDtos);
	}
	
 
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list1.size() > 0 ? list1.size():0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list1.get(position) != null ? list1.get(position) :null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position > 0 ? position : 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
        	convertView = LayoutInflater.from(context).inflate(R.layout.u_my_publish, parent, false);
        	holder = new ViewHolder(convertView);
        	holder.tv_time.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
 					 int index = Integer.parseInt(v.getTag().toString());
					 IntentionDto intentionDto = list1.get(index);
			 
					 Intent	intent = new Intent(context, MatchingActivity.class);
					 intent.putExtra("actiontype", intentionDto.type == Constants.DiscoverMoneyIntent
							                                        ? MatchingActivity.MatchProject
							                                        : MatchingActivity.MatchFund);
					 context.startActivity(intent);
 				}});
        	convertView.setTag(holder);
        }else{
        	holder = (ViewHolder) convertView.getTag();
        }
        
        IntentionDto  intention = list1.get(position);
        if( null != intention){
        	 if( null != intention.intro && !"".equals(intention.intro)){
        		 holder.tv_titile.setText(intention.intro.toString());
             }
        	 if( null != intention.attentionTimes && !"".equals(intention.attentionTimes)){
        		 holder.tv_guanzhu.setText("" +intention.attentionTimes);
             }
        	 if( null != intention.messageAmount && !"".equals(intention.messageAmount)){
        		 holder.tv_messagecount.setText("" +intention.messageAmount);
             }
        /*	 if( null != intention.pushTime && !"".equals(intention.pushTime)){
        		 holder.tv_time.setText(sdf.format(intention.pushTime));
             }*/
        	 
        	 holder.tv_time.setText(intention.type == Constants.DiscoverProjectIntent? "匹配资金":"匹配项目");
        	 holder.tv_time.setSelected(intention.type == Constants.DiscoverProjectIntent? true:false);
        	 
        }
        holder.tv_time.setTag(position);
		return convertView;
	}
	private class ViewHolder{
		TextView tv_titile,tv_guanzhu,tv_messagecount,tv_time;
		public ViewHolder(View v) {
			tv_titile = (TextView) v.findViewById(R.id.tv_message_title);
			tv_guanzhu = (TextView) v.findViewById(R.id.txt_attention);
			tv_messagecount = (TextView) v.findViewById(R.id.txt_message);
			tv_time = (TextView) v.findViewById(R.id.at_date);
			
		}
	}
	
	
}