package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
 
import com.zrb.mobile.R;
import com.zrb.mobile.Search_resultActivity;
import com.zrb.mobile.adapter.model.Article2Dto;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.NewsDtos;
import com.zrb.mobile.adapter.model.NewsmyDto;
import com.zrb.mobile.adapter.model.TagsDto;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;
 

public class News_mylistItemAdapter extends BaseAdapter
{
 	private LayoutInflater mInflater;
	private List<NewsmyDto> mDatas;
  
 	/**
	 * 使用了github开源的ImageLoad进行了数据加载
	 */
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
 
	private SimpleDateFormat sdf= new SimpleDateFormat("MM.dd");//yyyy.
	private int newType;
   	
	public NewsmyDto getCurItem(int position)
	{
		return mDatas.get(position);
	}
	
	public News_mylistItemAdapter(Context context, ArrayList<NewsmyDto> arrayList)
	{
		this.mDatas = arrayList;
		mInflater = LayoutInflater.from(context);
		//imageLoader = ImageLoader.getInstance();
  		//.showStubImage(R.drawable.images)
		options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.loadpic)
				.showImageForEmptyUri(R.drawable.loadpic).showImageOnFail(R.drawable.loadpic).cacheInMemory(true)
				//.displayer(new FadeInBitmapDisplayer(500))
				.cacheOnDisc(true).build();
 
	 
   }

	public void setImgLoader(ImageLoader curImgLoad) {
 		imageLoader = curImgLoad;
 	}
	
	public void addAll(List<NewsmyDto> mDatas)
	{
		this.mDatas.addAll(mDatas);
	}

	public int getMaxId(){
		
		if(mDatas==null||mDatas.size()<=0) return 1;
		else
			return mDatas.get(0).id;
	}
	
	public long getMinCollectiontime(){
		if(mDatas==null||mDatas.size()<=0) return 1;
		else
			return mDatas.get(mDatas.size()-1).collectionTime;//..News_mylistItemAda;
	}
	
	public void setDatas(List<NewsmyDto> mDatas)
	{
		this.mDatas.clear();
		this.mDatas.addAll(mDatas);
	}
	
	public void insertForward(List<NewsmyDto> mDatas){
		//this.mDatas.
		this.mDatas.addAll(0, mDatas);//.addAll(mDatas);
	}

	@Override
	public int getCount()
	{
		return mDatas.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}
  	 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder = null;
		NewsmyDto tmpDto=mDatas.get(position);
 		if (convertView == null){
			
			//convertView = mInflater.inflate(R.layout.news_list_item_zrb, null);
 			convertView = mInflater.inflate(R.layout.news_mylist_item_zrb, null);
    		holder = new ViewHolder();
  
			holder.mContent = (TextView) convertView.findViewById(R.id.id_content);
			holder.mTitle = (TextView) convertView.findViewById(R.id.id_title);
			holder.mDate = (TextView) convertView.findViewById(R.id.id_date);
			holder.mImg = (ImageView) convertView.findViewById(R.id.id_newsImg);
			holder.msource=(TextView) convertView.findViewById(R.id.news_source);
			holder.favitorNum=(TextView) convertView.findViewById(R.id.news_favitorcount);
	 
 	 
 			convertView.setTag(holder);
			 
  		}
		else
			holder = (ViewHolder) convertView.getTag();
 
		//newType
	 
 	    holder.msource.setText(tmpDto.channel); 
// 	   holder.msource.setText(tmpDto.channel+(TextUtils.isEmpty(tmpDto.author) ? "":" 作者："+tmpDto.author));
    	
    	boolean isbankSummary=TextUtils.isEmpty(tmpDto.summary) ;
    	holder.mTitle.setText(isbankSummary ? tmpDto.title:tmpDto.shortTitle);
    	holder.mTitle.setMaxLines(isbankSummary? 2:1);
    	holder.mTitle.setSingleLine(!isbankSummary);
    	holder.mContent.setText(tmpDto.summary);
    	
    	holder.mDate.setText(CommonUtil.formatlongDate(tmpDto.createTime,sdf));
    	holder.favitorNum.setText(String.format("%1$d人关注",tmpDto.collectionCount ));
    	holder.favitorNum.setTag(position);
     	
        if(!TextUtils.isEmpty(tmpDto.shortImgPath))
        	imageLoader.displayImage(AppSetting.mediaServer+tmpDto.shortImgPath, holder.mImg,options);
   
  		return convertView;
 	}
	
	private boolean isRecent(long date){
 		java.util.Date nowdate=new java.util.Date();
		return CommonUtil.formatlongDate(date,sdf).equals(sdf.format(nowdate));
 	}
 
	private final class ViewHolder
	{
		TextView mTitle;
		TextView mContent;
		ImageView mImg;
		TextView mDate;
		TextView favitorNum;
		TextView msource;
 	}
	
 
 
	 
	
 
}
