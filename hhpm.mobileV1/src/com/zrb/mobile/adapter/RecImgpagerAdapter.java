package com.zrb.mobile.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.hhpm.lib.banner.RecyclingPagerAdapter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
 
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.Common_webview_activity;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.AdsDto;
import com.zrb.mobile.utility.CommonUtil;

public class RecImgpagerAdapter extends RecyclingPagerAdapter {

	private Context context;
	private List<AdsDto> imageIdList;

	private int size;
	private boolean isInfiniteLoop;
	DisplayImageOptions options;


	public RecImgpagerAdapter(Context context, List<AdsDto> imageIdList) {
		this.context = context;
		this.imageIdList = imageIdList;
		this.size = imageIdList == null ? 0 : imageIdList.size();
		isInfiniteLoop = false;
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.loadpic)
		.showImageForEmptyUri(R.drawable.loadpic)
		.showImageOnFail(R.drawable.loadpic).cacheInMemory(false)
		.cacheOnDisc(true).build();
	}

	public void setDatas(List<AdsDto> imageLists){
		imageIdList.clear();
 		imageIdList.addAll(imageLists);
	}
	
	@Override
	public int getCount() {
		// Infinite loop
		return isInfiniteLoop ? Integer.MAX_VALUE : (imageIdList == null ? 0 : imageIdList.size());
	}

	/**
	 * get really position
	 * 
	 * @param position
	 * @return
	 */
	private int getPosition(int position) {
		return isInfiniteLoop ? position % getRealsize() : position;
	}
	
	@Override
	public View getView(int position, View view, ViewGroup container) {
		// TODO Auto-generated method stub
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
              view = holder.imageView = new ImageView(context);
            holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
            
            holder.imageView.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(v.getTag()!=null){
						
						int pos= Integer.parseInt(v.getTag().toString());
						Common_webview_activity.launch(context, imageIdList.get(pos).actionUrl, imageIdList.get(pos).remark);
					}
 				}});
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }
        String tmpurl= imageIdList.get(getPosition(position)).url;
        holder.imageView.setTag(position);
        CommonUtil.InfoLog("getView", AppSetting.mediaServer+ tmpurl);
       // holder.imageView.setImageResource(imageIdList.get(getPosition(position)));AppSetting.mediaServer+
        ImageLoader.getInstance().displayImage(AppSetting.mediaServer+ tmpurl,holder.imageView,options);//,options);
        return view;
	}
	
	private static class ViewHolder {

        ImageView imageView;
    }
	 /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public RecImgpagerAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }

	@Override
	public int getRealsize() {
		// TODO Auto-generated method stub
		return imageIdList == null ? 0 : imageIdList.size();
	}

}
