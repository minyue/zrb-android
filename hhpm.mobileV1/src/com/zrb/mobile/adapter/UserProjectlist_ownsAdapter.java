package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

 
import com.zrb.matching.MatchingActivity;
import com.zrb.mobile.DiscoverDetailOneActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.*;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
 
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.view.View.OnClickListener;

import android.widget.BaseAdapter;

import android.widget.ImageView;

import android.widget.TextView;

public class UserProjectlist_ownsAdapter extends MatchingBaseAdapter {

	private LayoutInflater mInflater;

	DisplayImageOptions options;
	public List<OwnProjectDto> CurrentDatas=new ArrayList<OwnProjectDto>();
	private int curpageNum=0;
	 
	private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");//yyyy.
   	private Context mcontext;
	private class ViewHolder {
		public TextView title;//id_projectstype
		public ImageView projectType;
 		public ImageView image;
 		
		public TextView attentiontxt;
 		public TextView commenttxt;
 		//public TextView surveytxt;
  		public TextView publishDate;
  		public TextView matchtxt;
	}
 
 

	public UserProjectlist_ownsAdapter(Context context) {

		mcontext=context;
		this.mInflater = LayoutInflater.from(context);
 	     options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.loadpic)
		.showImageForEmptyUri(R.drawable.loadpic)
		.showImageOnFail(R.drawable.loadpic).cacheInMemory(false)
	//	.displayer(new FadeInBitmapDisplayer(300))
		.cacheOnDisc(true)
		.build();
  	}
 	
	public void setDatas(List<OwnProjectDto> tmpDtos,boolean isAppend){
		
		if(!isAppend) CurrentDatas.clear();
		CurrentDatas.addAll(tmpDtos);
	}
	
	@Override
	public void setJsonDatas(String tmpDtos, boolean isAppend) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		OwnProjectDtos tmpDto = gson.fromJson(tmpDtos,OwnProjectDtos.class);
		curpageNum=CommonUtil.getSize(tmpDto.data);
		setDatas(tmpDto.data,isAppend);
	}
	
	@Override
	public int getCurpageSize() {
		// TODO Auto-generated method stub
		return curpageNum;
	}
	
 	public long getMinDate() {
		if (CurrentDatas != null&&CurrentDatas.size()>0)
			return CurrentDatas.get(CurrentDatas.size()-1).pushTime;
		return -1;
	} 
  
	@Override
	public int getCount() {
		if (CurrentDatas != null) {
			return CurrentDatas.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
 		// TODO Auto-generated method stub
 		return CurrentDatas.get(position);
 	}

	@Override
	public long getItemId(int position) {
 		// TODO Auto-generated method stub
 		return position;
 	}
     
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;
		OwnProjectDto curDto=CurrentDatas.get(position);
		final ViewHolder holder;
		if (convertView == null) {

			view = mInflater.inflate(R.layout.project_owns_list_item,null);
			holder = new ViewHolder();
		 	holder.title = (TextView) view.findViewById(R.id.id_project_title);
 			holder.image = (ImageView) view.findViewById(R.id.id_projectsImg);
 			holder.projectType = (ImageView) view.findViewById(R.id.id_projectstype);
 			
 			holder.attentiontxt= (TextView) view.findViewById(R.id.project_attention_txt);
 			holder.commenttxt= (TextView) view.findViewById(R.id.project_comment_txt);
 			//holder.surveytxt= (TextView) view.findViewById(R.id.project_survey_txt);
 			holder.publishDate=(TextView)view.findViewById(R.id.project_date_txt); 
 			holder.matchtxt=(TextView)view.findViewById(R.id.project_match_txt); 
 			holder.matchtxt.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
 					int index = Integer.parseInt(v.getTag().toString());
 					OwnProjectDto intentionDto = CurrentDatas.get(index);
					// CommonUtil.showToast("test", context);
					Intent	intent = new Intent(mcontext, MatchingActivity.class);
					
					intent.putExtra("id",intentionDto.id);
						intent.putExtra("actiontype",Constants.MatchingFund);
						intent.putExtra("curId",(int)intentionDto.id);
 						mcontext.startActivity(intent);
 				}});
 			
			view.setTag(holder);

		} else {
 			holder = (ViewHolder) view.getTag();
 		}
  		
	    holder.title.setText(curDto.title);
 	 	//setprojectType(curDto.projectType,holder.projectType);
		holder.attentiontxt.setText(""+curDto.collectNum);
     	holder.commenttxt.setText(""+curDto.queryNum);
     	 
 		holder.publishDate.setText(CommonUtil.formatlongDate(curDto.pushTime,sdf)); 
  	 
	 	if(!TextUtils.isEmpty(curDto.pic))
	 		ImageLoader.getInstance().displayImage(AppSetting.mediaServer+curDto.pic, holder.image, options);  
	 	else
	 		holder.image.setImageResource(R.drawable.loadpic); 
	 	
	 	 holder.matchtxt.setTag(position);
	  	 holder.matchtxt.setText("匹配资金");
    	 holder.matchtxt.setSelected(true);
    	return view;

	}
 
}
