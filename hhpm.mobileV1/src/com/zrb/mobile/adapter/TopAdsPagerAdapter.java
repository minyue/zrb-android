package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.zrb.mobile.Project_info_lience;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.AdsDto;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.applib.utils.AppSetting;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;




public class TopAdsPagerAdapter extends PagerAdapter {

	private ArrayList<View> mPageViewList  ;
	private List<AdsDto> mImageList= new ArrayList<AdsDto>();
	private Context mContext;
	private LayoutInflater mInflater;
	private boolean       isInfiniteLoop;
	
	private ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	
	public TopAdsPagerAdapter(Context context) {
		mContext = context;
		
	
		if (mInflater == null) {
			mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

	   options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.guess_like_img).showImageOnFail(R.drawable.guess_like_img).cacheInMemory(false)
				.cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new FadeInBitmapDisplayer(500))
				.build();
	   
	   
	    mPageViewList=new ArrayList<View>();
 	/*	for (int i = 0; i <6; i++) {
			View view = mInflater.inflate(R.layout.banner_header_item, null);
			mPageViewList.add(view);
		} */
  	}
 
	public void SetDataList(List<AdsDto> ImageList){
		
		mImageList.clear();
		mImageList.addAll(ImageList);
	
		mPageViewList.clear();
		for (int i = 0; i <ImageList.size(); i++) {
			View view = mInflater.inflate(R.layout.banner_header_item, null);
			mPageViewList.add(view);
		}
	}
	
	
	@Override
	public int getCount() {
		//return mImageList != null ? mImageList.size() : 0;
		 return isInfiniteLoop ? Integer.MAX_VALUE : CommonUtil.getSize(mImageList);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public int getItemPosition(Object object) {
		return super.getItemPosition(object);
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView(mPageViewList.get(arg1));
	}

	@Override
	public Object instantiateItem(View arg0, final int arg1) {
	 
		((ViewPager) arg0).addView(mPageViewList.get(arg1));
		final ImageView ImageV = (ImageView) mPageViewList.get(arg1).findViewById(R.id.image);
	
	//	imageLoader.displayImage(AppSetting.mediaServer+mImageList.get(arg1).pic, ImageV, options);
	/*	String imgurl=mImageList.get(arg1).pic;
		if(!TextUtils.isEmpty(imgurl))  
	    imageLoader.displayImage(AppSetting.mediaServer+imgurl, ImageV, options);
		 
		ImageV.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Toast.makeText(mContext, "���ͼƬ"+arg1, Toast.LENGTH_LONG).show();
				AdsDto curDto = mImageList.get(arg1);
	 			Intent intent = new Intent(mContext, Project_info_lience.class);
 				intent.putExtra("projectId", Integer.parseInt(curDto.infoid));
 				intent.putExtra("title", curDto.title);
 				
				mContext.startActivity(intent); 
			}
		});*/
		return mPageViewList.get(arg1);
	}

	 /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public TopAdsPagerAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }
	
	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
		//
	}

	@Override
	public Parcelable saveState() {
		//
		return null;
	}

	@Override
	public void startUpdate(View arg0) {
		//
	}

	@Override
	public void finishUpdate(View arg0) {
		//
	}

}
