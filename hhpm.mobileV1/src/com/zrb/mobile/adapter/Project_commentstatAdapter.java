package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.CommentStatisticDto;
import com.zrb.mobile.adapter.model.CommentstatDto;
import com.zrb.mobile.adapter.model.ProjectDto;
import com.zrb.mobile.utility.CommonUtil;

public  class Project_commentstatAdapter extends BaseAdapter {

	 class ViewHolder {
		public TextView title;
		public TextView titledes;
		public ProgressBar mprogressbar;
		//public CheckBox checked;
	}

	public List<CommentStatisticDto> CurrentDatas = new ArrayList<CommentStatisticDto>();
 	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
 	
	public Project_commentstatAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return CurrentDatas.size();// 返回数组的长度
 	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView( int position, View convertView, ViewGroup parent) {
 	
		ViewHolder holder;
		CommentStatisticDto curDto=CurrentDatas.get(position);
 		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.project_commentstat_list_item, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.pro_comment_title);
			holder.titledes= (TextView) convertView.findViewById(R.id.pro_comment_percent);
			holder.mprogressbar=(ProgressBar)convertView.findViewById(R.id.pro_comment_progressbar);
	       // holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
	 	}
 		
 /*		 if(holder==null){
 			 
 			 CommonUtil.InfoLog("holder", "null");
 		 }
 		 else{

 		   if(holder.title==null){
 			 
 			 CommonUtil.InfoLog("title", "null");
 		   }
 		 }*/
	 	holder.title.setText(curDto.name);
		holder.titledes.setText(curDto.percent);
		double process=0;
		try{
		   process=Double.parseDouble(curDto.percent.replace("%", ""));
		}
		catch(Exception e){
			process=0;
 		}
 		
		holder.mprogressbar.setProgress((int)process); 
 		return convertView;
	 
	}/* 存放控件 */

}