package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.IndustryDto;
 

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

  

	public class Cityindustry_listAdapter extends BaseExpandableListAdapter {

		class ExpandableListHolder {
			View titleColor;
			TextView titlename;
			TextView titleValue;
		}

		private Context context;
	    int[] colors={Color.parseColor("#59e387"),Color.parseColor("#fabb7d"),Color.parseColor("#2e96ff")};
		private LayoutInflater mGroupInflater;
		private List<IndustryDto> group;

		public Cityindustry_listAdapter(Context c, List<IndustryDto> group) {
			this.context = c;
 			mGroupInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.group = group;
		}

		public Object getChild(int childPosition, int itemPosition) {
			 return group.get(childPosition).getChild(itemPosition);
			 
		}

		public long getChildId(int childPosition, int itemPosition) {

			return itemPosition;
		}

		@Override
		public int getChildrenCount(int index) {
			return group.get(index).getChildSize();
		}

		public Object getGroup(int index) {
			return group.get(index);
		}

		public int getGroupCount() {
			return group.size();
		}

		public long getGroupId(int index) {
			return index;
		}

		public View getGroupView(int position, boolean flag, View view,
				ViewGroup parent) {
			ExpandableListHolder holder = null;
			if (view == null) {
				view = mGroupInflater.inflate( R.layout.cityindustry_list_groupitem, null);
				holder = new ExpandableListHolder();
				holder.titleColor =   view.findViewById(R.id.group_titlecolor);
				
				holder.titlename = (TextView) view.findViewById(R.id.group_titlename);
				holder.titleValue = (TextView)view.findViewById(R.id.group_titleValue);
				view.setTag(holder);
			} else {
				holder = (ExpandableListHolder) view.getTag();
			}
			IndustryDto info = this.group.get(position);
			if (info != null) {
				holder.titlename.setText(info.name);
 				holder.titleValue.setText(info.value);
			}
			if(position<3) holder.titleColor.setBackgroundColor(colors[position]);
				
		 
			return view;
		}

		public boolean hasStableIds() {
			return false;
		}

		public boolean isChildSelectable(int arg0, int arg1) {
			return false;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {
			ExpandableListHolder holder = null;
			if (convertView == null) {
				convertView = mGroupInflater.inflate( R.layout.cityindustry_list_item, null);
				holder = new ExpandableListHolder();
 		     	holder.titlename = (TextView) convertView.findViewById(R.id.child_titlename);
				convertView.setTag(holder);
			} else {
				holder = (ExpandableListHolder) convertView.getTag();
			}
			String info = this.group.get(groupPosition).getChild(childPosition);
			if (info != null) {
				holder.titlename.setText(info);
 			}
			return convertView;
		}

	}
