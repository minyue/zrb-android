package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.TagsDto;
import com.zrb.mobile.utility.CommonUtil;
 

public class News_searchResultAdapter extends BaseAdapter
{

	private LayoutInflater mInflater;
	private List<NewsDto> mDatas=new ArrayList<NewsDto>();
	private int hotColor=Color.rgb(221, 85, 68);
	private int newColor=Color.rgb(29, 167, 201);
	private int greyColor=Color.rgb(95, 95, 95);
	
	private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");
	
	private String tagwords;
	private int tagType; //1 tag,2 keyword
	public void SetTag(int tagtype,String curtag){
		tagwords=curtag;
		tagType=tagtype;
	}
	
	public News_searchResultAdapter(Context context)
	{
		mInflater = LayoutInflater.from(context);
    }
 	
	public void addAll(List<NewsDto> mDatas)
	{
		if(mDatas==null) return;
		this.mDatas.addAll(mDatas);
	}
  	
	public void setDatas(List<NewsDto> mDatas)
	{
		this.mDatas.clear();
		this.mDatas.addAll(mDatas);
	}

	@Override
	public int getCount()
	{
		return mDatas.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}
 	
	private void setTitleHightlight(final String tmpStrTitle,TextView mTitle){
		
 	 	int idx2=tmpStrTitle.indexOf(tagwords.toUpperCase());
 		if(idx2>-1){
 			SpannableStringBuilder styled2 = new SpannableStringBuilder(tmpStrTitle);
			styled2.setSpan(new ForegroundColorSpan(hotColor), idx2,idx2+ tagwords.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// i 未起始字符索引，j 为结束字符索引
	    	mTitle.setText(styled2); 
		}
		else
			mTitle.setText(tmpStrTitle);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder = null;
		NewsDto tmpDto=mDatas.get(position);
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.news_item_searchresult, null);
 		    holder = new ViewHolder();
 	 	  
			holder.mTitle = (TextView) convertView.findViewById(R.id.id_title);
			holder.mDate = (TextView) convertView.findViewById(R.id.id_date);
  		    holder.msource=(TextView) convertView.findViewById(R.id.news_source);
			holder.favitorNum=(TextView) convertView.findViewById(R.id.news_favitorcount);
			holder.tagContainer=(LinearLayout)convertView.findViewById(R.id.tag_container);
			
			convertView.setTag(holder);
		} 
		else{
			holder = (ViewHolder) convertView.getTag();
		}
		 
	    settag(holder.tagContainer,tmpDto.tagsInfoList);
		boolean ishot=TextUtils.isEmpty(tmpDto.hotPointId) ? true:false;
		if(ishot){
			String tmphot="热点";
	 	 	String msource=tmpDto.channel+"｜"+tmphot;
	 		SpannableStringBuilder styled = new SpannableStringBuilder(msource);
	         int idx=msource.indexOf("｜");
	    	 
	 		styled.setSpan(new ForegroundColorSpan(hotColor), idx+1,idx+ tmphot.length()+1,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// i 未起始字符索引，j 为结束字符索引
	    	holder.msource.setText(styled); 
 		}
		else
			holder.msource.setText(tmpDto.channel);
  		
		if(tagType==2){
 			setTitleHightlight(tmpDto.title,holder.mTitle);
		}
		else
    	    holder.mTitle.setText(tmpDto.title);
    	 
    	holder.mDate.setText(CommonUtil.formatlongDate(tmpDto.createTime,sdf));
    	holder.favitorNum.setText(String.format("+%1$d人收藏",tmpDto.collectionCount ));
  		return convertView;
 	}

	
	//	private int SearchType;//1 tag,2 keyword
	private void settag(LinearLayout tagLayout,List<TagsDto> tags)
	{
  		int tmplen=tags==null ? 0:tags.size();
		for(int i=0;i<3;i++){
			
			TextView tmpview=(TextView)tagLayout.getChildAt(i);
			if(i<tmplen) { 
				tmpview.setText(tags.get(i).labelName);
				tmpview.setVisibility(View.VISIBLE);
				if(String.valueOf(tags.get(i).labelName).equals(tagwords)) {
					tmpview.setTextColor(hotColor);
				}else if(String.valueOf(tags.get(i).id).equals(tagwords)){
					tmpview.setTextColor(hotColor);//#5f5f5f
				}else
					tmpview.setTextColor(greyColor);//#5f5f5f
			}
			else{
				tmpview.setVisibility(View.GONE);
			}
		}
 	}
	
	private final class ViewHolder
	{
		TextView mTitle;
 		TextView mDate;
		TextView favitorNum;
		TextView msource;
 
		LinearLayout tagContainer;
	}

}
