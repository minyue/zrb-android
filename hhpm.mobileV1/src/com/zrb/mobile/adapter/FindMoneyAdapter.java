package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.R;
 
 

import com.zrb.mobile.adapter.model.FindMoneyDto;
import com.zrb.mobile.adapter.model.FindMoneyDtos;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;


public class FindMoneyAdapter extends MatchingBaseAdapter {

	private LayoutInflater mInflater;

	DisplayImageOptions options;
	public List<FindMoneyDto> CurrentDatas = new ArrayList<FindMoneyDto>();

	private ImageLoader imageLoader;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");// yyyy.

	 
	Drawable defaultpic,localAuthpic,infoAuthpic;

	private class ViewHolder {
		public TextView title; 
 		public TextView pppType;

		public ImageView image;
  		public TextView publishDate;
 		public TextView project_address;
		public TextView project_auth_status;
		public TextView project_price;
		public ImageView mlefticon;
		//public GradientDrawable iconBg;
	}

	public void setImgLoader(ImageLoader curImgLoad) {
		imageLoader = curImgLoad;

	}

	public FindMoneyAdapter(Context context) {

		this.mInflater = LayoutInflater.from(context);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.loadpic)
				.showImageForEmptyUri(R.drawable.loadpic)
				.showImageOnFail(R.drawable.loadpic).cacheInMemory(false)
				.cacheOnDisc(true).build();
  
		defaultpic = context.getResources().getDrawable(R.drawable.loadpic3);
		localAuthpic= context.getResources().getDrawable(R.drawable.sdrzx);
		infoAuthpic= context.getResources().getDrawable(R.drawable.zlrz);

	}

	public void setDatas(List<FindMoneyDto> tmpDtos,boolean isAppend) {
		if(!isAppend) CurrentDatas.clear();
		
		CurrentDatas.addAll(tmpDtos);
	}
	
	@Override
	public void setJsonDatas(String tmpDtos, boolean isAppend) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		FindMoneyDtos   tmpFundDto= gson.fromJson(tmpDtos, FindMoneyDtos.class);
		setDatas(tmpFundDto.data,isAppend);
	}

	@Override
	public int getCurpageSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public long getMinId() {
		if (CurrentDatas != null && CurrentDatas.size() > 0)
			return CurrentDatas.get(CurrentDatas.size() - 1).id;
		return -1;
	}

	public long getMinTime() {
		if (CurrentDatas != null && CurrentDatas.size() > 0)
			return CurrentDatas.get(CurrentDatas.size() - 1).pushTime;
		return -1;
	}

	@Override
	public int getCount() {
		if (CurrentDatas != null) {
			return CurrentDatas.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
 		// TODO Auto-generated method stub
 		return CurrentDatas.get(position);
 	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;

	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;
		FindMoneyDto curDto = CurrentDatas.get(position);
		ViewHolder holder;
		if (convertView == null) {

			view = mInflater.inflate(R.layout.funds_list_item_zrb, null);
			holder = new ViewHolder();
			holder.title = (TextView) view.findViewById(R.id.id_project_title);
			holder.image = (ImageView) view.findViewById(R.id.id_projectsImg);
			//holder.projectType = (ImageView) view.findViewById(R.id.id_projectstype);
			//holder.txt_project_pppType = (TextView) view.findViewById(R.id.txt_project_pppType);
			//holder.project_date = (TextView) view.findViewById(R.id.project_date);
			holder.project_address = (TextView) view.findViewById(R.id.project_address);
			holder.project_auth_status = (TextView) view.findViewById(R.id.project_auth_status);
			holder.project_price = (TextView) view.findViewById(R.id.project_price);
			holder.mlefticon = (ImageView) view.findViewById(R.id.left_icon);
 
			holder.pppType = (TextView) view.findViewById(R.id.txt_project_pppType);
			//holder.price = (TextView) view.findViewById(R.id.project_price);
			holder.publishDate = (TextView) view.findViewById(R.id.project_date);
			
/*			GradientDrawable drawable = new GradientDrawable();
			drawable.setShape(GradientDrawable.RECTANGLE);
			drawable.setCornerRadii(new float[] { 5, 5, 5, 5, 5, 5, 5, 5 });
			holder.iconBg=drawable;*/
			
			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.title.setText(curDto.title);
		
        if(holder.image.getTag()!=null&&holder.image.getTag().toString().equals(String.valueOf(position))){
 			
		}
		else {
			if (!TextUtils.isEmpty(curDto.pic)) {
				ImageLoader.getInstance().displayImage( AppSetting.mediaServer + curDto.pic, holder.image, options);
			} else
				holder.image.setBackgroundDrawable(defaultpic);//.setBackgroundDrawable(defaultpic);
 		}
 		
  		holder.image.setTag(position);
	 
		holder.pppType.setText(curDto.investMode);
 		holder.publishDate.setText(CommonUtil.formatlongDate(curDto.pushTime,sdf));
 		holder.project_address.setText(TextUtils.isEmpty(curDto.areas) ?"":CommonUtil.cutString(curDto.areas.replace("省", "").replace("市", ""),2,true));
 		holder.project_auth_status.setText("投资行业: "+curDto.industryName);
		
	/*	if (Constants.PROJECT_SCENE_AUTH == curDto.auth) {
 			holder.mlefticon.setImageDrawable(localAuthpic);
			holder.mlefticon.setVisibility(View.VISIBLE);
 		} 
		else if (Constants.PROJECT_MATERIALS_AUTH == curDto.auth) {
 			holder.mlefticon.setImageDrawable(infoAuthpic);
			holder.mlefticon.setVisibility(View.VISIBLE);
 		}else {
			holder.mlefticon.setVisibility(View.GONE);
		}*/

		holder.project_price.setText((int)curDto.total + "万");//"估值: "
		return view;
	}
 

	private String getfisrtChar(String name) {
		if (TextUtils.isEmpty(name))
			return "";
		else
			return name.substring(0, 1);
	}

	public void updateSingleRow(ListView listView, long id) {

		 if (listView != null) {
			int start = listView.getFirstVisiblePosition();
			for (int i = start, j = listView.getLastVisiblePosition(); i <= j; i++)
				if (id == ((FindMoneyDto) listView.getItemAtPosition(i)).id) {
					View view = listView.getChildAt(i - start);
					getView(i, view, listView);
					break;
				}
		} 
		
	 
	}





 

}
