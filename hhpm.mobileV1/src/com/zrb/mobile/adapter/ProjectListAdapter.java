package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.ProjectDto;

public  class ProjectListAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public TextView text;
		public Button bt;
	}

	public List<ProjectDto> CurrentDatas = new ArrayList<ProjectDto>();

	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/

	public ProjectListAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {

		return CurrentDatas.size();// 返回数组的长度

	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(
					R.layout.app_list_item_recentproject, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.name);
			holder.text = (TextView) convertView.findViewById(R.id.destype);
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
			/* 设置TextView显示的内容，即我们存放在动态数组中的数据 */
			// holder.title.setText(getDate().get(position).get("ItemTitle").toString());
			// holder.text.setText(getDate().get(position).get("ItemText").toString());

			/* 为Button添加点击事件 */
			/*
			 * holder.bt.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override publicvoid onClick(View v) { Log.v("MyListViewBase",
			 * "你点击了按钮" + position); //打印Button的点击信息 } });
			 */

		}
		holder.title.setText(CurrentDatas.get(position).title);
		holder.text.setText("用途："+CurrentDatas.get(position).desc1+" | 出让时间："+CurrentDatas.get(position).desc2);

		return convertView;
	}/* 存放控件 */

}