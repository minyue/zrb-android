package com.zrb.mobile.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.ProjectSurveyDto;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
 

public class Project_mysurvey_listAdapter extends BaseAdapter
{
 	private LayoutInflater mInflater;
	private List<ProjectSurveyDto> mDatas;
  
 	/**
	 * 使用了github开源的ImageLoad进行了数据加载
	 */
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
 
	private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");//yyyy.
	private int newType;
	/** 弹出的更多选择框  */
	private PopupWindow popupWindow;
 	
	OnDelListener mOnDelListener;
	public void setOnDelListener(OnDelListener curDeleteListener){
		mOnDelListener=curDeleteListener;
	}
 	
	public Project_mysurvey_listAdapter(Context context, ArrayList<ProjectSurveyDto> arrayList)
	{
		this.mDatas = arrayList;
		mInflater = LayoutInflater.from(context);
		options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.loadpic)
				.showImageForEmptyUri(R.drawable.loadpic).showImageOnFail(R.drawable.loadpic).cacheInMemory(true)
 				.cacheOnDisc(true).build();
		initPopWindow();
    }
	
	public ProjectSurveyDto getCurItem(int position)
	{
		return mDatas.get(position);
	}

	public void removeAtIndex(int position)
	{
		  mDatas.remove(position);
	}
	
	public void setImgLoader(ImageLoader curImgLoad) {
 		imageLoader = curImgLoad;
 	}
	
	public void addAll(List<ProjectSurveyDto> mDatas) {
		this.mDatas.addAll(mDatas);
	}

	public int getMaxId(){
 	 	if(mDatas==null||mDatas.size()<=0) return 1;
		else
			return mDatas.get(0).autoId;
	}
	
	public int getMinId(){
		if(mDatas==null||mDatas.size()<=0) return 1;
		else
			return mDatas.get(mDatas.size()-1).autoId;//..News_mylistItemAda;
	}
	
	public void setDatas(List<ProjectSurveyDto> mDatas)
	{
		this.mDatas.clear();
		this.mDatas.addAll(mDatas);
	}
	
	public void insertForward(List<ProjectSurveyDto> mDatas){
		//this.mDatas.
		this.mDatas.addAll(0, mDatas);//.addAll(mDatas);
	}

	@Override
	public int getCount() {
		return mDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
  	
	private void setProcess(){
		
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder = null;
		ProjectSurveyDto tmpDto=mDatas.get(position);
 		if (convertView == null){
			
			//convertView = mInflater.inflate(R.layout.news_list_item_zrb, null);
 			convertView = mInflater.inflate(R.layout.project_mysurveylist_item, null);
    		holder = new ViewHolder();
   	 
			holder.mTitle = (TextView) convertView.findViewById(R.id.id_title);
			holder.mDate = (TextView) convertView.findViewById(R.id.tag_surveydate);
			holder.mImg = (ImageView) convertView.findViewById(R.id.id_newsImg);
  	 
			holder.process_sep1 = (TextView) convertView.findViewById(R.id.process_sep1);
			holder.process_sep2 = (TextView) convertView.findViewById(R.id.process_sep2);
			holder.process_sep3 = (TextView) convertView.findViewById(R.id.process_sep3);
			
			holder.processLayout=(LinearLayout) convertView.findViewById(R.id.survey_processlayout);
 			holder.mprocessdel=(TextView) convertView.findViewById(R.id.survey_processdel);
		 
  			convertView.setTag(holder);
  			holder.mprocessdel.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
  				        int pos=Integer.parseInt(v.getTag().toString());
  				    
 			            int[] arrayOfInt = new int[2];
						//获取点击按钮的坐标
						v.getLocationOnScreen(arrayOfInt);
				        int x = arrayOfInt[0];
				        int y = arrayOfInt[1];
				        showPop(v, x , y, pos);
 					 }
 				});
 		}
		else
			holder = (ViewHolder) convertView.getTag();
 
		//newType   /**勘察状态，0：取消，1：申请；2、确认；3、完成**/
 		holder.mDate.setText(CommonUtil.formatlongDate(tmpDto.inspectTime,sdf));
 		
 		 holder.mprocessdel.setTag(position);
 		if(tmpDto.inspectStatus==Constants.InspectCancel){
 			holder.processLayout.setVisibility(View.GONE);//0
 		    holder.mprocessdel.setVisibility(View.VISIBLE);
 		}
 		else {
 			holder.processLayout.setVisibility(View.VISIBLE);
 		    holder.mprocessdel.setVisibility(View.GONE);
 		    
 		   if( tmpDto.inspectStatus == Constants.InspectApply){
 	        	holder.process_sep2.setEnabled(false);
 	        	holder.process_sep3.setEnabled(false);
 	        }else if(tmpDto.inspectStatus == Constants.InspectConfirm){
 	        	holder.process_sep2.setEnabled(true);
 	        	holder.process_sep3.setEnabled(false);
 	        }else if(tmpDto.inspectStatus == Constants.InspectFinish){
 	        	holder.process_sep2.setEnabled(true);
 	        	holder.process_sep3.setEnabled(true);
   	        }
 	        
  		}
 		holder.mTitle.setText(tmpDto.projectName);
 
        if(!TextUtils.isEmpty(tmpDto.projectPicUrl))
        	imageLoader.displayImage(AppSetting.mediaServer+tmpDto.projectPicUrl, holder.mImg,options);
        else
        	holder.mImg.setImageResource(R.drawable.loadpic);
        
        
      
        /*InspectApply=1;
    	public   static final int InspectConfirm=2;
    	public   static final int InspectFinish=3;        */
        
  		return convertView;
 	}
	

	private RelativeLayout btn_pop_close;
 	/**
	 * 初始化弹出的pop
	 * */
	private void initPopWindow() {
		View popView = mInflater.inflate(R.layout.inspectlist_pop_item, null);
		popupWindow = new PopupWindow(popView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setBackgroundDrawable(new ColorDrawable(0));
		//设置popwindow出现和消失动画
		popupWindow.setAnimationStyle(R.style.PopRightMenuAnimation);
		btn_pop_close = (RelativeLayout) popView.findViewById(R.id.root_view);
	}
	/** 
	 * 显示popWindow
	 * */
	public void showPop(View parent, int x, int y,final int position) {
		//设置popwindow显示位置
		
		x+=CommonUtil.dip2px(parent.getContext(), 1);
		y+=CommonUtil.dip2px(parent.getContext(), 2);
 	    popupWindow.showAtLocation(parent, 0, x, y);
		//获取popwindow焦点
		popupWindow.setFocusable(true);
		//设置popwindow如果点击外面区域，便关闭。
		popupWindow.setOutsideTouchable(true);
		popupWindow.update();
		if (popupWindow.isShowing()) {
			
		}
		btn_pop_close.setOnClickListener(new OnClickListener() {
			public void onClick(View paramView) {
				popupWindow.dismiss();
				if(mOnDelListener!=null)
					mOnDelListener.onDeleteEvent(position);
			}
		});
	}
 
	public interface OnDelListener{
 		void onDeleteEvent(int position);
	}

	
	private final class ViewHolder {
		TextView mTitle;
 		ImageView mImg;
		TextView mDate;
		TextView process_sep1,process_sep2,process_sep3;
		LinearLayout processLayout;
 		TextView mprocessdel;
	 
 	}
	
 
 
	 
	
 
}
