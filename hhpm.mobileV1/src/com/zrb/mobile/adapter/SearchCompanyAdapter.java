package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.CompanyDto;
import com.zrb.mobile.common.QueryhistoryItem;
import com.zrb.mobile.utility.CommonUtil;

 
public  class SearchCompanyAdapter extends BaseAdapter {

	 
	final class ViewHolder {
		public TextView title;
		public TextView tag;
		public ImageView lefticon;
		public ImageView righticon;
	}
	
	private int _selIndex = 0;
    private Context mcontext;
 
    
	public List<CompanyDto> CurrentDatas = new ArrayList<CompanyDto>();
 	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
 	
 
	public SearchCompanyAdapter(Context context) {
		mcontext=context;
		this.mInflater = LayoutInflater.from(context);
	}

	public void setAll(List<CompanyDto> items){
		CurrentDatas.clear();
		CurrentDatas.addAll(items);
 	}
	
	public int getCount() {

		return CurrentDatas.size();// 返回数组的长度

	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.search_history_list_item2, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.remind_text);
			//holder.tag = (TextView) convertView.findViewById(R.id.tag_txt);
			holder.lefticon = (ImageView) convertView.findViewById(R.id.left_icon);
			holder.righticon=(ImageView) convertView.findViewById(R.id.add_search);
 			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag(); 
 		}
  
		 
		holder.title.setText(CurrentDatas.get(position).name);
        holder.lefticon.setImageResource(R.drawable.search_history);
		return convertView;
	}/* 存放控件 */
	
	
 


}