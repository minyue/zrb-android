package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.common.QueryhistoryItem;
import com.zrb.mobile.utility.CommonUtil;

 
public  class SearchResultAdapter extends BaseAdapter {

	
	public interface onDeleteListener{
 		public void onDelclick(int postion);
	}
	final class ViewHolder {
		public TextView title;
		public TextView tag;
		public ImageView lefticon;
		public ImageView righticon;
	}
	
	private int _selIndex = 0;
    private Context mcontext;
    private onDeleteListener mdelListener;
    
	public List<QueryhistoryItem> CurrentDatas = new ArrayList<QueryhistoryItem>();
 	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
 	
 	public void setDelListener(onDeleteListener delListener){
 		mdelListener=delListener;
 	}
 	
	public SearchResultAdapter(Context context) {
		mcontext=context;
		this.mInflater = LayoutInflater.from(context);
	}

	public void setAll(List<QueryhistoryItem> items){
		CurrentDatas.clear();
		CurrentDatas.addAll(items);
 	}
	
	public int getCount() {

		return CurrentDatas.size();// 返回数组的长度

	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.search_history_list_item3, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.remind_text);
			holder.tag = (TextView) convertView.findViewById(R.id.tag_txt);
			holder.lefticon = (ImageView) convertView.findViewById(R.id.left_icon);
			holder.righticon=(ImageView) convertView.findViewById(R.id.add_search);
			holder.righticon.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//CommonUtil.showToast(v.getTag().toString(), v.getContext());
					if(mdelListener!=null) mdelListener.onDelclick(Integer.parseInt(v.getTag().toString()));
					
				}
				
				
			});
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
			/* 设置TextView显示的内容，即我们存放在动态数组中的数据 */
			// holder.title.setText(getDate().get(position).get("ItemTitle").toString());
			// holder.text.setText(getDate().get(position).get("ItemText").toString());

			/* 为Button添加点击事件 */
			/*
			 * holder.bt.setOnClickListener(new OnClickListener() {
			 * 
			 * @Override publicvoid onClick(View v) { Log.v("MyListViewBase",
			 * "你点击了按钮" + position); //打印Button的点击信息 } });
			 */

		}
  
		holder.righticon.setTag(position);
 		if(!CurrentDatas.get(position).keywords.endsWith(",")){
		    holder.title.setText(CurrentDatas.get(position).keywords);
		    holder.title.setTextColor(mcontext.getResources().getColor(R.color.nomal_tag_black)); //
		    holder.title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 17) ;
 		    holder.tag.setVisibility(View.GONE);
 		}
		else
		{
			 holder.tag.setVisibility(View.VISIBLE);
			 holder.tag.setText(CurrentDatas.get(position).keywords.replace(",", ""));
			 holder.title.setText("热门标签");
 			 holder.title.setTextColor(mcontext.getResources().getColor(R.color.nomalGray));
 			holder.title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9) ;
 			 
		}
 		
        holder.lefticon.setImageResource(R.drawable.search_history);
		return convertView;
	}/* 存放控件 */
	
	
 


}