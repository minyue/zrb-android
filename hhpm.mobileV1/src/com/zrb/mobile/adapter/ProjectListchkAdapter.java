package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.lang.*;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.UserInfoDto;

public class ProjectListchkAdapter extends BaseAdapter {
	public enum ChooseType {
		single, multi
	}

	private class ButtonViewHolder {
		public TextView peoplename;
		public TextView peopleaddress;
		public TextView alphaTitle;
		public RelativeLayout Selected;
	}
 
	// public Dictionary<string, int> alphaIndexer;
	// //保存每个索引在list中的位置【#-0，A-4，B-10】
	private String[] sections;// 每个分组的索引表【A,B,C,F...】
	private ButtonViewHolder _holder;

	private Context _context;
	private Integer _singleIndex = -1;
	private List<Integer> _checkedIDs = new ArrayList<java.lang.Integer>();
	private LayoutInflater inflater;
	private List<UserInfoDto> _dataListperson;

	public List<Integer> getCheckedIDs() {
		if (CurchooseType == ChooseType.single && _singleIndex != -1)
			_checkedIDs.add(_singleIndex);
		return _checkedIDs;
	}

	public ChooseType CurchooseType;

	public ProjectListchkAdapter(Context context, List<UserInfoDto> datalist) {
		_context = context;
		_dataListperson = datalist;
		this.inflater = LayoutInflater.from(context);
		// this.alphaIndexer = new Dictionary<string, int>();
		//
		// for (int i = 0; i < _dataListperson.Count; i++)
		// {
		// if (!alphaIndexer.ContainsKey(_dataListperson[i].fistname))
		// //只记录在list中首次出现的位置
		// {
		// alphaIndexer.Add(_dataListperson[i].fistname, i);
		// }
		// }
		// var tmpkeys = alphaIndexer.Keys.ToList();
		// tmpkeys.Sort();
		// sections = tmpkeys.ToArray();

	}

	// public System.Action<string, string> selectHander;

	public List<UserInfoDto> getContactpeoples() {
		return _dataListperson;
	}

	@Override
	public int getCount() {
		{
			return _dataListperson == null ? 0 : _dataListperson.size();
		}
	}

	@Override
	public Object getItem(int position) {

		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
	 
			View view;
			if (convertView == null) {
				// 获得布局

				convertView = inflater.inflate(R.layout.u_center_project_item,null);// compose_contact_item
								// .XZPeopleCheckbox_list_item

				view = convertView;
				_holder = new ButtonViewHolder();
				_holder.peoplename = (TextView) view.findViewById(R.id.compose_contact_item_name_tv);
				_holder.alphaTitle = (TextView) view.findViewById(R.id.compose_contact_item_category);
				_holder.peopleaddress = (TextView) view.findViewById(R.id.compose_contact_item_addr_iv);
				_holder.Selected = (RelativeLayout) view.findViewById(R.id.compose_contact_item_ll);
				view.setTag(_holder);
			} else {
				view = convertView;
				_holder = (ButtonViewHolder) view.getTag();// .Tag;
			}

			if (CurchooseType == ChooseType.single) {
				if (_singleIndex == position)
					_holder.Selected.setSelected(true);// .Selected = true;
				else
					_holder.Selected.setSelected(false);// = false;
			} else if (CurchooseType == ChooseType.multi) {
				if (_checkedIDs.contains(position))// .Contains(position))
					_holder.Selected.setSelected(true);
				else
					_holder.Selected.setSelected(false);
			}
			_holder.peoplename.setText(_dataListperson.get(position).name);// .Text
																			// =
																			// _dataListperson[position].person.UserName;
			_holder.peopleaddress.setText(_dataListperson.get(position).phone);// =
																				// _dataListperson[position].person.Mobile;

			/*
			 * 
			 * string currentStr = _dataListperson[position].fistname;//当前字母
			 * string previewStr = (position - 1) >= 0 ?
			 * _dataListperson[position - 1].fistname : " "; if
			 * (!previewStr.Equals(currentStr)) //
			 * 当前联系人的sortKey！=上一个联系人的sortKey，说明当前联系人是新组。 {
			 * _holder.alphaTitle.Visibility =
			 * ViewStates.Visible;//(View.VISIBLE); _holder.alphaTitle.Text =
			 * currentStr; } else { _holder.alphaTitle.Visibility =
			 * (ViewStates.Gone); }
			 */
			return view;
		 
	}

	public void setSelectItem(int index, View selectItem) {
		if (CurchooseType == ChooseType.single) {
			_singleIndex = index;
			this.notifyDataSetChanged();
		} else {
			if (_checkedIDs.contains(index)) {
				selectItem.setSelected(false);// .Selected = false;
				_checkedIDs.remove(index);
			} else {
				selectItem.setSelected(true);// .Selected = true;
				_checkedIDs.add(index);
			}
		}
	}

	/*
	 * public int GetIndexByChar(String currentChar) { if
	 * (alphaIndexer.ContainsKey(currentChar)) { return
	 * alphaIndexer[currentChar]; } return 0; }
	 */

}
