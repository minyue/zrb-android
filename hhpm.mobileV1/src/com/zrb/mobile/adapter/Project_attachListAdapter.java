package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.ProjectDto;

public  class Project_attachListAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		//public CheckBox checked;
	}

	public List<ArticleDto> CurrentDatas = new ArrayList<ArticleDto>();
 	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
 	
	public Project_attachListAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return CurrentDatas.size();// 返回数组的长度
 	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
 	
		ViewHolder holder;
		ArticleDto curDto=CurrentDatas.get(position);
 		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.project_attach_list_item, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title = (TextView) convertView.findViewById(R.id.remind_text);
			 
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
	 	}
		holder.title.setText(curDto.title);
 		return convertView;
	 
	}/* 存放控件 */

}