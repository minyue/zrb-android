package com.zrb.mobile.adapter.scrollInterfaceImpl;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import android.R.integer;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

public class DiscoverListViewScrollListenerImpl implements OnScrollListener{
	private ArrayList<OnScrollListener> mScrollList = new ArrayList<OnScrollListener>();
	
	public void addScrollListener(OnScrollListener scrollListener){
		mScrollList.add(scrollListener);
	}
	
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
			for(int index = 0;index<mScrollList.size();index++){
				OnScrollListener scrollListener = mScrollList.get(index);
				if(scrollListener != null){
					scrollListener.onScroll(view, firstVisibleItem, 
							visibleItemCount, totalItemCount);
				}
			}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		for(int index = 0;index<mScrollList.size();index++){
			OnScrollListener scrollListener = mScrollList.get(index);
			if(scrollListener != null){
				scrollListener.onScrollStateChanged(view, scrollState);
			}
		}

	}
	
}
