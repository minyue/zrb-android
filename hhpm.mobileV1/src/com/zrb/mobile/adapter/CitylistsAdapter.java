package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.CityDto;
import com.zrb.mobile.adapter.model.HotRankDto;
import com.zrb.mobile.utility.CommonUtil;

public class CitylistsAdapter extends BaseAdapter {

	public static class ViewHolder {
		public TextView title;
		public CheckBox rightchk;
	}

	private int _selIndex = 0;
	private int choicemode = ListView.CHOICE_MODE_SINGLE;

	private List<Integer> mcurSets = new ArrayList<Integer>();
	public List<CityDto> CurrentDatas = new ArrayList<CityDto>();

	private LayoutInflater mInflater;//
	// 用来控制CheckBox的选中状况
	private static HashMap<Integer, Boolean> isSelected;
	// 上下文
	private Context mContext;

	public CitylistsAdapter(Context context,List<CityDto> curDatas) {
		this.mInflater = LayoutInflater.from(context);
		this.mContext = context;
		this.CurrentDatas=curDatas;
		isSelected = new HashMap<Integer, Boolean>();
		initDate();
	}

	// 初始化isSelected的数据
	private void initDate() {
		for (int i = 0; i < CurrentDatas.size(); i++) {
			getIsSelected().put(i, false);
		}
	}

	public void setDatalist(List<CityDto> curDatas) {
		CurrentDatas.clear();
		CurrentDatas.addAll(curDatas);
		// 初始化数据
	}

	@Override
	public int getCount() {
		return CurrentDatas == null ? 0 : CurrentDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return CurrentDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.app_city_lists_item, null);
			holder = new ViewHolder();

			holder.title = (TextView) convertView
					.findViewById(R.id.remind_text);
			holder.rightchk = (CheckBox) convertView
					.findViewById(R.id.city_item_cbs);

			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);//
		} else {
			holder = (ViewHolder) convertView.getTag();//
		}

		// holder.lefticon.setImageResource(CurrentDatas.get(position).Rid);
		// holder.righticon.setTag(position);

		// if (choicemode == ListView.CHOICE_MODE_MULTIPLE) {
		// holder.rightchk.setChecked(mcurSets.contains(position) ? true
		// : false);
		// }
		holder.title.setText(CurrentDatas.get(position).cityName);
		if (getIsSelected().get(position)!=null) {
			holder.rightchk.setChecked(getIsSelected().get(position));
		}

		return convertView;
	}

	public void UpdateIndex(int selIndex, View convertView) {

		if (mcurSets.contains(selIndex)) {

		}

	}

	public static HashMap<Integer, Boolean> getIsSelected() {
		return isSelected;
	}

	public static void setIsSelected(HashMap<Integer, Boolean> isSelected) {
		CitylistsAdapter.isSelected = isSelected;
	}

}