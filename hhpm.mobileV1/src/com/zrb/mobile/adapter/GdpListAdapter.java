package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.GdpInfo;
import com.zrb.mobile.adapter.model.ProjectDto;
import com.zrb.mobile.common.AppInfo;
import com.zrb.mobile.utility.CommonUtil;

public  class GdpListAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title1;
		public TextView value1;
		public TextView title2;
		public TextView value2;
	}

	private List<GdpInfo> CurrentDatas=null;// = new ArrayList<GdpInfo>();
 	private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/

	
	
	public GdpListAdapter(Context context,List<GdpInfo> datas) {
		this.mInflater = LayoutInflater.from(context);
		CurrentDatas=datas;
	}

	@Override
	public int getCount() {
		
		int left=CurrentDatas.size()%2;
		
		int total=left==0? CurrentDatas.size()/2:(CurrentDatas.size()/2+1);
		CommonUtil.InfoLog("total", total+"");
	    return total;
 		//return CurrentDatas.size();// 返回数组的长度
 	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* 书中详细解释该方法 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.project_city_gdp_listitem, null);
			holder = new ViewHolder();
			/* 得到各个控件的对象 */
			holder.title1 = (TextView) convertView.findViewById(R.id.gpd_title1);
			holder.value1 = (TextView) convertView.findViewById(R.id.gpd_value1);
			holder.title2 = (TextView) convertView.findViewById(R.id.gpd_title2);
			holder.value2 = (TextView) convertView.findViewById(R.id.gpd_value2);
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
 		}
		
		int pos=2*position;
		
		holder.title1.setText(CurrentDatas.get(pos).name);
		holder.value1.setText(CurrentDatas.get(pos).value);
	    
		if(pos+1< CurrentDatas.size()){
			holder.title2.setVisibility(View.VISIBLE);
			holder.value2.setVisibility(View.VISIBLE);
			holder.title2.setText(CurrentDatas.get(pos+1).name);
			holder.value2.setText(CurrentDatas.get(pos+1).value);
 		}
		else{
			holder.title2.setVisibility(View.INVISIBLE);
			holder.value2.setVisibility(View.INVISIBLE);
		}
	 
		return convertView;
	}/* 存放控件 */

}