package com.zrb.mobile.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.RegisterIndustryDto;

public class IndustrylistItemAdapter extends BaseAdapter {
	private boolean isProject;
	private LayoutInflater mInflater;
	private List<RegisterIndustryDto> mDatas;

	// private Drawable firstDrawable,secDrawable,thirdDrawable;

	public IndustrylistItemAdapter(Context context,
			List<RegisterIndustryDto> datas) {
		this.mDatas = datas;
		mInflater = LayoutInflater.from(context);

	}

	public void setProject(boolean isProject) {
		this.isProject = isProject;
	}

	public void addAll(List<RegisterIndustryDto> mDatas) {
		this.mDatas.addAll(mDatas);
	}

	public void setDatas(List<RegisterIndustryDto> mDatas) {
		this.mDatas.clear();
		this.mDatas.addAll(mDatas);
	}

	@Override
	public int getCount() {
		return mDatas.size();
	}

	public RegisterIndustryDto getCurItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public Object getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.industry_list_item, null);
			holder = new ViewHolder();
			holder.mTitle = (TextView) convertView
					.findViewById(R.id.industryname_text);
			holder.mlefticon = (TextView) convertView
					.findViewById(R.id.left_icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		RegisterIndustryDto newsItem = mDatas.get(position);
		holder.mTitle.setText(newsItem.industryName);// .getTitle()
		holder.mlefticon.setText(getfisrtChar(newsItem.industryName));
		if (newsItem.color != null) {
			GradientDrawable drawable = new GradientDrawable();
			drawable.setShape(GradientDrawable.RECTANGLE);
			drawable.setCornerRadii(new float[] { 5, 5, 5, 5, 5, 5, 5, 5 });
			try {
				drawable.setColor(Color.parseColor(newsItem.color));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				drawable.setColor(Color.parseColor("#ffcccccc"));
				e.printStackTrace();
			}
			holder.mlefticon.setBackgroundDrawable(drawable);
		} else {
			holder.mlefticon.setBackgroundResource(R.drawable.industry_otherbg);
		}

		// if(position==0&&!isProject)
		// holder.mlefticon.setBackgroundResource(R.drawable.industry_all) ;
		// else
		// if(position==0&&isProject)holder.mlefticon.setBackgroundResource(R.drawable.industry_firstbg)
		// ;
		// else if(position>=1&&position<6)
		// holder.mlefticon.setBackgroundResource(R.drawable.industry_firstbg) ;
		// else if(position>=6&&position<12){
		// holder.mlefticon.setBackgroundResource(R.drawable.industry_secondbg)
		// ;
		// }
		// else if(position>=12){
		// holder.mlefticon.setBackgroundResource(R.drawable.industry_thridbg) ;
		// }
		// if(position==getCount()-1)
		// holder.mlefticon.setBackgroundResource(R.drawable.industry_otherbg) ;
		return convertView;
	}

	private String getfisrtChar(String name) {
		if (TextUtils.isEmpty(name))
			return "";
		else
			return name.substring(0, 1);
	}

	private final class ViewHolder {
		TextView mTitle;
		TextView mlefticon;

	}

}
