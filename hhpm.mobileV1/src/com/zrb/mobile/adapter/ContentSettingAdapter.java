package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.zrb.mobile.News_WebContentActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.common.NightConfig;
import com.zrb.mobile.ui.SwitchButton;
import com.zrb.mobile.utility.CommonUtil;

 
public  class ContentSettingAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public ImageView lefticon;
		public FrameLayout container;
	}
	private Context mcontext;
	
	private RadioGroup mRadiogroup;
	private boolean isNight;
	private int _selIndex = 0;
	private float _curfontsize;
	
	OnCheckedChangeListener mCheckedChangeListener;
	OnSeekBarChangeListener mSeekBarChangeListener ;
	RadioGroup.OnCheckedChangeListener mGroupCheckedChangeListener;
 
	
	public void setOnCheckedChangeListener(OnCheckedChangeListener  changeListener){
 		mCheckedChangeListener=changeListener;
	}
	
    public void setOnSeekBarChangeListener(OnSeekBarChangeListener  SeekchangeListener){
    	mSeekBarChangeListener=SeekchangeListener;
	}
    
    public void setOnGroupChangeListener(RadioGroup.OnCheckedChangeListener  groupchangeListener){
    	mGroupCheckedChangeListener=groupchangeListener;
	}

	public List<RadioItem> CurrentDatas = new ArrayList<RadioItem>();
 	private LayoutInflater mInflater; 
 	
	public ContentSettingAdapter(Context context,boolean isNightmode) {
		this.mInflater = LayoutInflater.from(context);
		
		isNight=isNightmode;
		mcontext=context;
		_curfontsize= NightConfig.getInstance(mcontext).getFontSize();
	}
	
	@Override
	public int getCount() {
		return CurrentDatas.size();// 返回数组的长度
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.detail_more_title_item, null);
			holder = new ViewHolder();
	
			holder.title = (TextView) convertView.findViewById(R.id.desc_more_title);
			holder.lefticon = (ImageView) convertView.findViewById(R.id.ic_more_title);
			holder.container = (FrameLayout) convertView.findViewById(R.id.custom_more_title);
			
			convertView.setTag(holder);// 绑定ViewHolder对象
		} else {
			holder = (ViewHolder) convertView.getTag();// 取出ViewHolder对象 }
	    }
  
		holder.title.setText(CurrentDatas.get(position).Name);
		if(position==0){  
			holder.lefticon.setImageResource(R.drawable.ic_night_detail);
			View tmpView = mInflater.inflate(R.layout.detail_more_title_switch, holder.container,true);
			
			SwitchButton tmpswitch=(SwitchButton)tmpView.findViewById(R.id.night_mode_btn);
			if(isNight) tmpswitch.setChecked(true);
			tmpswitch.setOnCheckedChangeListener(mCheckedChangeListener);
		}
		else if(position==3)  {
			holder.lefticon.setImageResource(R.drawable.ic_bright_detail);
			View tmpView = mInflater.inflate(R.layout.detail_more_title_seekbar, holder.container,true);
 			
			SeekBar tmpSeekBar=(SeekBar)tmpView.findViewById(R.id.bright_adjust_seekbar);
			tmpSeekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);
			//holder.container.addView(tmpView);   
		}
		else if(position==1) {
			holder.lefticon.setImageResource(R.drawable.ic_font_detail);

			View tmpView = mInflater.inflate(R.layout.detail_more_title_option, holder.container,true);
			RadioGroup tmpRadioGroup=(RadioGroup)tmpView.findViewById(R.id.detail_font_size);
			if(_curfontsize==9) ((RadioButton)tmpRadioGroup.getChildAt(0)).setChecked(true);
			if(_curfontsize==10.5) ((RadioButton)tmpRadioGroup.getChildAt(1)).setChecked(true);
			if(_curfontsize==12) ((RadioButton)tmpRadioGroup.getChildAt(2)).setChecked(true);
			if(_curfontsize==13) ((RadioButton)tmpRadioGroup.getChildAt(3)).setChecked(true);
			tmpRadioGroup.setOnCheckedChangeListener(mGroupCheckedChangeListener);
			mRadiogroup=tmpRadioGroup;
			setNight(isNight);
			//holder.container.addView(tmpView); 
		}
 
		setTextandLine(convertView,isNight);
		return convertView;
	} 

   public void setNight(boolean istrue){
	   
	   if(mRadiogroup!=null){
 		   for(int i=0;i<mRadiogroup.getChildCount();i++) {
 			   RadioButton tmpbtn=(RadioButton) mRadiogroup.getChildAt(i);
			   if(istrue)  tmpbtn.getBackground().setLevel(1);
			   else
				   tmpbtn.getBackground().setLevel(0);
		   }
	   }
   } 

   private final void setTextandLine(View convertView,boolean isNight){
	   
	   TextView tmpview = (TextView) convertView.findViewById(R.id.desc_more_title);
		if (tmpview != null)
			tmpview.setTextColor(mcontext.getResources().getColor(
					isNight ? R.color.detail_more_title_item_text_night
							: R.color.detail_more_title_item_text));
		
		View tmpview2 =  convertView.findViewById(R.id.divider_more_title);
		if (tmpview2 != null)
			tmpview2.setBackgroundColor(mcontext.getResources().getColor(
					isNight ? R.color.detail_more_title_cancel_btn_stroke_night
							: R.color.detail_more_title_cancel_btn_stroke));
	   
   }
   
   //detail_more_title_cancel_btn_stroke
 //detail_more_title_cancel_btn_stroke_night
	public void SetNightlist(ListView tmpsetlistView, boolean istrue) {
		ListView rootview = tmpsetlistView;
		for (int i = 0; i < tmpsetlistView.getChildCount(); i++) {
 			setTextandLine(tmpsetlistView.getChildAt(i),istrue);
 		}
		NightConfig.getInstance(mcontext).setNightModeSwitch(istrue);
		isNight=istrue;
		setNight(istrue);
	}
}