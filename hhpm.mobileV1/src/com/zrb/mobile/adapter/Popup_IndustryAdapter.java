package com.zrb.mobile.adapter;

import java.util.List;
 
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.RegisterIndustryDto;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Popup_IndustryAdapter extends BaseAdapter {
	private Context context;
	public List<RegisterIndustryDto> channelList;
	private TextView item_text;
 
    private int selIndustryCode=-1;
 
    public void setSelectIndustry(int selectCode){
    	
    	selIndustryCode=selectCode;
    }
    
	public Popup_IndustryAdapter(Context context, List<RegisterIndustryDto> channelList) {
		this.context = context;
		this.channelList = channelList;
	}

	@Override
	public int getCount() {
		return channelList == null ? 0 : channelList.size();
	}

	@Override
	public RegisterIndustryDto getItem(int position) {
		if (channelList != null && channelList.size() != 0) {
			return channelList.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view=null;
	   if(convertView==null){
		   view = LayoutInflater.from(context).inflate(R.layout.tag_industry_item, null);
 		   convertView=view;
	   }
	   else
		   view= convertView;
		
	    item_text = (TextView) view.findViewById(R.id.text_item);
		RegisterIndustryDto channel = getItem(position);
		item_text.setText(channel.industryName);
 		
	   if(selIndustryCode==channel.id){
			  item_text.setSelected(true);
		}
		else{
			item_text.setSelected(false);
		} 
			
 	
	 
		return view;
	}
	
   

 
}