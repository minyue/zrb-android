package com.zrb.mobile.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 通用适配器（适合一些常规的适配器）
 * @author Jenly
 *
 * @param <T>
 */
public abstract class AbstractHolderAdapter<T> extends AbstractAdapter<T>{

	public AbstractHolderAdapter(Context context, List<T> listData) {
		super(context, listData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Object holder = null;
		if(convertView==null){
			convertView = buildConvertView(layoutInflater);
			holder = buildHolder(convertView);
			
			convertView.setTag(holder);
		}else{
			holder = convertView.getTag();
		}
		bindViewData(holder,listData.get(position),position);
		
		return convertView;
	}
	
	/**
	 * 建立convertView
	 * @param layoutInflater
	 * @return
	 */
	public abstract View buildConvertView(LayoutInflater layoutInflater);
	
	/**
	 * 建立视图Holder
	 * @param convertView
	 * @return
	 */
	public abstract Object buildHolder(View convertView);
	
	/**
	 * 绑定数据
	 * @param holder
	 * @param t
	 * @param position
	 */
	public abstract void bindViewData(Object holder,T t,int position);

}


/*public class TestHolderAdapter extends HolderAdapter<String>{

	public TestHolderAdapter(Context context, List<String> listData) {
		super(context, listData);
	}

	@Override
	public View buildConvertView(LayoutInflater layoutInflater) {
		return layoutInflater.inflate(R.layout.activity_list_item, null);
	}

	@Override
	public Object buildHolder(View convertView) {
		ViewHolder holder = new ViewHolder();
		holder.tv = (TextView)convertView.findViewById(R.id.tv);
		return holder;
	}

	@Override
	public void bindViewDatas(Object holder, String t, int position) {
		((ViewHolder)holder).tv.setText(t);
	}
	
	private static class ViewHolder{
		TextView tv;
	}

}
*/


