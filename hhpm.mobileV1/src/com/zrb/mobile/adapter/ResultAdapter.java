package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.*;
import com.zrb.applib.utils.AppSetting;
 
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;

import android.widget.BaseAdapter;

import android.widget.ImageView;

import android.widget.TextView;

public class ResultAdapter extends BaseAdapter {

	private LayoutInflater mInflater;

	DisplayImageOptions options;
	public List<ProjectDto> CurrentDatas = new ArrayList<ProjectDto>();
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private ImageLoader imageLoader;
	private class ViewHolder {
		public TextView text;
		public TextView des;
		public ImageView image;
	}

	public void setImgLoader(ImageLoader curImgLoad) {

		imageLoader = curImgLoad;

	}

	public ResultAdapter(Context context) {

		this.mInflater = LayoutInflater.from(context);

		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.guess_like_img)
		.showImageForEmptyUri(R.drawable.guess_like_img)
		.showImageOnFail(R.drawable.guess_like_img).cacheInMemory(true)
		.cacheOnDisc(true).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(20)).build();

	}

	@Override
	public int getCount() {

		if (CurrentDatas != null) {

			return CurrentDatas.size();

		}

		return 0;

	}

	@Override
	public Object getItem(int position) {

		// TODO Auto-generated method stub

		return null;

	}

	@Override
	public long getItemId(int position) {

		// TODO Auto-generated method stub

		return position;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;
		final ViewHolder holder;
		if (convertView == null) {

			view = mInflater.inflate(R.layout.app_list_item_recentproject,	parent, false);
			holder = new ViewHolder();
			holder.text = (TextView) view.findViewById(R.id.name);
			holder.des = (TextView) view.findViewById(R.id.destype);
			holder.image = (ImageView) view.findViewById(R.id.main_pic);
			view.setTag(holder);

		} else {

			holder = (ViewHolder) view.getTag();

		}

	/*	holder.text.setText(CurrentDatas.get(position).title);
		holder.des.setText("��;��" + CurrentDatas.get(position).desc1	+ " | ����ʱ�䣺" +formatdate( CurrentDatas.get(position).desc2));

		String url =AppSetting.mediaServer+CurrentDatas.get(position).img ;
		imageLoader.displayImage(url, holder.image, options,animateFirstListener);*/

		return view;

	}
	
	private String formatdate(String curDate)
	{
		if(TextUtils.isEmpty(curDate)) return "";
		if(curDate.contains(" "))
			return curDate.split(" ")[0];
		else
			return curDate;
	}

	public void displayedImageclear() {

		AnimateFirstDisplayListener.displayedImages.clear();

	}

	private static class AnimateFirstDisplayListener extends

	SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
		.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,Bitmap loadedImage) {

			if (loadedImage != null) {

				ImageView imageView = (ImageView) view;

				boolean firstDisplay = !displayedImages.contains(imageUri);

				if (firstDisplay) {

					FadeInBitmapDisplayer.animate(imageView, 500);

					displayedImages.add(imageUri);

				}

			}

		}

	}

}
