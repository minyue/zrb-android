package com.zrb.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuListAdapter.ViewHolder;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.CityDto;
import com.zrb.mobile.adapter.model.HotRankDto;
import com.zrb.mobile.utility.CommonUtil;

 
public  class CitylistAdapter extends BaseAdapter {

	final class ViewHolder {
		public TextView title;
		public ImageView lefticon;
		public CheckBox rightchk;
	}
	
	private int _selIndex = 0;
 	private int choicemode=ListView.CHOICE_MODE_SINGLE;
	
	private List<Integer> mcurSets=new ArrayList<Integer>();
 	public List<CityDto> CurrentDatas = new ArrayList<CityDto>();

	private LayoutInflater mInflater;// 
 	
	public CitylistAdapter(Context context) {
		this.mInflater = LayoutInflater.from(context);
		 
	}

	public void setDatalist(List<CityDto> curDatas){
		CurrentDatas.clear();
		CurrentDatas.addAll(curDatas);
	}
	
	@Override
	public int getCount() {
 		return CurrentDatas.size();// 
 	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
 	 
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.app_city_list_item, null);
			holder = new ViewHolder();
		 
			holder.title = (TextView) convertView.findViewById(R.id.remind_text);
			holder.lefticon = (ImageView) convertView.findViewById(R.id.left_icon);
			holder.rightchk=(CheckBox) convertView.findViewById(R.id.city_item_cb);
			 
			// holder.bt = (Button) convertView.findViewById(R.id.ItemButton);
			convertView.setTag(holder);//  
		} else {
			holder = (ViewHolder) convertView.getTag();// 
		}
  
		//holder.lefticon.setImageResource(CurrentDatas.get(position).Rid);
 		//holder.righticon.setTag(position);
 		
 		if(choicemode==ListView.CHOICE_MODE_MULTIPLE) {
 			holder.rightchk.setChecked(mcurSets.contains(position) ? true:false);
  		}
 		holder.title.setText(CurrentDatas.get(position).cityName);
  		return convertView;
	 } 
	
	 public void UpdateIndex(int selIndex,View convertView){
		 
		   if(mcurSets.contains(selIndex)){
			   
			   
		   }
        
     }
 
}