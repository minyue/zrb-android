package com.zrb.mobile;

import com.google.gson.Gson;
import com.zrb.mobile.adapter.model.ArticleDtos;
import com.zrb.mobile.adapter.model.CustomProjectDto;
import com.zrb.mobile.adapter.model.ProjectTradingDto;
import com.zrb.mobile.common.ShareDialog;
import com.zrb.mobile.ui.RemoteImageView;
import com.zrb.mobile.ui.SimpleViewPagerIndicator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

 

public class TestProject_infoActivity extends FragmentActivity implements OnClickListener
{
	private String[] mTitles = new String[] { "基本信息", "周边环境", "附加信息" };
	private SimpleViewPagerIndicator mIndicator;
	private ViewPager mViewPager;
	private FragmentPagerAdapter mAdapter;
	//private TestTabFragment[] mFragments = new TestTabFragment[mTitles.length];
	TextView mtitle;
	TextView mProtitle;
	
	
	CustomProjectDto currentDto;
	ProjectTradingDto currentTradingDto;
	String currentObj;
	int curType=0;
	RemoteImageView remoteview;
	String uid="";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.project_content_detail);

		currentObj=	this.getIntent().getStringExtra("curObj");//curObj
		curType=	this.getIntent().getIntExtra("curType", 1);
		initViews();
		initDatas();
		initEvents();
	}
	private void initViews()
	{
		mIndicator = (SimpleViewPagerIndicator) findViewById(R.id.id_stickynavlayout_indicator);
		mViewPager = (ViewPager) findViewById(R.id.id_stickynavlayout_viewpager);
		mViewPager.setOffscreenPageLimit(3);
 		
		this.findViewById(R.id.btn_share).setOnClickListener(this);
		//this.findViewById(R.id.btn_save).setOnClickListener(this);
		
		mtitle= (TextView)this.findViewById(R.id.titleText);
		mtitle.setText("详情信息");
		this.findViewById(R.id.title_left_root).setOnClickListener(this);
		remoteview=(RemoteImageView)this.findViewById(R.id.activity_icon);
		
		
	}
	
	private void initDatatrading()
	{
		if(curType==2&&!TextUtils.isEmpty(currentObj)) 
		{
		    Gson gson = new Gson();    
		    currentTradingDto=	gson.fromJson(currentObj, ProjectTradingDto.class);
			
			 mProtitle=(TextView)this.findViewById(R.id.app_info_activitys_title);
			mProtitle.setText(currentTradingDto.title);
			
			TextView mProtype=(TextView)this.findViewById(R.id.project_typetxt);
			mProtype.setText(currentTradingDto.transferWay.replace("所属行业:", ""));
			
			TextView mCooperation=(TextView)this.findViewById(R.id.cooperation_txt);
			mCooperation.setText(currentTradingDto.Landuse.replace("合作方式:", ""));
			
			
			uid=currentTradingDto.id;
			remoteview.setImageUrl(currentTradingDto.img);
		}
		
	}
	
	private void initDatas()
	{
	
		if(curType==1&&!TextUtils.isEmpty(currentObj)) 
		{
			
			Gson gson = new Gson();    
 			currentDto=	gson.fromJson(currentObj, CustomProjectDto.class);
 			
 		    mProtitle=(TextView)this.findViewById(R.id.app_info_activitys_title);
 			mProtitle.setText(currentDto.title);
 			
 			TextView mProtype=(TextView)this.findViewById(R.id.project_typetxt);
 			mProtype.setText("行业："+currentDto.belongBusiness.replace("所属行业:", ""));
 			
 			TextView mCooperation=(TextView)this.findViewById(R.id.cooperation_txt);
 			mCooperation.setText("合作方式："+currentDto.cooperation.replace("合作方式:", ""));
 			
 			
 			uid=currentDto.id;
 			remoteview.setImageUrl(currentDto.img);
		}
		else
			initDatatrading();
		
		mIndicator.setTitles(mTitles);
 		for (int i = 0; i < mTitles.length; i++)
		{
			//mFragments[i] = (TestTabFragment) TestTabFragment.newInstance(mTitles[i],uid);
		}
 		mAdapter = new FragmentPagerAdapter(getSupportFragmentManager())
		{
			@Override
			public int getCount()
			{
				return mTitles.length;
			}

			@Override
			public Fragment getItem(int position)
			{
				//return mFragments[position];
				return null;
//				 if(position!=1){
//					
//					 TabFragment tmptab=new TabFragment();
//					return tmptab;
//					
//				}
//				else
//					return new Project_aroundinfo_fragment();
			}

		};

		mViewPager.setAdapter(mAdapter);
		mViewPager.setCurrentItem(0);
	}
	
	private void initEvents()
	{
		mViewPager.setOnPageChangeListener(new OnPageChangeListener()
		{
			@Override
			public void onPageSelected(int position)
			{
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels)
			{
				mIndicator.scroll(position, positionOffset);
			}

			@Override
			public void onPageScrollStateChanged(int state)
			{

			}
		});

	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		  case R.id.title_left_root:
			this.finish();
			break;
		  case R.id.btn_share:
				ShareDialog tmpd=new ShareDialog(this);
				tmpd.share(mProtitle.getText().toString(),"http://www.zhaorongbao.com"+uid);
			  break;
	/*	  case R.id.btn_save:
				Intent it = new Intent(this, MultyLocationActivity.class);
 				startActivity(it);
				 
			  
			  break;*/
		}
		
	}



}
