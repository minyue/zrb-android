package com.zrb.mobile;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.CenterProgressWebView.IOnJsPrompt;
import com.zrb.mobile.ui.CenterProgressWebView.ScrollInterface;
import com.zrb.applib.utils.AppSetting;
public class System_message extends BaseActivity implements OnClickListener, IOnJsPrompt, ScrollInterface {

	CenterProgressWebView mWebView;
    String mtitle="系统通知";
    String murl="sysMessage/list";
    
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.system_message);
		initView();
	}
 	 
	private void initView() {
 		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText(mtitle);
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		mWebView = (CenterProgressWebView) this.findViewById(R.id.webView1);
 		String url =AppSetting.BASE_URL+murl;
		mWebView.loadUrl(url);
		mWebView.setOnJsPromptListener(this);
		mWebView.setOnPageListener(null);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_left_root:
			this.finish();
			break;
		}

	}

	@Override
	public void onSChanged(int l, int t, int oldl, int oldt) {
	}

	@Override
	public void PromptInfo(String message, String defaultValue) {
		Log.e("PromptInfo", message);
		if(TextUtils.isEmpty(message)) return;
		if(!message.contains(",")) return;
		String[] tmpArr=message.split(",");
//		pId =  Integer.parseInt(tmpArr[1]);
//   	 	pName = tmpArr[2];
//1,21,您的项目有新的评论,notify,通知,5,proj_cment_new,888888,系统,2015-07-31
		String content = tmpArr[2];
		String type = tmpArr[3];
		String typeName = tmpArr[4];
		String bizKey = tmpArr[5];
		String bizType = tmpArr[6];
		String senderUid = tmpArr[7];
		String senderUname = tmpArr[8];
		String ctime = tmpArr[9];
		Intent it = new Intent(this, System_message_details.class);
		it.putExtra("content",content);
		it.putExtra("typeName",typeName);
		it.putExtra("bizType",bizType);
		it.putExtra("ctime",ctime);
		it.putExtra("bizKey",bizKey);
		startActivity(it);
	}
	/**
	 * 注册成功
	 *//*
	user_regiester_succes("恭喜您，注册成功"),
	
	*//**
	 * 认证通过
	 *//*
	user_auth_succes("你的身份认证通过"),
	
	*//**
	 * 用户认证信息修改
	 *//*
	user_auth_modify("您的身份认证信息已经变更"),
	
	*//**
	 * 用户所属机构变化
	 *//*
	user_org_modify("你的所属机构已经变更"),
	
	*//**
	 * 项目进展变化
	 * bizKey 格式 pid
	 *//*
	proj_prog_change("您的项目进展有变化"),
	
	*//**
	 * 项目新建
	 * bizKey 格式 pid
	 *//*
	proj_prog_new("您的项目已经新建成功"),
	
	*//**
	 * 项目评论回复
	 * bizKey格式 pid：id（评论id）
	 *//*
	proj_cment_relay("你的评论有新的回复"),
	
	*//**
	 * 项目有新的评论
	 * bizKey格式 pid
	 *//*
	proj_cment_new("您的项目有新的评论"),*/
	@Override
	public void ReceivedTitle(String title) {
		
	}
}
