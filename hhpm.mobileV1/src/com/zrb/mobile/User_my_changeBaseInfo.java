package com.zrb.mobile;

import org.apache.http.Header;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.ui.NumEditText;
import com.zrb.mobile.ui.NumEditText.OnNumslistener;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

public class User_my_changeBaseInfo extends BaseActivity implements OnClickListener, OnNumslistener  {

	 //Toast提示框
  	private RelativeLayout notify_view;
  	private TextView notify_view_text;
  	Handler mhandler = new Handler();
	String lefttitle="",ehit="";
	TextView mtxtNum;
	NumEditText multeditText;
	EditText meditphone;
	LinearLayout layout_container;
	int curType=0;
   // int resultCode=0;
    String curValue,userValue;
	ProgressBar toploading;
	TextView rightSave;
	int maxSize=30;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

 	   setContentView(R.layout.u_my_changename);
		// setContentView(R.layout.user_register_activity);fragment_container
 	   if (bundle != null)
		restoreSelf(bundle);
	   else{ 
		  curType=this.getIntent().getIntExtra("curType", -1);
 	      userValue=this.getIntent().getStringExtra("userInfo");
 	   }
 	   initView();
	}
 
	 @Override
	 protected  void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curType", curType);
     }

    private void restoreSelf(Bundle savedInstanceState){
    	curType = savedInstanceState.getInt("curType",-1);
    }
    
	private void initView(){
 	
		toploading=(ProgressBar)this.findViewById(R.id.topbar_loading);
		
		notify_view = (RelativeLayout)this.findViewById(R.id.notify_view);
		notify_view_text = (TextView)this.findViewById(R.id.notify_view_text);
		
		rightSave=	(TextView)this.findViewById(R.id.right_title);
		rightSave.setVisibility(View.VISIBLE);
		rightSave.setText("保存");
		rightSave.setOnClickListener(this);
		
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("设置邮箱");
 		
		if(curType==4){
			txtTitle.setText("个人说明");
 	     	this.findViewById(R.id.input_info).setVisibility(View.VISIBLE);
			this.findViewById(R.id.layout_baseinfo).setVisibility(View.GONE);
			multeditText=(NumEditText)this.findViewById(R.id.editText1);
			multeditText.setText(userValue);
			multeditText.setMaxNums(maxSize);
			multeditText.setOnNumslistener(this);
			mtxtNum=(TextView)this.findViewById(R.id.txtNum);
			if( null != userValue && !"".equals(userValue)){
				mtxtNum.setText(String.valueOf(maxSize-userValue.trim().length()));
				multeditText.setSelection(userValue.length());
			}
			else
				rightSave.setEnabled(false);
 			return ;
		}
		TextView txtleft=	(TextView)this.findViewById(R.id.txt_phone);
	 	meditphone=	(EditText)this.findViewById(R.id.edit_phone);
	 	meditphone.addTextChangedListener(baseinfoWatcher);
	 
		switch (curType) {
		case 0:
			meditphone.setText(userValue);
			lefttitle = "用户名";
			ehit = "请输入用户名";
			txtTitle.setText("修改用户名");
			break;
		case 1:
			meditphone.setText(userValue);
			lefttitle = "邮 箱";
			ehit = "请输入您有效的电子邮箱";
			break;
		case 2:
			meditphone.setText(userValue);
			lefttitle = "姓 名";
			ehit = "请输入您的真实姓名";
			txtTitle.setText("真实姓名");
			break;
		case 3:
			meditphone.setText(userValue);
			lefttitle = "身份证";
			ehit = "请输入您的18位身份证";
			txtTitle.setText("身份证");
			break;
		case 6:
			meditphone.setText(userValue);
			lefttitle = "公司名称";
			ehit = "请输入您的公司名称";
			txtTitle.setText("公司名称");
			break;
		case 5:
			meditphone.setText(userValue);
			lefttitle = "职 务";
			ehit = "请输入您担任的职务";
			txtTitle.setText("担任职务");
			break;
		}
		txtleft.setText(lefttitle);
		meditphone.setHint(ehit);
		
		if(!TextUtils.isEmpty(userValue)) 
			meditphone.setSelection(userValue.length());
		else
			rightSave.setEnabled(false);
 	}

	private TextWatcher baseinfoWatcher=new TextWatcher(){

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			if( s.length() != 0 && !TextUtils.isEmpty(meditphone.getText().toString() )){
				rightSave.setEnabled(true);
 			}else{
				rightSave.setEnabled(false);
 			}
		}
 	};

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
    
     	case R.id.right_title:
     		changeBaseInfo() ;
     		break;
 		}
 	}

	
	ZrbRestClient mZrbRestClient;
	 private void changeBaseInfo() {
		 if( curType != 4){
			 if( TextUtils.isEmpty(meditphone.getText()) ){
				 initNotify(ehit);
				 return;
			 }
		 }
 	    	//progTip=ProgressDialogEx.Show(this," title", "loadtxt", true, true);
		String paramName="";
		RequestParams param = new RequestParams();
	    //param.put("zrb_front_ut", AppSetting.curUser.zrb_front_ut);
			
		switch (curType) {
		case 0:
 			if(!CommonUtil.checkUserName(meditphone.getText().toString())){
				initNotify("位数大于2个汉字或4个字符");
				return;
			}
 			param.put("uname", meditphone.getText().toString());
 			break;
		case 1:
			param.put("uemail", meditphone.getText().toString());
 			break;
		case 2:
			param.put("realname", meditphone.getText().toString());
 			break;
		case 3:
			param.put("idcard", meditphone.getText().toString());
 			break;
		case 4:
			param.put("info", multeditText.getText().toString());
 		   break;
		case 5:
			param.put("position", meditphone.getText().toString());
 		   break;
		case 6:
			param.put("org", meditphone.getText().toString());
 		   break;
		}
//		showloading();
		toploading.setVisibility(View.VISIBLE);
		curValue=curType==4 ? multeditText.getText().toString(): meditphone.getText().toString();
		
		
		CommonUtil.InfoLog("changeBaseInfo", param.toString());
		
		   if(mZrbRestClient==null){
			
			    mZrbRestClient=new ZrbRestClient(this);
			    mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
 				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
					// TODO Auto-generated method stub
 					toploading.setVisibility(View.GONE);
 					initNotify("加载数据出错！");
				}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
 				    String results=new String(arg2);
 				    
 				   CommonUtil.InfoLog("changeBaseInfo2",results);
 				   toploading.setVisibility(View.GONE);
					try 
					{
 			            Gson gson = new Gson();  
 			            RegisterDto tmpDto=	gson.fromJson(results, RegisterDto.class);
			            if(tmpDto.res==1) {
			                new Handler().postDelayed(new Runnable() {
		                        @Override
		                        public void run() {
 	                             	Intent data=new Intent();
		                        	data.putExtra("newvalue", curValue);
		                        	
		                        	User_my_changeBaseInfo.this.setResult(RESULT_OK, data);
		                        	User_my_changeBaseInfo.this.finish();
	 	                        }
 		                    }, 1000);
			            }
			            else{
			            	//Log.e("tag", tmpDto.msg);
			            	initNotify(tmpDto.msg);
			            }
 					}
					catch(Exception e) {
						initNotify("error:加载数据出错！");
						//System.out.println("eeee!!!!"+e.getMessage());
					}
					
				}});
		    }
	    	mZrbRestClient.post("user/updateAppUserInfo", param  );
	    }
	
//	 private void showloading() {
//			if (progTip == null)
//				progTip = ProgressDialogEx.Show(this, " title", "正在保存中...", true,
//						true);
//			else
//				progTip.Show("正在保存中...");
//	}
	 
	@Override
	public void showtextNum(int number) {
		// TODO Auto-generated method stub
 		 rightSave.setEnabled(maxSize==number ? false:true);
 		 mtxtNum.setText(number+"");
	}

	/* 初始化通知栏目*/
	private void initNotify(final String msg) {
		mhandler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				notify_view_text.setText(msg);
				notify_view.setVisibility(View.VISIBLE);
				mhandler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						notify_view.setVisibility(View.GONE);
					}
				}, 2000);
			}
		}, 1000);
	}

}
