/*
 * Copyright (C) 2013 Manuel Peinado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zrb.mobile.fadingactionbar;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.SherlockListActivity;
import com.zrb.mobile.fadingactionbar.FadingActionBarHelperBase;

public final class ActionBarHelper  {

    private ActionBar mActionBar;
    private Drawable mActionBarBackgroundDrawable;
    private int mActionBarBackgroundResId;
    
    public void initActionBar(Activity activity) {
        mActionBar = getActionBar(activity);
        initActionBar2(activity);
    }
    
    public final void   actionBarBackground(int drawableResId) {
        mActionBarBackgroundResId = drawableResId;
    }
    
     void initActionBar2(Activity activity) {
        if (mActionBarBackgroundDrawable == null) {
            mActionBarBackgroundDrawable = activity.getResources().getDrawable(mActionBarBackgroundResId);
        }
        setActionBarBackgroundDrawable(mActionBarBackgroundDrawable);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }
        mActionBarBackgroundDrawable.setAlpha(0);
    }
     
     
     private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
         @Override
         public void invalidateDrawable(Drawable who) {
             setActionBarBackgroundDrawable(who);
         }

         @Override
         public void scheduleDrawable(Drawable who, Runnable what, long when) {
         }

         @Override
         public void unscheduleDrawable(Drawable who, Runnable what) {
         }
     };

    private ActionBar getActionBar(Activity activity) {
        if (activity instanceof SherlockActivity) {
            return ((SherlockActivity) activity).getSupportActionBar();
        }
        if (activity instanceof SherlockFragmentActivity) {
            return ((SherlockFragmentActivity) activity).getSupportActionBar();
        }
        if (activity instanceof SherlockListActivity) {
            return ((SherlockListActivity) activity).getSupportActionBar();
        }
        ActionBar actionBar = getActionBarWithReflection(activity, "getSupportActionBar");
        if (actionBar == null) {
            throw new RuntimeException("Activity should derive from one of the ActionBarSherlock activities "
                + "or implement a method called getSupportActionBar");
        }
        return actionBar;
    }

    
    protected <T> T getActionBarWithReflection(Activity activity, String methodName) {
        try {
            Method method = activity.getClass().getMethod(methodName);
            return (T)method.invoke(activity);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
           e.printStackTrace();
        }
        return null;
    }
 
    public int getActionBarHeight() {
        return mActionBar.getHeight();
    }

 
    public boolean isActionBarNull() {
        return mActionBar == null;
    }

  
    protected void setActionBarBackgroundDrawable(Drawable drawable) {
        mActionBar.setBackgroundDrawable(drawable);
    }
    
    public void setActionBarBackgroundAlpha(int newAlpha){
    	
    	 mActionBarBackgroundDrawable.setAlpha(newAlpha);
    }
}
