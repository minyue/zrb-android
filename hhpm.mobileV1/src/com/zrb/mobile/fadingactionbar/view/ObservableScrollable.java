package com.zrb.mobile.fadingactionbar.view;


public interface ObservableScrollable {
    void setOnScrollChangedCallback(OnScrollChangedCallback callback);
}
