package com.zrb.mobile.fadingactionbar.view;

public interface OnScrollChangedCallback {
    void onScroll(int l, int t);
}
