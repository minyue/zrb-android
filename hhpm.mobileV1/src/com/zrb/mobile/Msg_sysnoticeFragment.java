package com.zrb.mobile;

import java.text.SimpleDateFormat;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.XList.IXListViewLoadMore;
import com.zrb.mobile.XList.IXListViewRefreshListener;
import com.zrb.mobile.XList.XListView;
import com.zrb.mobile.adapter.MsgNoticelistAdapter;
import com.zrb.mobile.adapter.News_tablistItemAdapter;
import com.zrb.mobile.adapter.model.MsgNoticeDto;
import com.zrb.mobile.adapter.model.MsgNoticeDtos;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.NewsDtos;
import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.CenterProgressWebView.IOnJsPrompt;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.MsgformatUtil;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.applib.utils.AppSetting;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Msg_sysnoticeFragment extends BaseLazyFragment   implements IXListViewRefreshListener, IXListViewLoadMore, OnClickListener{

	private static final int RESULT_OK = 0;
	String mtitle="ϵͳ֪ͨ";
    String murl="sysMessage/list";
	boolean isFirst=true;
 
	private int currentPage = 1;
	private int newsType=800;
	Handler mhandler = new Handler() ;
	/**
	 * ��չ��ListView
	 */
	private XListView mXListView;
	private MsgNoticelistAdapter msgNoticeAdapter;
	private TextView loadhite,quicklogin;  
	MsgNoticeDto tmpDtos;    
	View layoutnoTip;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view =  inflater.inflate(R.layout.message_sysnotice_fragment, null);	
 	 	
		layoutnoTip=view.findViewById(R.id.notice_login_layout);
		layoutnoTip.setVisibility(View.VISIBLE);
		layoutnoTip.setOnTouchListener(new OnTouchListener(){
 			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
 		});
		
		quicklogin= (TextView)view.findViewById(R.id.notice_quick);
		quicklogin.setOnClickListener(this);;
		
		mXListView = (XListView) view.findViewById(R.id.id_xlistView);
		mXListView.setPullRefreshEnable(this);
		
		mXListView.setPullLoadEnable(this);
		mXListView.setRefreshTime(CommonUtil.getRefreashTime(getActivity(), newsType));
 		// mXListView.NotRefreshAtBegin();

		mXListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
 			    tmpDtos = msgNoticeAdapter.getCurItem(position-1);
			    if(tmpDtos.read==0) {
 			    	msgNoticeAdapter.setReadStatus(view);
			    	setRead();
			    }
			    tmpDtos.read=1;
			/*	Intent intent = MsgformatUtil.getQuickAction(getActivity(),tmpDtos);
				startActivity(intent); */
			}

		});
		return view;
  	}	
	 
 	private void setRead(){
 		
 		ZrbRestClient.get("message/readMessage?receiverUid="+tmpDtos.receiverUid+"&id="+tmpDtos.id+"", null, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
 				CommonUtil.InfoLog("�޸�״̬�ɹ�id:",+tmpDtos.id+"");
			}
			
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				//System.out.println("�޸�״̬ʧ��id:"+tmpDtos.id);
			}
		});
 	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
  		msgNoticeAdapter = new MsgNoticelistAdapter(getActivity());
 		mXListView.setAdapter(msgNoticeAdapter);
 	}

	@Override
	public void onRefresh()
	{
		//new LoadDatasTask().execute(LOAD_REFREASH);
		currentPage=1;
		//LoadDate(mAdapter.getMaxId(),true,10);
		mhandler.postDelayed(new Runnable(){
	 		   @Override
				public void run() {
 	 			   LoadDate("",false,10);
				}
	    }, 1000);
 	}

	@Override
	public void onLoadMore()
	{
		currentPage++;
		LoadDate(msgNoticeAdapter.getMinId()+"",false,10);
	}
	
	@Override 
	public void onFirstUserVisible() {
		//LoadDate("",false,10);
		if(AppSetting.curUser!=null){
		   mXListView.startRefresh();
		   layoutnoTip.setVisibility(View.GONE);
		}
    }
  
	@Override 
	public void onUserVisible(){
		
	   layoutnoTip.setVisibility(AppSetting.curUser==null ? View.VISIBLE:View.GONE);
	//	CommonUtil.showToast("onUserVisible",this.getActivity());
	}
	
	private void LoadDate(String idx,final boolean isforward,int pagesize)
	{
  	    //String url="http://172.17.32.18:8080/message/queryMessage?receiverUid=1000000318&limit=10";
		
	   if(AppSetting.curUser==null) {
 		   CommonUtil.showToast("��ǰ�û���ϢΪ��",this.getActivity());
		   return;
	   } 
       String url=String.format("message/queryMessage?receiverUid=%1$s&limit=%2$d", AppSetting.curUser.uid,pagesize);
  	   if(!TextUtils.isEmpty(idx)) url+="&maxId="+idx;
  
       CommonUtil.InfoLog("url", url);
       ZrbRestClient.get(url, null,  new AsyncHttpResponseHandler() {
  			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				//loadingview.HideText("������ݳ��?").ShowRefleshBtn(true).ShowError();
  				CommonUtil.showToast("������ݳ���", getActivity());
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
 				//loadingview.Hide();
 				String results=new String(arg2);
 			//	CommonUtil.InfoLog("test", results);
				try 
				{
 			        Gson gson = new Gson();  
 			        MsgNoticeDtos tmpDto=	gson.fromJson(results, MsgNoticeDtos.class);
			        if(currentPage==1) {
 			        	msgNoticeAdapter.setDatalist(tmpDto.data);
			        }
 			        else
 			        	msgNoticeAdapter.addAll(tmpDto.data);
 
			        msgNoticeAdapter.notifyDataSetChanged();
					//initNotify(tmpDto.data.size());
     			}
				catch(Exception e){
 				    
					System.out.println("eeee!!!!"+e.getMessage());
 				}
				CommonUtil.setRefreashTime(getActivity(), newsType);
				// ����ˢ��ʱ��
				//mXListView.setRefreshTime(CommonUtil.getRefreashTime(getActivity(), newsType));
				mXListView.stopRefresh();
				mXListView.stopLoadMore();
 			}});
 	}
 

	public void refleshUI(){
		   mXListView.startRefresh();
		   layoutnoTip.setVisibility(View.GONE);
 	}
  
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
  			 Intent it2 = new Intent(this.getActivity(), User_phone_login.class);
 			 it2.putExtra("forResult", true);
			 this.getActivity().startActivityForResult(it2, 234);
	  
	}
}
