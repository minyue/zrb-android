package com.zrb.mobile;

 

import com.zrb.mobile.anim.Techniques;
import com.zrb.mobile.anim.YoYo;
import com.zrb.mobile.register.INotifyEvent;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
 

import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RegisterBaseActivity  extends BaseFragmentActivity implements  INotifyEvent{

    private RelativeLayout notify_view;
  	private TextView notify_view_text;
  	 Handler mhandler = new Handler();
  	private boolean isTiping=false;
  	Tip progTip;
  	
  	protected Handler getHandler(){
  		
  		return mhandler;
  	}
  	
  	
  	protected void initView(){
     	//Toast提示框
		notify_view = (RelativeLayout)this.findViewById(R.id.notify_view);
		notify_view_text = (TextView)this.findViewById(R.id.notify_view_text);
  	}
  	
  	 @Override     
  	public void showloading(String loadingtxt) {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", loadingtxt, true,
					true);
		else
			progTip.Show(loadingtxt);
	}
  	
  	 @Override     
  	public void hideloading(){
  		
  		if (progTip!= null) progTip.Dismiss();
  	}
  	
    @Override     
  	public void initNotify(final String msg) {
  		 
  		if(isTiping) return;
  		isTiping=true;
		notify_view_text.setText(msg);
		notify_view.setVisibility(View.INVISIBLE);
		mhandler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				notify_view.setVisibility(View.VISIBLE);
				  YoYo.with(Techniques.BounceIn)
				   .duration(300)
				   .playOn(notify_view);
				mhandler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						notify_view.setVisibility(View.GONE);
						isTiping=false;
					}
				}, 3000);
			}
		}, 10);
	}

}
