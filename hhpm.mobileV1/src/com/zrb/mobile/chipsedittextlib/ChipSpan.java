package com.zrb.mobile.chipsedittextlib;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

class ChipSpan extends ImageSpan {
    public ChipSpan(Context context, int viewResource, int textViewId, int imageViewId, Uri iconUri, CharSequence text) {
        super(context, createBitmap(context, viewResource, textViewId, imageViewId, iconUri, text));
    }

    private static Bitmap createBitmap(Context context, int viewResource, int textViewId,
                                       int imageViewId, Uri iconUri, CharSequence text) {
        LayoutInflater inflater
                = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(viewResource, null);
        ((TextView) view.findViewById(textViewId)).setText(text);
        ((ImageView) view.findViewById(imageViewId)).setImageURI(iconUri);

        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);

        // Viewの描画サイズを計算する
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        // Viewの描画結果を取得するためBitmap/Canvasを用意する
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(canvas);
        view.setDrawingCacheEnabled(true);

        // 描画結果を新しいBitmapとしてコピー
        Bitmap viewBmp = view.getDrawingCache().copy(Bitmap.Config.ARGB_8888, true);
        view.destroyDrawingCache();
        return viewBmp;
    }
}


/*
SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();

//文字の指定
for (int i = 0; i < selectedItems.size(); i++) {
 spannableStringBuilder.append(selectedItems.get(i).getDisplayName());
 spannableStringBuilder.append(delimiter);
}

//装飾の指定
int cursorIndex = 0;
for(ChipItem item : selectedItems){
 String chipText = item.getDisplayName();
 spannableStringBuilder.setSpan(
         new ChipSpan(textView.getContext(), R.layout.default_chip_item,
                 R.id.chip_item_name, R.id.chip_item_icon,
                 item.getIconUri(), chipText),
         cursorIndex, cursorIndex + chipText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
 cursorIndex = cursorIndex + chipText.length() + delimiter.length();
}

textView.setText(spannableStringBuilder);*/



