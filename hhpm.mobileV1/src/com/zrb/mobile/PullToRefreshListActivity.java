/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.zrb.mobile;

import java.util.Arrays;
import java.util.LinkedList;

import org.apache.http.Header;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
 
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.R;
 
import com.zrb.mobile.adapter.MenuListAdapter;
import com.zrb.mobile.adapter.TopAdsPagerAdapter;
import com.zrb.mobile.adapter.model.AdsDtoResult;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.ArticleDtos;
import com.zrb.mobile.adapter.model.ProjectDto;
import com.zrb.mobile.adapter.model.ProjectDtos;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;
 

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

public final class PullToRefreshListActivity extends FragmentActivity implements OnClickListener {

	static final int MENU_MANUAL_REFRESH = 0;
	static final int MENU_DISABLE_SCROLL = 1;
	static final int MENU_SET_MODE = 2;
	static final int MENU_DEMO = 3;

	private LinkedList<String> mListItems;
	private PullToRefreshListView mPullRefreshListView;
	private ArrayAdapter<String> mAdapter;
	MenuListAdapter listsAdapter;
	int Pagesize=15;
	int pageindex=1;
	
	CustomloadingView loadingview;
	int currentType;
	String curTitle;

	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ptr_list);
		
		currentType= this.getIntent().getIntExtra("type", 0);
		curTitle= this.getIntent().getStringExtra("title");
		
		InitView();
		LoadData();
		//LoadAds();
	}

	
	private void InitView()
	{
		
		this.findViewById(R.id.title_left_root).setOnClickListener(this);

		TextView titleview = (TextView) this.findViewById(R.id.titleText);
		titleview.setText(curTitle);
		
		loadingview=(CustomloadingView)this.findViewById(R.id.list_emptyview);
		loadingview.SetOnRefleshListener(this);
	    mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
 		
		// Set a listener to be invoked when the list should be refreshed.
		mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				// Do work to refresh the list here.
				//new GetDataTask().execute();
				pageindex++;
				LoadData();
			}
		});
	
		// Add an end-of-list listener
/*		mPullRefreshListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				Toast.makeText(PullToRefreshListActivity.this, "End of List!", Toast.LENGTH_SHORT).show();
			}
		});
*/
		ListView actualListView = mPullRefreshListView.getRefreshableView();

		
		 
		// Need to use the Actual ListView when registering for Context Menu
		//registerForContextMenu(actualListView);
		
 		listsAdapter=new MenuListAdapter(this);
		/**
		 * Add Sound Event Listener
		 */
	/*	SoundPullEventListener<ListView> soundListener = new SoundPullEventListener<ListView>(this);
		soundListener.addSoundEvent(State.PULL_TO_REFRESH, R.raw.pull_event);
		soundListener.addSoundEvent(State.RESET, R.raw.reset_sound);
		soundListener.addSoundEvent(State.REFRESHING, R.raw.refreshing_sound);
		mPullRefreshListView.setOnPullEventListener(soundListener);*/

		// You can also just use setListAdapter(mAdapter) or
		// mPullRefreshListView.setAdapter(mAdapter)
 		
 		
 		
 		LayoutInflater inflater = LayoutInflater.from(this);
        View  _bannerView = inflater.inflate(R.layout.ads_banner_fragment, null);
         
        actualListView.addHeaderView(_bannerView);
        
		actualListView.setAdapter(listsAdapter);
		actualListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				 
				
			  	   CommonUtil.showToast(arg2+"___",  PullToRefreshListActivity.this);
			  	   
			  	   
				   Intent it = new Intent(PullToRefreshListActivity.this, Webview_Activity.class);
					 
				    ArticleDto  tmpDto=listsAdapter.CurrentDatas.get(arg2-1);
			     	 String url="file:///android_asset/www/detail.html?from=listpage&type="+currentType+"&id="+tmpDto.id;
			     	 
				 	it.putExtra("url", url );
				 	it.putExtra("title",tmpDto.title);
					startActivity(it);
				
				
			}} );
		
		
	}
	
	
	
	 private void LoadData() {
	    	//progTip=ProgressDialogEx.Show(this," title", "loadtxt", true, true);
		    String parmers="?type="+currentType+"&pageSize="+Pagesize+"&pageNo="+pageindex;
	    	ZrbRestClient.get("bu.jspx"+parmers, null,  new AsyncHttpResponseHandler()
	    	{

				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					//System.out.println("ff");
					loadingview.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
				}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
 
					loadingview.Hide();
					mPullRefreshListView.onRefreshComplete();
				 	
 					String results=new String(arg2);
					try 
					{
 					    GsonBuilder gsonb = new GsonBuilder();  
 			            Gson gson = gsonb.create();  
 			          //  Type type=new TypeToken<List<QxybResultDTO>>(){}.getType();
 			            ArticleDtos tmpDto=	gson.fromJson(results, ArticleDtos.class);
 			            if(tmpDto.results!=null&&tmpDto.results.size()>0)
 			            {
	 		              listsAdapter.CurrentDatas.addAll(tmpDto.results);
			              listsAdapter.notifyDataSetChanged();
 			            }
 			            else
 			            	 CommonUtil.showToast("暂无更多的数据！", PullToRefreshListActivity.this);
			            
					}
					catch(Exception e)
					{
 						System.out.println("eeee!!!!"+e.getMessage());
 					}
					
				}});
 	    }
 	
	private class GetDataTask extends AsyncTask<Void, Void, String[]> {

		@Override
		protected String[] doInBackground(Void... params) {
			// Simulates a background job.
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
			//return mStrings;
			return new String[]{"",""};
		}

		@Override
		protected void onPostExecute(String[] result) {
			mListItems.addFirst("Added after refresh...");
			mAdapter.notifyDataSetChanged();

			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			super.onPostExecute(result);
		}
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_MANUAL_REFRESH, 0, "Manual Refresh");
		menu.add(0, MENU_DISABLE_SCROLL, 1,
				mPullRefreshListView.isScrollingWhileRefreshingEnabled() ? "Disable Scrolling while Refreshing"
						: "Enable Scrolling while Refreshing");
		menu.add(0, MENU_SET_MODE, 0, mPullRefreshListView.getMode() == Mode.BOTH ? "Change to MODE_PULL_DOWN"
				: "Change to MODE_PULL_BOTH");
		menu.add(0, MENU_DEMO, 0, "Demo");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;

		menu.setHeaderTitle("Item: " + getListView().getItemAtPosition(info.position));
		menu.add("Item 1");
		menu.add("Item 2");
		menu.add("Item 3");
		menu.add("Item 4");

		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem disableItem = menu.findItem(MENU_DISABLE_SCROLL);
		disableItem
				.setTitle(mPullRefreshListView.isScrollingWhileRefreshingEnabled() ? "Disable Scrolling while Refreshing"
						: "Enable Scrolling while Refreshing");

		MenuItem setModeItem = menu.findItem(MENU_SET_MODE);
		setModeItem.setTitle(mPullRefreshListView.getMode() == Mode.BOTH ? "Change to MODE_FROM_START"
				: "Change to MODE_PULL_BOTH");

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case MENU_MANUAL_REFRESH:
				new GetDataTask().execute();
				mPullRefreshListView.setRefreshing(false);
				break;
			case MENU_DISABLE_SCROLL:
				mPullRefreshListView.setScrollingWhileRefreshingEnabled(!mPullRefreshListView
						.isScrollingWhileRefreshingEnabled());
				break;
			case MENU_SET_MODE:
				mPullRefreshListView.setMode(mPullRefreshListView.getMode() == Mode.BOTH ? Mode.PULL_FROM_START
						: Mode.BOTH);
				break;
			case MENU_DEMO:
				mPullRefreshListView.demo();
				break;
		}

		return super.onOptionsItemSelected(item);
	}*/


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.btn_reflesh:
			loadingview.HideText("努力加载中...").ShowLoading();
			LoadData();
			break;
		
		}
	}
}
