package com.zrb.mobile;

 

import com.zrb.mobile.adapter.AppSharelistAdapter;
import com.zrb.mobile.adapter.RadioItemEx;
import com.zrb.mobile.ui.NumEditText;
import com.zrb.mobile.ui.NumEditText.OnNumslistener;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

public class App_shareActivity  extends BaseFragmentActivity implements OnClickListener    {

 public  FragmentManager fm;  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	 
    	setContentView(R.layout.activity_appshare);
 		InitView();
  	}
	
	private void InitView() {

		TextView txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setText("分享招融宝");

		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);

		fm = getSupportFragmentManager();
		// 只當容器，主要內容已Fragment呈現
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.share_container, new App_shareFragment());
		ft.commit();

	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
 		   case R.id.title_left_root:
 			  this.finish();
			break;
		}
	}
	
}
