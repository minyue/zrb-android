package com.zrb.mobile;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.applib.utils.LocalUserInfo;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.adapter.model.UserbaseInfoDto;
import com.zrb.mobile.adapter.model.UserbaseinfoDtos;
import com.zrb.mobile.common.LoadUserAvatar;
import com.zrb.mobile.common.LoadUserAvatar.ImageDownloadedCallBack;
import com.zrb.mobile.ui.CircleImageView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

public class User_my_profiefragment extends Fragment implements OnClickListener {

	View RootView;
	LinearLayout myinfoView;
	private String imageName;
	View loginbtn, bottomLayout, logoview;
	private static final int PHOTO_REQUEST_TAKEPHOTO = 1;// 拍照
	private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
	private static final int PHOTO_REQUEST_CUT = 3;// 结果
    private String oldpwd;
	
	
	public enum ActionEnum {
		saveIndustry, checkPwd, getUserinfo, updateAvatar;
	}
	ZrbRestClient mZrbRestClient;
	UserbaseInfoDto curUserinfo;
	Dialog CheckPwdDialog, AuthDialog, ErrorpwdDialog;
	Tip progTip;
	private LoadUserAvatar avatarLoader;
	ProgressBar toploading;

	CircleImageView iv_avatar;

	ActionEnum tmpAction;
	
	@Override
	public void  onCreate( Bundle savedInstanceState){
	 	super.onCreate(savedInstanceState);
        if(null==mZrbRestClient){
			
			mZrbRestClient=new ZrbRestClient(this.getActivity());
			mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
					// loadingview.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
					switch(tmpAction){
					  case getUserinfo:
						toploading.setVisibility(View.GONE);
						showToast("加载数据出错！");
						break;
					  case saveIndustry:
						  showToast("保存失败");
						  break;
					  case checkPwd:
						progTip.Dismiss();
						showToast("加载数据出错！");
						break;
					  case updateAvatar:
						  progTip.Dismiss();
						showToast("头像上传失败");
						break;
	 
					}
				}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// loadingview.Hide();
					String results = new String(arg2);
					switch(tmpAction){
					  case getUserinfo:
						showInfo(results);
						break;
					  case saveIndustry:
						  try {
								Gson gson = new Gson();
								RegisterDto tmpDto = gson.fromJson(results, RegisterDto.class);
								if (tmpDto.res == 1) {
									showToast("保存成功");
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						  break;
					  case checkPwd:
						    progTip.Dismiss();
	 						try {
								Gson gson = new Gson();
								RegisterDto tmpDto = gson.fromJson(results,RegisterDto.class);
								if (tmpDto.res == 1) {
									Intent it2 = new Intent(User_my_profiefragment.this.getActivity(),
											                User_change_Password.class);
									it2.putExtra("changepwd", true);
									it2.putExtra("oldpassword", oldpwd);
									User_my_profiefragment.this.startActivityForResult(it2, 101);
								} else
									showPwdError();
							} catch (Exception e) {
								// loadingview.HideText(e.getMessage()).ShowRefleshBtn(true).ShowError();
								showToast("加载数据出错！");
							}
							break;
					  case updateAvatar:
	 						  progTip.Dismiss();
   						     LocalUserInfo.getInstance(User_my_profiefragment.this.getActivity())
									       .setUseravatar(LoadUserAvatar.AvatarDir + "/"+ imageName);
				 
							showToast("头像上传成功");
						  break;
					}
				}
			});
		}
	 	
	}
	
	
	private void LoadUserinfo() {
  		tmpAction=ActionEnum.getUserinfo;
 		mZrbRestClient.sessionGet("user/doUserInfo?time=1");
	}
	
   private void saveIndustry(String name, String code) {
		
	    tmpAction=ActionEnum.saveIndustry;
		RequestParams param = new RequestParams();
		param.put("industry", name);
		param.put("industryId", code);
	 	mZrbRestClient.post("user/updateAppUserInfo", param);
 	}
	
   private void checkpwd(String curOldpwd) {
	   
	    tmpAction=ActionEnum.checkPwd;
 		oldpwd=curOldpwd;
		RequestParams param = new RequestParams();
  		param.put("passWord", oldpwd);
		showloading();
 		mZrbRestClient.post("user/checkPWord", param);
	}
   

	private void updateAvatarInServer( String imagename)   {
 		File file = new File(LoadUserAvatar.getAvatarpath(), imagename);
		if (file.exists() && file.length() > 0) {
		 
			tmpAction=ActionEnum.updateAvatar;
			RequestParams params = new RequestParams();
			try {
				params.put("uploadExcel", file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			showloading();
  			mZrbRestClient.post("avatarApi/fileUpload", params );
		} else {
			showToast("文件不存在");
		}
	}
   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// return inflater.inflate(R.layout.u_center_registerstep1, null);
      
		View view = inflater
				.inflate(R.layout.u_center_myinfo, container, false);
		RootView = view;
		initView();
		return RootView;
	}

	private void initView() {

		TextView txtview = (TextView) RootView.findViewById(R.id.txt_title);
		txtview.setText("个人中心");

		toploading = (ProgressBar) RootView.findViewById(R.id.topbar_loading);
		toploading.setVisibility(View.VISIBLE);
		// ((TextView)RootView.findViewById(R.id.tv_enterprise)).setText(TextUtils.isEmpty(AppSetting.curUser.org)
		// ? "未认证":AppSetting.curUser.org);
		// if( null==AppSetting.curUser.org || "".equals(AppSetting.curUser.org)
		// ||"null".equals(AppSetting.curUser.org)){
		// ((TextView)RootView.findViewById(R.id.tv_enterprise)).setText("未认证");
		// }else{
		// ((TextView)RootView.findViewById(R.id.tv_enterprise)).setText(AppSetting.curUser.org);
		// }

		// ((TextView)RootView.findViewById(R.id.tv_temp_enterprise)).setText(AppSetting.curUser.userType.equals(Constants.InvUserType+"")
		// ?"企业认证":"机构认证");
		// ((TextView)RootView.findViewById(R.id.tv_name)).setText(AppSetting.curUser.uname);

		RootView.findViewById(R.id.title_left_root).setOnClickListener(this);
		RootView.findViewById(R.id.re_avatar).setOnClickListener(this);

		// RootView.findViewById(R.id.re_enterprise).setOnClickListener(this);
		// RootView.findViewById(R.id.re_name).setOnClickListener(this);
		RootView.findViewById(R.id.re_phone).setOnClickListener(this);
		RootView.findViewById(R.id.re_password).setOnClickListener(this);
		RootView.findViewById(R.id.re_email).setOnClickListener(this);
		RootView.findViewById(R.id.re_truename).setOnClickListener(this);

		((TextView) RootView.findViewById(R.id.tv_companyname))
				.setText(TextUtils.isEmpty(AppSetting.curUser.org) ? ""
						: AppSetting.curUser.org);
		((TextView) RootView.findViewById(R.id.tv_companyjob))
				.setText(TextUtils.isEmpty(AppSetting.curUser.position) ? ""
						: AppSetting.curUser.position);
		((TextView) RootView.findViewById(R.id.tv_trade)).setText(TextUtils
				.isEmpty(AppSetting.curUser.industry) ? ""
				: AppSetting.curUser.industry);

		RootView.findViewById(R.id.re_companyname).setOnClickListener(this);
		RootView.findViewById(R.id.re_companyjob).setOnClickListener(this);
		RootView.findViewById(R.id.re_trade).setOnClickListener(this);

		// RootView.findViewById(R.id.re_idcard).setOnClickListener(this);

		RootView.findViewById(R.id.re_area).setOnClickListener(this);
		RootView.findViewById(R.id.re_instruction).setOnClickListener(this);
		iv_avatar = (CircleImageView) RootView.findViewById(R.id.iv_avatar);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// mNewsItemDao = new NewsItemDao(getActivity());
		avatarLoader = new LoadUserAvatar(getActivity());
		String avatar = LocalUserInfo.getInstance(getActivity()).getUseravatar();
		showUserAvatar(iv_avatar, avatar);
		// CommonUtil.showToast("results",
		// User_my_profiefragment.this.getActivity());
		LoadUserinfo();
	}

	private void showUserAvatar(ImageView iamgeView, String avatar) {

		if (TextUtils.isEmpty(avatar))
			return;
		final String url_avatar = AppSetting.BASE_URL + avatar;
		iamgeView.setTag(url_avatar);
		if (url_avatar != null && !url_avatar.equals("")) {
			Bitmap bitmap = avatarLoader.loadImage(iamgeView, url_avatar,
					new ImageDownloadedCallBack() {

						@Override
						public void onImageDownloaded(ImageView imageView,
								Bitmap bitmap) {
							if (imageView.getTag() == url_avatar) {
								imageView.setImageBitmap(bitmap);
							}
						}
					});
			if (bitmap != null)
				iamgeView.setImageBitmap(bitmap);

		}
	}

	private void showToast(String msg) {
		if (User_my_profiefragment.this.getActivity() != null)
			CommonUtil
					.showToast(msg, User_my_profiefragment.this.getActivity());
	}
 

	private void showInfo(String results) {

		toploading.setVisibility(View.GONE);
 		CommonUtil.InfoLog("tag", results);
 		try {
			Gson gson = new Gson();
			UserbaseinfoDtos tmpDto = gson.fromJson(results, UserbaseinfoDtos.class);
			if (tmpDto.res == 1){
				curUserinfo = tmpDto.data;
				((TextView) RootView.findViewById(R.id.tv_phone))
				                    .setText(curUserinfo.uMobile);
				((TextView) RootView.findViewById(R.id.tv_email))
				                    .setText(TextUtils.isEmpty(curUserinfo.uEmail) ? "未绑定" : curUserinfo.uEmail);
				((TextView) RootView.findViewById(R.id.tv_truename))
				                    .setText(TextUtils.isEmpty(curUserinfo.realname) ? "未填写" : curUserinfo.realname);
  				((TextView) RootView.findViewById(R.id.tv_area))
				                    .setText(TextUtils.isEmpty(curUserinfo.LOCATIONNAME) ? "未填写" : curUserinfo.LOCATIONNAME);
				((TextView) RootView.findViewById(R.id.tv_instruction))
				                    .setText(TextUtils.isEmpty(curUserinfo.INFO) ? "未填写": curUserinfo.INFO);
				
			}
			else
				showToast(tmpDto.msg);
		} catch (Exception e) {
			// loadingview.HideText(e.getMessage()).ShowRefleshBtn(true).ShowError();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		// if (this.getUserVisibleHint()) showRelateview();
	}

	private void changename() {

		FragmentManager fragmentManager = this.getFragmentManager();// .getSupportFragmentManager();//
																	// getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		/*
		 * transaction.setCustomAnimations(R.anim.right_in, R.anim.left_fadeout,
		 * R.anim.right_fadein, R.anim.left_fadeout);
		 */
		/*
		 * transaction.setCustomAnimations(R.anim.fragment_slide_in, 0, 0,
		 * R.anim.fragment_slide_out);
		 */
		transaction.setCustomAnimations(R.anim.slide_out_right, 0, 0,
				R.anim.slide_in_left);

		User_changenamefragment newFragment = new User_changenamefragment();
		// User_my_profiefragment newFragment=new User_my_profiefragment();
		transaction.replace(R.id.fragment_container, newFragment);
		transaction.addToBackStack(null);
		transaction.commit();
	}

	private void changeinfo(int curType, String userinfo, int requestcode) {
		Intent it8 = new Intent(this.getActivity(), User_my_changeBaseInfo.class);
		it8.putExtra("curType", curType);
		it8.putExtra("userInfo", userinfo);
		this.startActivityForResult(it8, requestcode);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.txt_findpwd:
			Intent it9 = new Intent(this.getActivity(), User_forget_Password.class);
			this.startActivity(it9);
			break;
		case R.id.title_left_root:
			this.getActivity().finish();
			break;
  		case R.id.re_avatar:
			showPhotoDialog();
			break;
		case R.id.re_phone:
		/*	if (curUserinfo == null) {
				showToast("当前用户信息为空");
				return;
			}
			Intent it = new Intent(this.getActivity(), User_registerphone.class);
			it.putExtra("changephone", true);
			it.putExtra("phone", curUserinfo.uMobile);
			this.startActivityForResult(it, 100);*/
			break;
		case R.id.re_password:
			showCheckpwd();
			break;
		case R.id.re_email:
			if (curUserinfo == null) {
				showToast("当前用户信息为空");
				return;
			}
			changeinfo(1, curUserinfo.uEmail, 102);
			break;
		case R.id.re_truename:
			if (curUserinfo == null) {
				showToast("当前用户信息为空");
				return;
			}
			changeinfo(2, curUserinfo.realname, 103);
			break;

		case R.id.re_companyname:
			if (curUserinfo == null) {
				showToast("当前用户信息为空");
				return;
			}
			changeinfo(6, curUserinfo.org, 107);
			break;

		case R.id.re_companyjob:
			if (curUserinfo == null) {
				showToast("当前用户信息为空");
				return;
			}
			changeinfo(5, AppSetting.curUser.position, 108);
			break;

		case R.id.re_trade:
			if (curUserinfo == null) {
				showToast("当前用户信息为空");
				return;
			}
			Intent ittrade = new Intent(this.getActivity(),
					User_registerIndustry.class);
			ittrade.putExtra("isProject", true);
			this.startActivityForResult(ittrade, 110);
			break;
		// case R.id.re_idcard:
		// if(curUserinfo==null){ showToast("当前用户信息为空"); return;}
		// changeinfo(3,curUserinfo.idCard,104);
		// break;
		case R.id.re_area:
			Intent it6 = new Intent(this.getActivity(),
					User_locationActivity.class);
			// Intent it6 = new Intent(this.getActivity(),
			// MultyLocationActivity.class);
			it6.putExtra("isucenter", true);
			this.startActivityForResult(it6, 105);

			break;
		case R.id.re_instruction:
			if (curUserinfo == null) {
				showToast("当前用户信息为空");
				return;
			}
			changeinfo(4, curUserinfo.INFO, 106);
			break;

		}
	}
 

	private void showCheckpwd() {

		DialogBuilder builder2 = new DialogBuilder(
				User_my_profiefragment.this.getActivity());
		builder2.setParentlayout(R.layout.custom_ucenter_dialog);
		builder2.setContentView(R.layout.u_center_verify_oldpassword); // ac_dialog_cancel;
		final EditText pwdedit = (EditText) builder2.getCurContentView()
				.findViewById(R.id.pwd_editText);

		builder2.setCloseOnclick(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (pwdedit.getText() == null
						|| pwdedit.getText().toString() == "") {
					CommonUtil.showToast("请输入密码！",
							User_my_profiefragment.this.getActivity());
					return;
				}
				CheckPwdDialog.dismiss();
				if (arg0.getId() == R.id.btn_confirm) {
					checkpwd(pwdedit.getText().toString());
				}
			}
		});
		CheckPwdDialog = builder2.create();
		CheckPwdDialog.show();
	}

	private void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this.getActivity(), " title",
					"正在提交中...", true, true);
		else
			progTip.Show("正在提交中...");
	}

	private void showPwdError() {

		DialogBuilder builder2 = new DialogBuilder(
				User_my_profiefragment.this.getActivity());
		builder2.setParentlayout(R.layout.custom_ucenter_dialog2);
		builder2.setContentView(R.layout.u_center_verify_error); // ac_dialog_cancel;

		builder2.getCurContentView().findViewById(R.id.txt_findpwd)
				.setOnClickListener(this);
		builder2.setCloseOnclick(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				ErrorpwdDialog.dismiss();
				if (arg0.getId() == R.id.btn_confirm) {
					showCheckpwd();
				}
			}
		});
		ErrorpwdDialog = builder2.create();
		ErrorpwdDialog.show();
	}

	

	private void showPhotoDialog() {
		final AlertDialog dlg = new AlertDialog.Builder(this.getActivity())
				.create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,shrew_exit_dialog.xml文件中定义view内容
		window.setContentView(R.layout.alertdialog);
		// 为确认按钮添加事件,执行退出应用操作
		TextView tv_paizhao = (TextView) window.findViewById(R.id.tv_content1);
		tv_paizhao.setText("拍照");
		tv_paizhao.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				imageName = CommonUtil.getNowTime() + ".png";
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				// 指定调用相机拍照后照片的储存路径
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
						LoadUserAvatar.getAvatarpath(), imageName)));
				startActivityForResult(intent, PHOTO_REQUEST_TAKEPHOTO);
				dlg.cancel();
			}
		});
		TextView tv_xiangce = (TextView) window.findViewById(R.id.tv_content2);
		tv_xiangce.setText("相册");
		tv_xiangce.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				// getNowTime();
				imageName = CommonUtil.getNowTime() + ".png";
				/*
				 * Intent intent = new Intent(Intent.ACTION_PICK, null);
				 * intent.setDataAndType(
				 * MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				 * startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
				 */

				Intent intent = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, PHOTO_REQUEST_GALLERY);

				dlg.cancel();
			}
		});
	}

	@SuppressLint("SdCardPath")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == android.app.Activity.RESULT_OK) {
			switch (requestCode) {
			case PHOTO_REQUEST_TAKEPHOTO:

				Intent it = new Intent(
						User_my_profiefragment.this.getActivity(),
						User_clipimgaeActivity.class);
				// Intent it = new Intent(Splash_Activity.this,
				// HomeActivity.class);
				it.putExtra("filename", LoadUserAvatar.getAvatarpath() + "/"
						+ imageName);
				it.putExtra("imagename", imageName);
				startActivityForResult(it, PHOTO_REQUEST_CUT);

				// startPhotoZoom( Uri.fromFile(new
				// File(LoadUserAvatar.getAvatarpath(), imageName)), 120);
				break;

			case PHOTO_REQUEST_GALLERY:
				/*
				 * data有两种方式，除了这种方式之外，还有一种方式是返回Uri，而不是返回bitmap。因此，可以对返回的数据作出判断，分别处理
				 */
				if (data != null) {
					if (data.getData() != null) {
						Bundle extras = data.getExtras();
						if (null != extras) {
							CommonUtil.InfoLog("getPath1", data.getData().getPath());
							Intent it2 = new Intent(getActivity(),User_clipimgaeActivity.class);
							String path = CommonUtil.getImagebyUri(data.getData(),getActivity());
							if (TextUtils.isEmpty(path)) {
								CommonUtil.showToast("返回路径为空",User_my_profiefragment.this.getActivity());
								return;
							}
							it2.putExtra("filename", path);
							it2.putExtra("imagename", imageName);
							startActivityForResult(it2, PHOTO_REQUEST_CUT);
						} else {
							Uri uri = data.getData();
							if (uri != null) {
								CommonUtil.InfoLog("getPath2",getImageAbsolutePath(getActivity(), uri));
								Intent it2 = new Intent(getActivity(),User_clipimgaeActivity.class);
								if (TextUtils.isEmpty(uri.getPath())) {
									CommonUtil.showToast("返回路径为空", User_my_profiefragment.this.getActivity());
									return;
								}
								it2.putExtra("filename",getImageAbsolutePath(getActivity(), uri));
								it2.putExtra("imagename", imageName);
								startActivityForResult(it2, PHOTO_REQUEST_CUT);

							}
						}

					}
				}
				break;

			case PHOTO_REQUEST_CUT:
				// BitmapFactory.Options options = new BitmapFactory.Options();
				//
				// /**
				// * 最关键在此，把options.inJustDecodeBounds = true;
				// * 这里再decodeFile()，返回的bitmap为空
				// * ，但此时调用options.outHeight时，已经包含了图片的高了
				// */
				// options.inJustDecodeBounds = true;
				Bitmap bitmap = BitmapFactory.decodeFile(LoadUserAvatar
						.getAvatarpath() + "/" + imageName);
				iv_avatar.setImageBitmap(bitmap);
				updateAvatarInServer(imageName);
				break;

			case 100:// phonenum
				String phonenum = data.getStringExtra("phonenum");
				curUserinfo.uMobile = phonenum;
				((TextView) RootView.findViewById(R.id.tv_phone))
						.setText(phonenum);
				break;

			case 110:// industry
				String name = data.getStringExtra("name");
				String code = String.valueOf(data.getIntExtra("code", -1));
				curUserinfo.industry = name;
				curUserinfo.industryId = code;
				LocalUserInfo.getInstance(getActivity()).setUserIndustry(name);
				((TextView) RootView.findViewById(R.id.tv_trade)).setText(name);
				saveIndustry(name, code);
				break;

			case 102:// re_email
				String curvalue = data.getStringExtra("newvalue");
				curUserinfo.uEmail = curvalue;
				LocalUserInfo.getInstance(getActivity()).setUserEmail(curvalue);

				((TextView) RootView.findViewById(R.id.tv_email))
						.setText(curvalue);
				break;
			case 103:// tv_truename
				String curvalue1 = data.getStringExtra("newvalue");
				curUserinfo.realname = curvalue1;
				LocalUserInfo.getInstance(getActivity()).setUserName(curvalue1);
				((TextView) RootView.findViewById(R.id.tv_truename))
						.setText(curvalue1);
				break;

			case 107:// tv_companyname
				String curvalue2 = data.getStringExtra("newvalue");
				curUserinfo.org = curvalue2;
				LocalUserInfo.getInstance(getActivity()).setUserCompany(
						curvalue2);
				((TextView) RootView.findViewById(R.id.tv_companyname))
						.setText(curvalue2);
				break;

			case 108:// tv_companyjob
				String curvalue4 = data.getStringExtra("newvalue");
				curUserinfo.companyjob = curvalue4;
				LocalUserInfo.getInstance(getActivity()).setUserPosition(
						curvalue4);
				((TextView) RootView.findViewById(R.id.tv_companyjob))
						.setText(curvalue4);
				break;
			// case 104://tv_idcard
			// String curvalue2=data.getStringExtra("newvalue");
			// curUserinfo.idCard=curvalue2;
			// ((TextView)RootView.findViewById(R.id.tv_idcard)).setText(curvalue2);
			// break;
			case 105:// tv_area
				String curArea = data.getStringExtra("area");
				String curCode = data.getStringExtra("code");

				curUserinfo.LOCATION = curCode;
				curUserinfo.LOCATIONNAME = curArea;
				((TextView) RootView.findViewById(R.id.tv_area))
						.setText(curArea);
				break;
			case 106:// tv_instruction
				String curvalue3 = data.getStringExtra("newvalue");
				curUserinfo.INFO = curvalue3;
				((TextView) RootView.findViewById(R.id.tv_instruction))
						.setText(curvalue3);
				break;
			}
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	
 

	/**
	 * 根据Uri获取图片绝对路径，解决Android4.4以上版本Uri转换
	 * 
	 * @param activity
	 * @param imageUri
	 * @author yaoxing
	 * @date 2014-10-12
	 */
	@TargetApi(19)
	public static String getImageAbsolutePath(Activity context, Uri imageUri) {
		if (context == null || imageUri == null)
			return null;
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT
				&& DocumentsContract.isDocumentUri(context, imageUri)) {
			if (isExternalStorageDocument(imageUri)) {
				String docId = DocumentsContract.getDocumentId(imageUri);
				String[] split = docId.split(":");
				String type = split[0];
				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/"
							+ split[1];
				}
			} else if (isDownloadsDocument(imageUri)) {
				String id = DocumentsContract.getDocumentId(imageUri);
				Uri contentUri = ContentUris.withAppendedId(
						Uri.parse("content://downloads/public_downloads"),
						Long.valueOf(id));
				return getDataColumn(context, contentUri, null, null);
			} else if (isMediaDocument(imageUri)) {
				String docId = DocumentsContract.getDocumentId(imageUri);
				String[] split = docId.split(":");
				String type = split[0];
				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}
				String selection = MediaStore.Images.Media._ID + "=?";
				String[] selectionArgs = new String[] { split[1] };
				return getDataColumn(context, contentUri, selection,
						selectionArgs);
			}
		} // MediaStore (and general)
		else if ("content".equalsIgnoreCase(imageUri.getScheme())) {
			// Return the remote address
			if (isGooglePhotosUri(imageUri))
				return imageUri.getLastPathSegment();
			return getDataColumn(context, imageUri, null, null);
		}
		// File
		else if ("file".equalsIgnoreCase(imageUri.getScheme())) {
			return imageUri.getPath();
		}
		return null;
	}

	public static String getDataColumn(Context context, Uri uri,
			String selection, String[] selectionArgs) {
		Cursor cursor = null;
		String column = MediaStore.Images.Media.DATA;
		String[] projection = { column };
		try {
			cursor = context.getContentResolver().query(uri, projection,
					selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				int index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is Google Photos.
	 */
	public static boolean isGooglePhotosUri(Uri uri) {
		return "com.google.android.apps.photos.content".equals(uri
				.getAuthority());
	}
}