package com.zrb.mobile;


import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.hhpm.lib.datepicker.OnWheelScrollListener;
import com.hhpm.lib.datepicker.WheelView;
import com.hhpm.lib.datepicker.adapter.NumericWheelAdapter;
 

 

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Date_pickerActivity extends Activity implements OnTouchListener, OnClickListener {
	
	private LayoutInflater inflater = null;
	private WheelView year;
	private WheelView month;
	private WheelView day;
	//private WheelView time;
	private WheelView min;
	private WheelView sec;
	
 	private int mbgYear=1996;
	private int mMonth=0;
	private int mDay=1;
	
	LinearLayout ll;
  	View view=null;
  	private String mCuryear;
  	private String mCurhour;
  	private String mCurmin;
	private int curYear,curMonth,curDate,curHour,curMin;
   	
	boolean isMonthSetted=false,isDaySetted=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.datepicker_activity);
		inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
		
		this.findViewById(R.id.empty_overlay).setOnTouchListener(this);
		this.findViewById(R.id.txt_confirm).setOnClickListener(this);
		this.findViewById(R.id.txt_cancel).setOnTouchListener(this);
		
		ll=(LinearLayout) findViewById(R.id.ll);
		ll.addView(getDataPick());
 	}
	
	private View getDataPick() {
		Calendar c = Calendar.getInstance();
		
		curYear = c.get(Calendar.YEAR);
	    curMonth = c.get(Calendar.MONTH) + 1;//通过Calendar算出的月数要+1
		curDate = c.get(Calendar.DATE);
 
		curHour=c.get(Calendar.HOUR_OF_DAY);
		curMin=c.get(Calendar.MINUTE);
 
		//int curYear = mYear;
		//int curMonth =mMonth+1;
		//int curDate = mDay;
		mbgYear=curYear-1;
		view = inflater.inflate(R.layout.wheel_date_picker, null);
		
		year = (WheelView) view.findViewById(R.id.year);
		NumericWheelAdapter numericWheelAdapter1=new NumericWheelAdapter(this,mbgYear, mbgYear+10); 
		numericWheelAdapter1.setLabel("年");
		year.setViewAdapter(numericWheelAdapter1);
		year.setCyclic(true);//是否可循环滑动
		year.addScrollingListener(scrollListener);
		
		month = (WheelView) view.findViewById(R.id.month);
		NumericWheelAdapter numericWheelAdapter2=new NumericWheelAdapter(this,1, 12, "%02d"); 
		numericWheelAdapter2.setLabel("月");
		month.setViewAdapter(numericWheelAdapter2);
		month.setCyclic(true);
		month.addScrollingListener(scrollListener);
		
		day = (WheelView) view.findViewById(R.id.day);
		initDay(curYear,curMonth);
		day.setCyclic(true);
		
//		time= (WheelView) view.findViewById(R.id.time);
//		String[] times = {"上午","下午"} ;
//		ArrayWheelAdapter<String> arrayWheelAdapter=new ArrayWheelAdapter<String>(MainActivity.this,times );
//		time.setViewAdapter(arrayWheelAdapter);
//		time.setCyclic(false);
//		time.addScrollingListener(scrollListener);
		
		min = (WheelView) view.findViewById(R.id.min);
		NumericWheelAdapter numericWheelAdapter3=new NumericWheelAdapter(this,1, 23, "%02d"); 
		numericWheelAdapter3.setLabel("时");
		min.setViewAdapter(numericWheelAdapter3);
		min.setCyclic(true);
		min.addScrollingListener(scrollListener);
		
		sec = (WheelView) view.findViewById(R.id.sec);
		NumericWheelAdapter numericWheelAdapter4=new NumericWheelAdapter(this,1, 59, "%02d"); 
		numericWheelAdapter4.setLabel("分");
		sec.setViewAdapter(numericWheelAdapter4);
		sec.setCyclic(true);
		sec.addScrollingListener(scrollListener);
		
		
		year.setVisibleItems(7);//设置显示行数
		month.setVisibleItems(7);
		day.setVisibleItems(7);
//		time.setVisibleItems(7);
		min.setVisibleItems(7);
		sec.setVisibleItems(7);
		
		year.setCurrentItem(curYear - mbgYear);
		month.setCurrentItem(curMonth - 1);
		day.setCurrentItem(curDate - 1);
		
		min.setCurrentItem(curHour - 1);
		sec.setCurrentItem(curMin - 1);
 		return view;
	}
	
	OnWheelScrollListener scrollListener = new OnWheelScrollListener() {
		@Override
		public void onScrollingStarted(WheelView wheel) {

		}

		@Override
		public void onScrollingFinished(WheelView wheel) {
			int n_year = year.getCurrentItem() + mbgYear;//年
			int n_month = month.getCurrentItem() + 1;//月
			
			initDay(n_year,n_month);
	 		mCuryear=new StringBuilder().append((year.getCurrentItem()+mbgYear)).append("-").append((month.getCurrentItem() + 1) < 10 ? "0" + (month.getCurrentItem() + 1) : (month.getCurrentItem() + 1)).append("-").append(((day.getCurrentItem()+1) < 10) ? "0" + (day.getCurrentItem()+1) : (day.getCurrentItem()+1)).toString();
		/*    tv1.setText("年龄             "+calculateDatePoor(birthday)+"岁");
			tv2.setText("星座             "+getAstro(month.getCurrentItem() + 1,day.getCurrentItem()+1));*/
			
			mCurhour=(min.getCurrentItem() + 1) < 10 ? "0" + (min.getCurrentItem() + 1):(min.getCurrentItem() + 1)+"";
			mCurmin=(sec.getCurrentItem() + 1) < 10 ? "0" + (sec.getCurrentItem() + 1):(sec.getCurrentItem() + 1)+"";
			 
		}
	};


	OnWheelScrollListener dayscrollListener = new OnWheelScrollListener() {
		@Override
		public void onScrollingStarted(WheelView wheel) {

		}

		@Override
		public void onScrollingFinished(WheelView wheel) {
			
			mCuryear=new StringBuilder().append((year.getCurrentItem()+mbgYear)).append("-").append((month.getCurrentItem() + 1) < 10 ? "0" + (month.getCurrentItem() + 1) : (month.getCurrentItem() + 1)).append("-").append(((day.getCurrentItem()+1) < 10) ? "0" + (day.getCurrentItem()+1) : (day.getCurrentItem()+1)).toString();
			mCurhour=(min.getCurrentItem() + 1) < 10 ? "0" + (min.getCurrentItem() + 1):(min.getCurrentItem() + 1)+"";
			mCurmin=(sec.getCurrentItem() + 1) < 10 ? "0" + (sec.getCurrentItem() + 1):(sec.getCurrentItem() + 1)+"";
 		}
	};
	
	/**
	 * 
	 * @param year
	 * @param month
	 * @return
	 */
	private int getDay(int year, int month) {
		int day = 30;
		boolean flag = false;
		switch (year % 4) {
		case 0:
			flag = true;
			break;
		default:
			flag = false;
			break;
		}
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			day = flag ? 29 : 28;
			break;
		default:
			day = 30;
			break;
		}
		return day;
	}

	/**
	 */
	private void initDay(int arg1, int arg2) {
		NumericWheelAdapter numericWheelAdapter=new NumericWheelAdapter(this,1, getDay(arg1, arg2), "%02d");
		numericWheelAdapter.setLabel("日");
		day.setViewAdapter(numericWheelAdapter);
		day.addScrollingListener(dayscrollListener);
 	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * 根据日期计算年龄
	 * @param birthday
	 * @return
	 */
	public static final String calculateDatePoor(String birthday) {
		try {
			if (TextUtils.isEmpty(birthday))
				return "0";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date birthdayDate = sdf.parse(birthday);
			String currTimeStr = sdf.format(new Date());
			Date currDate = sdf.parse(currTimeStr);
			if (birthdayDate.getTime() > currDate.getTime()) {
				return "0";
			}
			long age = (currDate.getTime() - birthdayDate.getTime())
					/ (24 * 60 * 60 * 1000) + 1;
			String year = new DecimalFormat("0.00").format(age / 365f);
			if (TextUtils.isEmpty(year))
				return "0";
			return String.valueOf(new Double(year).intValue());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "0";
	}
	
	@Override
	public void onBackPressed(){
		 this.finish();
		 overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
	}
	
	/**
	 * 根据月日计算星座
	 * @param month
	 * @param day
	 * @return
	 */
	public String getAstro(int month, int day) {
         String[] astro = new String[] { "摩羯座", "水瓶座", "双鱼座", "白羊座", "金牛座",
                         "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座" };
         int[] arr = new int[] { 20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22, 22 };// 两个星座分割日
         int index = month;
         // 所查询日期在分割日之前，索引-1，否则不变
         if (day < arr[month - 1]) {
                 index = index - 1;
         }
         // 返回索引指向的星座string
         return astro[index];
 }

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		   this.finish();
	          //overridePendingTransition(Resource.Animation.slide_down_enter, Resource.Animation.slide_down_exit);
		   overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
		   return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		  case R.id.txt_confirm:
 	        Intent intent =new Intent();
 	        if(!TextUtils.isEmpty(mCuryear))
               intent.putExtra("time",  mCuryear +" "+mCurhour+":"+mCurmin);
 	        else{
 	 	        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
 	            intent.putExtra("time",  dateFormat.format(new Date()) +" "+(curHour<10? "0"+curHour:curHour)+":"+(curMin<10? "0"+curMin:curMin));//"01:01"
 	        }
         	this.setResult(android.app.Activity.RESULT_OK, intent);
 		    this.finish();
		    overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
			break;
		  case R.id.txt_cancel:
			 this.finish();
			 overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
			 break;
			
		
		}
	}

}
