package com.zrb.mobile.register;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.hhpm.lib.ui.ScrollLayout;
import com.hhpm.lib.ui.ScrollLayout.OnViewChangeListener;
import com.zrb.discover.widget.ArrowlineView;
import com.zrb.mobile.R;
import com.zrb.mobile.RegisterBaseActivity;
import com.zrb.mobile.utility.AlertDialog;
import com.zrb.mobile.utility.CommonUtil;

public class UserRegister extends RegisterBaseActivity implements IShowNext, OnClickListener {

	ScrollLayout mlayout;
	ArrowlineView mpindicator2;
	  
	public static    String tmpsmsCode;
	public static  String registerPhone; 
	
	
	public static void launch (Activity mActivity){
 		 if(mActivity==null) return;
		 Intent it = new Intent(mActivity, UserRegister.class);
 		 mActivity.startActivity(it);
	}
	
	int curPage=0;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.user_register);
 
		 initView();
	}

	@Override
	protected void onDestroy(){
 		tmpsmsCode=null;
		registerPhone=null;
		super.onDestroy();
	}
	
	@Override
	protected void initView() {
 		super.initView();
 		TextView right_title = (TextView) this.findViewById(R.id.right_title);
		right_title.setText(R.string.project_float_consult);
		right_title.setVisibility(View.VISIBLE);
		right_title.setOnClickListener(this);
		
		TextView txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setText("注册");
		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		
		mpindicator2 = (ArrowlineView) this.findViewById(R.id.viewIndicator2);
  		mlayout = (ScrollLayout) this.findViewById(R.id.viewpager);
		mlayout.setIsScroll(false);
		mlayout.SetOnViewChangeListener(new OnViewChangeListener() {
 			@Override
			public void OnViewChange(int view) {
				// TODO Auto-generated method stub
				if (view == 0)
					mpindicator2.scroll(0, 0);
				else if (view == 1)
					mpindicator2.scroll(1, 0);
				else if (view == 2)
					mpindicator2.scroll(2, 0);
 			}
		});
		;
	}
	
	@Override
	public void showNextStep(){
		curPage++;
		mlayout.scrollToScreen(curPage);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			if(curPage==1){
				curPage--;
				mlayout.scrollToScreen(curPage);
			}
			else if(curPage==2) GetBack();
			else
				this.finish();
			break;
 		case R.id.right_title:
			//user_register();
			CommonUtil.callServerPhone(UserRegister.this);
			break;
 		}
 	}
	
	
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (curPage==2&&event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
 			GetBack();
 			return false;
		}
		return super.dispatchKeyEvent(event);
	}

	private void GetBack() {
		
		 new AlertDialog(UserRegister.this).builder()
		   .setTitle("中断填写?")
		   .setMsg("你离招融宝只有一步之遥了，真的要放弃吗?")
		   .setPositiveButton("先不填了", new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserRegister.this.finish();
			}
		    }).setNegativeButton("继续填写").show();
 	}
}
