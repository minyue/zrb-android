package com.zrb.mobile.register;

import org.apache.http.Header;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.applib.utils.LocalUserInfo;
import com.zrb.mobile.BaseActivity;
import com.zrb.mobile.MainTabActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.RegisterBaseActivity;
import com.zrb.mobile.User_registerBase1;
import com.zrb.mobile.adapter.model.UserDtos;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;

public class UserRegisterFinish extends RegisterBaseActivity implements OnClickListener {

	String registerPhone;
	String registerPwd;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.register_finish);
		registerPhone = this.getIntent().getStringExtra("phonenum");
		registerPwd = this.getIntent().getStringExtra("pwd");
		 initView();
	}
   
	@Override
	protected void initView() {
 		super.initView();
 		
 		
 		this.findViewById(R.id.title_left_root).setOnClickListener(this);;
 		TextView title=(TextView)this.findViewById(R.id.txt_title);
 		title.setText("注册完成");
 		
 		TextView righttitle=(TextView)this.findViewById(R.id.right_title);
 		righttitle.setText("咨询");
 		righttitle.setVisibility(View.VISIBLE);
 		righttitle.setOnClickListener(this);
 		
 		
 		this.findViewById(R.id.finishbtn).setEnabled(true);
 		this.findViewById(R.id.finishbtn).setOnClickListener(this);
	}
	
	private void user_Login() {

		showloading("登陆中...");
		RequestParams param = new RequestParams();
		param.put("name", registerPhone);
		param.put("password", registerPwd);

		// String loginurlparms = "?name=" + name + "&password="+ password;
		ZrbRestClient.post("dologin", param, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				// TODO Auto-generated method stub
				hideloading();
				initNotify("服务端网络连接出错");
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				hideloading();
				String results = new String(arg2);
				try {
					Gson gson = new Gson();
					UserDtos tmpDto = gson.fromJson(results, UserDtos.class);
					if (tmpDto.res == 1) {
						AppSetting.curUser = tmpDto.data;
						AppSetting.curUser.pwd = registerPwd;
						LocalUserInfo.getInstance(UserRegisterFinish.this)
								.saveUserInfo(tmpDto.data);
						 boolean hasGuide=	LocalUserInfo.getInstance(UserRegisterFinish.this).hasGuide();
						 if(hasGuide){
							    UserRegisterFinish.this.finish();
								CommonUtil.InfoLog("Regin_Success_FINISH", "注册成功");
								Intent intent=new Intent(Constants.Regin_Success_FINISH);
								sendBroadcast(intent);
						 }else{
							 Intent intent=new Intent(UserRegisterFinish.this, MainTabActivity.class);
							 intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							 startActivity(intent);
							 UserRegisterFinish.this.finish();
						 }
						
						

					} else {
						initNotify(tmpDto.msg);
					}
				} catch (Exception e) {
					initNotify(UserRegisterFinish.this
							.getString(R.string.error_serverdata_loadfail));
				}

			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	    if(v.getId()==R.id.finishbtn)
		  user_Login();
	    else if(v.getId()==R.id.title_left_root){
	    	
	    	finish();
	    }
	    else
	    	
	    	CommonUtil.callServerPhone(UserRegisterFinish.this);
	}
}
