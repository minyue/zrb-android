package com.zrb.mobile.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.hhpm.lib.ui.ScrollLayout;
import com.hhpm.lib.ui.ScrollLayout.OnViewChangeListener;
import com.zrb.discover.widget.ArrowlineView;
import com.zrb.mobile.R;
import com.zrb.mobile.RegisterBaseActivity;

public class UserFindPassword extends RegisterBaseActivity implements IShowNext, OnClickListener {

	ScrollLayout mlayout;
	ArrowlineView mpindicator2;
	  
	public static    String tmpsmsCode;
	public static  String registerPhone; 
	TextView right_title;
	int curPage=0;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.register_findpassword);
 
		 initView();
	}

	@Override
	protected void onDestroy(){
 		tmpsmsCode=null;
		registerPhone=null;
		super.onDestroy();
	}
	
	@Override
	protected void initView() {
 		super.initView();
 		right_title= (TextView) this.findViewById(R.id.right_title);
		right_title.setText("注册");
		right_title.setVisibility(View.VISIBLE);
		right_title.setOnClickListener(this);
		
		TextView txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setText("找回密码");
		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		
		mpindicator2 = (ArrowlineView) this.findViewById(R.id.viewIndicator2);
  		mlayout = (ScrollLayout) this.findViewById(R.id.viewpager);
		mlayout.setIsScroll(false);
		mlayout.SetOnViewChangeListener(new OnViewChangeListener() {
 			@Override
			public void OnViewChange(int view) {
				// TODO Auto-generated method stub
				if (view == 0)
					mpindicator2.scroll(0, 0);
				else if (view == 1){
					mpindicator2.scroll(1, 0);
					right_title.setText("完成");
					startTicks();
				}
					
		 
 			}
		});
		;
	}
	
	private void startTicks(){
		
		getHandler().postDelayed(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				FragmentManager tmpFragmanger=UserFindPassword.this.getSupportFragmentManager();
 				FindPwdResetPasswordFragment curFrag=(FindPwdResetPasswordFragment)	tmpFragmanger.findFragmentById(R.id.input_layout_step2);
 				if(curFrag!=null) curFrag.startTicks();
			}
 		}, 200);
	}
	
	@Override
	public void showNextStep(){
		curPage++;
		mlayout.scrollToScreen(curPage);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
 				this.finish();
			break;
 		case R.id.right_title:
			//user_register();
 			if(curPage==1){
 				FragmentManager tmpFragmanger=this.getSupportFragmentManager();
 				FindPwdResetPasswordFragment curFrag=(FindPwdResetPasswordFragment)	tmpFragmanger.findFragmentById(R.id.input_layout_step2);
 				if(curFrag!=null) curFrag.changePwd();
 			}
 			else {
 				Intent it = new Intent(this, UserRegister.class);
 				startActivity(it);
 				this.finish();
 			}
			 
			break;
 		}
 	}
	
 
}
