package com.zrb.mobile.register;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.ui.ClearEditTextEx;
import com.zrb.mobile.ui.ClearEditTextEx.onTextCorrectListener;
import com.zrb.mobile.ui.fragment.NotifyFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class RegPasswordFragment  extends NotifyFragment implements onTextCorrectListener, OnClickListener{

 
	ClearEditTextEx editpwd;
	ClearEditTextEx editRepeatpwd;
 	Button nextstep_btn;
 
    View rootView;
     String registerPhone;
    String tmpsmsCode; 
  	
    
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// return inflater.inflate(R.layout.u_center_registerstep1, null);

		View view = inflater.inflate(R.layout.register_password, container, false);
		rootView=view;
		initView();
		return view;
	}
	
	private void initView() {
 
		editpwd = (ClearEditTextEx) rootView.findViewById(R.id.layout_code);
		
		editpwd.setInputType(2);
		 editpwd.setOnTextCorrectListener(this);
		
		editRepeatpwd = (ClearEditTextEx) rootView.findViewById(R.id.layout_repeatcode);
		 
		editRepeatpwd.setInputType(2);
		editRepeatpwd.setOnTextCorrectListener(this);
		
		
		nextstep_btn=(Button)rootView.findViewById(R.id.nextbtn);
		nextstep_btn.setEnabled(false);
		nextstep_btn.setOnClickListener(this);
  	}
	
 
	private boolean isPwdOk(boolean showHite){
		String pwd=this.editpwd.getContentValue(); 
		String repeatpwd=editRepeatpwd.getContentValue(); 
		
		if (TextUtils.isEmpty(pwd)) {
			if(showHite)initNotify("密码不能为空");
			return false;
 		}
  		if(!CommonUtil.checkPassword(pwd)){
  			if(showHite) initNotify("密码不符合规则");
			return false;
 		}
  		if(!pwd.equals(repeatpwd)){
  			if(showHite)initNotify("两次密码不一致");
			return false;
  		}
		return true;
	}
 	
	private void user_register() {
 
		if(!isPwdOk(true)) return ;
		
		
		registerPhone=UserRegister.registerPhone;
		tmpsmsCode=UserRegister.tmpsmsCode;
 		showloading("正在提交中..");
 		//register/checkMobileRegister?mobile=13720202020
 		final String pwd=this.editpwd.getContentValue();
 		RequestParams param = new RequestParams();
		param.put("mobile",registerPhone);
		param.put("passWord", pwd);
		param.put("smsCode", tmpsmsCode);
   
		ZrbRestClient.post("register/saveAppUser" , param,
				new AsyncHttpResponseHandler() {
 					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
 						hideloading();
 						initNotify(RegPasswordFragment.this.getString(R.string.error_net_loadfail));
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						hideloading();
						String results = new String(arg2);
 						try {
 							Gson gson = new Gson();
 							RegisterDto tmpDto = gson.fromJson(results,	RegisterDto.class);
 							if(tmpDto.res==1){
 								showNextStep(pwd);
   							}
 							else
 								initNotify(tmpDto.msg);
  					      } 
 						 catch (Exception e) {
 							initNotify(RegPasswordFragment.this.getString(R.string.error_serverdata_loadfail));
 						}
 					}
				});

	}
   
   
	private void showNextStep(String pwd){
 		
 		if(RegPasswordFragment.this.getActivity()==null) return;
 		UserRegister.tmpsmsCode=pwd;
 	 
  	    FragmentTransaction ft = RegPasswordFragment.this.getFragmentManager().beginTransaction();
  	    ft.replace(R.id.register_step3_layout, new BaseInfoFragment()).commit();
 		
  	    
  	    new Handler().postDelayed(new Runnable(){
 		@Override
		public void run() {
			// TODO Auto-generated method stub
 			IShowNext tmp=(IShowNext)RegPasswordFragment.this.getActivity();
	 		tmp.showNextStep();
		}}, 600);
 		
  	}

		@Override
		public void onTextCorrect(boolean isok) {
			// TODO Auto-generated method stub
 			if(isok) {
				if(isPwdOk(false))  nextstep_btn.setEnabled(isok);
 			}
			else
				nextstep_btn.setEnabled(isok);
 		}

		onTextCorrectListener repeatTextcorrect=new onTextCorrectListener(){

			@Override
			public void onTextCorrect(boolean isok) {
				// TODO Auto-generated method stub
 			}
 		};
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
  
			case R.id.nextbtn:
				user_register();
				break;
  		  }
	 	}
	
	
	
}
