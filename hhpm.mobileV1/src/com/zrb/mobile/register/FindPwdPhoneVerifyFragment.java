package com.zrb.mobile.register;

import org.apache.http.Header;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.R;
import com.zrb.mobile.User_forget_Password;
import com.zrb.mobile.User_registerBase1;
import com.zrb.mobile.User_registername;
 
 
 
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.ui.ClearEditTextEx;
import com.zrb.mobile.ui.ClearEditTextEx.onTextCorrectListener;
import com.zrb.mobile.ui.ClearPhoneEditText;
import com.zrb.mobile.ui.fragment.NotifyFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FindPwdPhoneVerifyFragment  extends NotifyFragment implements OnClickListener, onTextCorrectListener{

 
	ClearPhoneEditText editPhone;
 
 	Button nextstep_btn;
 
    View rootView;
     String registerPhone;
    String tmpsmsCode; 
    private String mobile;
    
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// return inflater.inflate(R.layout.u_center_registerstep1, null);

		View view = inflater.inflate(R.layout.register_findpwdstep1, container, false);
		rootView=view;
		initView();
		return view;
	}
	
	private void initView() {
 
		editPhone = (ClearPhoneEditText) rootView.findViewById(R.id.layout_username);
		editPhone.setOnTextCorrectListener(this);
		nextstep_btn=(Button)rootView.findViewById(R.id.nextbtn);
		nextstep_btn.setEnabled(false);
		nextstep_btn.setOnClickListener(this);
  	}
	
 
	private void checkPhone() {
		mobile = this.editPhone.isEmpty() ? "":this.editPhone.getContentValue().replace(" ", "");
		if (TextUtils.isEmpty(mobile)) {
			initNotify("手机号不能为空");
			return ;
 		}
 		if(!CommonUtil.isMobileNO(mobile))//CommonUtil.isMobileNO(this.editTextphone.getText()
		{
 			initNotify("手机号不正确");
  			return ;
		}
		
		showloading("正在校验中..");
		String loginurlparms = "?mobile=" + mobile;
		ZrbRestClient.get("register/checkMobileRegister" + loginurlparms, null,
				new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
						hideloading();
 						initNotify(FindPwdPhoneVerifyFragment.this.getString(R.string.error_net_loadfail));
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
 						String results = new String(arg2);
 						try {
							JSONObject object = new JSONObject(results);
							int res = object.getInt("res");
							String msg = object.getString("msg");//{"res":1,"msg":"手机号未注册！","data":null}
							if( res == 0 ){
	 							sendSMS();
							}else{
								hideloading();
								initNotify(msg);
							}
 
						} catch (Exception e) {
							hideloading();
							initNotify(FindPwdPhoneVerifyFragment.this.getString(R.string.error_serverdata_loadfail));
							//CommonUtil.showToast( "fail:"+e.getMessage(),User_forget_Password.this);
 						}
 					}
				});

	}

	private void sendSMS(){
		String loginurlparms = "?mobile=" + mobile;// register/sendSMSFindPW
		ZrbRestClient.get("register/sendFindPasswordAuthCode" + loginurlparms, null,
				new AsyncHttpResponseHandler() {
					
					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						hideloading();
						String results = new String(arg2);
						
						try {
							JSONObject object = new JSONObject(results);
							int res = object.getInt("res");
							String msg = object.getString("msg");
							if( res == 1){
								showNextStep(mobile);
								//showLogin();
							}else{
								initNotify(msg);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							initNotify(FindPwdPhoneVerifyFragment.this.getString(R.string.error_serverdata_loadfail));
						}
					}
					
					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
						// TODO Auto-generated method stub
						hideloading();
						initNotify(FindPwdPhoneVerifyFragment.this.getString(R.string.error_net_loadfail));
					}
				});
	} 
 	 
   
	private void showNextStep(String pwd){
 		
 		if(FindPwdPhoneVerifyFragment.this.getActivity()==null) return;
 		UserRegister.registerPhone=pwd;
 	 
 		IShowNext tmp=(IShowNext)FindPwdPhoneVerifyFragment.this.getActivity();
	 	tmp.showNextStep();
  	}
 		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
  
			case R.id.nextbtn:
				checkPhone();
				break;
  		  }
	 	}

		@Override
		public void onTextCorrect(boolean isok) {
			// TODO Auto-generated method stub
			nextstep_btn.setEnabled(isok);
 		}
	
	
	
}
