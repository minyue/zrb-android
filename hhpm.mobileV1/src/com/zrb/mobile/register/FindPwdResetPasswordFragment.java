package com.zrb.mobile.register;

import org.apache.http.Header;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.R;
 
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.ui.ClearEditTextEx;
import com.zrb.mobile.ui.ClearPhoneEditText;
import com.zrb.mobile.ui.fragment.NotifyFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FindPwdResetPasswordFragment  extends NotifyFragment implements OnClickListener{

	ClearEditTextEx editTextpwd,editRepeatpwd;
	EditText editCode;
	ClearPhoneEditText editPhone;
 
 	Button btn_send;
 
    View rootView;
    
    String tmpsmsCode; 
    private String mobile;
    MyCount mc;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// return inflater.inflate(R.layout.u_center_registerstep1, null);

		View view = inflater.inflate(R.layout.register_findpwdstep2, container, false);
		rootView=view;
		initView();
		return view;
	}
	
	private void initView() {
 
  		btn_send=(Button)rootView.findViewById(R.id.nextbtn);
		btn_send.setOnClickListener(this);
		btn_send.setEnabled(false);
 		
		//this.findViewById(R.id.button_register).setOnClickListener(this);
		
		editTextpwd=(ClearEditTextEx)rootView.findViewById(R.id.layout_code);
		editTextpwd.setInputType(2);
		editRepeatpwd=(ClearEditTextEx)rootView.findViewById(R.id.layout_repeatcode);
		editRepeatpwd.setInputType(2);
		
		editCode=(EditText)rootView.findViewById(R.id.edit_verifyCode);
  	}
  	 
	public void startTicks(){
 		mc = new MyCount(60000, 1000);  
	    mc.start();  
	}
	
	/*定义一个倒计时的内部类*/  
    class MyCount extends CountDownTimer {     
        public MyCount(long millisInFuture, long countDownInterval) {     
            super(millisInFuture, countDownInterval);     
        }     
        @Override     
        public void onFinish() { 
        	btn_send.setEnabled(true);
        	btn_send.setText("获取验证码");        
        }     
        @Override     
        public void onTick(long millisUntilFinished) {     
        	btn_send.setText("重新取验证码(" + millisUntilFinished / 1000 + ")秒");     
           // Toast.makeText(NewActivity.this, millisUntilFinished / 1000 + "", Toast.LENGTH_LONG).show();//toast有显示时间延迟       
        }    
    }  
 
    
    private void getvalidateCode(){
  		if(mc==null) mc = new MyCount(60000, 1000);  
  		
  	   mobile=UserRegister.registerPhone;
        mc.start();  
        RequestParams param = new RequestParams();
		param.put("mobile", mobile);
//	 	 String url=String.format("register/sendSMSRegister?mobile=%1$s", phoneNum);
		 ZrbRestClient.post("register/sendSMSFindPW", param,
				new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
						// TODO Auto-generated method stub
  						initNotify(FindPwdResetPasswordFragment.this.getString(R.string.error_net_loadfail));
 					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
 					   String results = new String(arg2);
  						try {
							Gson gson = new Gson();
							RegisterDto tmpDto = gson.fromJson(results,	RegisterDto.class);
							if(tmpDto.res!=1)   initNotify("发送验证码出错");
 
						 } catch (Exception e) {
							 initNotify(FindPwdResetPasswordFragment.this.getString(R.string.error_serverdata_loadfail));
						}
					}
				});
	}
	 

	public void changePwd() {
		
		String pwdStr=this.editTextpwd.getContentValue(); 
		String RepeatpwdStr=this.editRepeatpwd.getContentValue(); ;
		String verifyCodeStr=this.editCode.getText()==null ? "":this.editCode.getText().toString();
		
		if (TextUtils.isEmpty(pwdStr)) {
			initNotify("密码不能为空");
			return;
		}
		if (!CommonUtil.checkPassword(pwdStr)) {
			initNotify("密码不符合规则");
			return;
		}
 		if (!RepeatpwdStr.equals(pwdStr)) {
			initNotify("密码输入不一致");
			return;
		}
 		if (TextUtils.isEmpty(verifyCodeStr)) {
			initNotify("验证码不能为空");
			return;
		}
 		mobile=UserRegister.registerPhone;
 		showloading("正在提交中..");
 		String url=String.format("register/changePWord?mobile=%1$s&smsCode=%2$s&passWord=%3$s", mobile,verifyCodeStr,pwdStr);
 	/*	if(ischangepwd) 
			 url=String.format("user/changePWord?passWord=%1$s&newpassWord=%2$s&zrb_front_ut=%3$s", oldpwdstr,pwdStr,AppSetting.curUser.zrb_front_ut);*/
		 ///user/changePWord?passWord=1234567&newpassWord=1234567&zrb_front_ut=306d8e1d90f87e7f55c075e959ede8c7
		ZrbRestClient.get(url, null, new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
						// TODO Auto-generated method stub
						hideloading();
						initNotify(FindPwdResetPasswordFragment.this.getString(R.string.error_net_loadfail));
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						hideloading();
						String results = new String(arg2);
 						try {

							Gson gson = new Gson();
 							RegisterDto tmpDto = gson.fromJson(results,	RegisterDto.class);
							if( tmpDto.res == 1){
								CommonUtil.showToast("密码修改成功",FindPwdResetPasswordFragment.this.getActivity());
								new Handler().postDelayed(new Runnable() {
									@Override
									public void run() {
										
									/*	if(ischangepwd) {
	 										User_change_Password.this.setResult(android.app.Activity.RESULT_OK);
											User_change_Password.this.finish();
											return;
										}*/
										Intent it = new Intent(FindPwdResetPasswordFragment.this.getActivity(), User_phone_login.class);
										it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										startActivity(it);
										FindPwdResetPasswordFragment.this.getActivity().finish();
 									}
								}, 2000);
							}else{
								initNotify(tmpDto.msg);
							}
  						} catch (Exception e) {
 							initNotify(FindPwdResetPasswordFragment.this.getString(R.string.error_serverdata_loadfail));
 						}
 					}
				});

	}
		
	 @Override
	 public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
  
			case R.id.nextbtn:
				btn_send.setEnabled(false);
			    getvalidateCode();
				break;
  		  }
	 	}

 
	
	
	
}
