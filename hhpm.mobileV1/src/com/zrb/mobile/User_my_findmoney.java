package com.zrb.mobile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.http.Header;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.adapter.UserMyFindMoneyAdapter;
import com.zrb.mobile.adapter.model.UserMyFindMoneyDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

public class User_my_findmoney extends BaseActivity implements OnClickListener,
		OnItemClickListener, onLoadMoreListener {
	// public String ACTION_NAME_SIX = "user_my_findmoney";
	UserMyFindMoneyAdapter userMyFindMoneyAdapter;
	private PullToRefreshListViewEx listview;
	// private List<Intention> list;
	// int curPageindex=0;
	int currentPage;
	Handler myhandler = new Handler();

	// private long maxId = 1;
	// public String ACTION_NAME_THREE = "my_findmoney";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_home_activity1);
		// registerBoradcastReceiver1();
		initView();
		myhandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				listview.setRefreshing(true);
			}
		}, 200);
	}

	/**
	 * 初始化UI
	 */
	private void initView() {
		registerBoradcastReceiver1();
		View v = findViewById(R.id.include_top);
 		
		((TextView) v.findViewById(R.id.txt_title)).setText("我的资金");
		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		listview = (PullToRefreshListViewEx) findViewById(R.id.discover_list_view);
		listview.setPagesize(14);

		listview.setOnItemClickListener(this);// 点击事件
		listview.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(
						CommonUtil.getcurTime(User_my_findmoney.this));
				currentPage = 1;
				myhandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						LoadData(-1, true);
					}
				}, 1000);
			}
		});

		userMyFindMoneyAdapter = new UserMyFindMoneyAdapter(this,
				new ArrayList<MyFindMoney>());
		listview.initAdapter(userMyFindMoneyAdapter);
		listview.setOnLoadMoreListener(this);
		// listview.setAdapter(usermyPublishAdapter);
	}

	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		currentPage++;
		myhandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				LoadData(userMyFindMoneyAdapter.getMinPushTime(), false);
			}
		}, 300);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_left_root:
			this.finish();
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Intent intent = new Intent(User_my_findmoney.this,
				MoneyDetailActivity.class);
		MyFindMoney myFindMoney = userMyFindMoneyAdapter.list1
				.get(position - 1);
		intent.putExtra("id", myFindMoney.id);
		intent.putExtra("title", myFindMoney.title);
		intent.putExtra("position", position - 1);
		intent.putExtra("from", "3");
		startActivity(intent);

		// Toast.makeText(this, "点击事件", 0).show();
	}

	ZrbRestClient mZrbRestClient;
	private void LoadData(long pushTime, final boolean isreflesh) {
		RequestParams param = new RequestParams();
 		param.put("limit", 12);
		if (-1 != pushTime) {

			param.put("pushTime", pushTime);
		}

		CommonUtil.InfoLog("url", param.toString());
		if(null==mZrbRestClient){
			
			mZrbRestClient=new ZrbRestClient(this);
			mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
					CommonUtil.showToast(arg0, User_my_findmoney.this);
					if (currentPage > 1)
						listview.hideloading();
					else
						listview.onRefreshComplete();
				}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
 
					String results = new String(arg2);
					// CommonUtil.InfoLog("intention/list", results);
					if (currentPage == 1)
						listview.onRefreshComplete();
					else
						listview.hideloading();
					try {
						Gson gson = new Gson();
						UserMyFindMoneyDto tmpDto = gson.fromJson(results, UserMyFindMoneyDto.class);
						if (tmpDto.res == 1) {

							if (tmpDto.data != null && tmpDto.data.size() > 0) {
								if (currentPage == 1) {
									userMyFindMoneyAdapter.setCurDatas(tmpDto.data);
								} else {
									userMyFindMoneyAdapter.addAll(tmpDto.data);
								}
								userMyFindMoneyAdapter.notifyDataSetChanged();
							} else {
  								CommonUtil.showToast(String.format("暂无%1$s信息~~", currentPage == 1 ? "": "更多"),
										User_my_findmoney.this);
							}
						} else
							CommonUtil.showToast(tmpDto.msg,
									User_my_findmoney.this);
					} catch (Exception e) {
						CommonUtil.showToast(
								User_my_findmoney.this
										.getString(R.string.error_serverdata_loadfail),
								User_my_findmoney.this);
					}
				}
			});
		}
		mZrbRestClient.post("userStatisticDetail/getMyFunds", param );
	}

	public void registerBoradcastReceiver1() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(Constants.FocusNum_MYMONEY_mainlist);
		// 注册广播
		registerReceiver(mBroadcastReceiver1, myIntentFilter);
	}

	private BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(Constants.FocusNum_MYMONEY_mainlist)) {
				String from = intent.getStringExtra("from");
				int position = intent.getIntExtra("position", -1);

				if ("1".equals(from)) {
					int attention = intent.getIntExtra("attention", -1);
					Log.i("from", from);
					Log.i("attention", attention + "");
					Log.i("position", position + "");
					if (attention != -1) {
						if (attention == 0) {
							// 取消关注
							CanFocus(position);
						} else {
							// 添加关注
							AddFocus(position);
						}
					}

				}
			}
		}
	};

	// 取消关注效果
	protected void CanFocus(int position) {
		// TODO Auto-generated method stub
		userMyFindMoneyAdapter.list1.get(position).collectNum = userMyFindMoneyAdapter.list1
				.get(position).collectNum - 1;
		Log.i("collectNum--",
				userMyFindMoneyAdapter.list1.get(position).collectNum + "");
		userMyFindMoneyAdapter.notifyDataSetChanged();
	}

	// 添加关注效果
	protected void AddFocus(int position) {
		// TODO Auto-generated method stub
		userMyFindMoneyAdapter.list1.get(position).collectNum = userMyFindMoneyAdapter.list1
				.get(position).collectNum + 1;
		Log.i("collectNum++",
				userMyFindMoneyAdapter.list1.get(position).collectNum + "");
		userMyFindMoneyAdapter.notifyDataSetChanged();
	}

	/*
	 * 
	 * 
	 * 广播接收者的注册和接收
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// 销毁广播
		unregisterReceiver(mBroadcastReceiver1);
	}

}
