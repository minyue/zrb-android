package com.zrb.mobile;

import java.util.Date;

import org.apache.http.Header;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.zrb.mobile.adapter.Project_CitylistAdapter;
import com.zrb.mobile.adapter.Projectlist_showAdapter;
import com.zrb.mobile.adapter.model.CityDto;
import com.zrb.mobile.adapter.model.CityDtos;
import com.zrb.mobile.adapter.model.FindProjectDto;
import com.zrb.mobile.adapter.model.FindProjectDtos;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.EmptyView;
import com.zrb.mobile.ui.OnCityChoose;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.PopBuilder;
import com.zrb.mobile.utility.PopBuilder.onDismissListener;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
public class FindProject_MainFragment extends BaseLazyFragment implements
		onLoadMoreListener {

	PullToRefreshListViewEx msearchResults;
	Projectlist_showAdapter mAdapter2;
	int currentPage;
	Handler mhandler = new Handler();
	boolean isFirstIn = true;
//	TextView titleText,txt_locationBtn,txt_industryBtn ;
	Project_CitylistAdapter mcitylistAdapter;
	int curPostcode=0;	
	int curIndustryCode=Constants.ALLIndustryCode;//全部行业
	
	EmptyView emptylayout;
	
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.findproject_mainfragment,
				container, false);
/*	 	
		txt_locationBtn=(TextView)view.findViewById(R.id.txt_btn_location);
		txt_locationBtn.setOnClickListener(this);
		
		txt_industryBtn=(TextView)view.findViewById(R.id.txt_btn_industry);
		txt_industryBtn.setOnClickListener(this);*/
		emptylayout=(EmptyView)view.findViewById(R.id.include_empty_layout);
		
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// mNewsItemDao = new NewsItemDao(getActivity());

		msearchResults = (PullToRefreshListViewEx) this.getView().findViewById(
				R.id.findproject_list_view);
		msearchResults.setPagesize(10);
		// Set a listener to be invoked when the list should be refreshed.
		msearchResults.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {

				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(
						CommonUtil.getcurTime(getActivity()));
				currentPage = 1;
				mhandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						LoadData(CommonUtil.formateDate(new Date()), true);
					}
				}, 1000);
			}
		});

		mAdapter2 = new Projectlist_showAdapter(getActivity());
		msearchResults.initAdapter(mAdapter2);
		msearchResults.setOnLoadMoreListener(this);
		msearchResults.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				FindProjectDto tmpDto = mAdapter2.CurrentDatas.get(arg2 - 1);
				Intent intent = new Intent(FindProject_MainFragment.this
						.getActivity(), ProjectDetailActivity.class);
				intent.putExtra("id", tmpDto.id);
				intent.putExtra("title", tmpDto.projectName);
				startActivity(intent);
			}
		});
	}

	// 加载更多
	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		currentPage++;
		mhandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				LoadData(mAdapter2.getMinTime(), false);
			}
		}, 400);
	}

	// 上拉加载更多 fund/list?pushTime=2015-09-24%2023:59:59&limit=10
	
	
	//http://backend.zhaorongbao.com:8080/projectApi/searchProList?rows=30&postcode=230000&lastPushTime=2015-10-14 11:52:43
	ZrbRestClient mZrbRestClient;
	private void LoadData(String minTime, boolean isreflesh) {
		String url = String.format("projectApi/searchProList?lastPushTime=%1$s&rows=%2$s", minTime, 10);// &tradeDates=%2$s

		if(curPostcode>0)                                 url+="&postcode="+curPostcode;
		if(curIndustryCode!=Constants.ALLIndustryCode)    url+="&industrys="+curIndustryCode; 
		
		CommonUtil.InfoLog("projectApi/list", url);
 		if(mZrbRestClient==null){
			mZrbRestClient=new ZrbRestClient(this.getActivity());
			mZrbRestClient.setOnhttpResponseListener( new HttpResponseListener() {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,Throwable arg3) {
				CommonUtil.showToast(R.string.error_net_loadfail, getActivity());
				if (currentPage > 1) msearchResults.hideloading();
				else
					msearchResults.onRefreshComplete();
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// loadingview.Hide();
				String results = new String(arg2);
				if (currentPage == 1)
					msearchResults.onRefreshComplete();
				else
					msearchResults.hideloading();
				try {
					Gson gson = new Gson();
					FindProjectDtos tmpDto = gson.fromJson(results,FindProjectDtos.class);
				   	if (tmpDto.res == 1) {
						if (tmpDto.data != null && tmpDto.data.size() > 0) {
 							  emptylayout.checkhide() ;   
 							  mAdapter2.setDatas(tmpDto.data,currentPage>1);
							  mAdapter2.notifyDataSetChanged();
  						} else {
  							if(currentPage==1){
  								 mAdapter2.CurrentDatas.clear();
  							     mAdapter2.notifyDataSetChanged();
  							     emptylayout.show();
   							} 
  							else
  								CommonUtil.showToast("暂无更多信息~~",  getActivity());
						}
 					}else
						CommonUtil.showToast(tmpDto.msg, getActivity());
				} catch (Exception e) {
  					CommonUtil.showToast(R.string.error_serverdata_loadfail, getActivity());
				}
			}
		   });
		}
		mZrbRestClient.getcheckSession(url );
	}

	private void firstLoad() {
		if (isFirstIn) {
			isFirstIn = false;
			 applyScrollListener();
			mhandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					msearchResults.setRefreshing(true);
				}
			}, 200);
		}
	}
	
	private void applyScrollListener() {
		msearchResults.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
	}

	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}
	/*view.findViewById(R.id.txt_btn_location).setOnClickListener(this);
	view.findViewById(R.id.txt_btn_industry).setOnClickListener(this);*/
	// 点击事件
 

 
 
 	 
 	 
}
