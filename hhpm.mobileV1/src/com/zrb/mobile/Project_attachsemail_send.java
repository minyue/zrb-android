package com.zrb.mobile;

 

import java.util.ArrayList;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.adapter.Project_attachListAdapter;
import com.zrb.mobile.adapter.model.ArticleDto;
import com.zrb.mobile.adapter.model.NewsDtos;
import com.zrb.mobile.ui.LinearLayoutForListView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

public class Project_attachsemail_send  extends BaseActivity implements OnClickListener  {

	LinearLayoutForListView attachlistview;
	TextView rightTxt;
	Project_attachListAdapter mattachlistAdapter;
	EditText macceptPerson,mprojectTitle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.project_attachemail_send);
	    initview();
	    LoadData();
	}
	
	private void initview(){
 		TextView txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setText("发送至邮箱");
 		
		macceptPerson=(EditText)this.findViewById(R.id.edit_acceptperson);
		mprojectTitle=(EditText)this.findViewById(R.id.edit_projecttitle);	
		
		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		
		rightTxt= (TextView) this.findViewById(R.id.right_title);
		rightTxt.setVisibility(View.VISIBLE);
		rightTxt.setText("发送");
		rightTxt.setEnabled(false);
		rightTxt.setOnClickListener(this);
		attachlistview=(LinearLayoutForListView)this.findViewById(R.id.attach_listview);
		//mattachlistAdapter=new Project_attachListAdapter(this);
		
		attachlistview.setOnItemClickListener(null);
		
		macceptPerson.addTextChangedListener(acceptPersonWatcher);
		
		
   	}

	TextWatcher acceptPersonWatcher=new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if( s.length() != 0 && null != macceptPerson.getText() && !"".equals(macceptPerson.getText().toString())){
				
			   if(CommonUtil.isEmail(macceptPerson.getText().toString()))
				   rightTxt.setEnabled(true);
			   else  
				   rightTxt.setEnabled(false);
  			}else{
				rightTxt.setEnabled(false);
 			}
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			
		}
	};
	
	private void LoadData()
	{
     	String url=String.format("news/query?id=%1$s&forward=%2$b&limit=%3$d", "",false,10);
  		ZrbRestClient.get(url, null,  new AsyncHttpResponseHandler()
    	{
		 
 			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				//loadingview.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
			}
 		   @Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
 				//loadingview.Hide();
 				String results=new String(arg2);
				try 
				{
 			       Gson gson =new Gson();  
 			       mattachlistAdapter=new Project_attachListAdapter(Project_attachsemail_send.this);
 			       for(int i=0;i<3;i++){
 			        	ArticleDto tmpDto=new ArticleDto();
 			        	tmpDto.title="附件adasdf"+i;
 			        	mattachlistAdapter.CurrentDatas.add(tmpDto);
 			        }
  		           attachlistview.setAdapter(mattachlistAdapter);
			      
     			}
				catch(Exception e){
 				   //loadingview.HideText(e.getMessage()).ShowRefleshBtn(true).ShowError();
					System.out.println("eeee!!!!"+e.getMessage());
 				}
  			}});
 	}
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
 		 case R.id.title_left_root:
			this.finish();
			break;
		 case R.id.right_title:
			//this.finish();
			break;
		
		}
	}
}
