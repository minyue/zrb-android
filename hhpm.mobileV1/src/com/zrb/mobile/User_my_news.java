package com.zrb.mobile;

import java.util.ArrayList;
import java.util.Date;
import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
/*import com.zrb.mobile.PullToRefreshBase.OnRefreshListener;*/
import com.zrb.mobile.adapter.News_mylistItemAdapter;
 
import com.zrb.mobile.adapter.model.NewsmyDto;
import com.zrb.mobile.adapter.model.NewsmyDtos;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class User_my_news extends BaseImgActivity implements OnClickListener  {

	PullToRefreshListView msearchResults;
	LinearLayout layout_container;
	News_mylistItemAdapter mynewslistAdapter;
	CustomloadingView mloadingView;
	int curPageindex=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.u_center_mynews);
		// setContentView(R.layout.user_register_activity);fragment_container
		 //initView();
		initView();
		loadData(CommonUtil.formateDate(new java.util.Date()));
 	}
  	
	private void initView(){
 	
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("关注资讯");
		
		mloadingView=(CustomloadingView)this.findViewById(R.id.list_emptyview);
		mloadingView.SetOnRefleshListener(this);
		
 		msearchResults=(PullToRefreshListView)this.findViewById(R.id.mylist_news);
		
		mynewslistAdapter=new News_mylistItemAdapter(this,new ArrayList<NewsmyDto>());
		mynewslistAdapter.setImgLoader(imageLoader);
		msearchResults.setAdapter(mynewslistAdapter);
 		msearchResults.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(User_my_news.this, News_WebContentActivity.class);
				NewsmyDto tmpDto=	(NewsmyDto)mynewslistAdapter.getItem(arg2-1);
			 
				intent.putExtra("title", tmpDto.title);
				intent.putExtra("newid",tmpDto.id);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}});
		
		// Set a listener to be invoked when the list should be refreshed.
		msearchResults.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(PullToRefreshBase<ListView> refreshView) {
						String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						//new GetDataTask().execute();
						curPageindex++;
 						onLoadMore(); 
					}
				});
		
		mloadingView.ShowLoading();
 	}

	public void onLoadMore()
	{
 		//CommonUtil.formateDate(new java.util.Date())
	    String time= CommonUtil.formateDate(new Date(mynewslistAdapter.getMinCollectiontime()));
	    loadData(time);
	}
	
	private void loadData(String time){
	
		String url=String.format("news/queryCollections?createTime=%1$s&type=0&userId=%2$s&limit=10&zrb_front_ut=%3$s",
			                                         	time,AppSetting.curUser.uid,AppSetting.curUser.zrb_front_ut);
		
		Log.e("tag", url);
	    ZrbRestClient.get(url, null,  new AsyncHttpResponseHandler()
	   {
			@Override
		    public void onFailure(int arg0, Header[] arg1, byte[] arg2,
				Throwable arg3) {
			// TODO Auto-generated method stub
				// progTip.Dismiss();
				  if(curPageindex==0)
					  mloadingView.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
				  else
		   	         CommonUtil.showToast("加载数据出错！", User_my_news.this);
		   }

		   @Override
		   public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
			// TODO Auto-generated method stub
			   //progTip.Dismiss();
			    String results=new String(arg2);
 			    msearchResults.onRefreshComplete();
			    if(curPageindex==0)   mloadingView.Hide();
	 		    try 
			    {
		            Gson gson = new Gson();  
		            NewsmyDtos tmpDto=	gson.fromJson(results, NewsmyDtos.class);
	                if(tmpDto.res==1) {
	                	if(tmpDto.data!=null&&tmpDto.data.size()>0)
	                	{
 	                	  mynewslistAdapter.addAll(tmpDto.data);
 	                	  mynewslistAdapter.notifyDataSetChanged();
 	                	}
	                	else{
	                		if(curPageindex==0) mloadingView.HideText("暂无关注资讯").ShowNull();
	                		else
	                			CommonUtil.showToast("暂无更多信息~~", User_my_news.this);	
	                	}
	                }
	                else{
 	            	  CommonUtil.showToast(tmpDto.msg, User_my_news.this);
	                }
	            
			     }
			     catch(Exception e) {
				 CommonUtil.showToast("error:加载数据出错！", User_my_news.this);
				//System.out.println("eeee!!!!"+e.getMessage());
			   } 
			
		}});
	}
  
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
    
     	case R.id.right_title:
     		//changeBaseInfo() ;
     		break;
    	case R.id.btn_reflesh:
     		//changeBaseInfo() ;
    		mloadingView.HideText("努力加载中～～").ShowLoading();
    		onLoadMore();
     		break;
     	 
 		}
 	}
 

}
