package com.zrb.mobile;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class User_registername extends RegisterBaseActivity implements OnClickListener,TextWatcher {

	ImageView btnAccountclear,btnphoneclear, btnPwdclear,btnshowPwd;
 
	EditText editTextName;
	EditText editTextPwd;
	TextView mbtn_verifycode;
	Tip progTip;
	View next_line;
	View next_line1;
    boolean isShow=false;
    Button changephone_btn;
  	private String registerPhone;	
  	private String tmpsmsCode;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    	setContentView(R.layout.u_center_registername);
 		
		registerPhone=this.getIntent().getStringExtra("phonenum");
		tmpsmsCode=this.getIntent().getStringExtra("smscode");
		
  		initView();
	}
 
	protected void initView() {
	  	super.initView();
 
		
		TextView txtTitle=(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("注 册");
		
		btnAccountclear = (ImageView) this.findViewById(R.id.name_btn_clean);
 	    editTextName = (EditText) this.findViewById(R.id.edit_registerpwd);
		editTextName.addTextChangedListener(onNameTextChanged);
		
		btnPwdclear = (ImageView) this.findViewById(R.id.password_btn_clean);
	 	editTextPwd = (EditText) this.findViewById(R.id.edit_repeatpwd);
	 	editTextPwd.addTextChangedListener(onPwdTextChanged);
  
	 	changephone_btn=(Button)this.findViewById(R.id.changephone_btn);
		//mbtn_nextstep.setEnabled(false);
	 	changephone_btn.setOnClickListener(this);
		
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
 	 
		next_line = this.findViewById(R.id.next_line);
		next_line1 = this.findViewById(R.id.next_line1);
		//this.findViewById(R.id.button_register).setOnClickListener(this);
 	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
     	case R.id.changephone_btn:
			user_register();
			break;
	 
 		}
 	}
 	
	private void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在注册中...", true,
					true);
		else
			progTip.Show("正在注册中...");
	}

	private boolean isPwdOk(){
		String pwd=this.editTextName.getText()==null ? "":this.editTextName.getText().toString();
		String repeatpwd=editTextPwd.getText()==null ? "":editTextPwd.getText().toString();
		
		if (TextUtils.isEmpty(pwd)) {
			initNotify("密码不能为空");
			return false;
 		}
  		if(!CommonUtil.checkPassword(pwd)){
  			initNotify("密码不符合规则");
			return false;
 		}
  		if(!pwd.equals(repeatpwd)){
   			initNotify("两次密码不一致");
			return false;
  		}
		return true;
	}
 	
	private void user_register() {
 
		if(!isPwdOk()) return ;
 		showloading();
 		//register/checkMobileRegister?mobile=13720202020
 		final String pwd=this.editTextName.getText().toString();
 		RequestParams param = new RequestParams();
		param.put("mobile",registerPhone);
		param.put("passWord", pwd);
		param.put("smsCode", tmpsmsCode);
   
		ZrbRestClient.post("register/saveAppUser" , param,
				new AsyncHttpResponseHandler() {
 					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
 						progTip.Dismiss();
 						initNotify(User_registername.this.getString(R.string.error_net_loadfail));
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						progTip.Dismiss();
						String results = new String(arg2);
 						try {
 							Gson gson = new Gson();
 							RegisterDto tmpDto = gson.fromJson(results,	RegisterDto.class);
 							if(tmpDto.res==1){
  				 		       Intent it = new Intent(User_registername.this, User_registerBase1.class);
  				 		       it.putExtra("phonenum", registerPhone);
  				 		       it.putExtra("pwd", pwd);
  			 			       startActivity(it); 
  			 			       User_registername.this.finish();
  			 			   
  			 		
 							}
 							else
 								initNotify(tmpDto.msg);
  					      } 
 						 catch (Exception e) {
 							initNotify(User_registername.this.getString(R.string.error_serverdata_loadfail));
 						}
 					}
				});

	}
 
	private TextWatcher onNameTextChanged = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editTextName.getText().length() == 0)
				btnAccountclear.setVisibility(View.INVISIBLE);
			else{
				boolean flag=CommonUtil.checkUserName(editTextName.getText().toString());
				btnAccountclear.setVisibility(flag ? View.VISIBLE:View.INVISIBLE);
			}
			
			if( s.length() != 0 && null != editTextPwd.getText().toString() && !"".equals(editTextPwd.getText().toString())){
				changephone_btn.setEnabled(true);
				next_line.setBackgroundColor(getResources().getColor(R.color.login_name_password));
				next_line1.setBackgroundColor(getResources().getColor(R.color.login_name_password));
			}else{
				changephone_btn.setEnabled(false);
				next_line.setBackgroundColor(getResources().getColor(R.color.white));
				next_line1.setBackgroundColor(getResources().getColor(R.color.white));
			}
		}
	};
 	
	private TextWatcher onPwdTextChanged = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editTextPwd.getText().length() == 0)
				btnPwdclear.setVisibility(View.INVISIBLE);
			else {
				boolean flag=CommonUtil.checkPassword(editTextPwd.getText().toString());
			    btnPwdclear.setVisibility(flag ? View.VISIBLE:View.INVISIBLE);
			}
			
			if( s.length() != 0 && null != editTextName.getText().toString() && !"".equals(editTextName.getText().toString())){
				changephone_btn.setEnabled(true);
				next_line.setBackgroundColor(getResources().getColor(R.color.login_name_password));
				next_line1.setBackgroundColor(getResources().getColor(R.color.login_name_password));
			}else{
				changephone_btn.setEnabled(false);
				next_line.setBackgroundColor(getResources().getColor(R.color.white));
				next_line1.setBackgroundColor(getResources().getColor(R.color.white));
			}
		}
	};
	
	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		if (editTextName.getText().length() == 0)
			btnPwdclear.setVisibility(View.INVISIBLE);
		else
			btnPwdclear.setVisibility(View.VISIBLE);
	}
	
	 
}
