package com.zrb.mobile;

public class MyFindMoney {

	/** 主键,自增长 **/
	public Long id;

	/** 简介/标题 **/
	public String title;

	/** 意向发布时间 **/
	public Long pushTime;

	/** 关注次数 **/
	public Integer collectNum;
	
	/** 咨询次数 **/
	public Long queryNum;
	
}
