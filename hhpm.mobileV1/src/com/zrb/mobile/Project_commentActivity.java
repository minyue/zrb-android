package com.zrb.mobile;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.adapter.model.UserDtos;
import com.zrb.mobile.ui.NumEditText;
import com.zrb.mobile.ui.NumEditText.OnNumslistener;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

public class Project_commentActivity extends BaseActivity implements
		OnNumslistener, OnClickListener {

	private EditText editText2;
	private TextView txtNum, right_title;
	NumEditText numEditText;
	private Button submit_btn;
	Handler mhandler = new Handler();
	Tip progTip;
	Dialog AuthDialog;
	// Toast��ʾ��
	private RelativeLayout notify_view;
	private TextView notify_view_text;
	Map<String, Object> item1, item2, item3, item4, item5, item6, item7;
	int projectId;
	long targetId;
	String targetName;
	private int selectId = -1;
	int questionType = 0;
	int norColor = Color.parseColor("#ea8010");
	String replyuserStr;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.project_comment);
		if (bundle != null)
			restoreSelf(bundle);
		else {
			projectId = this.getIntent().getIntExtra("projectId", -1);
			targetId = this.getIntent().getLongExtra("targetId", -1);
			targetName = this.getIntent().getStringExtra("targetName");
		}
		initView2();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("projectId", projectId);
		outState.putLong("targetId", targetId);
		outState.putString("targetName", targetName);
	}

	private void restoreSelf(Bundle savedInstanceState) {
		projectId = savedInstanceState.getInt("projectId", -1);
		targetId = savedInstanceState.getLong("targetId", -1);
		targetName = savedInstanceState.getString("targetName");
	}

	private void initView2() {
		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		right_title = (TextView) this.findViewById(R.id.right_title);
		right_title.setVisibility(View.VISIBLE);
		right_title.setText("����");
		right_title.setOnClickListener(this);
		TextView txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setText("д����");

		numEditText = (NumEditText) this.findViewById(R.id.editText1t);
		numEditText.setMaxNums(240);
		numEditText.setOnNumslistener(this);
		txtNum = (TextView) this.findViewById(R.id.txtNum2);

		if (!TextUtils.isEmpty(targetName)) {
			replyuserStr = "@" + targetName + " ";

			// ����ɫ����ı�
			SpannableString ss = new SpannableString(replyuserStr);
			ss.setSpan(new ForegroundColorSpan(norColor), 0,
					replyuserStr.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			numEditText.setText(ss);
			numEditText.setSelection(replyuserStr.length());
		}

		LinearLayout tmpContainer1 = (LinearLayout) this
				.findViewById(R.id.check_option1);
		for (int i = 0; i < tmpContainer1.getChildCount(); i++)
			tmpContainer1.getChildAt(i).setOnClickListener(checkboxListener);
		LinearLayout tmpContainer2 = (LinearLayout) this
				.findViewById(R.id.check_option2);
		for (int i = 0; i < tmpContainer2.getChildCount(); i++)
			tmpContainer2.getChildAt(i).setOnClickListener(checkboxListener);

	}

	@Override
	public void showtextNum(int number) {
		txtNum.setText(number + "");
	}

	private OnClickListener checkboxListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() != selectId) {
				v.setSelected(true);
				if (selectId != -1)
					Project_commentActivity.this.findViewById(selectId)
							.setSelected(false);
				;

				selectId = v.getId();
			}

			switch (v.getId()) {
			case R.id.tv_temp_name1:
				questionType = 1101;
				break;
			case R.id.tv_temp_name2:
				questionType = 1102;
				break;
			case R.id.tv_temp_name3:
				questionType = 1103;
				break;
			case R.id.tv_temp_name4:
				questionType = 1104;
				break;
			case R.id.tv_temp_name5:
				questionType = 1105;
				break;
			case R.id.tv_temp_name6:
				questionType = 1106;
				break;
			case R.id.tv_temp_name7:
				questionType = 1107;
				break;
			}

		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.right_title:
			createNormal();
			break;
		}
	}

	private void createNormal() {
		if (questionType == 0) {
			CommonUtil.showToast("��ѡ������", Project_commentActivity.this);
			return;
		}
		if (TextUtils.isEmpty(numEditText.getText())) {
			CommonUtil.showToast("����д����", Project_commentActivity.this);
			return;
		}
		if (AppSetting.curUser == null) {
			CommonUtil.showToast("�������½", Project_commentActivity.this);
			return;
		}
		if (projectId == -1) {
			CommonUtil.showToast("��ĿidΪ��", Project_commentActivity.this);
			return;
		}
		// projectComment/createReply?pid=2090&content=I333fffdfdfd&targetId=1000000216&targetName=abc&zrb_front_ut=0f4ec09bca0783621edbfaf1cbba63dd

		RequestParams param = new RequestParams();
		param.put("pid", projectId);
		// param.put("content",numEditText.getText().toString());
		param.put("questionType", questionType);
		param.put("zrb_front_ut", AppSetting.curUser.zrb_front_ut);

		if (targetId != -1) {
			param.put("targetId", targetId);
			param.put("targetName", targetName);
			param.put("content",
					numEditText.getText().toString().replace(replyuserStr, ""));
		} else
			param.put("content", numEditText.getText().toString());

		CommonUtil.InfoLog("param", param.toString());

		ZrbRestClient.post("projectComment/"
				+ (targetId == -1 ? "createNormal" : "createReply"), param,
				new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						CommonUtil.showToast("���糬ʱ,����������",
								Project_commentActivity.this);
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						String results = new String(arg2);

						CommonUtil.InfoLog("test", results);
						try {
							Gson gson = new Gson();
							RegisterDto tmpDto = gson.fromJson(results,
									RegisterDto.class);
							if (tmpDto.res == 1) {
								CommonUtil.showToast("���۷���ɹ�",
										Project_commentActivity.this);
								Project_info_lience.hasNew = true;
								Project_commentActivity.this
										.setResult(RESULT_OK);
								Project_commentActivity.this.finish();
							}
						} catch (Exception e) {

							CommonUtil.showToast("fail:" + e.getMessage(),
									Project_commentActivity.this);
							System.out.println("eeee!!!!" + e.getMessage());

						}

					}

				});
	}

	/* ��ʼ��֪ͨ��Ŀ */
	private void initNotify(final String msg) {
		mhandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				notify_view_text.setText(msg);
				notify_view.setVisibility(View.VISIBLE);
				mhandler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						notify_view.setVisibility(View.GONE);
					}
				}, 2000);
			}
		}, 1000);
	}

	public void comment_type() {
		item1 = new HashMap<String, Object>();
		item1.put("id", 51);
		item1.put("itemName", "�ʽ��ѹ");
		item1.put("itemCode", 1101);
		item1.put("topItemCode", 1100);
		item1.put("status", 1);
		item1.put("weight", 0);
		item1.put("parentItem", null);

		item2 = new HashMap<String, Object>();
		item2.put("id", 55);
		item2.put("itemName", "���㷽ʽ");
		item2.put("itemCode", 1102);
		item2.put("topItemCode", 1100);
		item2.put("status", 1);
		item2.put("weight", 0);
		item2.put("parentItem", null);

		item3 = new HashMap<String, Object>();
		item3.put("id", 56);
		item3.put("itemName", "��Ŀ�ض�");
		item3.put("itemCode", 1103);
		item3.put("topItemCode", 1100);
		item3.put("status", 1);
		item3.put("weight", 0);
		item3.put("parentItem", null);

		item4 = new HashMap<String, Object>();
		item4.put("id", 57);
		item4.put("itemName", "�滮����");
		item4.put("itemCode", 1104);
		item4.put("topItemCode", 1100);
		item4.put("status", 1);
		item4.put("weight", 0);
		item4.put("parentItem", null);

		item5 = new HashMap<String, Object>();
		item5.put("id", 58);
		item5.put("itemName", "�۸��");
		item5.put("itemCode", 1105);
		item5.put("topItemCode", 1100);
		item5.put("status", 1);
		item5.put("weight", 0);
		item5.put("parentItem", null);

		item6 = new HashMap<String, Object>();
		item6.put("id", 59);
		item6.put("itemName", "������������");
		item6.put("itemCode", 1106);
		item6.put("topItemCode", 1100);
		item6.put("status", 1);
		item6.put("weight", 0);
		item6.put("parentItem", null);

		item7 = new HashMap<String, Object>();
		item7.put("id", 60);
		item7.put("itemName", "����");
		item7.put("itemCode", 1107);
		item7.put("topItemCode", 1100);
		item7.put("status", 1);
		item7.put("weight", 0);
		item7.put("parentItem", null);
	}

}
