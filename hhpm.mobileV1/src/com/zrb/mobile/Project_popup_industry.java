package com.zrb.mobile;
 
import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.adapter.Popup_IndustryAdapter;
import com.zrb.mobile.adapter.model.RegisterIndustryDto;
import com.zrb.mobile.adapter.model.RegisterIndustryDtos;
import com.zrb.mobile.ui.MyGridView;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import com.hhpm.lib.text.TagGroup.TagView;
 
import com.hhpm.lib.text.TagGroup.OnTagClickListener;

public class Project_popup_industry  extends BaseActivity implements OnClickListener, OnTagClickListener  {

	 
	int postCode=-1;
	//TagGroup	 mDefaultTagGroup;
	List<RegisterIndustryDto> mIndustrys;
	TagView curView;
	int curIndustry;
	GridView tagGridview;
	Popup_IndustryAdapter mindustryAdapter;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
   		this.setContentView(R.layout.popup_industy_choose);
  	
   		 curIndustry=this.getIntent().getIntExtra("industry", Constants.ALLIndustryCode) ;
  		 initView();
   		 LoadData();
	}
  	
	private void initView(){
		// mDefaultTagGroup = (TagGroup) findViewById(R.id.tag_group);
		//mDefaultTagGroup.setOnTagClickListener(this);
		tagGridview=(GridView)findViewById(R.id.otherGridView);
		tagGridview.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				   RegisterIndustryDto tmpDto= mindustryAdapter.channelList.get(arg2);
				   if(tmpDto.id==curIndustry) return;
				   setResult(tmpDto);
			}} );
		
		this.findViewById(R.id.close_city_img).setOnClickListener(this);
		TextView txtTitle=	(TextView)this.findViewById(R.id.titleText);
		txtTitle.setText("行业筛选");
  	}

	 
    private void  LoadData(){
	 
 
	  	 ZrbRestClient.get("industryApi/findAllIndustry", null, new AsyncHttpResponseHandler() {

				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,Throwable arg3) {
					// TODO Auto-generated method stub
					//progTip.Dismiss();
					//initNotify(User_registerIndustry.this.getString(R.string.error_net_loadfail));
	 			}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
					//progTip.Dismiss();
					String results = new String(arg2);
	 				try {
	 						Gson gson = new Gson();
	 						RegisterIndustryDtos tmpDto = gson.fromJson(results,	RegisterIndustryDtos.class);
	 						
	 						if(tmpDto.res==1){
	 							
	 							mindustryAdapter = new Popup_IndustryAdapter(Project_popup_industry.this, tmpDto.data);
	 							mindustryAdapter.setSelectIndustry(curIndustry);
	 							tagGridview.setAdapter(mindustryAdapter);//.setAdapter(otherAdapter);
	 							 /*if(!CommonUtil.isEmpty(tmpDto.data)){
	 								 mIndustrys=tmpDto.data;
  	 								 String[] tags =new String[tmpDto.data.size()];
  	 								 int selectIndex=-1;
		 							 for(int i=0;i<tmpDto.data.size();i++){
		 								tags[i]=tmpDto.data.get(i).industryName;
		 								if(curIndustry==tmpDto.data.get(i).id)
		 									selectIndex=i;
		 							 }
		 							mDefaultTagGroup.setTags(tags);
		 							if(selectIndex!=-1)	 {
		 								curView=mDefaultTagGroup.getTagAt(selectIndex);
		 								curView.setSelected(true);
		 							}
 	 							 }*/
	 							
	 						}
	 						else{}
	 							//initNotify(tmpDto.msg);
	  				    } catch (Exception e) {
	  					    //initNotify(User_registerIndustry.this.getString(R.string.error_serverdata_loadfail));
	 				}
	 			}
			});
		
	}
    
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
 
	 	 case R.id.close_city_img:
			this.finish();
			  overridePendingTransition(R.anim.slide_still, R.anim.slide_down_exit);
			break;
		}
	}
	
	@Override
	public void onBackPressed(){
		
		 this.finish();
		  overridePendingTransition(R.anim.slide_still, R.anim.slide_down_exit);
	}
	
 
	
/*	this.startActivityForResult(intent, 100);
	FindProject_MainFragment.this.getActivity().overridePendingTransition(R.anim.slide_up_enter,R.anim.slide_up_exit);*/
	
	void setResult(RegisterIndustryDto tag){
		RegisterIndustryDto curDto=tag;
		/*for(int i=0;i<mIndustrys.size();i++){
			curDto=mIndustrys.get(i);
			if(tag.equals(curDto.industryName)) break;
		} */
		
		 if(curDto!=null){
         	Intent tmpIntent=new Intent();
 			tmpIntent.putExtra("name", curDto.industryName);
 			tmpIntent.putExtra("code", curDto.id);
 			Project_popup_industry.this.setResult(Activity.RESULT_OK, tmpIntent);
 			Project_popup_industry.this.finish();
 			overridePendingTransition(R.anim.slide_still, R.anim.slide_down_exit);
         }
		/*final RegisterIndustryDto tmpDto=curDto;
		myHandler.postDelayed(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}}, 50);
       */
		
	}

	//Handler myHandler=new Handler();
 
	@Override
	public void onTagClick(View view,String tag) {
		// TODO Auto-generated method stub
		if(curView!=view){
 			 if(null!=curView)  curView.setSelected(false);
 			 
 			TagView tmpview=(TagView)view;
  			tmpview.setSelected(true);
			curView=tmpview;
			//setResult(tag);
 		}
			
	}



	 

}
