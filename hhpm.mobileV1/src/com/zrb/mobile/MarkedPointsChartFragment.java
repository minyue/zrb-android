package com.zrb.mobile;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.zrb.mobile.adapter.model.inspectStatisticDto;
import com.zrb.mobile.chart.Line;
import com.zrb.mobile.chart.LineChartView;
import com.zrb.mobile.chart.LineChartView.OnChartPointClickListener;
import com.zrb.mobile.chart.LinePoint;
import com.zrb.mobile.chart.LinePoint.Type;

 

import android.app.Activity;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MarkedPointsChartFragment extends Fragment {
 
    private LineChartView chart;
    private String title;
    private List<inspectStatisticDto> mchatpoints;
    private int  attentColor=Color.parseColor("#ff9999");
    private int  shareColor=Color.parseColor("#058cf3");
    private int  surveyColor=Color.parseColor("#eea14b");
    private float mMaxY=0;
    private float mMinY=0;
    private int mchartType=-1;
    private boolean isfloatShow=false;
    
    public static MarkedPointsChartFragment newInstance(String title,List<inspectStatisticDto> chatpoints,int chartType){
    	
    	return newInstance(title,chatpoints,chartType,false);
    }
    
    public static MarkedPointsChartFragment newInstance(String title,List<inspectStatisticDto> chatpoints,int chartType,boolean isfloat) {
        MarkedPointsChartFragment fragment = new MarkedPointsChartFragment();
        fragment.title = title;
        fragment.mchartType=chartType;
        fragment.mchatpoints=chatpoints;
        fragment.isfloatShow=isfloat;
        return fragment;
    }

    public MarkedPointsChartFragment() {
        // Required empty public constructor
    }

    
    private void getRange(){
    	
    	for(int i=0;i<mchatpoints.size();i++){
    		float curValue=mchatpoints.get(i).number;
    		if(curValue>mMaxY)  mMaxY=curValue;
    		if(curValue<mMinY)  mMinY=curValue;
    	}
    	
    	if(mMaxY<20){
    		mMaxY=20;
    		mMinY=0;
    		return;
    	}
    	if(mMaxY==mMinY){
    		if(mMinY==0) mMaxY=40;
    		else  
    			mMaxY= mMaxY+10*4;
    	}
    	else{
    		if(mMinY<10){ mMinY=0;}
    		if(mMaxY-mMinY<4){mMaxY+=10*4;}
    		else
    			mMaxY=mMaxY+ (int)((mMaxY-mMinY)/4);
     	}
     }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       // getActivity().getActionBar().setTitle(title);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chartmain, container, false);
       // ((TextView) view.findViewById(R.id.description)).setText("test");

        if(mchatpoints==null||mchatpoints.size()<=0){
        	View maskView=	view.findViewById(R.id.nodata_mask);
        	maskView.setVisibility(View.VISIBLE);
        	return view;
        }
        int stepsize=18;
        getRange();
        chart = (LineChartView) view.findViewById(R.id.chart);
        //chart.setViewPort(0, 0, 100, 160);
        chart.setViewPort(-10, -2, 115,mMaxY);// 40
        chart.setGridSize(stepsize, 0, (int)((mMaxY-mMinY)/4), 0);//10
       // chart.enableVerticalGrid(false, true);
         

    
        // default line
  /*      Line line = generateLine(-100, 360, 10, 50, 90);
        for (LinePoint point : line.getPoints()) {
            point.setVisible(true);
            point.setType(Type.SQUARE);
        }
        chart.addLine(line);*/
        // fat orange line with under fill
        
       
      /*  Line line2 = generateLine(0, 360, 20, 20, 50).setColor(0xffff8800)
                                                        .setFilled(true)
                                                        .setFilledColor(0x44ff8800)
                                                        .setStrokeWidth(4);*/
        int curColor=0;
		switch (mchartType) {
		  case 1:
			curColor = attentColor;
			break;
		  case 2:
			curColor = shareColor;
			break;
		  default:
			curColor = surveyColor;
			break;
 		}
   
        int pointNum= mchatpoints.size();
        Line line2 = generateLine(0, pointNum*stepsize, stepsize, 0, 0).setColor(curColor)//0xffff8800
                .setFilled(false)
                .setFilledColor(0x44ff8800)
                .setStrokeWidth(3);//.smoothLine(1)
        
        
        for (LinePoint point : line2.getPoints()) {
            point.setVisible(true);
            point.setType(LinePoint.Type.CIRCLE);
            point.getStrokePaint().setColor(curColor);//0xffff8800
        }
        chart.addLine(line2);
        //
/*        Line line3 = generateLine(-100, 360, 5, 100, 150).setColor(0xff009966);
        for (LinePoint point : line3.getPoints()) {
            point.setVisible(true);
            point.setType(Type.TRIANGLE);
            point.getStrokePaint().setColor(0xff009966);
        }
        chart.addLine(line3);*/
        
         
        SparseArray<String> horValues = new SparseArray<String>();
       // Calendar c = new GregorianCalendar();
        for (int i = 0; i < pointNum; i++) {
          //  c.set(Calendar.MONTH, i);
            horValues.put(i * stepsize,fromatDate(mchatpoints.get(i).date));
        }

        chart.setHorValuesText(horValues); 
        
        chart.setOnPointClickListener(new OnChartPointClickListener() {

            @Override
            public void onPointClick(LinePoint point, Line line) {
                for (LinePoint p: line.getPoints()){
                    p.setRadius(5);
                    p.setTextVisible(false);
                }
                point.setRadius(10);
                point.setTextVisible(true);
                point.setText(isfloatShow ? point.getY()+"":((int)point.getY()+""));
                point.getTextPaint().setColor(line.getPaint().getColor());
             }
        });
        
        setHasOptionsMenu(true);
        return view;
    }

    private String fromatDate(String date){
    	
    	if(!TextUtils.isEmpty(date)){
    		if(date.contains("-")){
    			String[] tmpArr=date.split("-");
        		return tmpArr[1]+"."+tmpArr[2];
    		}
    		else
    			return date;
     	}
    	return "";
    }
    
    private Line generateLine(int startX, int endX, int step, int minY, int maxY) {
        Line line = new Line(getActivity());
        
        int k=0;
        for (int i = startX; i < endX; i += step) {
        	
        	float curValue=mchatpoints.get(k).number;
        	//LinePoint tmp=new LinePoint(getActivity(), i, (int) (Math.random() * (maxY - minY) + minY));
        	LinePoint tmp=new LinePoint(getActivity(), i, curValue);
        /*	tmp.setText("2015."+k);
        	tmp.setTextVisible(true);*/
            line.addPoint(tmp);
            k++;
        }
        return line;
    }
 
}
