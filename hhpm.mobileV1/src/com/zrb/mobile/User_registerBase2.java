package com.zrb.mobile;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.content.Intent;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class User_registerBase2 extends RegisterBaseActivity implements OnClickListener   {


	TextView txtTitle;
 	Button nextBtn;
 
	EditText medittemp_name,medit_dept;
	String registerPhone,registerPwd;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
 
		setContentView(R.layout.u_center_registerbase_step1);
 		if (bundle != null)
			restoreSelf(bundle);
		else {
			registerPhone=this.getIntent().getStringExtra("phonenum");
			registerPwd=this.getIntent().getStringExtra("pwd");
		}
 		initView();
 	}
 	
	 @Override
	 protected  void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString("registerPhone", registerPhone);
     }

    private void restoreSelf(Bundle savedInstanceState){
    	registerPhone = savedInstanceState.getString("registerPhone");
    }
	
    protected void initView() {
  		super.initView();
		txtTitle=(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("企业认证");
		
		TextView txtcompany=(TextView)this.findViewById(R.id.tv_temp_name);
		txtcompany.setText(this.getString(R.string.personnal_user_registerbase_companyname));
		
		medittemp_name =(EditText)this.findViewById(R.id.edit_temp_name);
		medittemp_name.setHint(this.getString(R.string.personnal_user_register_companyhite));
		medittemp_name.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
		
		this.findViewById(R.id.btn_step_process).setEnabled(true);
		this.findViewById(R.id.line_step_start).setSelected(true);
 		this.findViewById(R.id.title_left_root).setOnClickListener(this);
 		
 		medit_dept=(EditText)this.findViewById(R.id.edit_dept_name);
 		
		this.findViewById(R.id.re_location).setVisibility(View.GONE);
		this.findViewById(R.id.re_industry).setVisibility(View.GONE);
		this.findViewById(R.id.re_deptlayout).setVisibility(View.VISIBLE);
		
		nextBtn=(Button)this.findViewById(R.id.send_btn);
		nextBtn.setOnClickListener(this);
		nextBtn.setEnabled(true);
		
 	}
    
 
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.send_btn:
			saveStep2();
			break;
 
		}

	}

	private void saveStep2(){
		 showloading("提交中...");
		 RequestParams param = new RequestParams();
		 
		 param.put("phone",registerPhone);
		 param.put("org", medittemp_name.getText().toString());
		 param.put("position", medit_dept.getText().toString());//职务
 	 
		 CommonUtil.InfoLog("saveUserInfoApiparam", param.toString());
 		 ZrbRestClient.post("register/saveUserInfoApi", param,  
		  new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				  hideloading();
				  initNotify("网络出错");	 
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				progTip.Dismiss();
				String results = new String(arg2);
				CommonUtil.InfoLog("saveUserInfoApi", results);
				try {
					Gson gson = new Gson();
					RegisterDto tmpDto = gson.fromJson(results,RegisterDto.class);
					if(tmpDto.res==1){
						 Intent it5 = new Intent(User_registerBase2.this, User_registerBase1.class);
						 it5.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						 it5.putExtra("phonenum", registerPhone);
 					 	 it5.putExtra("pwd", registerPwd);
						 it5.putExtra("isfinish", true);
			 		     startActivity(it5); 
			 		     User_registerBase2.this.finish();
					}else{
						initNotify(tmpDto.msg);
					}
				} catch (Exception e) {
					 initNotify("网络出错");	 
 				}
			}} ); 
	}
	
	
	 

}
