package com.zrb.mobile;

 

import java.lang.reflect.Type;
import java.sql.Date;
import java.util.List;

import org.apache.http.Header;

 
 
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.adapter.model.ProjectDtos;
import com.zrb.mobile.ui.CustomScrollView;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.mobile.ui.NoScrollListView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.zrb.mobile.adapter.*;


public class TestApp_ProjectActivity extends AbsListViewBaseActivity implements OnGestureListener, OnTouchListener, OnClickListener {

	private ViewFlipper viewFlipper;
	private GestureDetector mGestureDetector;
	private int currentPage = 0;
	private static final int FLING_MIN_DISTANCE = 50;
	private static final int FLING_MIN_VELOCITY = 0;
	//ProjectListAdapter mlistAdapter;
	ResultAdapter mlistAdapter;
	//NoScrollListView mlistview;
	CustomloadingView loadingview;
	private String[] menus={"市场监测","行业动态","金融市场","政策新闻","政府招商","项目转让","规划设计","推介会集"} ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		
		setContentView(R.layout.app_home_activity);
	
		InitView();
		LoadData();
	}
	
	 @Override

     public void onDestroy() {

		 if(mlistAdapter!=null) mlistAdapter.displayedImageclear();
          super.onDestroy();
         // super.onDestroy();
        
     }


	
	private void InitView()
	{
 		 
 		
		LinearLayout menus=	(LinearLayout)this.findViewById(R.id.menu_container);
		for(int i=0;i<menus.getChildCount();i++)    menus.getChildAt(i).setOnClickListener(this);
		 
		LinearLayout menus2=(LinearLayout)this.findViewById(R.id.menu_container2);
		for(int i=0;i<menus2.getChildCount();i++)  	menus2.getChildAt(i).setOnClickListener(this);
		
		
 		loadingview=(CustomloadingView)this.findViewById(R.id.list_emptyview);
		mlistAdapter=new ResultAdapter(this);
		mlistAdapter.setImgLoader(imageLoader);
		
		//findViewById(R.id.button1).;
        viewFlipper = (ViewFlipper) findViewById(R.id.mViewFliper_vf);
        mGestureDetector = new GestureDetector(this);
        viewFlipper.setOnTouchListener(this);
        viewFlipper.setLongClickable(true);
        viewFlipper.setOnClickListener(new OnClickListener() {
    		
    		@Override
    		public void onClick(View v) {
    			// TODO Auto-generated method stub
    			//toastInfo("点击事件");
    		}
    	});
		//com.zrb.mobile.ui.CustomScrollView
        CustomScrollView myScrollView = (CustomScrollView) findViewById(R.id.viewflipper_scrollview);
        myScrollView.setOnTouchListener(onTouchListener);
        myScrollView.setGestureDetector(mGestureDetector);
        
        listView = (NoScrollListView) findViewById(R.id.listView1);
       // listView.setAdapter(mlistAdapter);
        
	}

	Tip progTip;
    private void LoadData()
    {
    	//progTip=ProgressDialogEx.Show(this," title", "loadtxt", true, true);
    	ZrbRestClient.get("proList.jspx", null,  new AsyncHttpResponseHandler()
    	{

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				// TODO Auto-generated method stub
				System.out.println("ff");
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				//progTip.Dismiss();
				loadingview.Hide();
			/*	String results=new String(arg2);
				Toast.makeText(App_HomeActivity.this, results+"text", Toast.LENGTH_LONG).show();
				System.out.println(results+"sss");*/
				String results=new String(arg2);
				try 
				{
					
					
			 
					
				    GsonBuilder gsonb = new GsonBuilder();  
		            //Json中的日期表达方式没有办法直接转换成我们的Date类型, 因此需要单独注册一个Date的反序列化类.   
		          //  DateDeserializer ds = new DateDeserializer();  
		            //给GsonBuilder方法单独指定Date类型的反序列化方法   
		           // gsonb.registerTypeAdapter(Date.class, ds);  
		              
		            Gson gson = gsonb.create();  
					
		            
		          //  Type type=new TypeToken<List<QxybResultDTO>>(){}.getType();
		            ProjectDtos tmpDto=	gson.fromJson(results, ProjectDtos.class);
		            //System.out.println(tmp.Result.ALARMTIME);
		            
		            
		           // List<QxybResultDTO> tmp=gson.fromJson(tmpstr, type);
		          //  System.out.println(tmp.get(0).ALARMTIME);
		          
		          //  Toast.makeText(App_HomeActivity.this,   tmpDto.count+"text", Toast.LENGTH_LONG).show();
		            
		          /*  mlistAdapter.CurrentDatas.addAll(tmpDto.results);
		            mlistAdapter.notifyDataSetChanged();*/
		            
				}
				catch(Exception e)
				{
					
					System.out.println("eeee!!!!"+e.getMessage());
					
				}
				
			}});
    	
    	
    }
	
    private OnTouchListener onTouchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			return mGestureDetector.onTouchEvent(event);
		}
	};
  
	
	private void showNextView(){

		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.push_left_in));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.push_left_out));		
		viewFlipper.showNext();
		currentPage ++;
		if (currentPage == viewFlipper.getChildCount()) {
			//displayRatio_normal(currentPage - 1);
			currentPage = 0;
			//displayRatio_selelct(currentPage);
		} else {
			//displayRatio_selelct(currentPage);
			//displayRatio_normal(currentPage - 1);
		}
		Log.e("currentPage", currentPage + "");		
		
	}
	private void showPreviousView(){
		//displayRatio_selelct(currentPage);
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.push_right_in));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.push_right_out));
		viewFlipper.showPrevious();
		currentPage --;
		if (currentPage == -1) {
			//displayRatio_normal(currentPage + 1);
			currentPage = viewFlipper.getChildCount() - 1;
			//displayRatio_selelct(currentPage);
		} else {
			//displayRatio_selelct(currentPage);
			//displayRatio_normal(currentPage + 1);
		}
		Log.e("currentPage", currentPage + "");		
	}
	
//	OnGestureListener
	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		Log.e("view", "onFling");
		if (e1.getX() - e2.getX()> FLING_MIN_DISTANCE  
                && Math.abs(velocityX) > FLING_MIN_VELOCITY ) {
			Log.e("fling", "left");
			showNextView();
			//showNext = true;
//			return true;
		} else if (e2.getX() - e1.getX() > FLING_MIN_DISTANCE  
                && Math.abs(velocityX) > FLING_MIN_VELOCITY){
			Log.e("fling", "right");
			showPreviousView();
			//showNext = false;
//			return true;
		}
		return false;
	}


	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return mGestureDetector.onTouchEvent(event);
	}
//	OnGestureListener  end


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		//LoadData();
		if(arg0.getTag()!=null)
		{
			//list.html?listtype=1
			
			String url="file:///android_asset/www/list.html?listtype=";
			//CommonUtil.showToast(arg0.getTag().toString(), this);
			

		 	Intent it = new Intent(this, Webview_Activity.class);
		//	Intent it = new Intent(Splash_Activity.this, HomeActivity.class);
			int index=Integer.parseInt(arg0.getTag().toString());
			
		 	it.putExtra("url", url+arg0.getTag().toString());
		 	it.putExtra("title", menus[index-1]);
			startActivity(it);
		}
	}
	
	
}
