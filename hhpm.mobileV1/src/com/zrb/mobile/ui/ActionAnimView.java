package com.zrb.mobile.ui;

import com.zrb.mobile.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

public class ActionAnimView extends TextView
{
  private Animation a;

  public ActionAnimView(Context paramContext)
  {
    super(paramContext);
    onAnimationEnd();
    this.a = AnimationUtils.loadAnimation(paramContext, R.anim.action_exit);
  }

  public ActionAnimView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.a = AnimationUtils.loadAnimation(paramContext, R.anim.action_exit);
  }

  public ActionAnimView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.a = AnimationUtils.loadAnimation(paramContext, R.anim.action_exit);
  }

  public void fadeOut()
  {
    clearAnimation();
    this.a.reset();
    setVisibility(View.VISIBLE);
    //setVisibility(0);
    startAnimation(this.a);
  }

  public void offsetFadeout(int paramInt1, int paramInt2)
  {
    clearAnimation();
    int i = getWidth();
    int j = getHeight();
    int k = paramInt1 - i / 2;
    int l = paramInt2 - j;
    int i1 = l - j;
    AnimationSet localAnimationSet = new AnimationSet(true);
    TranslateAnimation localTranslateAnimation = new TranslateAnimation(0, k, 0, k, 0, l, 0, i1);
    AlphaAnimation localAlphaAnimation = new AlphaAnimation(1.0F, 0.0F);
    localAnimationSet.addAnimation(localTranslateAnimation);
    localAnimationSet.addAnimation(localAlphaAnimation);
    localAnimationSet.setDuration(this.a.getDuration());
    //setVisibility(0);
    setVisibility(View.VISIBLE);
    startAnimation(localAnimationSet);
  }

  protected void onAnimationEnd()
  {
	  setVisibility(View.INVISIBLE);
    //setVisibility(4);
  }
}