package com.zrb.mobile.ui.fragment;

import com.zrb.mobile.register.INotifyEvent;

import android.app.Activity;
import android.support.v4.app.Fragment;

public class NotifyFragment extends Fragment{
	
	 INotifyEvent mNotifyEvent;
	 @Override
	 public void onAttach(Activity activity) {
	        super.onAttach(activity);

	        if (activity instanceof INotifyEvent)
	        	mNotifyEvent= (INotifyEvent)activity;
	 }
  	
	  
	    @Override
	    public void onDetach() {
	        super.onDetach();
 	        mNotifyEvent=null;
	    }
	 
	 public void initNotify(String msg){
	    	
		/*  if(this.getActivity()==null) return;
		   if(this.getActivity() instanceof INotifyEvent){
			   
			   ((INotifyEvent)this.getActivity()).initNotify(msg);
		   }*/
		 
	    	 if(null!=mNotifyEvent) mNotifyEvent.initNotify(msg);
	    }
	    
	 public void showloading(String msg){
		   
		   if(null!=mNotifyEvent) mNotifyEvent.showloading(msg);
	   }
	    
	 public void hideloading(){
	 	   if(null!=mNotifyEvent) mNotifyEvent.hideloading();
	   }
}
