package com.zrb.mobile.ui.fragment;

 

import android.app.Activity;
import android.support.v4.app.Fragment;

public class DelayFragment extends Fragment{
	
	 IpostDelayed mNotifyEvent;
	 @Override
	 public void onAttach(Activity activity) {
	        super.onAttach(activity);

	        if (activity instanceof IpostDelayed)
	        	mNotifyEvent= (IpostDelayed)activity;
	 }
  	
	  
	 @Override
	 public void onDetach() {
	        super.onDetach();
 	        mNotifyEvent=null;
	 }
	 
 
	    
 
	 public void postDelayed(Runnable runnable,long delay){
	 	   if(null!=mNotifyEvent) mNotifyEvent.postDelayed(runnable,delay);
	   }
}
