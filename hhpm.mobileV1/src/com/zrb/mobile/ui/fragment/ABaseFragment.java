package com.zrb.mobile.ui.fragment;

import com.hhpm.lib.support.inject.InjectUtility;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class ABaseFragment extends Fragment {

	  private ViewGroup rootView;// 根视图
	  // 标志是否ContentView是否为空
	    private boolean contentEmpty = true;
	  
	  @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);

	    /*    if (activity instanceof BaseActivity)
	            ((BaseActivity) activity).addFragment(toString(), this);*/
	    }

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	      /*  taskManager = new TaskManager();
	        if (savedInstanceState != null)
	            taskManager.restore(savedInstanceState);*/
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	        if (inflateContentView() > 0) {
	            rootView = (ViewGroup) inflater.inflate(inflateContentView(), null);
	            rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

	            _layoutInit(inflater, savedInstanceState);

	            layoutInit(inflater, savedInstanceState);

	            return rootView;
	        }

	        return super.onCreateView(inflater, container, savedInstanceState);
	    }

	    /**
	     * 根视图
	     *
	     * @return
	     */
	    ViewGroup getRootView() {
	        return rootView;
	    }

	    @Override
	    public void onActivityCreated(Bundle savedInstanceState) {
	        super.onActivityCreated(savedInstanceState);

	        if (savedInstanceState == null)
	            requestData();
	    }

	    /**
	     * Action的home被点击了
	     *
	     * @return
	     */
	    public boolean onHomeClick() {
	        return onBackClick();
	    }

	    /**
	     * 返回按键被点击了
	     *
	     * @return
	     */
	    public boolean onBackClick() {
	        return false;
	    }

	    /**
	     * 初次创建时默认会调用一次
	     */
	    public void requestData() {

	    }
	
	    /**
	     * A*Fragment重写这个方法
	     *
	     * @param inflater
	     * @param savedInstanceSate
	     */
	    void _layoutInit(LayoutInflater inflater, Bundle savedInstanceSate) {
	    	
	    	InjectUtility.initInjectedView(this, rootView);
	       /* InjectUtility.initInjectedView(this, rootView);

	        if (emptyLayout != null) {
	            View reloadView = emptyLayout.findViewById(R.id.layoutReload);
	            if (reloadView != null)
	                setViewOnClick(reloadView);
	        }

	        if (loadFailureLayout != null) {
	            View reloadView = loadFailureLayout.findViewById(R.id.layoutReload);
	            if (reloadView != null)
	                setViewOnClick(reloadView);
	        }

	        setViewVisiable(loadingLayout, View.GONE);
	        setViewVisiable(loadFailureLayout, View.GONE);
	        setViewVisiable(emptyLayout, View.GONE);
	        if (isContentEmpty()) {
	            if (savedInstanceSate != null) {
	                requestData();
	            }
	            else {
	                setViewVisiable(emptyLayout, View.VISIBLE);
	                setViewVisiable(contentLayout, View.GONE);
	            }
	        }
	        else {
	            setViewVisiable(contentLayout, View.VISIBLE);
	        }*/
	    }

	    /**
	     * 子类重写这个方法，初始化视图
	     *
	     * @param inflater
	     * @param savedInstanceSate
	     */
	    protected void layoutInit(LayoutInflater inflater, Bundle savedInstanceSate) {

	    }

	    protected View findViewById(int viewId) {
	        if (rootView == null)
	            return null;

	        return rootView.findViewById(viewId);
	    }

	    abstract protected int inflateContentView();

	    public void setContentEmpty(boolean empty) {
	        this.contentEmpty = empty;
	    }

	    public boolean isContentEmpty() {
	        return contentEmpty;
	    }

	    /**
	     * 视图点击回调，子类重写
	     *
	     * @param view
	     */
	    public void onViewClicked(View view) {
	   /*     if (view.getId() == R.id.layoutReload)
	            requestData();
	        else if (view.getId() == R.id.layoutRefresh)
	            requestData();*/
	    }

	    void setViewVisiable(View v, int visibility) {
	        if (v != null)
	            v.setVisibility(visibility);
	    }

	    protected void setViewOnClick(View v) {
	        if (v == null)
	            return;

	        v.setOnClickListener(innerOnClickListener);
	    }

	    View.OnClickListener innerOnClickListener = new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	            onViewClicked(v);
	        }

	    };
	
}
