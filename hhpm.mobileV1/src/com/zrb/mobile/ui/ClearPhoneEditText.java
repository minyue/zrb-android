package com.zrb.mobile.ui;

import com.zrb.mobile.R;
import com.zrb.mobile.ui.ClearEditTextEx.onTextCorrectListener;
import com.zrb.mobile.utility.CommonUtil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ClearPhoneEditText extends RelativeLayout implements View.OnFocusChangeListener, TextWatcher {

 

	onTextCorrectListener monTextCorrectListener;

	public void setOnTextCorrectListener(onTextCorrectListener ontextcorrectListener) {
		monTextCorrectListener = ontextcorrectListener;

	}

	Context paramContext;
	ImageView hiteImg;
	EditText inputcontent;
	ImageView img_clean;
	int curInputType = -1;// 1 phone,2 pwd
	Drawable mIndicator;
	String hitetext = "";

	public ClearPhoneEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		paramContext = context;
		Init(attrs);
	}

	private void Init(AttributeSet attrs) {

		TypedArray a = paramContext.obtainStyledAttributes(attrs, R.styleable.clearEditText, 0, 0);
		mIndicator = a.getDrawable(R.styleable.clearEditText_clr_drawableleft);
		hitetext = a.getString(R.styleable.clearEditText_clr_texthite);

		a.recycle();

		View localView = LayoutInflater.from(paramContext).inflate(R.layout.clear_edit_textex, this, true);
		
		hiteImg = (ImageView) localView.findViewById(R.id.img_icon);
		hiteImg.setImageDrawable(mIndicator);
 
		inputcontent = (EditText) localView.findViewById(R.id.edit_phone);
		inputcontent.setHint(hitetext);
		inputcontent.setInputType(InputType.TYPE_CLASS_PHONE);
		inputcontent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(13)});
	 
		img_clean = (ImageView) localView.findViewById(R.id.phone_btn_clean);
		img_clean.setVisibility(View.GONE);

		ImageView img_right = (ImageView) localView.findViewById(R.id.phone_btn_right);
		img_right.setVisibility(View.GONE);

		inputcontent.setOnFocusChangeListener(this);
		inputcontent.addTextChangedListener(this);

		img_clean.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inputcontent.setText("");
			}
		});
	}

	public void setText(CharSequence titledes) {
		inputcontent.setText(titledes);
	}

	public void setHiteResId(int resid) {
		inputcontent.setHint(resid);
	}

	public String getContentValue() {
		if (TextUtils.isEmpty(inputcontent.getText()))
			return "";
		return inputcontent.getText().toString();
	}

	public boolean isEmpty() {

		if (TextUtils.isEmpty(inputcontent.getText()))
			return true;
		return false;
	}

	public void setInputType(int inputType) {
		curInputType = inputType;
	}

	/*
	 * 
	 * editNameText .setOnFocusChangeListener(new
	 * android.view.View.OnFocusChangeListener() {
	 * 
	 * @Override public void onFocusChange(View v, boolean hasFocus) { if
	 * (!TextUtils.isEmpty(editNameText.getText())) { if
	 * (editNameText.getText().length() != 0 && !hasFocus &&
	 * CommonUtil.checkPassword(editNameText .getText().toString())) {
	 * phone_btn_clean.setVisibility(View.GONE);
	 * phone_btn_right.setVisibility(View.VISIBLE); } else {
	 * phone_btn_clean.setVisibility(View.VISIBLE);
	 * phone_btn_right.setVisibility(View.GONE); } } } });
	 */

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
/*		if (!TextUtils.isEmpty(inputcontent.getText())) {

			boolean inputflag = true;
			switch (curInputType) {
			case 1:
				inputflag = CommonUtil.isMobileNO(inputcontent.getText().toString().replace(" ", ""));
				break;
			case 2:
				inputflag = CommonUtil.checkPassword(inputcontent.getText().toString().replace(" ", ""));
				break;
			}
			if (!hasFocus && inputflag) {
				img_clean.setVisibility(View.GONE);
			 
			} else {
				img_clean.setVisibility(View.VISIBLE);
				 
			}
		}*/
		
		if (v.getId() == R.id.edit_phone) {
			if (hasFocus && inputcontent.getText().length() != 0)
				img_clean.setVisibility(View.VISIBLE);
			else
			{
				img_clean.setVisibility(View.INVISIBLE);
//				if(isPhoneOk(true,false)) { 
//					mbtn_nextstep.setEnabled(true);
//					mbtn_verifycode.setEnabled(true);
//				}
//				else  { 
//					mbtn_nextstep.setEnabled(false);
//					mbtn_verifycode.setEnabled(false);
//				}
			}
 		}  
		
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
 
 		if(s.length() == 13) { 
			if (null != monTextCorrectListener)
				monTextCorrectListener.onTextCorrect(true);
			 
		}
		else  { 
			if (null != monTextCorrectListener)
				monTextCorrectListener.onTextCorrect(false);
	 
		}
		
		if (inputcontent.getText().length() == 0)
			img_clean.setVisibility(View.INVISIBLE);
		else
			img_clean.setVisibility(View.VISIBLE);
		
	    String contents = s.toString();
		int length = contents.length();
		if (length == 4) {
			if (contents.substring(3).equals(" ")) { // -
				contents = contents.substring(0, 3);
				inputcontent.setText(contents);
				inputcontent.setSelection(contents.length());
			} else { // +
				contents = contents.substring(0, 3) + " "+ contents.substring(3);
				inputcontent.setText(contents);
				inputcontent.setSelection(contents.length());
			}
		} else if (length == 9) {
			if (contents.substring(8).equals(" ")) { // -
				contents = contents.substring(0, 8);
				inputcontent.setText(contents);
				inputcontent.setSelection(contents.length());
			} else {// +
				contents = contents.substring(0, 8) + " "+ contents.substring(8);
				inputcontent.setText(contents);
				inputcontent.setSelection(contents.length());
			}
		}

	}

	/*
	 * editNameText.addTextChangedListener(new TextWatcher() {
	 * 
	 * @Override public void onTextChanged(CharSequence s, int start, int
	 * before, int count) { if (s.length() != 0 && null !=
	 * editPassWord.getText().toString() &&
	 * !"".equals(editPassWord.getText().toString())) {
	 * loginButton.setEnabled(true); //
	 * loginButton.setBackgroundDrawable(getResources
	 * ().getDrawable(R.drawable.login_button_blue)); } else {
	 * loginButton.setEnabled(false); //
	 * loginButton.setBackgroundDrawable(getResources
	 * ().getDrawable(R.drawable.login_button_blue2)); } }
	 * 
	 * @Override public void beforeTextChanged(CharSequence s, int start, int
	 * count, int after) {
	 * 
	 * }
	 * 
	 * @Override public void afterTextChanged(Editable s) {
	 * 
	 * } });
	 */
}
