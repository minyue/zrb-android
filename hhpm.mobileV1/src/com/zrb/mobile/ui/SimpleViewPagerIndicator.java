package com.zrb.mobile.ui;

import com.zrb.discover.widget.DrawableCenterTextView;
import com.zrb.mobile.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SimpleViewPagerIndicator extends LinearLayout
{

	private static final int COLOR_TEXT_NORMAL =Color.parseColor("#666666");// 0xFF000000;
	private static final int COLOR_INDICATOR_COLOR =Color.parseColor("#058cf3");// Color.GREEN;

	private String[] mTitles;
	private int mTabCount;
	private int mIndicatorColor = COLOR_INDICATOR_COLOR;
	
	private int mtextColor= COLOR_INDICATOR_COLOR;
	private float mTranslationX;
	private Paint mPaint = new Paint();
	private int mTabWidth;
	private TextView selview;
	boolean isShowtitle=true;
    float mtextsize=16;
	
	
	public SimpleViewPagerIndicator(Context context) {
		this(context, null);
	}

	@SuppressLint("NewApi")
	public SimpleViewPagerIndicator(Context context, AttributeSet attrs) {
		super(context, attrs,0);
		init(context, attrs, 0);
 	}
	
 

   private void init(Context context, AttributeSet attrs, int defStyle) {
		
	  
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.simpleindicator, defStyle, 0);

		mtextColor = a.getColor(R.styleable.simpleindicator_text_color, 0);
		
		mIndicatorColor = a.getColor(R.styleable.simpleindicator_line_color, COLOR_INDICATOR_COLOR);
		isShowtitle = a.getBoolean(R.styleable.simpleindicator_show_title, true); 
		mtextsize=a.getDimensionPixelSize(R.styleable.simpleindicator_text_size, 16);
		 
		a.recycle();
		
		mPaint.setColor(mIndicatorColor);
		mPaint.setStrokeWidth(4.0F);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		mTabWidth = w / mTabCount;
	}

	public void setTitles(String[] titles)
	{
		mTitles = titles;
		mTabCount = titles.length;
		generateTitleView();

	}

	public void setIndicatorColor(int indicatorColor)
	{
		this.mIndicatorColor = indicatorColor;
	}

	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		super.dispatchDraw(canvas);
		canvas.save();
		canvas.translate(mTranslationX, getHeight() - 2);
		canvas.drawLine(0, 0, mTabWidth, 0, mPaint);
		canvas.restore();
	}

	public void scroll(int position, float offset)
	{
		/**
		 * <pre>
		 *  0-1:position=0 ;1-0:postion=0;
		 * </pre>
		 */
		mTranslationX = getWidth() / mTabCount * (position + offset);
		invalidate();
	}

	
	int mTEXT_NORMAL=COLOR_TEXT_NORMAL;
	int mTEXT_SELECT=COLOR_TEXT_NORMAL;
	public void setColor(int norColor,int selColor){
		mTEXT_NORMAL=norColor;
		mTEXT_SELECT=selColor;
		
	}
	
	public void setSelectTextAt(int position){
 		if(selview!=null){
			selview.setTextColor( mTEXT_NORMAL);
			selview.setSelected(false);
		}
		View tmpview=this.getChildAt(position);
		if(tmpview!=null){
			TextView tmptext= 	((TextView)tmpview);
			if(tmptext!=null){
				tmptext.setTextColor( mTEXT_SELECT);
				tmptext.setSelected(true);
				selview=tmptext;
			}
 		}
	}

	public TextView getTextViewAt(int position){
		
		if(position>this.getChildCount()||position<0) return null;
		View tmpview=this.getChildAt(position);
		return (TextView)tmpview;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev)
	{
		return super.dispatchTouchEvent(ev);
	}

	private void generateTitleView()
	{
		if(!isShowtitle) return;
		if (getChildCount() > 0)
			this.removeAllViews();
		int count = mTitles.length;

		setWeightSum(count);
		
		for (int i = 0; i < count; i++)
		{
			DrawableCenterTextView tv = new DrawableCenterTextView(getContext());
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,
					LayoutParams.MATCH_PARENT);
			lp.weight = 1;
			tv.setGravity(Gravity.CENTER_VERTICAL);
			tv.setTextColor(mtextColor);
		 
			tv.setText(mTitles[i]);
			//tv.setTextSize(mtextsize);
			 tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mtextsize);
			tv.setLayoutParams(lp);
			tv.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{

				}
			});
			addView(tv);
			//if(i==0) selview=tv;
		}
	}

}
