package com.zrb.mobile.ui;

import com.zrb.mobile.R;
import com.zrb.mobile.utility.CommonUtil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class ClearEditTextEx extends RelativeLayout implements View.OnFocusChangeListener, TextWatcher {

	public interface onTextCorrectListener {

		void onTextCorrect(boolean isok);
	}

	onTextCorrectListener monTextCorrectListener;

	public void setOnTextCorrectListener(onTextCorrectListener ontextcorrectListener) {
		monTextCorrectListener = ontextcorrectListener;

	}

	Context paramContext;
	ImageView hiteImg;
	EditText inputcontent;
	ImageView img_clean, img_right;
	int curInputType = -1;// 1 phone,2 pwd
	Drawable mIndicator;
	String hitetext = "";

	public ClearEditTextEx(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		paramContext = context;
		Init(attrs);
	}

	private void Init(AttributeSet attrs) {

		TypedArray a = paramContext.obtainStyledAttributes(attrs, R.styleable.clearEditText, 0, 0);
		mIndicator = a.getDrawable(R.styleable.clearEditText_clr_drawableleft);
		hitetext = a.getString(R.styleable.clearEditText_clr_texthite);

		a.recycle();

		View localView = LayoutInflater.from(paramContext).inflate(R.layout.clear_edit_textex, this, true);
		
		hiteImg = (ImageView) localView.findViewById(R.id.img_icon);
		hiteImg.setImageDrawable(mIndicator);
 
		inputcontent = (EditText) localView.findViewById(R.id.edit_phone);
		inputcontent.setHint(hitetext);
		
		img_clean = (ImageView) localView.findViewById(R.id.phone_btn_clean);
		img_clean.setVisibility(View.GONE);

		img_right = (ImageView) localView.findViewById(R.id.phone_btn_right);
		img_right.setVisibility(View.GONE);

		inputcontent.setOnFocusChangeListener(this);
		inputcontent.addTextChangedListener(this);

		img_clean.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inputcontent.setText("");
			}
		});
	}

	public void setText(CharSequence titledes) {
		inputcontent.setText(titledes);
	}

	public void setHiteResId(int resid) {
		inputcontent.setHint(resid);
	}

	public String getContentValue() {
		if (TextUtils.isEmpty(inputcontent.getText()))
			return "";
		return inputcontent.getText().toString();
	}

	public boolean isEmpty() {

		if (TextUtils.isEmpty(inputcontent.getText()))
			return true;
		return false;
	}

	public void setInputType(int inputType) {
		curInputType = inputType;
		switch(curInputType){
		  case 1:
			inputcontent.setInputType(InputType.TYPE_CLASS_PHONE);
			break;
		  case 2:
			 inputcontent.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);  
			// TYPE_CLASS_TEXT |TYPE_TEXT_VARIATION_PASSWORD.
		}
		
	}

	/*
	 * 
	 * editNameText .setOnFocusChangeListener(new
	 * android.view.View.OnFocusChangeListener() {
	 * 
	 * @Override public void onFocusChange(View v, boolean hasFocus) { if
	 * (!TextUtils.isEmpty(editNameText.getText())) { if
	 * (editNameText.getText().length() != 0 && !hasFocus &&
	 * CommonUtil.checkPassword(editNameText .getText().toString())) {
	 * phone_btn_clean.setVisibility(View.GONE);
	 * phone_btn_right.setVisibility(View.VISIBLE); } else {
	 * phone_btn_clean.setVisibility(View.VISIBLE);
	 * phone_btn_right.setVisibility(View.GONE); } } } });
	 */

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (!TextUtils.isEmpty(inputcontent.getText())) {

			boolean inputflag = true;
			switch (curInputType) {
			case 1:
				inputflag = CommonUtil.isMobileNO(inputcontent.getText().toString());
				break;
			case 2:
				inputflag = CommonUtil.checkPassword(inputcontent.getText().toString());
				break;
			}
			if (!hasFocus && inputflag) {
				img_clean.setVisibility(View.GONE);
				img_right.setVisibility(View.VISIBLE);
			} else {
				img_clean.setVisibility(View.VISIBLE);
				img_right.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		if (inputcontent.getText().length() == 0) {
			img_clean.setVisibility(View.GONE);
			img_right.setVisibility(View.GONE);
			if (null != monTextCorrectListener)
				monTextCorrectListener.onTextCorrect(false);
		}

		else {

			img_clean.setVisibility(View.VISIBLE);
			img_right.setVisibility(View.GONE);
			if (null != monTextCorrectListener)
				monTextCorrectListener.onTextCorrect(true);
		}

	}

	/*
	 * editNameText.addTextChangedListener(new TextWatcher() {
	 * 
	 * @Override public void onTextChanged(CharSequence s, int start, int
	 * before, int count) { if (s.length() != 0 && null !=
	 * editPassWord.getText().toString() &&
	 * !"".equals(editPassWord.getText().toString())) {
	 * loginButton.setEnabled(true); //
	 * loginButton.setBackgroundDrawable(getResources
	 * ().getDrawable(R.drawable.login_button_blue)); } else {
	 * loginButton.setEnabled(false); //
	 * loginButton.setBackgroundDrawable(getResources
	 * ().getDrawable(R.drawable.login_button_blue2)); } }
	 * 
	 * @Override public void beforeTextChanged(CharSequence s, int start, int
	 * count, int after) {
	 * 
	 * }
	 * 
	 * @Override public void afterTextChanged(Editable s) {
	 * 
	 * } });
	 */
}
