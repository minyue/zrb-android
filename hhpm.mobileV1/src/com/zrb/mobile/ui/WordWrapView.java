package com.zrb.mobile.ui;

import com.zrb.mobile.utility.CommonUtil;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class WordWrapView extends ViewGroup {
	  private static  int PADDING_HOR = -1;//6;//水平方向padding
	  private static  int PADDING_VERTICAL = 1;//垂直方向padding
	  private static  int SIDE_MARGIN = 10;//左右间距
	  private static  int TEXT_MARGIN = 7;
	  
 
	  //private drawable bgdrawable;
	  /**
	   * @param context
	   */
	  public WordWrapView(Context context) {
	    super(context);
	    init();
	  }
	  
	  /**
	   * @param context
	   * @param attrs
	   * @param defStyle
	   */
	  public WordWrapView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    init();
  	  }

    

	  /**
	   * @param context
	   * @param attrs
	   */
	  public WordWrapView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    init();
	  }

      private void init(){
     	  if(PADDING_HOR==-1){
   	         PADDING_HOR=CommonUtil.dip2px(this.getContext(), 6);
    	     PADDING_VERTICAL=CommonUtil.dip2px(this.getContext(), PADDING_VERTICAL);
     	     SIDE_MARGIN=CommonUtil.dip2px(this.getContext(), SIDE_MARGIN);
     	     TEXT_MARGIN=CommonUtil.dip2px(this.getContext(), TEXT_MARGIN); 
    	  }
      }

	  @Override
	  protected void onLayout(boolean changed, int l, int t, int r, int b) {
	    int childCount = getChildCount();
	    int autualWidth = r - l;
	    int x = SIDE_MARGIN;// 横坐标开始
	    int y = 0;//纵坐标开始
	    int rows = 1;
	    for(int i=0;i<childCount;i++){
	      View view = getChildAt(i);
	      //view.setBackgroundColor(Color.GREEN);
	     // view.setBackgroundResource(com.zrb.mobile.R.drawable.hotword_tag_bkg);
	      view.setBackgroundResource(com.zrb.mobile.R.drawable.news_searchtag_bg);
	      
	      int width = view.getMeasuredWidth();
	      int height = view.getMeasuredHeight();
	      x += width+TEXT_MARGIN;
	      if(x>autualWidth){
	        x = width+SIDE_MARGIN;
	        rows++;
	      }
	      y = rows*(height+TEXT_MARGIN);
	      if(i==0){
	        view.layout(x-width-TEXT_MARGIN, y-height, x-TEXT_MARGIN, y);
	      }else{
	        view.layout(x-width, y-height, x, y);
	      }
	    }
	  };

	  @Override
	  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    int x = 0;//横坐标
	    int y = 0;//纵坐标
	    int rows = 1;//总行数
	    int specWidth = MeasureSpec.getSize(widthMeasureSpec);
	    int actualWidth = specWidth - SIDE_MARGIN * 2;//实际宽度
	    int childCount = getChildCount();
	    for(int index = 0;index<childCount;index++){
	      View child = getChildAt(index);

			child.setPadding(CommonUtil.dip2px(this.getContext(), PADDING_HOR),
					CommonUtil.dip2px(this.getContext(), PADDING_VERTICAL),
					CommonUtil.dip2px(this.getContext(), PADDING_HOR),
					CommonUtil.dip2px(this.getContext(), PADDING_VERTICAL));
  
	      child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
	      int width = child.getMeasuredWidth();
	      int height = child.getMeasuredHeight();
	      x += width+TEXT_MARGIN;
	      if(x>actualWidth){//换行
	        x = width;
	        rows++;
	      }
	      y = rows*(height+TEXT_MARGIN);
	    }
	    setMeasuredDimension(actualWidth, y);
	  }

	}
