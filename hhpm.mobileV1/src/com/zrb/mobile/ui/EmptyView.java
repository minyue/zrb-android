package com.zrb.mobile.ui;

import com.zrb.mobile.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

 
public class EmptyView extends LinearLayout implements View.OnTouchListener 
{
	private TextView HitTxt;
	private ImageView hitImg;
    private Animation anim;
    
    public EmptyView(Context paramContext, AttributeSet paramAttributeSet) {
         super(paramContext, paramAttributeSet);
         this.setOnTouchListener(this);
         this.setGravity(Gravity.CENTER);
         this.setOrientation(LinearLayout.VERTICAL);
 
        View localView = LayoutInflater.from(paramContext).inflate(R.layout.empty_layout, this, true);
        this.HitTxt = ((TextView)localView.findViewById(R.id.txt_hite));
        this.hitImg = ((ImageView)localView.findViewById(R.id.img_hite));
        
        this.setBackgroundColor(paramContext.getResources().getColor(R.color.white));
        anim = AnimationUtils.loadAnimation(paramContext, R.anim.umeng_socialize_fade_out);
    }
  
    public EmptyView show()  {
 
        this.setVisibility(View.VISIBLE);; 
        return this;
    }
 
    public EmptyView hideText(String paramString) {
        
    	HitTxt.setText( paramString);
        return this;
    }
   
    public boolean isShow(){
     	return this.getVisibility()==View.VISIBLE;
    }

    public EmptyView fadehide() {
      
    	this.clearAnimation();
    	this.startAnimation(anim);
        this.setVisibility(View.GONE);// = ViewStates.Gone;
        return this;
    }
    
    public EmptyView hide() {
         this.setVisibility(View.GONE);// = ViewStates.Gone;
        return this;
    }
 
    public void checkhide(){
     	if(this.getVisibility()==View.VISIBLE) 
    		this.setVisibility(View.GONE);
    	
    }
    
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

}