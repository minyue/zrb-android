package com.zrb.mobile.ui;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

 
import com.zrb.mobile.R;
import com.zrb.mobile.User_my_news;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
 

public class PullToRefreshListViewEx  extends PullToRefreshListView{
 	
	
    int pagesize=10;
	View mloadingview; 
	ListAdapter mBaseAdapter;	
	boolean isLoading=false;
	boolean canloadmore=true;
	
	onLoadMoreListener monLoadMoreListener;
	
	public PullToRefreshListViewEx(Context context) {
		super(context);
	}

	public PullToRefreshListViewEx(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PullToRefreshListViewEx(Context context, Mode mode) {
		super(context, mode);
	}

	public PullToRefreshListViewEx(Context context, Mode mode, AnimationStyle style) {
		super(context, mode, style);
	}
	
	public void initAdapter(ListAdapter madapter){
		mBaseAdapter=madapter;
		this.setAdapter(madapter);
	}
	
	public void setPagesize(int curPagesize){
		pagesize=curPagesize;
	}
	
   // public void set  onRefreshComplete
	public void setRefreshComplete(){
		canloadmore=true;
		super.onRefreshComplete();
		
	}  
	
	public void setCanPullToRefresh(boolean isenable){
		
		super.setPullToRefreshEnabled(isenable);
	}
	
	public void hideloading(){
		isLoading=false;
		mloadingview.setVisibility(View.GONE);
	}
	
	public void checkloadMore(int size){
 		if(size<pagesize) canloadmore=false;;
	}
	
	public void setOnLoadMoreListener(onLoadMoreListener onLoadMoreListener){
		
		LayoutInflater inflater = LayoutInflater.from(this.getContext());
		mloadingview = inflater.inflate(R.layout.ss_new_comment_footer, null);
		
		ListView actualListView= this.getRefreshableView();
		actualListView.addFooterView(mloadingview);
		mloadingview.setVisibility(View.GONE);
 	  	
		monLoadMoreListener=onLoadMoreListener;
		initOnLastItemVisibleListener();
	}
	
	private void initOnLastItemVisibleListener(){
		
		super.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {
 					@Override
					public void onLastItemVisible() {
  					 
 						if(!canloadmore) return;
 						if(mBaseAdapter.getCount()<pagesize) return;
 						if(isLoading) return;
 	 
 						isLoading=true;
 						mloadingview.setVisibility(View.VISIBLE);
 						if(monLoadMoreListener!=null) monLoadMoreListener.OnLoadMoreEvent();
  					}
		});
	}
}
