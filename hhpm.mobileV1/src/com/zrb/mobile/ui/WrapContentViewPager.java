package com.zrb.mobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class WrapContentViewPager extends SSViewPager
{
  public WrapContentViewPager(Context paramContext)
  {
    super(paramContext);
  }

  public WrapContentViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  @Override
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (getLayoutParams().height == -2)//WRAP_CONTENT 
    {
      int i = 0;
      int j = 0;
      while (i < getChildCount())
      {
        View localView = getChildAt(i);
        localView.measure(paramInt1, View.MeasureSpec.makeMeasureSpec(0, 0));
        int k = localView.getMeasuredHeight();
        if (k > j)
          j = k;
        ++i;
      }
      //paramInt2 = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
      paramInt2 = View.MeasureSpec.makeMeasureSpec(j, View.MeasureSpec.EXACTLY);
       
    }
    super.onMeasure(paramInt1, paramInt2);
  }
}