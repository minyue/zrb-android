package com.zrb.mobile.ui;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

public class NumEditText extends EditText  {
	
	int num = 140;//限制的最大字数　
	Context paramContext;
	OnNumslistener mNumslistener;
	
	public void setOnNumslistener(OnNumslistener numslistener){
 		mNumslistener=numslistener;
	}
	
	public void setMaxNums(int numbers){
 		num=numbers;
	}
	
	public NumEditText(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		paramContext=context;
		Init();
	}

	public NumEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		paramContext=context;
		Init();
	}
	
	void Init(){
		
		this.addTextChangedListener(new TextWatcher() {
			private CharSequence temp;
			private int selectionStart;
			private int selectionEnd;

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				temp = s;
			}

			@Override
			public void afterTextChanged(Editable s) {
				int number = num - s.length();
				// hasnumTV.setText("" + number);
				if(mNumslistener!=null) mNumslistener.showtextNum(number);
				
				selectionStart = NumEditText.this.getSelectionStart();
				selectionEnd = NumEditText.this.getSelectionEnd();
				if (temp.length() > num) {
					s.delete(selectionStart - 1, selectionEnd);
					int tempSelection = selectionEnd;
					NumEditText.this.setText(s);
					NumEditText.this.setSelection(tempSelection);// 设置光标在最后
				}
			}
		});
		
	}

	
	public interface OnNumslistener{
 		void showtextNum(int number);
	}
}
