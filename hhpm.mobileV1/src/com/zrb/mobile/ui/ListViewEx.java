package com.zrb.mobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AbsListView.OnScrollListener;
 
import com.zrb.mobile.R;
import com.zrb.mobile.utility.CommonUtil;

public class ListViewEx extends ListView implements OnScrollListener {

	int pagesize=10;
	ListAdapter mBaseAdapter;	
	View mloadingview; 
	boolean isLoading=false;
	onLoadMoreListener monLoadMoreListener;
	private boolean mLastItemVisible;
	boolean canloadmore=true;
	
	public ListViewEx(Context context) {
		super(context);
		initFoot();
	}

	public ListViewEx(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initFoot();

	}

	public ListViewEx(Context context, AttributeSet attrs) {
		super(context, attrs);
		initFoot();

	}
	
	private void initFoot(){
		  super.setOnScrollListener(this);
		//mloadingview= super.getFootView();
		
	}
 
	public void setOnLoadMoreListener(onLoadMoreListener onLoadMoreListener){
		
		LayoutInflater inflater = LayoutInflater.from(this.getContext());
		mloadingview = inflater.inflate(R.layout.ss_new_comment_footer, null);
		
 
		this.addFooterView(mloadingview);
		mloadingview.setVisibility(View.GONE);
 	  	
		monLoadMoreListener=onLoadMoreListener;
	 
	}
	
	public void setPagesize(int curPagesize){
		pagesize=curPagesize;
	}

	@Override
	public void setAdapter(ListAdapter madapter){
		mBaseAdapter=madapter;
		super.setAdapter(madapter);
	}
	
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
 
		  if (scrollState == OnScrollListener.SCROLL_STATE_IDLE && null != monLoadMoreListener && mLastItemVisible) {
				//mOnLastItemVisibleListener.onLastItemVisible();
		 
				 onLastItemVisible();
		  }
	    
	}

	
	 @Override
	 public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
	        // send to user's listener
	    	if (null !=monLoadMoreListener) {
 				mLastItemVisible = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount - 1);
			}
 	}
	
	 private void onLastItemVisible(){
			
		    if(!canloadmore) return;
	    	if(mBaseAdapter.getCount()<pagesize) return;
			if(isLoading) return;

			isLoading=true;
		
			mloadingview.setVisibility(View.VISIBLE);
 		//	mloadingview.setState(PullToRefreshListFooter.STATE_LOADING);
 			super.setSelection(mBaseAdapter.getCount()-1);
			if(null !=monLoadMoreListener) monLoadMoreListener.OnLoadMoreEvent();
	 } 
	    
	 

		public void hideloading(){
			isLoading=false;
			mloadingview.setVisibility(View.GONE);
		 	//mloadingview.setState(PullToRefreshListFooter.STATE_NORMAL);
		}
		
		public void checkloadMore(int size){
	 		if(size<pagesize) canloadmore=false;;
		}
		
		// public void set  onRefreshComplete
		public void setRefreshComplete(){
			canloadmore=true;
			//super.stopRefresh();
		}  
		
}
