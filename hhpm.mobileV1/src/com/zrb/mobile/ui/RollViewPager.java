package com.zrb.mobile.ui;



import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class RollViewPager extends ViewPager {
 
 
 
 
	private int downX;
	private int downY;
 
	
	public RollViewPager(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public RollViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			//按下的时候，夫控件不可以去拦截ACTION_DOWN的事件
			getParent().requestDisallowInterceptTouchEvent(true);
			downX = (int) ev.getX();
			downY = (int) ev.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			int moveX = (int) ev.getX();
			int moveY = (int) ev.getY();
			int distanceX=Math.abs(moveX-downX);
			//左右移动移动的比上下移动多，翻页
			if(distanceX>Math.abs(moveY - downY) ){
				getParent().requestDisallowInterceptTouchEvent(true);
			}else{
				getParent().requestDisallowInterceptTouchEvent(false);
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			getParent().requestDisallowInterceptTouchEvent(false);
			break;
		}
		return super.dispatchTouchEvent(ev);
	}
	 
	 
 
}
