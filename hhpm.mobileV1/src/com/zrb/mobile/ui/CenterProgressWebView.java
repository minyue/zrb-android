package com.zrb.mobile.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.zrb.mobile.R;
import com.zrb.applib.utils.AppSetting;

public class CenterProgressWebView extends WebView {

	public interface IOnJsPrompt {
		void PromptInfo(String message, String defaultValue);
		
		void ReceivedTitle(String title);
	}
	
    public interface ScrollInterface {
		  
	        public void onSChanged(int l, int t, int oldl, int oldt);
 	}
	

	private WebChromeClientEx _webChromeClient;
	ScrollInterface mScrollInterface ;
	/*
	 * private ViewGroup.LayoutParams mProgressBarLayoutParams = new
	 * ViewGroup.LayoutParams( LinearLayout.LayoutParams.WrapContent,
	 * LinearLayout.LayoutParams.WrapContent);
	 */

	private ImageView _progressbar;
	public TextView _mtxtView;
	private FrameLayout _tmpFrame;

	public CenterProgressWebView(Context context, AttributeSet attrs)

	{
		super(context, attrs);
		// progressBarStyleSmall
		// mLoadLayout.SetBackgroundColor(Android.Graphics.Color.Transparent);
		// mLoadLayout.SetGravity(GravityFlags.Center);

		_tmpFrame = new FrameLayout(context);

		// _tmpFrame.SetBackgroundColor(Android.Graphics.Color.Transparent);
		// _tmpFrame.SetBackgroundColor(context.Resources.GetColor(InfoEarth.Mobile.Geodisaster.Resource.Color.likeqq_bg));
		_mtxtView = new TextView(context);
		_mtxtView.setText("0%", BufferType.NORMAL);

		// LinearLayout.LayoutParams lp = new
		// LinearLayout.LayoutParams(LayoutParams.WrapContent,LayoutParams.WrapContent);
		// //此处相当于布局文件中的Android:layout_gravity属性
		// lp.Gravity = GravityFlags.Center;

		// animated-rotate
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		// 此处相当于布局文件中的Android:layout_gravity属性
		// lp.Gravity = GravityFlags.Center;
		lp.gravity = Gravity.CENTER;

		_progressbar = new ImageView(context);
		_progressbar.setImageDrawable(this.getContext().getResources()
				.getDrawable(R.drawable.webview_loading));// context.Resources.GetDrawable(InfoEarth.Mobile.Geodisaster.Resource.Drawable.flash_data_loading));

		// <item
		// name="android:indeterminateDrawable">@android:drawable/progress_medium</item>
		// _progressbar.IndeterminateDrawable =
		// context.Resources.GetDrawable(Android.Resource.Drawable.ProgressIndeterminateHorizontal);
		// _progressbar.ProgressDrawable=context.Resources.GetDrawable(InfoEarth.Mobile.Geodisaster.Resource.Drawable.flash_data_loading);

		_progressbar.setVisibility(View.GONE);// visibility..Visibility =
 		_tmpFrame.addView(_progressbar, lp);

		_tmpFrame.addView(_mtxtView, lp);

		this.addView(_tmpFrame, new ViewGroup.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));
		// setWebViewClient(new WebViewClient(){});

	
		_webChromeClient = new WebChromeClientEx();
		_webChromeClient.Progressbar = _progressbar;
		_webChromeClient.tmpTxtView = _mtxtView;
		_webChromeClient.tmpFrame = _tmpFrame;
		
 		setWebChromeClient(_webChromeClient);
		//setWebViewClient(new WebViewClientEx());

		Init();
	}
 	
	  @Override
	  protected void onScrollChanged(int l, int t, int oldl, int oldt) {
	 
	        super.onScrollChanged(l, t, oldl, oldt);
 	        if(mScrollInterface!=null) mScrollInterface.onSChanged(l, t, oldl, oldt);
 	  }
	 
	  public void setOnCustomScroolChangeListener(ScrollInterface scrollInterface) {
 	        this.mScrollInterface = scrollInterface;
	 
	  }
	
	
	public void setOnJsPromptListener(IOnJsPrompt promptListener) {
 		_webChromeClient.PromptListener = promptListener;
 	}

    public void setOnPageListener(IOnPageListener OnPageListener) {
    	setWebViewClient(new WebViewClientEx(OnPageListener));
    }
	
	private void Init() {
		this.setInitialScale(0);
		this.setVerticalScrollBarEnabled(true);

		// Enable JavaScript
		WebSettings settings = this.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);

		// Set the nav dump for HTC 2.x devices (disabling for ICS, deprecated
		// entirely for Jellybean 4.2)
		/*
		 * try { Method gingerbread_getMethod =
		 * WebSettings.class.getMethod("setNavDump", new Class[] { boolean.class
		 * });
		 * 
		 * String manufacturer = android.os.Build.MANUFACTURER; Log.d(TAG,
		 * "CordovaWebView is running on device made by: " + manufacturer);
		 * if(android.os.Build.VERSION.SDK_INT <
		 * android.os.Build.VERSION_CODES.HONEYCOMB &&
		 * android.os.Build.MANUFACTURER.contains("HTC")) {
		 * gingerbread_getMethod.invoke(settings, true); } } catch
		 * (NoSuchMethodException e) { Log.d(TAG,
		 * "We are on a modern version of Android, we will deprecate HTC 2.3 devices in 2.8"
		 * ); } catch (IllegalArgumentException e) { Log.d(TAG,
		 * "Doing the NavDump failed with bad arguments"); } catch
		 * (IllegalAccessException e) { Log.d(TAG,
		 * "This should never happen: IllegalAccessException means this isn't Android anymore"
		 * ); } catch (InvocationTargetException e) { Log.d(TAG,
		 * "This should never happen: InvocationTargetException means this isn't Android anymore."
		 * ); }
		 */

		// We don't save any form data in the application
		settings.setSaveFormData(false);
		settings.setSavePassword(false);

		// Jellybean rightfully tried to lock this down. Too bad they didn't
		// give us a whitelist
		// while we do this
		// if (android.os.Build.VERSION.SDK_INT >
		// android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
		// Level16Apis.enableUniversalAccess(settings);
		// Enable database
		// We keep this disabled because we use or shim to get around
		// DOM_EXCEPTION_ERROR_16
		String databasePath = this.getContext().getApplicationContext()
				.getDir("database", Context.MODE_PRIVATE).getPath();
		settings.setDatabaseEnabled(true);
		settings.setDatabasePath(databasePath);

		settings.setGeolocationDatabasePath(databasePath);

		// Enable DOM storage
		settings.setDomStorageEnabled(true);

		// Enable built-in geolocation
		settings.setGeolocationEnabled(true);

		// Enable AppCache
		// Fix for CB-2282
		settings.setAppCacheMaxSize(5 * 1048576);
		String pathToCache = this.getContext().getApplicationContext()
				.getDir("database", Context.MODE_PRIVATE).getPath();
		settings.setAppCachePath(pathToCache);
		settings.setAppCacheEnabled(true);

		
		//settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);//.cacheMode = CacheModes.CacheElseNetwork;
		// Fix for CB-1405
		// Google issue 4641
		// this.updateUserAgentString();

		// end CB-1405

	}

/*	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		// var lp = (LayoutParams) _progressbar.LayoutParameters;
		// //.getLayoutParams();
		// lp.X = l;
		// lp.Y = t;
		// _progressbar.LayoutParameters = (lp);
		super.onScrollChanged1(l, t, oldl, oldt);
	}*/

	public void Destroyself() {
		this.loadUrl("about:blank"); // Load blank page so that JavaScript
										// onunload is called
		this.destroy();
	}

	public class WebChromeClientEx extends WebChromeClient {
		public ImageView Progressbar;
		public TextView tmpTxtView;
		public FrameLayout tmpFrame;
		public IOnJsPrompt PromptListener;

		@Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            if (PromptListener != null)
				PromptListener.ReceivedTitle(title);
        }
		
		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			if (newProgress == 100) {
				Progressbar.setVisibility(view.GONE);// .Visibility =
														// ViewStates.Gone;
														// //.setVisibility(GONE);
				Progressbar.clearAnimation();
				tmpTxtView.setVisibility(view.GONE);// .Visibility=ViewStates.Gone;
				tmpFrame.setVisibility(view.GONE);// .Visibility =
													// ViewStates.Gone;

			} else {
				if (Progressbar.getVisibility() == view.GONE)// ViewStates.Gone
				{
					Progressbar.setVisibility(view.VISIBLE);// =
															// ViewStates.Visible;
															// //(VISIBLE);
					tmpTxtView.setVisibility(view.VISIBLE);// =
															// ViewStates.Visible;
					tmpFrame.setVisibility(view.VISIBLE);// =
															// ViewStates.Visible;
					Animation anim = AnimationUtils.loadAnimation(
							view.getContext(), R.anim.round_loading);
					Progressbar.startAnimation(anim);
				}

				tmpTxtView.setText(newProgress + "%");// .Text =
														// string.Format("{0}%",
														// newProgress);
				// Progressbar.Progress = newProgress;
			}
			// if (ProgressHander != null)
			// ProgressHander(null, new ProgressEventArgs() { progressNum =
			// newProgress });
			super.onProgressChanged(view, newProgress);
		}

		@Override
		public boolean onJsPrompt(WebView view, String url, String message,
				String defaultValue, JsPromptResult result) {
			boolean reqOk = false;
			if (url.startsWith("file://")) {
				reqOk = true;
			}
			if(url.startsWith(AppSetting.BASE_URL)) reqOk = true;
			// prompt(this.stringify(args), "gap:"+this.stringify([service,
			// action, callbackId, true]));
			if (reqOk && defaultValue != null && defaultValue.length() > 3
					&& defaultValue.substring(0, 4).equals("gap:")) {
				try {
					if (PromptListener != null)
						PromptListener.PromptInfo(message, defaultValue);
					result.confirm("");
				} catch (Exception ex) {
					// e.printStackTrace();
					// CommonUtil.InfoLog("OnJsPrompt", ex.Message);
					return false;
				}
			}
			// return base.OnJsPrompt(view,url,message,defaultValue,result);
			return true;
		}

	}

	public class WebViewClientEx extends WebViewClient {
		
		IOnPageListener mOnPageListener;
        public WebViewClientEx(IOnPageListener OnPageListener) {
            mOnPageListener = OnPageListener;
        }
  		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
 		}
		
		@Override
		 public  void onPageFinished(WebView view, String url)
          {
              //((CustomWebView) view).notifyPageFinished();
             if(mOnPageListener!=null){   
            	mOnPageListener.notifyPageFinished();
                mOnPageListener.onPageFinished(url);
             }
              super.onPageFinished(view, url);
          }

		 @Override
          public void onPageStarted(WebView view, String url, Bitmap favicon)
          {
              // Some magic here: when performing WebView.loadDataWithBaseURL, the url is "file:///android_asset/startpage,
              // whereas when the doing a "previous" or "next", the url is "about:start", and we need to perform the
              // loadDataWithBaseURL here, otherwise it won't load.
              //if (url.equals(Constants.URL_ABOUT_START)) {
              //    view.loadDataWithBaseURL("file:///android_asset/startpage/",
              //            ApplicationUtils.getStartPage(view.getContext()), "text/html", "UTF-8", "about:start");
              //}
              //((CustomWebView) view).notifyPageStarted();
			  if(mOnPageListener!=null){   
                mOnPageListener.notifyPageStarted();
                mOnPageListener.onPageStarted(url);
			  }
              super.onPageStarted(view, url, favicon);
          }
	 }
 
	public interface IOnPageListener 
	  {
          void onPageStarted(String url);
          void onPageFinished(String url);

          void notifyPageStarted();
          void notifyPageFinished();
      }

	
}
