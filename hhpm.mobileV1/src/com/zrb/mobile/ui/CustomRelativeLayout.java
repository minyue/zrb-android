package com.zrb.mobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class CustomRelativeLayout extends RelativeLayout {

	public CustomRelativeLayout(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
	}

	AutoScrollViewPager mScollViewpager;

	public void setAutoViewPager(AutoScrollViewPager ScollViewpager) {
		mScollViewpager = ScollViewpager;
	}

	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);

		//CommonUtil.showToast("onVisibilityChanged", this.getContext());
		if (visibility == VISIBLE) {
 			if(mScollViewpager!=null) mScollViewpager.startAutoScroll();
	 
		} else if (visibility == INVISIBLE) {
			if(mScollViewpager!=null) mScollViewpager.stopAutoScroll();
 		}
 	}

	 @Override
	    protected void onDetachedFromWindow() {
	        super.onDetachedFromWindow();
	        mScollViewpager=null;
	        
	    }
}
