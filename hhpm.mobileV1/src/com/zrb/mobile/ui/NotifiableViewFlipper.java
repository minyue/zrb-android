package com.zrb.mobile.ui;

import com.zrb.mobile.R;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

public class NotifiableViewFlipper extends ViewFlipper implements  OnTouchListener, OnGestureListener  {

	private static final int FLING_MIN_DISTANCE = 50;
	private static final int FLING_MIN_VELOCITY = 0;
	private int currentPage = 0;
	
	private GestureDetector mGestureDetector;
	public NotifiableViewFlipper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		Init();
	}

	
	 public NotifiableViewFlipper(Context context, AttributeSet attrs) { 
	        super(context, attrs); 
	        Init();
	    } 

	 private void Init()
	 {
 	        this.setOnTouchListener(this);
	        this.setLongClickable(true);
	        mGestureDetector = new GestureDetector(this);
	 }

	 
	 
	 private void showNextView(){

			this.setInAnimation(AnimationUtils.loadAnimation(this.getContext(), R.anim.push_left_in));
			this.setOutAnimation(AnimationUtils.loadAnimation(this.getContext(), R.anim.push_left_out));		
			this.showNext();
			currentPage ++;
			if (currentPage == this.getChildCount()) {
				//displayRatio_normal(currentPage - 1);
				currentPage = 0;
				//displayRatio_selelct(currentPage);
			} else {
				//displayRatio_selelct(currentPage);
				//displayRatio_normal(currentPage - 1);
			}
			Log.e("currentPage", currentPage + "");		
			
		}
		private void showPreviousView(){
			//displayRatio_selelct(currentPage);
			this.setInAnimation(AnimationUtils.loadAnimation(this.getContext(), R.anim.push_right_in));
			this.setOutAnimation(AnimationUtils.loadAnimation(this.getContext(), R.anim.push_right_out));
			this.showPrevious();
			currentPage --;
			if (currentPage == -1) {
				//displayRatio_normal(currentPage + 1);
				currentPage = this.getChildCount() - 1;
				//displayRatio_selelct(currentPage);
			} else {
				//displayRatio_selelct(currentPage);
				//displayRatio_normal(currentPage + 1);
			}
			Log.e("currentPage", currentPage + "");		
		}
	 

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		return mGestureDetector.onTouchEvent(arg1);
	}


	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		Log.e("view", "onFling");
		if (e1.getX() - e2.getX()> FLING_MIN_DISTANCE  
                && Math.abs(velocityX) > FLING_MIN_VELOCITY ) {
			Log.e("fling", "left");
			showNextView();
			//showNext = true;
//			return true;
		} else if (e2.getX() - e1.getX() > FLING_MIN_DISTANCE  
                && Math.abs(velocityX) > FLING_MIN_VELOCITY){
			Log.e("fling", "right");
			showPreviousView();
			//showNext = false;
//			return true;
		}
		return false;
	}


	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}
}
