package com.zrb.mobile.ui;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MenuCheckAdapter;
import com.zrb.mobile.adapter.RadioItem;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;

 

public class QMToggleView extends FrameLayout implements AnimationListener,View.OnTouchListener 
{
    private Context _mContext;
    private View _rootView;

    private View _maskView;
    private ListView _morechooselist;
    private MenuCheckAdapter _zqxqmenAdpter;
    private FrameLayout _menuMore;
    public int ReadStatus = 0;//0 未读  ,1已读，2全部
    //public int PageIndex = 0;
    private IOnToggleListener _mOnToggleListener;
    FrameLayout  list_content;
    private boolean _isShow=false;

    public QMToggleView(Context paramContext) 
    {
        super(paramContext);
       // this.c = ((Activity)paramContext);
        _mContext = paramContext;
        Initialize();
    }

    public QMToggleView(Context paramContext, AttributeSet paramAttributeSet)
    {
       super(paramContext, paramAttributeSet);
       // this.c = ((Activity)paramContext);
        //this.a = getResources().getInteger(17694721);
       // this.b = getResources().getInteger(17694721);
        //TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, c.QMToggleView);
       // this.a = localTypedArray.getInteger(0, this.a);
       // this.b = localTypedArray.getInteger(1, this.b);
        //localTypedArray.recycle();
        _mContext = paramContext;
        Initialize();
    }


    private void Initialize()
    {
        _rootView = LayoutInflater.from(_mContext).inflate(R.layout.qmtogglesubview, this, true);
          
    }

    public void setOnToggleListener(IOnToggleListener mOnToggleListener)
    {
        _mOnToggleListener = mOnToggleListener;
    }

    
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return true;
	}
 
    public void InitMenuView(String titledes)
    {
        _maskView = _rootView.findViewById(R.id.qmtoggleview_mask_view);

        _maskView.setOnTouchListener(this);
       // _menuMore = this;// FindViewById<FrameLayout>(Resource.Id.note_qmtoggleview);
        _morechooselist =(ListView) _rootView.findViewById(R.id.qmtoggleview_list_lv);
        list_content=(FrameLayout) _rootView.findViewById(R.id.qmtoggleview_list_content_ll);
        _menuMore = list_content;
        
        _zqxqmenAdpter = new MenuCheckAdapter(this._mContext);
        _zqxqmenAdpter.CurrentDatas.add( new RadioItem("待发布"+titledes, "0") );
        _zqxqmenAdpter.CurrentDatas.add( new RadioItem("已发布"+titledes, "1") );
        
        _morechooselist.setAdapter(_zqxqmenAdpter);//.Adapter = _zqxqmenAdpter;
        _morechooselist.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int Position,
					long arg3) {
				// TODO Auto-generated method stub
				    _zqxqmenAdpter.UpdateIndex(Position);
			        ShowmoreMenu();
			        ReadStatus = Position;
			       // PageIndex = 0;
			       //  Reset();
			        if (_mOnToggleListener != null) _mOnToggleListener.OnToggleFinished(ReadStatus);
			}
        }) ;
    }

 


    private int _tduration = 200;
    private int _aduration = 200;

    private Animation TranAnima(boolean isIn)
    {
        TranslateAnimation localTranslateAnimation;
        if (isIn)
        {
            localTranslateAnimation = new TranslateAnimation(0.0F, 0.0F, -_menuMore.getHeight(), 0.0F);
            localTranslateAnimation.setInterpolator(new DecelerateInterpolator());
        }
        else
        {
            localTranslateAnimation = new TranslateAnimation(0.0F, 0.0F, 0.0F, -_menuMore.getHeight());
            localTranslateAnimation.setInterpolator(new AccelerateInterpolator());
        }

        localTranslateAnimation.setDuration(this._tduration);//.Duration = (this._tduration);
        localTranslateAnimation.setFillEnabled(true);//.FillEnabled = (true);
        localTranslateAnimation.setFillAfter(true);//.FillAfter = (true);
      
        localTranslateAnimation.setAnimationListener(this);
        return localTranslateAnimation;
    }

    private Animation AlphaAnima(boolean isIn)
    {
        AlphaAnimation localAlphaAnimation;
        if (isIn){
         
            localAlphaAnimation = new AlphaAnimation(0.0F, 1.0F);
            localAlphaAnimation.setInterpolator(new DecelerateInterpolator());
        }
        else
        {
            localAlphaAnimation = new AlphaAnimation(1.0F, 0.0F);
            localAlphaAnimation.setInterpolator(new AccelerateInterpolator());
        }
        localAlphaAnimation.setDuration(this._tduration);
        localAlphaAnimation.setFillEnabled(true);//、、FillEnabled = (true);
        localAlphaAnimation.setFillAfter(true);// = (true);
        localAlphaAnimation.setAnimationListener(this);
        return localAlphaAnimation;
    }

  
    public void Show()
    {
        _menuMore.setVisibility(View.VISIBLE);//.Visibility = ViewStates.Visible;
        _maskView.setVisibility(View.INVISIBLE);// = ViewStates.Invisible;//.setVisibility(4);\\
        this.setVisibility(View.VISIBLE);
       // list_content.startAnimation(TranAnima(true));
         _menuMore.startAnimation(TranAnima(true));
        _maskView.startAnimation(AlphaAnima(true));
    }

    public void Hide()
    {
    	// list_content.startAnimation(TranAnima(true));
         _menuMore.startAnimation(TranAnima(false));
        _maskView.startAnimation(AlphaAnima(false));
    }
   
    public void ShowmoreMenu()
    {
        _isShow = !_isShow;
        if (_isShow)
            Show();
        else
            Hide();
    }
    
    public interface IOnToggleListener
    {
        void OnToggleFinished(int status);
    }

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		  _menuMore.clearAnimation();
	//	list_content.clearAnimation();
	        _maskView.clearAnimation();
	        if (_isShow)
	        {
	            _menuMore.setVisibility(View.VISIBLE);//.Visibility = ViewStates.Visible;
	            _maskView.setVisibility(View.VISIBLE);//.Visibility = ViewStates.Visible;
	            this.setVisibility(View.VISIBLE);
	        }
	        else
	        {
	            _menuMore.setVisibility(View.GONE);// = ViewStates.Gone;
	            _maskView.setVisibility(View.GONE);// = ViewStates.Gone;
	            this.setVisibility(View.GONE);
	        }
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
	}


}



 
