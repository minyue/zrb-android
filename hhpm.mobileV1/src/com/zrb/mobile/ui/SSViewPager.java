package com.zrb.mobile.ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SSViewPager extends ViewPager
{
  public SSViewPager(Context paramContext)
  {
    super(paramContext);
  }

  public SSViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  @Override
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool1;
    try
    {
      boolean bool2 = super.onTouchEvent(paramMotionEvent);
      bool1 = bool2;
      return bool1;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      bool1 = false;
    }
    return bool1;
    
  }
}
