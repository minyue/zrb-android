package com.zrb.mobile.ui;

import com.zrb.mobile.R;
import com.zrb.mobile.utility.CommonUtil;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class ClearlinesEditText extends RelativeLayout implements View.OnFocusChangeListener, TextWatcher {

	Context paramContext;
	TextView title;
	EditText inputcontent;
	ImageView img_clean;
 	public ClearlinesEditText(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		paramContext=context;
		Init();
	}

	public ClearlinesEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		paramContext=context;
		Init();
	}
	
	private void Init()
	{
		 View localView = LayoutInflater.from(paramContext).inflate(R.layout.clear_linesedit_text, this, true);
 		 title=(TextView)localView.findViewById(R.id.txt_project_title);
 		 inputcontent=(EditText)localView.findViewById(R.id.et_project_title);
 		 img_clean=(ImageView)localView.findViewById(R.id.project_content_clean);
 		 img_clean.setVisibility(View.GONE);
 		 inputcontent.setOnFocusChangeListener(this);
 		 inputcontent.addTextChangedListener(this);
 		 
 		 img_clean.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inputcontent.setText("");
			}
 		});
	}
	
	public void setTitle(CharSequence titledes){
		title.setText(titledes);
 	}
	
	public void setTilteRid(int Rid){
		title.setText(Rid);
	}
	
	public void setInputHeight(int dips){
		
	    int pixels=	CommonUtil.dip2px(paramContext, dips);
	    
	    ViewGroup.LayoutParams tmplayoutp=  inputcontent.getLayoutParams();
	    tmplayoutp.height=pixels;
	    inputcontent.setLayoutParams(tmplayoutp);
		//inputcontent.setHeight(pixels);
	}
	
	public String getContentValue()
	{
		if(TextUtils.isEmpty(inputcontent.getText())) return "";
		return inputcontent.getText().toString();
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
	
			if (hasFocus && inputcontent.getText().length() != 0)
				img_clean.setVisibility(View.VISIBLE);
			else
				img_clean.setVisibility(View.INVISIBLE);
 	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		if (inputcontent.getText().length() == 0)
			img_clean.setVisibility(View.INVISIBLE);
		else
			img_clean.setVisibility(View.VISIBLE);
	}
	
}

