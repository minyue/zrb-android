package com.zrb.mobile.ui;

import com.zrb.mobile.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

 
public class CustomloadingView extends RelativeLayout implements View.OnTouchListener 
{
    public TextView HitTxt;
    public Button HitBtn;
    public ProgressBar HitProgress;
    public ImageView hitImg;
    private Animation anim;
    
    public CustomloadingView(Context paramContext, AttributeSet paramAttributeSet)
       // : base(paramContext, paramAttributeSet)
    {
       super(paramContext, paramAttributeSet);
       this.setOnTouchListener(this);
        //Resources.list_emptyview
        //  super(paramContext, paramAttributeSet);
        View localView = LayoutInflater.from(paramContext).inflate(R.layout.common_loading, this, true);
        this.HitTxt = ((TextView)localView.findViewById(R.id.list_emptyview_textView));
        this.HitBtn = ((Button)localView.findViewById(R.id.btn_reflesh));
        this.HitProgress = ((ProgressBar)localView.findViewById(R.id.list_emptyview_progressbar));//#FFF0F0F0

        this.hitImg = ((ImageView)localView.findViewById(R.id.list_emptyview_imgView));
        
        this.setBackgroundColor(paramContext.getResources().getColor(R.color.white));
         anim = AnimationUtils.loadAnimation(paramContext, R.anim.umeng_socialize_fade_out);
        //setBackgroundColor(getResources().getColor(2131230815));
      
    }

    public void SetOnRefleshListener(View.OnClickListener clicklistener)
    {
        this.HitBtn.setOnClickListener(clicklistener);
    }

    public CustomloadingView Show()
    {
        //setVisibility(0);
        this.setVisibility(View.VISIBLE);;//.Visibility = ViewStates.Visible;
        return this;
    }


    public CustomloadingView HideText(int resid)
    {
        return HideText(this.getContext().getString(resid));
     }

    public CustomloadingView HideText(String paramString)
    {
        TextView localTextView = this.HitTxt;
        if (paramString == null) ;
        //for (int i = 8; ; i = 0)
        //{
        //  localTextView.setVisibility(i);
        //  this.a.setText(paramString);
        //  return this;
        //}
       // localTextView.Visibility = ViewStates.Gone;
        localTextView.setText( paramString);
        return this;
    }

    public CustomloadingView ShowProgress(boolean paramBoolean)
    {
        ProgressBar localProgressBar = this.HitProgress;
        //if (paramBoolean);
        //for (int i = 0; ; i = 8)
        //{
        //  localProgressBar.setVisibility(i);
        //  return this;
        //}
        localProgressBar.setVisibility(View.VISIBLE);// = ViewStates.Visible;
        return this;

    }

    public CustomloadingView ShowLoading()
    {
        this.HitBtn.setVisibility(View.GONE);// = ViewStates.Gone;
        this.setVisibility(View.VISIBLE);// = ViewStates.Visible;
        this.hitImg.setVisibility(View.GONE);//Visibility = ViewStates.Gone;
        this.HitProgress.setVisibility(View.VISIBLE);//Visibility = ViewStates.Visible;
        return this;
    }

    public CustomloadingView ShowNull()
    {
        this.HitBtn.setVisibility(View.GONE);//v.Visibility = ViewStates.Gone;
        this.setVisibility(View.VISIBLE);// = ViewStates.Visible;
        this.hitImg.setImageResource(R.drawable.share_dialog_icon_qustion);
        this.hitImg.setVisibility(View.VISIBLE);//Visibility = ViewStates.Visible;
        this.HitProgress.setVisibility(View.GONE);// = ViewStates.Gone;
        return this;
    }

    public CustomloadingView ShowError()
    {
        this.setVisibility(View.VISIBLE);// = ViewStates.Visible;
        this.hitImg.setImageResource(R.drawable.icon_sad);
        this.hitImg.setVisibility(View.VISIBLE);// = ViewStates.Visible;
        this.HitProgress.setVisibility(View.GONE);// = ViewStates.Gone;
        return this;
    }

    public CustomloadingView Hide()
    {
        // setVisibility(8);
    	this.startAnimation(anim);
        this.setVisibility(View.GONE);// = ViewStates.Gone;
        return this;
    }

    public CustomloadingView ShowRefleshBtn(boolean paramBoolean)
    {
        Button localButton = this.HitBtn;
        //if (paramBoolean);
        //for (int i = 0; ; i = 8)
        //{
        //  localButton.setVisibility(i);
        //  return this;
        //}

        localButton.setVisibility(View.VISIBLE);// = ViewStates.Visible;

        return this;
    }

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return true;
	}

}