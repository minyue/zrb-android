package com.zrb.mobile;

import org.w3c.dom.Text;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class Digg_Animation extends Activity implements OnClickListener{
	
	private Button button;
	private TextView textView;
	private android.view.animation.Animation animation;
	public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.digg_animation);
        
       animation=AnimationUtils.loadAnimation(Digg_Animation.this,R.anim.digg);
        
        button=(Button)findViewById(R.id.bt_one);
        button.setOnClickListener(this);
        textView=(TextView)findViewById(R.id.tv_one);
        
        
        
        
    }
	@Override
	public void onClick(View v) {
		if(v==button){	
			textView.setVisibility(View.VISIBLE);
			textView.startAnimation(animation);
			new Handler().postDelayed(new Runnable(){
	            public void run() {
	            	textView.setVisibility(View.GONE);
	            } 
			}, 1000);
		}
		
	}
	
	
	
	
	
	
	
	

}
