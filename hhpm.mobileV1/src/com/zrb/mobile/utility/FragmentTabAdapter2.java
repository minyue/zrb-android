package com.zrb.mobile.utility;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.RadioGroup;

import java.util.List;

import com.zrb.mobile.R;

/**
 * Created with IntelliJ IDEA.
 * Author: wangjie  email:tiantian.china.2@gmail.com
 * Date: 13-10-10
 * Time: 上午9:25
 */
public class FragmentTabAdapter2 {
    private List<Fragment> fragments; // 一个tab页面对应一个Fragment
    private RadioGroup rgs; // 用于切换tab
    private FragmentActivity fragmentActivity; // Fragment所属的Activity
    private int fragmentContentId; // Activity中所要被替换的区域的id

    private int currentTab=0; // 当前Tab页面索引

   

    public FragmentTabAdapter2(FragmentActivity fragmentActivity, List<Fragment> fragments, int fragmentContentId) {
        this.fragments = fragments;
        this.rgs = rgs;
        this.fragmentActivity = fragmentActivity;
        this.fragmentContentId = fragmentContentId;

        // 默认显示第一页
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(fragmentContentId, fragments.get(0));
        ft.commit();
        currentTab=0;
    }

    public void setCheckChanged(int checkindex)
    {
    	if(currentTab==checkindex) return;
    	
    	 Fragment currentfragment = fragments.get(checkindex);
         boolean isAdd=currentfragment.isAdded();
         
         FragmentTransaction ft = obtainFragmentTransaction(checkindex,isAdd);
        
         Fragment oldFragment=getCurrentFragment();
         oldFragment.onPause();          // 暂停当前tab
                                         //       getCurrentFragment().onStop();  // 暂停当前tab

         if(isAdd){
                                         //fragment.onStart(); // 启动目标tab的onStart()
         	 currentfragment.onResume();  // 启动目标tab的onResume()
         }else{
             ft.add(fragmentContentId, currentfragment);
         }
         
         ft.hide(oldFragment).show(currentfragment);
        // showTab(i); // 显示目标tab
         ft.commit();
         currentTab = checkindex; 
    }
  
    /**
     * 切换tab
     * @param idx
     */
    private void showTab(int idx){
        for(int i = 0; i < fragments.size(); i++){
            Fragment fragment = fragments.get(i);
            FragmentTransaction ft = obtainFragmentTransaction(idx,false);

            if(idx == i){
                ft.show(fragment);
            }else{
                ft.hide(fragment);
            }
            ft.commit();
        }
        currentTab = idx; // 更新目标tab为当前tab
    }

    /**
     * 获取一个带动画的FragmentTransaction
     * @param index
     * @return
     */
    private FragmentTransaction obtainFragmentTransaction(int index,boolean isAdd){
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        // 设置切换动画
        
     //  if(!isAdd ) 
        //	ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
   /*     if(index > currentTab){
          //ft.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_left_out);
        	ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
     
        }else{
        	  // ft.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out);
        	ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
         }*/
        return ft;
    }

    public int getCurrentTab() {
        return currentTab;
    }

    public Fragment getCurrentFragment(){
        return fragments.get(currentTab);
    }
 
}