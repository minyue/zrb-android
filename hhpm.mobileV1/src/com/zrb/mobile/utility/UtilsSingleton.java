package com.zrb.mobile.utility;

import com.zrb.mobile.adapter.model.DiscoverDto;

import android.util.Log;

public class UtilsSingleton {
	private static UtilsSingleton instance;
	private boolean isOpen = false;
	private static final String LOG_TAG = "ViewUtilsSingleton";
	private boolean mRefresh = false;
	private DiscoverDto mDiscoverDto = null;
	private UtilsSingleton() {

	}

	public static synchronized UtilsSingleton getInstance() {
		if (instance == null) {
			instance = new UtilsSingleton();
		}
		return instance;
	}
	
	public boolean getSwipeMenuLayoutState(){
		return isOpen;
	}
	
	public void setSwipeMenuLayoutState(boolean hasOpen){
		Log.d(LOG_TAG,"setSwipeMenuLayoutState hasOpen:" + hasOpen);
		isOpen = hasOpen;
	}
	
	public boolean getRefresh(){
		return mRefresh;
	}
	
	public void setRefresh(boolean refresh){
		mRefresh = refresh;
	}
	
	public void setPublicDiscover(DiscoverDto discoverDto){
		this.mDiscoverDto = discoverDto;
	}
	
	public DiscoverDto getPublicDiscover(){
		return this.mDiscoverDto;
	}
	
}
