package com.zrb.mobile.utility;

import java.lang.reflect.Type;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 

import android.text.format.DateFormat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;



public class DateDeserializer implements JsonDeserializer<Date> {  
    public Date deserialize(JsonElement json, Type typeOfT,  
            JsonDeserializationContext context) throws JsonParseException {  
        String JSONDateToMilliseconds = "\\/(Date\\((.*?)(\\+.*)?\\))\\/";  
        Pattern pattern = Pattern.compile(JSONDateToMilliseconds);  
        Matcher matcher = pattern.matcher(json.getAsJsonPrimitive()  
                .getAsString());  
        String result = matcher.replaceAll("$2");  
        return new Date(new Long(result));  
    }  
} 

/*
GsonBuilder gsonb = new GsonBuilder();  
//Json中的日期表达方式没有办法直接转换成我们的Date类型, 因此需要单独注册一个Date的反序列化类.   
//  DateDeserializer ds = new DateDeserializer();  
//给GsonBuilder方法单独指定Date类型的反序列化方法   
// gsonb.registerTypeAdapter(Date.class, ds);  
  
Gson gson = gsonb.create();  
*/



//  Type type=new TypeToken<List<QxybResultDTO>>(){}.getType();

//public class TimestampTypeAdapter implements JsonSerializer<Timestamp>,JsonDeserializer<Timestamp> {
//
//	
//	private final DateFormat formats=new SimpleDateFormat("yyyy-MM-dd");
//	@Override
//	public Timestamp deserialize(JsonElement arg0, Type arg1,
//			JsonDeserializationContext arg2) throws JsonParseException {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public JsonElement serialize(Timestamp arg0, Type arg1,
//			JsonSerializationContext arg2) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	
//
//}
