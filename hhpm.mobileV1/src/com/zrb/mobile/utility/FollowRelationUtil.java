package com.zrb.mobile.utility;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import com.zrb.mobile.R;

public class FollowRelationUtil {
	
	// 0未关注 ,1已关注，2被关注 3相互关注
  public static boolean haveFollowed(int code){
	 return code==1||code==3;
	  
  }
  
  
  
  public static SpannableStringBuilder getFormatText(int code ,String mobile,String name,boolean isFans){
	  
	    int  norColor =Color.parseColor("#007de3");
 	 	boolean isBothFollow=FollowRelationUtil.haveBothFollowed(code);
 	 //	name= TextUtils.isEmpty(name)? "test":name;
       	String tmpStr="";
       	if(!isFans)
       		tmpStr=String.format("我关注了%1$s，联系方式：%2$s",name,isBothFollow?mobile:"相互关注后，即可查看");
       	else
       		tmpStr=String.format("%1$s关注了我，联系方式：%2$s",name,isBothFollow?mobile:"相互关注后，即可查看");
       	
		SpannableStringBuilder styled2 = new SpannableStringBuilder(tmpStr);
		
    	if(!TextUtils.isEmpty(name)){
 			int idx2=tmpStr.indexOf(name);
 			styled2.setSpan(new ForegroundColorSpan(norColor), idx2,idx2+name.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
  		}  
		
 		if(isBothFollow){
 			int idx3=tmpStr.indexOf("方式：");
			styled2.setSpan(new ForegroundColorSpan(norColor), idx3+3,tmpStr.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);// i 未起始字符索引，j 为结束字符索引
		} 
 		return styled2;
  }
  
  
  
  public static boolean haveBothFollowed(int code){
	  
	  return code==3;
  }
  
  public static int getRelationIcon(int code){
	  
	  switch(code){
 		 case 1:
			 return R.drawable.icon_me_ygz ;
	 
		 case 2:
			 return R.drawable.icon_me_ygz ;
	 
		 case 3:
			 return R.drawable.icon_me_xhgz;
 		 }
	  return R.drawable.icon_me_wgz;
  }
  
   public static String getRelationdes(int code){
	   
	   switch(code){
		 case 0:
			 return "关注";
 
		 case 1:
			 return "已关注" ;
	 
		 case 2:
			 return "被关注" ;
	 
		 case 3:
			 return "相互关注";
		 
		 }
	   return "";
   }
   
public static String getfansRelationdes(int code){
	   
	   switch(code){
  		 case 2:
			 return "已关注" ;
	 
		 case 3:
			 return "相互关注";
		 
		 }
	   return "";
   }
}
