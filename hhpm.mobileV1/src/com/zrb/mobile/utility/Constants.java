package com.zrb.mobile.utility;

import java.util.ArrayList;
import java.util.List;

import android.widget.TextView;

import com.zrb.mobile.adapter.model.CityEntity;

public class Constants {

 
	//动态类型
	public final static int Choice_Project=101;
	public final static int Choice_Fund=102;
	public final static int Publish_Project=201;
	public final static int Publish_Fund=202;
	

	public final static int CHANNEL_CITY = 3;

	public final static int defaultMinUpdateDay = 4;

	public final static String apkCheckUpdateUrl = "http://192.168.1.179/zrb_update.txt";

	public final static String apkSaveDir = "downupdate/";

	public final static String serverPhone = "400-027-5955";

	public final static int purposeType = 101; // ����
	public final static int pppType = 102; //
	public final static int assetType = 103; // �ʲ�
 

	/** 勘察状态，0：取消，1：申请；2、确认；3、完成 **/
	public static final int InspectCancel = 0;
	public static final int InspectApply = 1;
	public static final int InspectConfirm = 2;
	public static final int InspectFinish = 3;

	public final static int DiscoverProjectIntent = 101;// 项目意向
	public final static int DiscoverMoneyIntent = 102;// 资金意向
	
	
	public final static int MatchingFund= 103; // 匹配资金
	public final static int MatchingProject = 104;   //匹配项目  

/*	public final static int DiscoverStockRight = 301;// 股权
	public final static int DiscoverCreditorRight = 302;// 债权
	public final static int DISCOVER_BULK_ACQUISITION = 303;//整体收购 
	public final static int DISCOVER_PROJECT_COOPERATION = 304;//项目合作
	public final static int DISCOVER_FULL_TRANSFERRING = 305;//整体转让
	public final static int DiscoverOtherRight = 399;// 其它
*/	
	public final static int FundStockRight = 901;    // 资金900  股权投资
	public final static int FundCreditorRight = 902;  //债权投资
	public final static int FundBULK_ACQUISITION = 903;  //整体收购
	public final static int FundPROJECT_COOPERATION = 904;  //项目合作
	public final static int FundOtherRight = 905;  //面议
	
	public final static int ProjectStockRight = 801;    // 项目800  股权融资
	public final static int ProjectCreditorRight = 802;  //债权融资
	public final static int ProjectBULK_ACQUISITION = 803;  //整体转让
	public final static int ProjectPROJECT_COOPERATION = 804;  //项目合作
	public final static int ProjectOtherRight = 805;  //面议
	

	public final static int FocusMoney = 702;// 关注资金
	public final static int FocusProject = 701;// 关注项目
	public final static int FocusIntent = 703;// 关注意向
	public final static int Fund_Query = 705;// 咨询资金
	public final static int Pro_Query = 704;// 咨询项目
	public final static int Inten_Query = 708;// 咨询意向
	public final static int Fund_Share = 707;// 分享资金
	public final static int Pro_Share = 706;// 分享项目
	public final static int Inten_Share = 709;// 分享意向
	
	public final static int ALLIndustryCode = 97;// 全部行业
	
	public final static int PROJECT_SCENE_AUTH = 502;// 项目实地认证
	public final static int PROJECT_MATERIALS_AUTH = 503;// 项目资料认证
 

	public static final String FindMoneytag = "FindMoney_MainFragment_";

	public static final String Refresh_Find_mainlist = "discover_main";// 刷新关注列表

	public static final String FocusNum_Find_mainlist = "discover_content";// 关注，留言数数
	public static final String FocusNum_PUBLISE_mainlist = "my_publish_numer";// 我的发布关注，留言数数
	public static final String FocusNum_ATTENTION_mainlist = "my_attention_numer";// 我的关注，留言数数
	public static final String FocusNum_FUNDETIAL_mainlist = "my_funddetial_numer";// 我的关注，留言数数
	public static final String FocusNum_MYPROJECT_mainlist = "my_project_numer";// 我的关注，留言数数
	public static final String FocusNum_MYMONEY_mainlist = "my_money_numer";// 我的关注，留言数数
	public static final String FocusNum_MONEYDETIALS_mainlist = "my_moneydetials_numer";// 我的关注，留言数数
	public static final String Regin_Success_FINISH = "regin_success_finish";// 登录成功关闭登录界面
	public static final String PRONum_ATTENTION_mainlist = "my_attention_pronumer";// 我的关注,项目数
	public static final String MONNum_ATTENTION_mainlist = "my_attention_monnumer";// 我的关注,资金数
	//public static final String SHARE_SUCCESS_FINISH = "share_success_finish";// 分享成功关闭当前页面
	

	public static final String FLAG = "·";

	public static final String DETAIL_TYPE = "5";

}
