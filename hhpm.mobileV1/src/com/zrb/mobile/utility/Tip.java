package com.zrb.mobile.utility;

 
import com.zrb.mobile.R;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.hhpm.lib.ui.*;
import com.hhpm.lib.ui.ProgressWheel;

public class Tip {

    private  Context _context;
    private  Dialog _mDialog;
    private View _contentView;
    private ImageView _loadingImg;

    public View getContentView() {  return _contentView;  }
    protected Dialog getDialog() {   return _mDialog;  }
    protected Context getContext() {   return _context;  }
    protected ImageView getloadingImg()  {  return _loadingImg; }

    ProgressHelper  mProgressHelper ;
    public Tip(Context context, int dialogThemeId)  {

       _context = context;
       _mDialog = new Dialog(context, dialogThemeId); // new AlertDialog.Builder(context).Create();
       Window window = _mDialog.getWindow();//.Window;

       WindowManager.LayoutParams wl = window.getAttributes();
       wl.x = 0;
       wl.y = 20;
       window.setAttributes(wl);

     //  window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.ForceNotFullscreen);
       window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
       window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

     //  window.setGravity(GravityFlags.CenterHorizontal | GravityFlags.Top); //Gravity.LEFT | Gravity.TOP); 

       window.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);
       //      mDialog.SetContentView(Resource.Layout.activity_dialog);
       //   mDialog.SetFeatureDrawableAlpha(Window.FEATURE_OPTIONS_PANEL, 0);
       //_mDialog.SetFeatureDrawableAlpha((int) WindowFeatures.OptionsPanel, 255);

       _mDialog.setFeatureDrawableAlpha(Window.FEATURE_OPTIONS_PANEL, 255);
       _mDialog.setOnKeyListener(keylistener);
       _mDialog.setCanceledOnTouchOutside(false);//设置区域外点击消失
    }

    DialogInterface.OnKeyListener keylistener = new DialogInterface.OnKeyListener(){
  		@Override
		public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
			 if (keyCode==KeyEvent.KEYCODE_BACK&&event.getRepeatCount()==0) {
				 Dismiss();
                return true;
            }
            else{
             return false;
            }
		}
    } ;
    
    public void SetGravity(int flags) {

       Window window = _mDialog.getWindow();
       window.setGravity(flags);
    }

    public void SetContentView(View root){

       //Window window = mDialog.Window;
       //window.SetContentView(root);
       _contentView = root;
       _mDialog.setContentView(root);
    }

    public void SetContentView(int layoutResId)  {

          LayoutInflater inflator =
           (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); //.LAYOUT_INFLATER_SERVICE);
           SetContentView(inflator.inflate(layoutResId, null));
    }


    public void Show(String showtxt)     {

       ((TextView) _contentView.findViewById(R.id.LoadingTxt)).setText(showtxt, BufferType.NORMAL);
       if(mProgressHelper==null){
            mProgressHelper = new ProgressHelper(_context);
            mProgressHelper.setProgressWheel((ProgressWheel)_contentView.findViewById(R.id.progressWheel));
       }
       else
       	mProgressHelper.spin();
       
/*        _loadingImg = (ImageView)_contentView.findViewById(R.id.LoadingImg);

       Animation anim = AnimationUtils.loadAnimation(_context, R.anim.round_loading);

       _loadingImg.startAnimation(anim);*/

       
       
       _mDialog.show();

    }


    
    public void Dismiss()  {

      // if (_loadingImg != null) _loadingImg.clearAnimation();
   	 if (mProgressHelper != null) mProgressHelper.stopSpinning();;
   	 
      _mDialog.dismiss();

    }

    public void SimpleShow() {

       if (_mDialog != null) _mDialog.show();
    }

    public boolean isShow() {

       return _mDialog.isShowing();//.isShowing;
   }




}


