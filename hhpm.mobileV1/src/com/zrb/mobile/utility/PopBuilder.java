﻿package com.zrb.mobile.utility;

import com.zrb.mobile.R;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
 
    public class PopBuilder
    {
        private View contentView;
        private Context context;
        private Drawable _background;
      //  private View.OnKeyListener _mOnKeyListener;
        private int _animationStyle=0;
         PopupWindow mPopupWindow;
         onDismissListener monDismissListener;
         public interface onDismissListener  {
         	 void onPopupClose();
         }
         
         public void setonDismissListener(onDismissListener DismissListener){
         	 monDismissListener=DismissListener;
         }
        
        public View getRootView(){
        	
        	return contentView;
        }  

        public PopBuilder(Context context)
        {
            this.context = context;
        }

        public PopBuilder setBackground(Drawable background)
        {
            this._background = background;
            return this;
        }

        /*public PopBuilder  SetOnKeyListener(View.OnKeyListener OnKeyListener)
        {
            _mOnKeyListener = OnKeyListener;
           return this;
        }*/

        public PopBuilder setContentView(View v)
        {
            this.contentView = v;
            return this;
        }

        public PopBuilder setAnimationStyle(int ResAniID)
        {
            this._animationStyle = ResAniID;
            return this;
        }

        public PopBuilder setContentView(int layoutResId) {
            LayoutInflater inflator =  (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //.LAYOUT_INFLATER_SERVICE);

            return setContentView(inflator.inflate(layoutResId, null));
        }

        public PopupWindow Create()
        {
            /*第一个参数弹出显示view  后两个是窗口大小*/
        	  mPopupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);//.WRAP_CONTENT
            /*设置背景显示*/
            //mPopupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_menu_popup));

            mPopupWindow.setBackgroundDrawable(_background == null ? new BitmapDrawable() : _background);
            /*设置触摸外面时消失*/
            mPopupWindow.setOutsideTouchable(true);
            /*设置系统动画*/
            mPopupWindow.setAnimationStyle( _animationStyle == 0 ? R.style.PopupAnimation : _animationStyle);// Android.Resource.Style.AnimationDialog;//(android.R.style.Animation_Dialog);
            mPopupWindow.update();//.update();
            mPopupWindow.setTouchable(true);
            /*设置点击menu以外其他地方以及返回键退出*/
            mPopupWindow.setFocusable(true);
     
            /** 1.解决再次点击MENU键无反应问题  
             *  2.sub_view是PopupWindow的子View
             */
            contentView.setFocusable(true);//comment by danielinbiti,设置view能够接听事件，标注1  
            contentView.setFocusableInTouchMode(true);
          //  contentView.FocusableInTouchMode = true;
           
           
            contentView.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
               // TODO Auto-generated method stub
             	  // if(true) return false;
              	   boolean keyback= event.getKeyCode() == KeyEvent.KEYCODE_BACK&& event.getAction() == KeyEvent.ACTION_DOWN;
                   if (keyback&&(mPopupWindow.isShowing())) {
                      mPopupWindow.dismiss();// 这里写明模拟menu的PopupWindow退出就行
                    
                     return true;
                   }
                    return false;
               } });
             
            mPopupWindow.setOnDismissListener(new OnDismissListener(){
 				@Override
				public void onDismiss() {
					// TODO Auto-generated method stub
 				   if(monDismissListener!=null)  monDismissListener.onPopupClose();
				}
            });
           // contentView.setOnKeyListener(_mOnKeyListener);

            return mPopupWindow;
      
            //contentView.FindViewById(Resource.Id.email_Content).SetOnClickListener(this);
            //contentView.FindViewById(Resource.Id.sms_content).SetOnClickListener(this);
            //contentView.FindViewById(Resource.Id.Close_content).SetOnClickListener(this);
            //sub_view.FindViewById(Resource.Id.Forward_content).SetOnClickListener(this);
            //sub_view.FindViewById(Resource.Id.Check_content).SetOnClickListener(this);
        }

       
    }
 