package com.zrb.mobile.utility;

import com.zrb.mobile.R;

import android.content.Context;

import android.view.Gravity;

public class ProgressDialogEx {

	public static Tip Show(Context context, String title, String loadtxt,boolean indeterminate, boolean cancelable)
	{

		Tip _actionTip = new Tip(context, R.style.customDialog);

		_actionTip.SetContentView(R.layout.loadingwait);
		_actionTip.SetGravity(Gravity.CENTER);
		_actionTip.Show(loadtxt);
		return _actionTip;

	}

}
