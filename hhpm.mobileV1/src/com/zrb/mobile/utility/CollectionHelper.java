package com.zrb.mobile.utility;

import org.apache.http.Header;

import android.content.Context;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.applib.utils.AppSetting;
import com.zrb.applib.utils.LocalUserInfo;
import com.zrb.mobile.User_registerBase1;
import com.zrb.mobile.adapter.model.CheckResultDto;
import com.zrb.mobile.adapter.model.FavoriteDto;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

public class CollectionHelper {

	private final static int  CollectAction=1;
	private final static int  HasFocusAction=2;
	// FavoritehistoryDao mfavoriteItemDao;
	int mfocusType;
	Context mcontext;
	ZrbRestClient mZrbRestClient;
	public int curPostion = -1;
	onCollectionListener mCollectionListener;
	checkCollection mhasCollectionListener;
	int curOper=CollectAction;//1 收藏，2 检查是否收藏
 
	 
	boolean isDoAdd=false;

	public interface onCollectionListener {
		void onCollectionEvent();
	}

	public interface checkCollection {
		void onhasCollection(boolean ishas);
	}

	public void setOnCollectionListener(onCollectionListener CollectionListener) {
		mCollectionListener = CollectionListener;
	}

	public void setOncheckCollection(checkCollection hasCollectionListener) {
		mhasCollectionListener = hasCollectionListener;
	}

	public CollectionHelper(Context context, int focusType) {
		mcontext = context;
		mfocusType = focusType;
		
		if(mZrbRestClient==null){
        	mZrbRestClient=new ZrbRestClient(mcontext);
        	mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
    			@Override
    			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
    				// progTip.Dismiss();
    				CommonUtil.showToast(arg0, mcontext);
    			}

    			@Override
    			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
    				String results = new String(arg2);
    				if(curOper==CollectAction)
    					handlerCollection(results);
    				else
    					HandlerCheck(results);
    			}
    		});
        	
        }
	}

	private void handlerCollection(String results){
		
		try {
			Gson gson = new Gson();
			CheckResultDto tmpDto = gson.fromJson(results,CheckResultDto.class);
			if (tmpDto.res == 1) {
				CommonUtil.showToast(isDoAdd ? "关注成功" : "取消关注成功", mcontext, true);

				if (AppSetting.curUser != null) {
					if (mfocusType == Constants.FocusProject) {
						AppSetting.curUser.collectProject += (isDoAdd ? 1 : -1);
						LocalUserInfo.getInstance(mcontext).saveUserInfo(AppSetting.curUser);
					} else if (mfocusType == Constants.FocusMoney) {
						AppSetting.curUser.collectFund += (isDoAdd ? 1 : -1);
						LocalUserInfo.getInstance(mcontext).saveUserInfo(AppSetting.curUser);
					}

				}

				/*
				 * if(isAdd) mfavoriteItemDao.save(new
				 * FavoritehistoryItem(newsId,"",1),isNews ); else
				 * mfavoriteItemDao.delete(newsId,isNews);
				 */
				if (null != mCollectionListener)
					mCollectionListener.onCollectionEvent();
			} else
				CommonUtil.showToast(tmpDto.msg, mcontext);
		} catch (Exception e) {
			CommonUtil.showToast("加载数据出错", mcontext);
		}
	}
	
	private void HandlerCheck(String results){
		
		try {
			Gson gson = new Gson();
			FavoriteDto tmpDto = gson.fromJson(results, FavoriteDto.class);
			if (tmpDto.res == 1) {

				if (null != mhasCollectionListener)
					mhasCollectionListener.onhasCollection(tmpDto.data.isCollected);
			} else
				CommonUtil.showToast(tmpDto.msg, mcontext);
		} catch (Exception e) {
			CommonUtil.showToast("加载数据出错", mcontext);
		}
	}
	
	/*
	 * public boolean isExistId(int curId,boolean isNews){
	 * if(mfavoriteItemDao==null) mfavoriteItemDao = new
	 * FavoritehistoryDao(mcontext); return
	 * mfavoriteItemDao.isExistId(curId,isNews); }
	 */
	 
	public void onClickCollect(final long newsId,  boolean isAdd) {
		// TODO Auto-generated method stub
  
		String url = String.format("userStatisticDetail/%1$s?id=%2$d&type=%3$s",  isAdd ? "doAdd" : "doCancel", newsId, mfocusType);
		
		CommonUtil.InfoLog("onClickCollect", url);
		isDoAdd=isAdd;
		curOper=CollectAction;
		mZrbRestClient.sessionGet(url);
	}

	public void checkCollect(final long newsId) {
		// TODO Auto-generated method stub
		// if(mfavoriteItemDao==null) mfavoriteItemDao = new
		// FavoritehistoryDao(mcontext);

		String url = String.format("userStatisticDetail/%1$s?id=%2$d&type=%3$s","isCollected", newsId, mfocusType);

		CommonUtil.InfoLog("checkCollect", url);
		curOper=HasFocusAction;
 		mZrbRestClient.sessionGet(url);
	}
}
