package com.zrb.mobile.utility;

public interface OnDownLoader {
	
    boolean CancelUpdate();
    void SetCurPercent(int progress);
    void DismissDialog();
    void SetFilePath(String path);
    void SetFileSize(int filesize);
}
 