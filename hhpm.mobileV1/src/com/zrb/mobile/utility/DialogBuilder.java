package com.zrb.mobile.utility;

import com.zrb.mobile.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

 
public   class DialogBuilder
{

    private Context context;
    private String title;
    private String message;
    private String positiveButtonText;
    private String negativeButtonText;
    private View contentView;
    private View.OnClickListener _mOnclickListener;

    private int _themeId=-1;
    private int _parentlayoutId=-1;
    private boolean showAlert=false;
    private int outsideMenuWidth=0;
    
    public View getCurContentView() {  return contentView;   }
    //private DialogInterface.OnClickListener 
    //                positiveButtonClickListener,
    //                negativeButtonClickListener;

    public DialogBuilder(Context context) {
        this.context = context;
    }

    public DialogBuilder(Context context, int theme)
    {
        this.context = context;
        this._themeId = theme;
    }
    public DialogBuilder setOutsideMenuWidth(int outWidth) {
        this.outsideMenuWidth = outWidth;
        return this;
    }
    /**
     * Set the Dialog message from String
     * @param title
     * @return
     */
    public DialogBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     * Set the Dialog message from resource
     * @param title
     * @return
     */
    public DialogBuilder setShowAlert(boolean isShowicon) {
        this.showAlert = isShowicon;
        return this;
    }

    /**
     * Set the Dialog title from resource
     * @param title
     * @return
     */
    public DialogBuilder setTitle(int title) {
        this.title = (String) context.getText(title);
        return this;
    }
    
    public DialogBuilder setParentlayout(int parentlayoutRid ) {
        this._parentlayoutId = parentlayoutRid;
        return this;
    }

    public DialogBuilder setMessage(int message) {
        this.message = (String) context.getText(message);
        return this;
    }
    
 
    public DialogBuilder setPositiveTitle(String title) {
        this.positiveButtonText = title;
        return this;
    }
    
    public void setNegativeButtonText(String negativeButtonText) {
		this.negativeButtonText = negativeButtonText;
	}

	/**
     * Set the Dialog title from String
     * @param title
     * @return
     */
    public DialogBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Set a custom content view for the Dialog.
     * If a message is set, the contentView is not
     * added to the Dialog...
     * @param v
     * @return
     */
    public DialogBuilder setContentView(View v) {
        this.contentView = v;
        return this;
    }

    public DialogBuilder setContentView(int layoutResId)
    {
        LayoutInflater inflator =
            (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //.LAYOUT_INFLATER_SERVICE);

       return  setContentView(inflator.inflate(layoutResId, null));
    }

    public DialogBuilder setCloseOnclick(View.OnClickListener CloseOnclick)
    {
        this._mOnclickListener = CloseOnclick;
        return this;
    
    }
    
    
    /**
     * Set the positive button resource and it's listener
     * @param positiveButtonText
     * @param listener
     * @return
     */
    //public DialogBuilder setPositiveButton(int positiveButtonText,
    //        DialogInterface.OnClickListener listener) {
    //    this.positiveButtonText = (String) context
    //            .getText(positiveButtonText);
    //    this.positiveButtonClickListener = listener;
    //    return this;
    //}

    /**
     * Set the positive button text and it's listener
     * @param positiveButtonText
     * @param listener
     * @return
     */
    //public DialogBuilder setPositiveButton(String positiveButtonText,
    //        DialogInterface.OnClickListener listener) {
    //    this.positiveButtonText = positiveButtonText;
    //    this.positiveButtonClickListener = listener;
    //    return this;
    //}

    /**
     * Set the negative button resource and it's listener
     * @param negativeButtonText
     * @param listener
     * @return
     */
    //public DialogBuilder setNegativeButton(int negativeButtonText,
    //        DialogInterface.OnClickListener listener) {
    //    this.negativeButtonText = (String) context
    //            .getText(negativeButtonText);
    //    this.negativeButtonClickListener = listener;
    //    return this;
    //}

    /**
     * Set the negative button text and it's listener
     * @param negativeButtonText
     * @param listener
     * @return
     */
    //public DialogBuilder setNegativeButton(String negativeButtonText,
    //        DialogInterface.OnClickListener listener) {
    //    this.negativeButtonText = negativeButtonText;
    //    this.negativeButtonClickListener = listener;
    //    return this;
    //}

    /**
     * Create the custom dialog
     */
    //public Dialog create() {

       
    //    LayoutInflater inflater = (LayoutInflater) context.GetSystemService(Context.LayoutInflaterService);
    //    // instantiate the dialog with the custom Theme
         
    //       Dialog dialog = new  Dialog(context, Resource.Style.Dialog);
    //    View layout = inflater.Inflate(Resource.R.layout.dialog, null);
    //    dialog.AddContentView(layout, new LayoutParams(
    //            LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    //    // set the dialog title
    //    ((TextView) layout.FindViewById(R.id.title)).setText(title);
    //    // set the confirm button
    //    //if (positiveButtonText != null) {
    //    //    ((Button) layout.FindViewById(R.id.positiveButton))
    //    //            .setText(positiveButtonText);
    //    //    if (positiveButtonClickListener != null) {
    //    //        ((Button) layout.FindViewById(R.id.positiveButton))
    //    //                .setOnClickListener(new View.IOnClickListener() {
    //    //                    public void onClick(View v) {
    //    //                        positiveButtonClickListener.onClick(
    //    //                          dialog, 
    //    //                                DialogInterface.BUTTON_POSITIVE);
    //    //                    }
    //    //                });
    //    //    }
    //    //} else {
    //    //    // if no confirm button just set the visibility to GONE
    //    //    layout.findViewById(R.id.positiveButton).setVisibility(
    //    //            View.GONE);
    //    //}
    //    // set the cancel button
    //    //if (negativeButtonText != null) {
    //    //    ((Button) layout.findViewById(R.id.negativeButton))
    //    //            .setText(negativeButtonText);
    //    //    if (negativeButtonClickListener != null) {
    //    //        ((Button) layout.findViewById(R.id.negativeButton))
    //    //                .setOnClickListener(new View.OnClickListener() {
    //    //                    public void onClick(View v) {
    //    //                        positiveButtonClickListener.onClick(
    //    //                          dialog, 
    //    //                                DialogInterface.BUTTON_NEGATIVE);
    //    //                    }
    //    //                });
    //    //    }
    //    //} else {
    //    //    // if no confirm button just set the visibility to GONE
    //    //    layout.findViewById(R.id.negativeButton).setVisibility(
    //    //            View.GONE);
    //    //}
    //    // set the content message
    //    if (message != null) {
    //        ((TextView) layout.findViewById(
    //          R.id.message)).setText(message);
    //    } else if (contentView != null) {
    //        // if no message set
    //        // add the contentView to the dialog body
    //        ((LinearLayout) layout.findViewById(R.id.content))
    //                .removeAllViews();
    //        ((LinearLayout) layout.findViewById(R.id.content))
    //                .addView(contentView, 
    //                        new LayoutParams(
    //                                LayoutParams.WRAP_CONTENT, 
    //                                LayoutParams.WRAP_CONTENT));
    //    }
    //    dialog.setContentView(layout);
    //    return dialog;
    //}

//}

     public Dialog create()  {
    	 
        LayoutInflater inflator =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Container = inflator.inflate(_parentlayoutId==-1 ?R.layout.custom_dialog:_parentlayoutId, null);

        if (!TextUtils.isEmpty(title) )
            ((TextView)Container.findViewById(R.id.title)).setText(title);//.s.Text = title;

        if (contentView != null)
        {
        	LinearLayout root = (LinearLayout)Container.findViewById(R.id.custom);
            root.addView(contentView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        if (null != _mOnclickListener)
        {
        	View tmpconfrim=Container.findViewById(R.id.btn_confirm);
        	if(tmpconfrim!=null){
        		tmpconfrim.setOnClickListener(_mOnclickListener);
        		((TextView)tmpconfrim).setText(TextUtils.isEmpty(positiveButtonText) ? "确定":positiveButtonText);
        	}
        	
        	View tmpcancel=Container.findViewById(R.id.btn_cancel);
        	if(tmpcancel!=null)  { tmpcancel.setOnClickListener(_mOnclickListener); 
        	((TextView)tmpcancel).setText(TextUtils.isEmpty(negativeButtonText) ? "取消":negativeButtonText);
        	}
        }
        
        if(showAlert) {       
         	Container.findViewById(R.id.icon).setVisibility(View.VISIBLE);
        }
        
        Dialog dialog = new Dialog(context,_themeId==-1 ? R.style.Dialog:_themeId);
        dialog.setContentView(Container);
   
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.width = CommonUtil.dip2px(context,300); // �߶�
        
        dialogWindow.setAttributes(lp);
        return dialog;
        

        //Dialog dialog = new Dialog(context,_themeId==-1 ? Resource.Style.Dialog:_themeId);
        //if (contentView != null)
        //{

        //    var tmpText = ((TextView)contentView.FindViewById(Resource.Id.dialogtitle));//.setText(title);
        //    if (tmpText != null) tmpText.Text = title;
        //    dialog.SetContentView(contentView);
         
        //}
        //else
        //{
        //       LayoutInflater inflater = (LayoutInflater) context.GetSystemService(Context.LayoutInflaterService);
        //       View layout = inflater.Inflate(Resource.Layout.dialog_del_confirm, null);
        //       dialog.AddContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FillParent, ViewGroup.LayoutParams.WrapContent));
        // }
        //return dialog;
    }

     public Dialog CreateNoBorder()
     {
    	 return CreateNoBorder( R.style.Dialog);
     }

     public Dialog CreateNoBorder(int themeId)
     {
         Dialog dialog = new Dialog(context, themeId);
         if (contentView != null)
         {
              if (!TextUtils.isEmpty(title) )
                 ((TextView)contentView.findViewById(R.id.title)).setText(title);//.s.Text = title;
             
             dialog.setContentView(contentView);

         }
         else
         {
             LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             View layout = inflater.inflate(R.layout.custom_dialog, null);
             dialog.addContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
         }
         return dialog;
     }
     
     
     public Dialog createDialog(Context context, int style){
    	 
    	 return  createDialog( context,  style, true) ;
     }
   //  tmpbuild.setCanceledOnTouchOutside(true); 
     public Dialog createDialog(Context context, int style,boolean isBottom) {
       
         final Dialog customDialog = new Dialog(context, style);

         WindowManager.LayoutParams localLayoutParams = customDialog.getWindow().getAttributes();
         localLayoutParams.gravity =isBottom?  (Gravity.BOTTOM | Gravity.LEFT):Gravity.CENTER;
         localLayoutParams.x = outsideMenuWidth;
         localLayoutParams.y = 0;

         int screenWidth = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
         this.contentView.setMinimumWidth(screenWidth - outsideMenuWidth);
         // dialogView.setMinimumHeight(10);

         customDialog.onWindowAttributesChanged(localLayoutParams);
         customDialog.getWindow().setSoftInputMode(
                 WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                         | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // customDialog.setCanceledOnTouchOutside(false);
         customDialog.setCancelable(true);
         customDialog.setCanceledOnTouchOutside(true);
         customDialog.setContentView(contentView);

         if (context instanceof Activity) {
             Activity activity = (Activity) context;
             if (!activity.isFinishing()) {
                 customDialog.show();
             }
         }
         return customDialog;
     }
     
}