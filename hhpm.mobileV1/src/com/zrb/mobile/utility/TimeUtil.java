package com.zrb.mobile.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.hhpm.lib.support.GlobalContext;
import com.zrb.mobile.R;

public class TimeUtil {

	public static final int SECONDS_IN_DAY = 60 * 60 * 24;
	public static final long MILLIS_IN_DAY = 1000L * SECONDS_IN_DAY;

	public static boolean isSameDayOfMillis(final long ms1, final long ms2) {
		final long interval = ms1 - ms2;
		return interval < MILLIS_IN_DAY && interval > -1L * MILLIS_IN_DAY
				&& toDay(ms1) == toDay(ms2);
	}

	private static long toDay(long millis) {
		return (millis + TimeZone.getDefault().getOffset(millis))
				/ MILLIS_IN_DAY;
	}

	public static String formatTime(long time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(time);
		String pushDate = format.format(date);
		return pushDate;
	}

	public static String getWeek(long time) {
		GlobalContext application = GlobalContext.getInstance();
		String Week = application.getResources().getString(
				R.string.user_detail_week);
		String[] days = application.getResources().getStringArray(R.array.week);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date curDate = new Date(time);
		String pTime = format.format(curDate);
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(format.parse(pTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		switch (c.get(Calendar.DAY_OF_WEEK)) {
		case 1:
			Week += days[0];
			break;
		case 2:
			Week += days[1];
			break;
		case 3:
			Week += days[2];
			break;
		case 4:
			Week += days[3];
			break;
		case 5:
			Week += days[4];
			break;
		case 6:
			Week += days[5];
			break;
		case 7:
			Week += days[6];
			break;
		default:
			break;
		}
		return Week;
	}
}