package com.zrb.mobile.utility;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.google.gson.Gson;
 
import com.zrb.discover.UserDetailActivity;
import com.zrb.mobile.Project_info_lience;
import com.zrb.mobile.System_message_details;
import com.zrb.mobile.User_my_surveyinfo;
import com.zrb.mobile.User_registercompany;
import com.zrb.mobile.adapter.model.MsgNoticeDto;
import com.zrb.mobile.adapter.model.MyMessageDto;
import com.zrb.mobile.*;

public class MsgformatUtil {
	
   static final 	int norColor=Color.parseColor("#ea8010");
	
   public static void formatSummary(TextView content ,TextView title,MyMessageDto curDto){
		
	   String bizType=curDto.messageInfo.bizType;
		if(bizType.equals("investor_regiester_succes_without_org")){
			content.setText(curDto.messageInfo.content.substring(0, curDto.messageInfo.content.length()-4));
			content.append(Html.fromHtml("<a href=" + "''" + " ><u>" + "立即认证" + "</u></a>"));
			title.setText("身份认证");
		}
	 
       else if(bizType.equals("pro_fund_pulished")){
 			content.setText(curDto.messageInfo.content);
			title.setText("发布成功");
		}
		else if(bizType.equals("pro_fund_collected")){
 			content.setText(curDto.messageInfo.content);
			title.setText("关注通知");
		}
       else if(bizType.equals("pro_fund_changed")){
 			content.setText(curDto.messageInfo.content);
			title.setText("变更通知");
		}
 		else{
 			content.setText(curDto.messageInfo.content);
			title.setText("系统通知");
		}
	}
   
	/*if ("pro_fund_pulished".equals(intention.messageInfo.bizType)) {
		holder.tv_titile.setText("发布成功");
	} else if ("pro_fund_collected"
			.equals(intention.messageInfo.bizType)) {
		holder.tv_titile.setText("关注通知");
	} else if ("pro_fund_changed"
			.equals(intention.messageInfo.bizType)) {
		holder.tv_titile.setText("变更通知");
	}*/
   

   public static Intent getAction(Context mcontext,MsgNoticeDto curDto){
	  // Intent tmpIntent=new Intent()
	      String bizType=curDto.messageInfo.bizType;
	     
	      //proj_cment_relay  proj_prog_change  proj_cment_new  user_confirm_inspect  user_complete_inspect
	      if("user_confirm_inspect".equals(bizType)||"user_complete_inspect".equals(bizType)){
	    	  Intent it = new Intent(mcontext, User_my_surveyinfo.class);
	 		  it.putExtra("inspectId", Integer.parseInt(curDto.messageInfo.bizKey));
	 		 return it;
	      }
	     //��Ŀ�Ѿ��½��ɹ�,��Ŀ���µ�����
	      else  if("proj_prog_change".equals(bizType)){  
	    	  Intent it = new Intent(mcontext, Project_info_lience.class);
	 		  it.putExtra("projectId", Integer.parseInt(curDto.messageInfo.bizKey));
	    	  return it;
	      }
	      else  if("proj_cment_new".equals(bizType)||"proj_cment_relay".equals(bizType)){//��ʽ pid(����id):cid(����id):uid(�ظ���id)��uname(�������û���)
	    	   String[] tmpArr=curDto.messageInfo.bizKey.split(":");
				int comMaxid = Integer.parseInt(tmpArr[1]) + 1;
				Intent it = new Intent(mcontext, Project_commentlist_Activity.class);
				it.putExtra("projectId", Integer.parseInt(tmpArr[0]));
				it.putExtra("maxid",comMaxid);
				 return it;
	      }
 		 return null;
    }
   
   public static boolean isNeedshow(String bizType){
		if ("proj_cment_relay".equals(bizType)
				|| "proj_prog_change".equals(bizType)
				|| "proj_cment_new".equals(bizType)
				|| "user_confirm_inspect".equals(bizType)
				|| "user_complete_inspect".equals(bizType))
			return true;
		else
			return false;

   }
   
  public static Intent getQuickAction(Context mcontext,MyMessageDto curDto){
	  String bizType=curDto.messageInfo.bizType;
	  if(bizType.equals("pro_fund_collected")){ //关注通知
 		 	
			Intent intent = new Intent(mcontext, UserDetailActivity.class);
/* 			intent.putExtra(Constants.USER_ID,Integer.parseInt(curDto.messageInfo.bizKey));
			intent.putExtra(Constants.USER_NAME,"");
			intent.putExtra(Constants.USER_AVATAR_URL,"");
			intent.putExtra(Constants.USER_ORG, "");
			intent.putExtra(Constants.USER_INDUSTRY,"");
			intent.putExtra(Constants.USER_JOB, "");*/
		 
 		  return intent;
	  }
/*	  else if(bizType.equals("proj_prog_new")){
		  Intent it = new Intent(mcontext, Project_info_lience.class);
 		  it.putExtra("projectId", Integer.parseInt(curDto.messageInfo.bizKey));
		  return it;
 	  }
	  else if(bizType.equals("user_apply_inspect")){ //
		  Intent it = new Intent(mcontext, User_my_surveyinfo.class);
 		  it.putExtra("inspectId", Integer.parseInt(curDto.messageInfo.bizKey));
		  return it;
		  
	  }*/
	  else {
		    Gson gson = new Gson();  
		    String strObj = gson.toJson(curDto);  
			Intent intent = new Intent(mcontext, System_message_details.class);
 		    intent.putExtra("jsonObj",strObj);
 		    return intent;
 	  }
 
  } 
}
