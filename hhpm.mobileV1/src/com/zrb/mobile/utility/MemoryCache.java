package com.zrb.mobile.utility;

import java.util.HashMap;

import com.zrb.mobile.adapter.model.FocusTmpDto;

public class MemoryCache {

	static MemoryCache instance=null;
  	HashMap<String , FocusTmpDto> cacheMap = new HashMap<String , FocusTmpDto>(); 
	private MemoryCache(){}
	
	public static MemoryCache getInstance(){
		
		if(instance==null) instance=new MemoryCache();
 		return instance;
	}
	
	public void add(String key,FocusTmpDto value){
		
		if(cacheMap.containsKey(key)) return;
		else
			cacheMap.put(key, value);
 	}
	
	
	public void putkey(String key,int postion,int value){
		
		if(cacheMap.containsKey(key)) {
 			FocusTmpDto tmp=cacheMap.get(key);
			tmp.postion=postion;
			tmp.num=value;
		}
		else
		{
			
			cacheMap.put(key, new FocusTmpDto(postion,value));
		}
		
		
 	}

    public FocusTmpDto get(String key){
		
		if(cacheMap.containsKey(key)) return cacheMap.get(key);
		else
			return null;
 	}
	
	public void remove(String key){
		if(cacheMap.containsKey(key))  cacheMap.remove(key);
	}
}
