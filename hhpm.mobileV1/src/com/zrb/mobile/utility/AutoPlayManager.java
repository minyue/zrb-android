package com.zrb.mobile.utility; 

import java.lang.ref.WeakReference;

import com.hhpm.lib.PageIndicator.CirclePageIndicator;

 

import android.os.Handler;
import android.os.Message;

 

/**
 * Auto BrocastManager for ImageIndicatorView
 * 
 * @author steven-pan
 * 
 */
public class AutoPlayManager {

	/**
	 * �Զ����ű�־λ��Ĭ�ϲ���
	 */
	private boolean broadcastEnable = false;
	/**
	 * �Զ���������Ĭ��ʱ��
	 */
	private static final long DEFAULT_STARTMILS = 2 * 1000;
	/**
	 * �Զ����ż��Ĭ��ʱ��
	 */
	private static final long DEFAULT_INTEVALMILS = 3 * 1000;
	/**
	 * ����ʱ��ms
	 */
	private long startMils = DEFAULT_STARTMILS;
	/**
	 * ���ms
	 */
	private long intevalMils = DEFAULT_INTEVALMILS;
	/**
	 * ����
	 */
	private final static int RIGHT = 0;
	/**
	 * ����
	 */
	private final static int LEFT = 1;

	/**
	 * ��ǰ����
	 */
	private int direction = RIGHT;

	/**
	 * �Զ�����Ĭ�ϴ������ޣ�
	 */
	private static final int DEFAULT_TIMES = -1;

	/**
	 * �Զ����Ŵ���
	 */
	private int broadcastTimes = DEFAULT_TIMES;

	/**
	 * �Զ����Ŵ������
	 */
	private int timesCount = 0;

	/**
	 * ѭ������
	 */
	private Handler broadcastHandler = null;

	/**
	 * target ImageIndicatorView
	 */
	private CirclePageIndicator mImageIndicatorView = null;

	public AutoPlayManager(CirclePageIndicator imageIndicatorView) {
		this.mImageIndicatorView = imageIndicatorView;
		this.broadcastHandler = new BroadcastHandler(AutoPlayManager.this);
	}

	/**
	 * �����Զ���������ʱ��ͼ��
	 * 
	 * @param startMils
	 *            ����ʱ��ms(>2, Ĭ��Ϊ8s)
	 * @param intevelMils
	 *            ���ms(Ĭ��Ϊ3s)
	 */
	public void setBroadcastTimeIntevel(long startMils, long intevelMils) {
		this.startMils = startMils;
		this.intevalMils = intevelMils;
	}

	/**
	 * �����Զ����ſ���
	 * 
	 * @param flag
	 *            �򿪻�ر�
	 */
	public void setBroadcastEnable(boolean flag) {
		this.broadcastEnable = flag;
	}

	/**
	 * ����ѭ�����Ŵ���
	 * 
	 * @param times
	 *            ѭ�����Ŵ���
	 */
	public void setBroadCastTimes(int times) {
		this.broadcastTimes = times;
	}

	/**
	 * ����ѭ������
	 */
	public void loop() {
		if (broadcastEnable) {
			broadcastHandler.sendEmptyMessageDelayed(0, this.startMils);
		}
	}

	protected void handleMessage(android.os.Message msg) {
		if (broadcastEnable) {
//			if (System.currentTimeMillis()
//					- mImageIndicatorView.getRefreshTime() < 2 * 1000) {// ���һ�λ������С��2s
//				return;
//			}
			if ((broadcastTimes != DEFAULT_TIMES)
					&& (timesCount > broadcastTimes)) {// ѭ����������
				return;
			}

//			if (direction == RIGHT) {// roll right
//				if (mImageIndicatorView.getCurrentIndex() < mImageIndicatorView.getTotalCount()) {
//					if (mImageIndicatorView.getCurrentIndex() == mImageIndicatorView.getTotalCount() - 1) {
//						timesCount++;// ѭ����������1
//						direction = LEFT;
//					} else {
//						mImageIndicatorView.getViewPager().setCurrentItem(mImageIndicatorView.getCurrentIndex() + 1,true);
//					}
//				}
//			} else {// roll left
//				if (mImageIndicatorView.getCurrentIndex() >= 0) {
//					if (mImageIndicatorView.getCurrentIndex() == 0) {
//						direction = RIGHT;
//					} else {
//						mImageIndicatorView.getViewPager().setCurrentItem(mImageIndicatorView.getCurrentIndex() - 1,true);
//					}
//				}
//			}

			broadcastHandler.sendEmptyMessageDelayed(1, this.intevalMils);
		}
	}

	private static class BroadcastHandler extends Handler {

		private final WeakReference<AutoPlayManager> autoBrocastManagerRef;

		public BroadcastHandler(AutoPlayManager autoBrocastManager) {
			this.autoBrocastManagerRef = new WeakReference<AutoPlayManager>(
					autoBrocastManager);
		}

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			AutoPlayManager autoBrocastManager = autoBrocastManagerRef.get();

			if (autoBrocastManager != null) {
				autoBrocastManager.handleMessage(msg);
			}
		}
	}

}
