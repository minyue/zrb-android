package com.zrb.mobile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

 
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.QZoneShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.TencentWBSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.sso.UMSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;
 
import com.zrb.mobile.adapter.AppSharelistAdapter;
import com.zrb.mobile.adapter.RadioItemEx;
import com.zrb.mobile.adapter.ShareGridAdapter;
import com.zrb.mobile.common.ShareDialog;
import com.zrb.mobile.ui.MyGridView;
import com.zrb.mobile.ui.NumEditText;
import com.zrb.mobile.ui.ViewPagerIndicator;
import com.zrb.mobile.ui.NumEditText.OnNumslistener;
import com.zrb.applib.utils.FileUtil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.umeng.socialize.controller.listener.SocializeListeners.SnsPostListener;
public class App_shareFragment extends BaseShareFragment implements  OnNumslistener
{
 
	 // 首先在您的Activity中添加如下成员变量
 
	AppSharelistAdapter mlistAdapter;
	ListView mlistview;
	
	TextView mtxtNum;
	NumEditText meditText;

	//Map<String, SHARE_MEDIA> mPlatformsMap = new HashMap<String, SHARE_MEDIA>();
 
 
	private View RootView;
	 
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
 	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
		View view = inflater.inflate(R.layout.app_share_fragment, container, false);
		RootView=view;
 		InitView();
  		return view;
	}
 	
   private void InitView(){
		
    
		mtxtNum=(TextView)RootView.findViewById(R.id.txtNum);
		mtxtNum.setText("98");
		meditText=(NumEditText)RootView.findViewById(R.id.editText1);
		meditText.setMaxNums(98);
		meditText.setOnNumslistener(this);
		
		mlistview=(ListView)RootView.findViewById(R.id.share_list);
		mlistAdapter=new AppSharelistAdapter(this.getActivity());
 		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
 		 super.onActivityCreated(savedInstanceState);
  		//initSocialSDK();
 		mcontext=this.getActivity();
 		murl="http://www.zhaorongbao.cn:81/";
		mtitle=mcontext.getResources().getString(R.string.share_txt_hite);
		init();
 	
 		loadData();
		//initPlatformMap();
  	}
 
	@Override
	public void showtextNum(int number) {
		// TODO Auto-generated method stub
		mtxtNum.setText(number+"");
	}
	
	private void loadData(){
		
		if(meditText.getText()==null)
			mtitle=meditText.getContext().getResources().getString(R.string.share_txt_hite);
		else
 	     	mtitle=meditText.getText().toString();

		mlistAdapter.CurrentDatas.add(new RadioItemEx("微信朋友圈","",R.drawable.news_control_center_wechatq));
		mlistAdapter.CurrentDatas.add(new RadioItemEx("微信好友","",R.drawable.news_control_center_wechat));
		mlistAdapter.CurrentDatas.add(new RadioItemEx("QQ好友","",R.drawable.news_control_center_qq));
		mlistAdapter.CurrentDatas.add(new RadioItemEx("QQ空间","",R.drawable.news_control_center_zone));
//		mlistAdapter.CurrentDatas.add(new RadioItemEx("新浪微博","",R.drawable.news_control_center_weibo));
//		mlistAdapter.CurrentDatas.add(new RadioItemEx("腾讯微博","",R.drawable.news_control_center_qq_weibo));
 		mlistview.setAdapter(mlistAdapter); 
 		mlistview.setOnItemClickListener(mItemclickListener);
 	}
	
	@Override
	public void onResume()
	{
 		super.onResume();
 	
 		
 		// if (this.getUserVisibleHint()) firstLoad();
   		//super.setUserVisibleHint(isVisibleToUser)
	}



	
  
	  
	 
	
	
		
		
}