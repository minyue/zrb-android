package com.zrb.mobile;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.adapter.model.ProjectDtos;
import com.zrb.mobile.adapter.model.UserInfoResult;
import com.zrb.mobile.common.ShareDialog;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

public class oldUser_loginActivity extends Activity implements OnClickListener,
		OnFocusChangeListener, TextWatcher {

	ImageView btnAccountclear, btnPwdclear;
	AutoCompleteTextView editTextname;
	EditText editTextpwd;
	Tip progTip;
	CheckBox isRemember;
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_login_activity);
		initView();
		SaveorShowPass(false);
	}

	private void initView() {
		this.findViewById(R.id.back)
				.setOnClickListener(oldUser_loginActivity.this);

		isRemember=(CheckBox)this.findViewById(R.id.remember_userinfo);
		
		btnAccountclear = (ImageView) this
				.findViewById(R.id.button_account_clear);
		btnAccountclear.setOnClickListener(this);

		btnPwdclear = (ImageView) this.findViewById(R.id.button_pwd_clear);
		btnPwdclear.setOnClickListener(this);

		editTextname = (AutoCompleteTextView) this
				.findViewById(R.id.edittext_account);
		editTextname.setOnFocusChangeListener(this);
		editTextname.addTextChangedListener(onUserNameTextChanged);

		editTextpwd = (EditText) this.findViewById(R.id.edittext_password);
		editTextpwd.setOnFocusChangeListener(this);
		editTextpwd.addTextChangedListener(this);

		this.findViewById(R.id.button_login).setOnClickListener(this);
		this.findViewById(R.id.button_register).setOnClickListener(this);
		//editTextname.setText("12345678901");
		//editTextpwd.setText("123456");

	}
	
 	
	  private void SaveorShowPass(boolean isSave)
      {
          if (!isSave)//读取
          {
              sharedPreferences = getSharedPreferences("userName", Activity.MODE_PRIVATE);//MODE_PRIVATE
              if (sharedPreferences.getBoolean("isRemember", false))
              {
            	  editTextname.setText(sharedPreferences.getString("userName", ""));
            	  editTextpwd.setText(sharedPreferences.getString("password", ""));
            	  isRemember.setChecked(true);//.Checked = (true);
              }
          }
          else //保存
          {
        	  boolean isRem = isRemember.isChecked();//.Checked;

              editor = sharedPreferences.edit();
              editor.putString("userName", isRem ? editTextname.getText().toString(): "");
              editor.putString("password", isRem ? editTextpwd.getText().toString() : "");
              editor.putBoolean("isRemember", isRem);
              editor.commit();
          }
      }


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.back:
			this.finish();
			break;
		case R.id.button_account_clear:
			this.editTextname.setText("");
			break;
		case R.id.button_pwd_clear:
			this.editTextpwd.setText("");
			break;
		case R.id.button_login:
			// Share();
			/*Intent it2 = new Intent(this, com.zrb.chat.activity.LoginActivity.class);
			startActivity(it2);*/
			//user_Login();
			break;
		case R.id.button_register:
 		 	//Intent it = new Intent(this, User_registerStep1.class);
  			//startActivity(it);
			break;
		}

	}

 
	private void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在登录中...", true,
					true);
		else
			progTip.Show("正在登录中...");
	}

	private void user_Login() {
		if (TextUtils.isEmpty(this.editTextname.getText())) {
			CommonUtil.showToast("用户名不能为空", this);
			return;
		}
		if (TextUtils.isEmpty(this.editTextpwd.getText())) {
			CommonUtil.showToast("密码不能为空", this);
			return;
		}

		showloading();
		String loginurlparms = "?name=" + this.editTextname.getText() + "&pwd="+ this.editTextpwd.getText();
		ZrbRestClient.get("login.jspx" + loginurlparms, null,
				new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						// TODO Auto-generated method stub
						CommonUtil.showToast("fail", oldUser_loginActivity.this);
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						progTip.Dismiss();
						String results = new String(arg2);

						try {

							Gson gson = new Gson();
							UserInfoResult tmpDto = gson.fromJson(results,UserInfoResult.class);
							if (tmpDto.result)  
								showLogin();
 						    else
 								CommonUtil.showToast( "fail:"+tmpDto.msg,oldUser_loginActivity.this);
 
						} catch (Exception e) {

							CommonUtil.showToast( "fail:"+e.getMessage(),oldUser_loginActivity.this);
							System.out.println("eeee!!!!" + e.getMessage());

						}

					}
				});

	}

	private void showLogin()
	{
		SaveorShowPass(true);
 		CommonUtil.showToast("ok",	oldUser_loginActivity.this);
  	
		 Intent it = new Intent(oldUser_loginActivity.this, oldUser_info_Activity.class);
		 startActivity(it); 
 	}
	
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.edittext_account) {
			if (hasFocus && editTextname.getText().length() != 0)
				btnAccountclear.setVisibility(View.VISIBLE);
			else
				btnAccountclear.setVisibility(View.INVISIBLE);

		} else if (v.getId() == R.id.edittext_password) {
			if (hasFocus && editTextpwd.getText().length() != 0)
				btnPwdclear.setVisibility(View.VISIBLE);
			else
				btnPwdclear.setVisibility(View.INVISIBLE);

		}

	}

	private TextWatcher onUserNameTextChanged = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editTextname.getText().length() == 0)
				btnAccountclear.setVisibility(View.INVISIBLE);
			else
				btnAccountclear.setVisibility(View.VISIBLE);
		}

	};

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		if (editTextpwd.getText().length() == 0)
			btnPwdclear.setVisibility(View.INVISIBLE);
		else
			btnPwdclear.setVisibility(View.VISIBLE);
	}
 	 
}
