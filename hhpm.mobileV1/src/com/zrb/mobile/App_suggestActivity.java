package com.zrb.mobile;

 

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.adapter.model.UserDtos;
import com.zrb.mobile.ui.NumEditText;
import com.zrb.mobile.ui.NumEditText.OnNumslistener;
 
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

public class App_suggestActivity  extends BaseActivity  implements OnNumslistener,OnClickListener  {

	private EditText editText2;
	private TextView txtNum,right_title;
	NumEditText numEditText;
	private Button submit_btn;
    Handler mhandler = new Handler();
    Tip progTip;
    Dialog AuthDialog;
  //Toast提示框
  	private RelativeLayout notify_view;
  	private TextView notify_view_text;
    
 	@Override
	protected void onCreate(Bundle savedInstanceState) {
		  super.onCreate(savedInstanceState);
 	      setContentView(R.layout.app_setting_suggest);
	 	  InitView();
	}
	
	private void InitView(){
				
		//Toast提示框
		notify_view = (RelativeLayout)this.findViewById(R.id.notify_view);
		notify_view_text = (TextView)this.findViewById(R.id.notify_view_text);
				
		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("意见反馈");
		
		right_title = (TextView) this.findViewById(R.id.right_title);
		right_title.setOnClickListener(this);
		txtNum = (TextView) this.findViewById(R.id.txtNum);
		numEditText = (NumEditText) this.findViewById(R.id.editText1);
		numEditText.setMaxNums(200);
		numEditText.setOnNumslistener(this);
		editText2 = (EditText) this.findViewById(R.id.editText2);
		if( null == AppSetting.curUser){
			editText2.setVisibility(View.VISIBLE);
		}else{
			editText2.setVisibility(View.GONE);
		}
		submit_btn = (Button) this.findViewById(R.id.submit_btn);
		submit_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submit_btn.setEnabled(false);
				 new Handler().postDelayed(new Runnable() {
		             @Override
		             public void run() {
		            	 submit_btn.setEnabled(true);
		             }

		     }, 2000);
				String contents = numEditText.getText().toString();
				String contact = editText2.getText().toString();
				
				if (TextUtils.isEmpty(contents)) {
					initNotify("反馈内容不能为空");
					return;
				}
				
				if( null == AppSetting.curUser  && TextUtils.isEmpty(contact)){
					initNotify("联系方式不能为空");
					return;
				}
				
				if( null == AppSetting.curUser ){
					if( !CommonUtil.isMobileNO(contact)&&!CommonUtil.isEmail(contact)){
						initNotify("请填写正确的手机号码或邮箱地址");
						return;
					}else{
						submit_info(contact, contents);
					}
				}else{
					contact = AppSetting.curUser.zrb_front_ut;
					submit_info(contact, contents);
				}
			}
		});
		
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
	}

	@Override
	public void showtextNum(int number) {
		txtNum.setText(number+"");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.right_title:
			showAuthentication();
			break;
		}
 	}

	
	private void submit_info(String contact,String contents){
//		showloading();
		RequestParams param = new RequestParams();
		param.put("contact", contact);
		param.put("contents", contents);
		ZrbRestClient.post("feedbackApi/insertDate" , param,
						new AsyncHttpResponseHandler() {

							@Override
							public void onFailure(int arg0, Header[] arg1, byte[] arg2,
									Throwable arg3) {
								CommonUtil.showToast("网络超时,请重新连接", App_suggestActivity.this);
							}

							@Override
							public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
								String results = new String(arg2);
								try {
									Gson gson = new Gson();
									UserDtos tmpDto = gson.fromJson(results,UserDtos.class);
									if(tmpDto.res==1){
										CommonUtil.showToast(tmpDto.msg, App_suggestActivity.this);
										 new Handler().postDelayed(new Runnable() {
								             @Override
								             public void run() {
								            	 App_suggestActivity.this.finish();
								             }

								     }, 2000);
 	 								}else{
 	 									CommonUtil.showToast(tmpDto.msg, App_suggestActivity.this);
 								   }
							} catch (Exception e) {

									CommonUtil.showToast( "fail:"+e.getMessage(),App_suggestActivity.this);
									System.out.println("eeee!!!!" + e.getMessage());

								}

							}
						});
	}
	
//	private void showloading() {
//		if (progTip == null)
//			progTip = ProgressDialogEx.Show(this, " title", "正在提交中...", true,
//					true);
//		else
//			progTip.Show("正在提交中...");
//	}
	
//	private void initNotify(final String msg) {
//		mhandler.postDelayed(new Runnable() {
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				notify_view_text.setText(msg);
//				notify_view.setVisibility(View.VISIBLE);
//				mhandler.postDelayed(new Runnable() {
//					
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						notify_view.setVisibility(View.GONE);
//					}
//				}, 2000);
//			}
//		}, 1000);
//	}
	
	
	public void showAuthentication()
	{
		DialogBuilder builder2 = new DialogBuilder(App_suggestActivity.this);
        //builder2.setTheme(Resource.Style.customDialog);
		builder2.setParentlayout(R.layout.custom_ucenter_dialog);
        builder2.setContentView(R.layout.u_center_org_auth); //ac_dialog_cancel; 
        builder2.setPositiveTitle("呼叫");
    	TextView hitetxt= (TextView)builder2.getCurContentView().findViewById(R.id.tv_authstr);
    	String msource=this.getResources().getString(R.string.user_center_profile_edit_org_authtrue1);
    	hitetxt.setText(msource);
            
        builder2.setCloseOnclick(new OnClickListener(){
				@Override
				public void onClick(View arg0) {
					AuthDialog.dismiss();
					if(arg0.getId()==R.id.btn_confirm){
 						   Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +Constants.serverPhone));
 						  App_suggestActivity.this.startActivity(intent);
					 }
					}
		         }); 
        AuthDialog = builder2.create();
        AuthDialog.show();
		
	}
	
	/* 初始化通知栏目*/
	private void initNotify(final String msg) {
		mhandler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				notify_view_text.setText(msg);
				notify_view.setVisibility(View.VISIBLE);
				mhandler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						notify_view.setVisibility(View.GONE);
					}
				}, 2000);
			}
		}, 1000);
	}
}
