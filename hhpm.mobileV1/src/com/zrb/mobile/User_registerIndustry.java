package com.zrb.mobile;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.adapter.IndustrylistItemAdapter;
import com.zrb.mobile.adapter.model.RegisterIndustryDto;
import com.zrb.mobile.adapter.model.RegisterIndustryDtos;
import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class User_registerIndustry extends BaseActivity implements OnClickListener   {

	public boolean isProject;
	TextView txtTitle;
 	boolean isfinish=false;
	ImageView successImg;
	ListView mindustrylist;
	IndustrylistItemAdapter mindustryAdapter;
	Tip progTip;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
 
		setContentView(R.layout.industry_choose_activity);
 		 initView();
 		 LoadData();
  	}
 	
 	
	private void initView() {
		isProject=getIntent().getBooleanExtra("isProject", false);
		this.findViewById(R.id.title_left_root).setOnClickListener(this);
		
		txtTitle=(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("行业");
 		mindustrylist=(ListView)this.findViewById(R.id.industrylist);
 		mindustrylist.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				RegisterIndustryDto tmpDto=  mindustryAdapter.getCurItem(arg2);
				Intent intent =new Intent();
	            intent.putExtra("name", tmpDto.industryName);
				intent.putExtra("code", tmpDto.id);
	       
				User_registerIndustry.this.setResult(android.app.Activity.RESULT_OK, intent);
	        	User_registerIndustry.this.finish();
			}});
		
 	}

	private void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在加载中...", true,
					true);
		else
			progTip.Show("正在加载中...");
	}
	
	private void  LoadData(){
		 showloading() ;
		 RequestParams param = new RequestParams();
		 Log.i("ismoney", isProject+"");
		 if(isProject){
			 param.put("type", Constants.DiscoverProjectIntent); 
		 }
 	  	 ZrbRestClient.post("industryApi/findAllIndustry", param, new AsyncHttpResponseHandler() {

				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,Throwable arg3) {
					// TODO Auto-generated method stub
					progTip.Dismiss();
					//initNotify(User_registerIndustry.this.getString(R.string.error_net_loadfail));
	 			}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
					progTip.Dismiss();
					String results = new String(arg2);
	 				try {
	 						Gson gson = new Gson();
	 						RegisterIndustryDtos tmpDto = gson.fromJson(results,	RegisterIndustryDtos.class);
	 						if(tmpDto.res==1){
	 							mindustryAdapter=new IndustrylistItemAdapter(User_registerIndustry.this,tmpDto.data);
	 							mindustryAdapter.setProject(isProject);
	 							mindustrylist.setAdapter(mindustryAdapter);
	 						}
	 						else
	 							initNotify(tmpDto.msg);
	  				    } catch (Exception e) {
	  					    initNotify(User_registerIndustry.this.getString(R.string.error_serverdata_loadfail));
	 				}
	 			}
			});
		
	}
	
	private void initNotify(final String msg){
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.btn_investor://1 ��ҵ
			Intent it2 = new Intent(User_registerIndustry.this, User_registerBase1.class);
			 
			 startActivity(it2);
			break;
		case R.id.btn_government:
		/*	Intent it3 = new Intent(User_registerIndustry.this, User_registerphone.class);
			 
			startActivity(it3);*/
			break;
			
		case R.id.lience_down:
			//OnshowChanged();
 		    Intent it = new Intent(User_registerIndustry.this, User_register_lience.class);
			 startActivity(it);
 			break;
		 case	R.id.finish_btn:
			 Intent it4 = new Intent(User_registerIndustry.this, User_phone_login.class);
			 it4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		      startActivity(it4); 
		     this.finish();
			break;
		}

	}

	 

}
