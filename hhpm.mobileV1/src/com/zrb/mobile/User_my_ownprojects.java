package com.zrb.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import com.zrb.mobile.BaseFragmentActivity;
import com.zrb.mobile.R;
 
import com.zrb.mobile.adapter.UserProjectlist_ownsAdapter;
import com.zrb.mobile.adapter.UsermyPublishAdapter;

import com.zrb.mobile.adapter.model.FindProjectDto;
import com.zrb.mobile.adapter.model.OwnProjectDto;
import com.zrb.mobile.adapter.model.OwnProjectDtos;
import com.zrb.mobile.adapter.model.UserPublishDto;

import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
import com.zrb.mobile.ucenter.User_OwnProjectFragment;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.ui.fragment.IpostDelayed;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.FragmentTabAdapter2;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.os.Bundle;
import android.os.Handler;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class User_my_ownprojects extends BaseFragmentActivity implements IpostDelayed, OnClickListener {
	// public String ACTION_NAME_SENVEN = "user_my_ownprojects";

	Handler myhandler = new Handler();
	FragmentTabAdapter2 tabAdapter;
	private List<Fragment> fragments = new ArrayList<Fragment>();
	private RadioButton seltextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.u_center_attention);
 		initView();
 	}

	private void initView() {

		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);

		checkListener checkradio = new checkListener();

		seltextView = (RadioButton) this.findViewById(R.id.mymsg_chk0);
		seltextView.setOnClickListener(checkradio);

		this.findViewById(R.id.sysmsg_chk1).setOnClickListener(checkradio);

		fragments.add(User_OwnProjectFragment.getNewFrament(Constants.MatchingFund));
		fragments.add(User_OwnProjectFragment.getNewFrament(Constants.MatchingProject));
		tabAdapter = new FragmentTabAdapter2(this, fragments, R.id.tab_content);
	}

	public class checkListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (seltextView.getId() == v.getId())
				return;

			if (v.getId() != -1) {
				((RadioButton) v).setChecked(true);
				seltextView.setChecked(false);
				seltextView = (RadioButton) v;
			}
			switch (v.getId()) {
			case R.id.mymsg_chk0:
				tabAdapter.setCheckChanged(0);
				break;
			case R.id.sysmsg_chk1:
				tabAdapter.setCheckChanged(1);
				break;
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		}
	}

	@Override
	public void postDelayed(Runnable runnable, long delay) {
		// TODO Auto-generated method stub
		myhandler.postDelayed(runnable, delay);
	}

	/*
	 * @Override public void onItemClick(AdapterView<?> parent, View view, int
	 * position, long id) {
	 * 
	 * OwnProjectDto tmpDto = myprojectlistAdapter.CurrentDatas .get(position -
	 * 1); Intent intent = new Intent(User_my_ownprojects.this,
	 * ProjectDetailActivity.class); intent.putExtra("id", tmpDto.id);
	 * intent.putExtra("title", tmpDto.title); intent.putExtra("position",
	 * position - 1); intent.putExtra("from", "3"); startActivity(intent); }
	 */

	/*
	 * public void registerBoradcastReceiver1() { IntentFilter myIntentFilter =
	 * new IntentFilter();
	 * myIntentFilter.addAction(Constants.FocusNum_MYPROJECT_mainlist); // 注册广播
	 * registerReceiver(mBroadcastReceiver1, myIntentFilter); }
	 * 
	 * private BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
	 * 
	 * @Override public void onReceive(Context context, Intent intent) { String
	 * action = intent.getAction(); if
	 * (action.equals(Constants.FocusNum_MYPROJECT_mainlist)) { String from =
	 * intent.getStringExtra("from"); int position =
	 * intent.getIntExtra("position", -1);
	 * 
	 * if ("1".equals(from)) { int attention = intent.getIntExtra("attention",
	 * -1); Log.i("from", from); Log.i("attention", attention + "");
	 * Log.i("position", position + ""); if (attention != -1) { if (attention ==
	 * 0) { // 取消关注 CanFocus(position); } else { // 添加关注 AddFocus(position); } }
	 * 
	 * } } } };
	 */
	/*
	 * // 取消关注效果 protected void CanFocus(int position) { // TODO Auto-generated
	 * method stub myprojectlistAdapter.CurrentDatas.get(position).collectNum =
	 * myprojectlistAdapter.CurrentDatas .get(position).collectNum - 1;
	 * Log.i("collectNum--",
	 * myprojectlistAdapter.CurrentDatas.get(position).collectNum + "");
	 * myprojectlistAdapter.notifyDataSetChanged(); }
	 * 
	 * // 添加关注效果 protected void AddFocus(int position) { // TODO Auto-generated
	 * method stub myprojectlistAdapter.CurrentDatas.get(position).collectNum =
	 * myprojectlistAdapter.CurrentDatas .get(position).collectNum + 1;
	 * Log.i("collectNum++",
	 * myprojectlistAdapter.CurrentDatas.get(position).collectNum + "");
	 * myprojectlistAdapter.notifyDataSetChanged(); }
	 */

	/*
	 * 
	 * 
	 * 广播接收者的注册和接收
	 */
	/*
	 * @Override public void onDestroy() { // TODO Auto-generated method stub
	 * super.onDestroy(); // 销毁广播 unregisterReceiver(mBroadcastReceiver1); }
	 */

}
