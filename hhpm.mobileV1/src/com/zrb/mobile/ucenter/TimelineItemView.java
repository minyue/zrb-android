package com.zrb.mobile.ucenter;

import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
 

import com.hhpm.lib.support.inject.ViewInject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.applib.utils.AppSetting;
import com.zrb.matching.MatchingActivity;
import com.zrb.mobile.R;
 
import com.zrb.mobile.adapter.model.OwnProjectDto;
import com.zrb.mobile.ui.fragment.ABaseAdapter;
import com.zrb.mobile.ui.fragment.ABaseFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;

/**
 * Created by wangdan on 15/4/15.
 */
public class TimelineItemView extends ABaseAdapter.AbstractItemView<OwnProjectDto>
                implements View.OnClickListener  {

    protected ABaseFragment fragment;
   // protected BizFragment bizFragment;

    private boolean showRetweeted;
    private OwnProjectDto reStatus;
    private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");//yyyy.
    DisplayImageOptions options;
    
    
    public TimelineItemView(ABaseFragment fragment, boolean showRetweeted) {
        this(fragment, null, showRetweeted);
    }

    // 2014-08-24 新增这个构造方法，解决转发列表，点击转发菜单时，没有带上原微博的BUG
    public TimelineItemView(ABaseFragment fragment, OwnProjectDto reStatue, boolean showRetweeted) {
        this();

        this.fragment = fragment;
        this.reStatus = reStatue;
 
        this.showRetweeted = showRetweeted;
    }

    private TimelineItemView() {
 	     options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.loadpic)
		.showImageForEmptyUri(R.drawable.loadpic)
		.showImageOnFail(R.drawable.loadpic).cacheInMemory(false)
	//	.displayer(new FadeInBitmapDisplayer(300))
		.cacheOnDisc(true)
		.build();
    }

    @ViewInject(id = R.id.id_project_title)
    TextView txtTitle;
    
    @ViewInject(id = R.id.id_projectsImg)
    ImageView imgPhoto;
  
    @ViewInject(id = R.id.id_projectstype)
    ImageView projectType;
     
    @ViewInject(id = R.id.project_attention_txt)
    TextView attentiontxt;

    @ViewInject(id = R.id.project_comment_txt)
    TextView commenttxt;
    
    @ViewInject(id = R.id.project_date_txt)
    TextView publishDate;
    
    @ViewInject(id = R.id.project_match_txt)
    TextView matchtxt;
  
    private int vPadding;
 
    private float textSize;

    @Override
    public int inflateViewId() {
        return R.layout.project_owns_list_item;
    }

    @Override
    public void bindingData(View convertView, OwnProjectDto curDto) {
    		
    	txtTitle.setText(curDto.title);
 		attentiontxt.setText(""+curDto.collectNum);
     	commenttxt.setText(""+curDto.queryNum);
  		publishDate.setText(CommonUtil.formatlongDate(curDto.pushTime,sdf)); 
  	 
	 	if(!TextUtils.isEmpty(curDto.pic))
	 		ImageLoader.getInstance().displayImage(AppSetting.mediaServer+curDto.pic, imgPhoto, options);  
	 	else
	 		imgPhoto.setImageResource(R.drawable.loadpic); 
	 	
	 	matchtxt.setTag(curDto);
	  	matchtxt.setText("匹配资金");
	  	matchtxt.setOnClickListener(this);
        matchtxt.setSelected(true);
     	
    }
 
    @Override
    public void onClick(View v) {
    	switch(v.getId() ){
    	case R.id.project_match_txt:
     		 final OwnProjectDto status = (OwnProjectDto) v.getTag();
     		 MatchingActivity.launch(fragment.getActivity(), Constants.MatchingFund, (int)status.id);
     		 break;
     	}
      
    }

    /*@Override
    public void onLikeRefreshUI() {
        if (fragment != null && fragment instanceof ARefreshFragment)
            ((ARefreshFragment) fragment).refreshUI();
    }

    @Override
    public void onLikeRefreshView(StatusContent data, final View likeView) {
        if (fragment.getActivity() == null)
            return;

        if (likeView.getTag() == data) {
            animScale(likeView);
        }
    }*/

    protected void animScale(final View likeView) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(200);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.start();
        likeView.startAnimation(scaleAnimation);
        likeView.postDelayed(new Runnable() {

            @Override
            public void run() {
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.5f, 1.0f, 1.5f, 1.0f,
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                scaleAnimation.setDuration(200);
                scaleAnimation.setFillAfter(true);
                likeView.startAnimation(scaleAnimation);
            }

        }, 200);
    }

}
