package com.zrb.mobile.ucenter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.hhpm.lib.support.inject.ViewInject;
import com.zrb.mobile.ProjectDetailActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.AttentionUserDto;
import com.zrb.mobile.adapter.model.OwnProjectDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.ui.fragment.ABaseAdapter.AbstractItemView;
import com.zrb.mobile.ui.fragment.ARefreshFragment;
import com.zrb.mobile.utility.CommonUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Attention_User_Fragment extends ARefreshFragment<AttentionUserDto> implements onLoadMoreListener   {
	 
	public static final int UserAttention=1;
	public static final int FansAttention=2;
	
	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    int OwnsType=-1;
	boolean isFirstIn = true;
	TypeToken<List<AttentionUserDto>> curType=new TypeToken<List<AttentionUserDto>>(){};
 	
    @ViewInject(idStr = "discover_list_view")
    PullToRefreshListViewEx mListView;
    String baseurl1="userfollow/followingListByUpdateTime?limit=10";
    String baseurl2="userfollow/followedListByUpdateTime?limit=10";
    
    private  String getCurUrl(){
    	
    	if(OwnsType==UserAttention) 
    		return baseurl1;
    	else
    		return baseurl2;
    }
    
	public static Attention_User_Fragment getNewFrament(int type) {

		Attention_User_Fragment tmp = new Attention_User_Fragment();
		tmp.OwnsType = type;
		return tmp;
	}
  
	@Override
	  protected void layoutInit(LayoutInflater inflater, Bundle savedInstanceSate) {
		mListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(CommonUtil.getcurTime());
  				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
 						LoadData(getCurUrl()+"&updateTime="+CommonUtil.formateDate(new Date()),RefreshMode.refresh,curType);
					}
				}, 1000);
			}
		});
		
		mListView.setOnLoadMoreListener(this);
	 }
	
	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}

	private void firstLoad() {
		if (isFirstIn) {
			isFirstIn = false;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					mListView.setRefreshing(true);
				}
			}, 200); 
		}
	}
	

	 
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		AttentionUserDto tmpDto =getAdapterItems().get(position-1);;
	/*	  Intent intent = new Intent(Attention_User_Fragment.this.getActivity(), ProjectDetailActivity.class);
		  intent.putExtra("id", tmpDto.id);
		  intent.putExtra("title", tmpDto.title);
		  startActivity(intent); */
 	} 
 	 
 
	@Override
	protected AbstractItemView<AttentionUserDto> newItemView() {
		// TODO Auto-generated method stub
		if(OwnsType==UserAttention)
		  return new TimelineAttention_UserItemView(this);
		else
			  return new TimelineAttention_FansItemView(this);	
	}

	@Override
	public PullToRefreshListViewEx getRefreshView() {
		// TODO Auto-generated method stub
		return mListView;
	}

	@Override
	public void onRefreshViewComplete() {
		// TODO Auto-generated method stub
		//CommonUtil.showToast("onRefreshViewComplete");
	}

	@Override
	public void onHandlerResult(String resStr,RefreshMode curMode) {
		// TODO Auto-generated method stub
 	}

	@Override
	protected int inflateContentView() {
		// TODO Auto-generated method stub
		return R.layout.common_refreshlist_layout2;
	}

	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				 
				// Type listType = new TypeToken<List<OwnProjectDto>>(){}.getType();
				LoadData(getCurUrl()+"&updateTime="+CommonUtil.formatlongDate(getLastItem().updateTime,dateformat),RefreshMode.loadmore,curType);
				//LoadData(myprojectlistAdapter.getItem(myprojectlistAdapter.getCount()-1).getMinDate(), false);
			}
		}, 300);
	}


	 
	 
 
}
