package com.zrb.mobile.ucenter;

import com.zrb.mobile.BaseFragmentActivity;
import com.zrb.mobile.R;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class User_my_dynamic extends BaseFragmentActivity implements OnClickListener  {
 
 	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    	setContentView(R.layout.common_fragment_container);
		// setContentView(R.layout.user_register_activity);fragment_container
		 //initView();
		initView();
 	}
  	
     private void initView(){
    	 
   		View lefticon= this.findViewById(R.id.title_left_root);
 		lefticon.setOnClickListener(this);
 		((TextView) this.findViewById(R.id.txt_title)).setText(R.string.user_menu_dynamic);
 		
 		FragmentManager fragmentManager =this.getSupportFragmentManager();

		FragmentTransaction transaction = fragmentManager.beginTransaction(); 
  		transaction.add(R.id.tab_content,User_myDynamictFragment.getNewFrament(1));  
		//transaction.addToBackStack(null); 
		transaction.commit();  
     }
	 

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		}
	}
	 

}
