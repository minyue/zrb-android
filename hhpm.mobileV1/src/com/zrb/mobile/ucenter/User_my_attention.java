package com.zrb.mobile.ucenter;

import java.util.ArrayList;
import java.util.List;

 
import com.zrb.mobile.BaseFragmentActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.FragmentTabAdapter2;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

public class User_my_attention extends BaseFragmentActivity implements OnClickListener  {

	FragmentTabAdapter2 tabAdapter;
	private List<Fragment> fragments = new ArrayList<Fragment>();
	private RadioButton seltextView;
 
    Tip	progTip ;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	 
		setContentView(R.layout.u_center_attention);
		// setContentView(R.layout.user_register_activity);fragment_container
		 //initView();
		initView();
 	}
 //http://172.17.32.253:8090/userStatisticDetail/getCollectFunds?limit=10&createTime=2015-12-23%2023:59:59&zrb_front_ut=f5e6efd8db15873bd1273c4f540a0c1c
	public void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在加载中...", true,
					true);
		else
			progTip.Show("正在加载中...");
	}
	
	public void hideloading(){
		
		if (progTip != null) progTip.Dismiss();
	}
	
     private void initView(){
    	 
   		View lefticon= this.findViewById(R.id.title_left_root);
 		lefticon.setOnClickListener(this);
    	 
 		 checkListener checkradio = new checkListener();
 		 
 		 
 		 seltextView=(RadioButton)this.findViewById(R.id.mymsg_chk0);
 	  	 seltextView.setOnClickListener(checkradio);
 	     seltextView.setText(R.string.user_menu_myattention);
 	  	 
 	    RadioButton seltextView2=(RadioButton)this.findViewById(R.id.sysmsg_chk1);
 	    seltextView2.setText(R.string.user_menu_myfans);
 	    seltextView2.setOnClickListener(checkradio);
	 
 	  
 		 fragments.add(Attention_User_Fragment.getNewFrament(Attention_User_Fragment.UserAttention));
    	 fragments.add(Attention_User_Fragment.getNewFrament(Attention_User_Fragment.FansAttention));
  		 tabAdapter = new FragmentTabAdapter2(this, fragments, R.id.tab_content);
     }
	 
     public class checkListener implements  View.OnClickListener{
	    	@Override
	    	public void onClick(View v) {
	    		// TODO Auto-generated method stub
	    		if(seltextView.getId()==v.getId()) return;
	    		
	 			if (v.getId() != -1) {
					((RadioButton)v).setChecked(true);
					seltextView.setChecked(false);
					seltextView = (RadioButton)v;
				}
	    		switch(v.getId()){
    		       case R.id.mymsg_chk0:
	    			tabAdapter.setCheckChanged(0);
  	    			break;
    		   	case R.id.sysmsg_chk1:
 	    			tabAdapter.setCheckChanged(1);
 	    			break;
   	    		}
	    	}
	    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		}
	}
	 

}
