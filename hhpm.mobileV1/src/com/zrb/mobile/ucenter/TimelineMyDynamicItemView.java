package com.zrb.mobile.ucenter;

import android.graphics.Color;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
 

import com.hhpm.lib.support.inject.ViewInject;
 
import com.zrb.discover.widget.TimelineView2;
 
import com.zrb.matching.MatchingActivity;
import com.zrb.mobile.MoneyDetailActivity;
import com.zrb.mobile.ProjectDetailActivity;
import com.zrb.mobile.R;
 
import com.zrb.mobile.adapter.model.Dynamic_CardDto;
 
import com.zrb.mobile.ui.fragment.ABaseAdapter;
import com.zrb.mobile.ui.fragment.ABaseFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;

/**
 * Created by wangdan on 15/4/15.
 */
public class TimelineMyDynamicItemView extends ABaseAdapter.AbstractItemView<Dynamic_CardDto>
                implements View.OnClickListener  {

    protected ABaseFragment fragment;
    
    private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");//yyyy.
    int FundColor=Color.parseColor("#b68f85");
	int ProjectColor=Color.parseColor("#8ea0be");
   
    public TimelineMyDynamicItemView(ABaseFragment fragment ) {
         this.fragment = fragment;

  
    }

  
    
   /* if (convertView == null) {
		convertView = mInflater.inflate(R.layout.user_fund_item, null);
		holder = new ViewHolder();
		holder.tv_message_title = (TextView) convertView.findViewById(R.id.tv_message_title);
		holder.investmentStyle = (TextView) convertView.findViewById(R.id.investment_amount);
		holder.investmentIndustry = (TextView) convertView.findViewById(R.id.investment_industry);
		holder.investmentArea = (TextView) convertView.findViewById(R.id.investment_area);

		holder.tv_find_authtype = (TextView) convertView.findViewById(R.id.tv_find_authtype);
		holder.attention = (TextView) convertView.findViewById(R.id.fund_attention);
		holder.reply = (TextView) convertView.findViewById(R.id.fund_reply);
		holder.share =   convertView.findViewById(R.id.share);
		
		
		holder.certification = (ImageView) convertView.findViewById(R.id.certification);
		holder.contentbar = (LinearLayout) convertView.findViewById(R.id.contentbar);
		initClickListener(holder);
		convertView.setTag(holder);
	} else {
		holder = (ViewHolder) convertView.getTag();
	}*/

    @ViewInject(id = R.id.contentbar)
    View contentlayout;
    
    
    @ViewInject(id = R.id.timelayout)
    View timelayout;
    
    @ViewInject(id = R.id.timelines)
    TimelineView2 lineView;
    
    @ViewInject(id = R.id.timelinetxt)
    TextView timelinetxt;
 
    @ViewInject(id = R.id.tv_dynamic_title_tv)
    TextView dynamic_title;

    @ViewInject(id = R.id.tv_dynamic_content)
    TextView dynamic_content;
    
    @ViewInject(id = R.id.tv_dynamic_edit)
    TextView dynamic_edit;
    
    @ViewInject(id = R.id.tv_dynamic_match)
    TextView dynamic_match;

    @Override
    public int inflateViewId() {
        return R.layout.usercard_mydynamic_item;
    }
   
    @Override
    public void bindingData(View convertView, Dynamic_CardDto curDto) {
    		
    	if(curDto.showTime) 
    		timelayout.setVisibility(View.VISIBLE);
    	else
    	    timelayout.setVisibility(View.GONE); 
    	
    	timelinetxt.setText(CommonUtil.formatlongDate(curDto.createTime, sdf));
     	dynamic_content.setText(curDto.title); 
		switch(curDto.type){
		case Constants.Publish_Fund:
			dynamic_title.setText("发布了一条"+Constants.FLAG+"投资意向");
			dynamic_title.setTextColor(FundColor);
			break;
		case Constants.Publish_Project:
		 	dynamic_title.setText("发布了一条"+Constants.FLAG+"项目意向");
		 	dynamic_title.setTextColor(ProjectColor);
			break;
		case Constants.Choice_Fund:
			dynamic_title.setText("入选"+Constants.FLAG+"资金精选");
			dynamic_title.setTextColor(FundColor);
			break;
		case Constants.Choice_Project:
			dynamic_title.setText("入选"+Constants.FLAG+"项目精选");
			dynamic_title.setTextColor(ProjectColor);
			break;
		
		}
		
		boolean isChoice=curDto.type==Constants.Choice_Project||curDto.type==Constants.Choice_Fund;
  		dynamic_edit.setVisibility(isChoice ?View.VISIBLE:View.GONE);
		dynamic_edit.setTag(curDto);
		dynamic_edit.setOnClickListener(this);
		if(isChoice){
			dynamic_edit.setText(curDto.type==Constants.Choice_Project? "项目详情":"资金详情");
			
		}
		
		
		
		boolean isProject=curDto.type==Constants.Choice_Project||curDto.type==Constants.Publish_Project;
		if (isProject) {
			contentlayout.setBackgroundResource(R.drawable.bg_business_card_project);
			dynamic_match.setText("资金匹配");
		} else {
			contentlayout.setBackgroundResource(R.drawable.bg_business_card_fund);
			dynamic_match.setText("项目匹配");
		}
		dynamic_match.setTag(curDto);
		dynamic_match.setOnClickListener(this);
    }
 
    @Override
    public void onClick(View v) {
    	switch(v.getId() ){
    	case R.id.tv_dynamic_edit:
    		 
     		 final Dynamic_CardDto status = (Dynamic_CardDto) v.getTag();
     		  if(status.type==Constants.Choice_Project)
				  ProjectDetailActivity.launch(fragment.getActivity(), Long.parseLong(status.target), status.title);
				else
				  MoneyDetailActivity.launch(fragment.getActivity(), Long.parseLong(status.target), status.title);
     		 break;
    	case R.id.tv_dynamic_match:
   		 
    		 final Dynamic_CardDto status2 = (Dynamic_CardDto) v.getTag();
    		 boolean isProject=status2.type==Constants.Choice_Project||status2.type==Constants.Publish_Project;
     		 MatchingActivity.launch(fragment.getActivity(),isProject?MatchingActivity.MatchFund:MatchingActivity.MatchProject,Integer.parseInt(status2.target) );
			 
    		 break;
     	}
      
    }

    /*@Override
    public void onLikeRefreshUI() {
        if (fragment != null && fragment instanceof ARefreshFragment)
            ((ARefreshFragment) fragment).refreshUI();
    }

    @Override
    public void onLikeRefreshView(StatusContent data, final View likeView) {
        if (fragment.getActivity() == null)
            return;

        if (likeView.getTag() == data) {
            animScale(likeView);
        }
    }*/

 
	

}
