package com.zrb.mobile.ucenter;

import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.hhpm.lib.support.inject.ViewInject;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.Dynamic_CardDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.ui.fragment.ABaseAdapter.AbstractItemView;
import com.zrb.mobile.ui.fragment.ARefreshFragment.RefreshMode;
import com.zrb.mobile.ui.fragment.ARefreshFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.TimeUtil;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

 

public class User_myDynamictFragment extends ARefreshFragment<Dynamic_CardDto> implements onLoadMoreListener   {
	 
	 
    int OwnsType=-1;
	boolean isFirstIn = true;
	TypeToken<List<Dynamic_CardDto>> curType=new TypeToken<List<Dynamic_CardDto>>(){};
 	
    @ViewInject(idStr = "discover_list_view")
    PullToRefreshListViewEx mListView;
    
    String baseurl="dynamic/queryMyDynamic?1=1";
    
	public static User_myDynamictFragment getNewFrament(int type) {

		User_myDynamictFragment tmp = new User_myDynamictFragment();
		tmp.OwnsType = type;
		return tmp;
	}
  
	@Override
	  protected void layoutInit(LayoutInflater inflater, Bundle savedInstanceSate) {
		mListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(CommonUtil.getcurTime());
  				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
 						LoadData(baseurl,RefreshMode.refresh,curType);
					}
				}, 1000);
			}
		});
		mListView.setPagesize(1000000);
		mListView.setOnLoadMoreListener(this);
	 }
	
	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}

	private void firstLoad() {
		if (isFirstIn) {
			isFirstIn = false;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					mListView.setRefreshing(true);
				}
			}, 200); 
		}
	}
	

	 
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
 	/*	  OwnProjectDto tmpDto =getAdapterItems().get(position-1);;
		  Intent intent = new Intent(User_OwnProjectFragment2.this.getActivity(), ProjectDetailActivity.class);
		  intent.putExtra("id", tmpDto.id);
		  intent.putExtra("title", tmpDto.title);
		  startActivity(intent); */
 	} 
 	 
 
	@Override
	protected AbstractItemView<Dynamic_CardDto> newItemView() {
		// TODO Auto-generated method stub
		return new TimelineMyDynamicItemView(this);
	}

	@Override
	public PullToRefreshListViewEx getRefreshView() {
		// TODO Auto-generated method stub
		return mListView;
	}

	@Override
	public void onRefreshViewComplete() {
		// TODO Auto-generated method stub
		//CommonUtil.showToast("onRefreshViewComplete");
	}

	@Override
	public void onHandlerResult(String resStr,RefreshMode curMode) {
		// TODO Auto-generated method stub
 	}

	@Override
	protected int inflateContentView() {
		// TODO Auto-generated method stub
		return R.layout.common_refreshlist_layout;
	}

	long mTempTime=0;
	@Override
	public void addItems(List<Dynamic_CardDto> curDtos,boolean isAppend) {
		 
		for (int index = 0; index < curDtos.size(); index++) {
			Dynamic_CardDto tempDiscoverDto = curDtos.get(index);
  			long time = tempDiscoverDto.createTime;

			if (!TimeUtil.isSameDayOfMillis(time, mTempTime)) {
				tempDiscoverDto.showTime=true;
				mTempTime = time;
			} else {
				tempDiscoverDto.showTime = false;
			}
 		}
		
		super.addItems(curDtos, isAppend);
	}
	 
	
 
	
	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				 
				// Type listType = new TypeToken<List<OwnProjectDto>>(){}.getType();
				LoadData(baseurl+"&pushTime="+getLastItem().createTime,RefreshMode.loadmore,curType);
				//LoadData(myprojectlistAdapter.getItem(myprojectlistAdapter.getCount()-1).getMinDate(), false);
			}
		}, 300);
	}


	 
	 
 
}