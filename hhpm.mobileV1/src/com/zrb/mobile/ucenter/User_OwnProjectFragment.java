package com.zrb.mobile.ucenter;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.RequestParams;
import com.zrb.mobile.MoneyDetailActivity;
import com.zrb.mobile.MyFindMoney;
import com.zrb.mobile.ProjectDetailActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.MatchingBaseAdapter;
import com.zrb.mobile.adapter.UserFundlist_ownsAdapter;
import com.zrb.mobile.adapter.UserProjectlist_ownsAdapter;
import com.zrb.mobile.adapter.model.OwnProjectDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.ui.fragment.DelayFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class User_OwnProjectFragment extends DelayFragment implements OnItemClickListener, onLoadMoreListener  {
	View RootView;
	MatchingBaseAdapter myprojectlistAdapter;
	//CustomloadingView mloadingView;
	int currentPage;
    int OwnsType=-1;
	boolean isFirstIn = true;
 	private PullToRefreshListViewEx listview;
 	
	public static User_OwnProjectFragment getNewFrament(int type) {

		User_OwnProjectFragment tmp = new User_OwnProjectFragment();
		tmp.OwnsType = type;
		return tmp;
	}
 	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// return inflater.inflate(R.layout.u_center_registerstep1, null);

		View view = inflater.inflate(R.layout.common_refreshlist_layout, container, false);
		RootView=view;
 		initView();
		return view;
	}
	
	private void initView() {
  	 
		listview = (PullToRefreshListViewEx) RootView.findViewById(R.id.discover_list_view);
		listview.setPagesize(14);

		listview.setOnItemClickListener(this);// 点击事件
		listview.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(CommonUtil.getcurTime());
				currentPage = 1;
				postDelayed(new Runnable() {
					@Override
					public void run() {
						LoadData(-1, true);
					}
				}, 1000);
			}
		});
 	}
  	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		if(OwnsType==Constants.MatchingFund)
	     	myprojectlistAdapter = new UserProjectlist_ownsAdapter(this.getActivity());
		else
			myprojectlistAdapter=new UserFundlist_ownsAdapter(this.getActivity());
		
 		listview.initAdapter(myprojectlistAdapter);
		listview.setOnLoadMoreListener(this);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}

	private void firstLoad() {
		if (isFirstIn) {
			isFirstIn = false;
 			postDelayed(new Runnable() {
				@Override
				public void run() {
					listview.setRefreshing(true);
				}
			}, 200);
		}
	}
	

	ZrbRestClient mZrbRestClient;
	private void LoadData(long minTime, final boolean isreflesh) {
		RequestParams param = new RequestParams();
		//param.put("zrb_front_ut", AppSetting.curUser.zrb_front_ut);
		param.put("limit", 14);
		if (minTime != -1)
			param.put("pushTime", minTime);

		CommonUtil.InfoLog("url", param.toString());
		if(null==mZrbRestClient){
			mZrbRestClient=new ZrbRestClient(this.getActivity());
			mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {

					CommonUtil.showToast(arg0,User_OwnProjectFragment.this.getActivity());
					if (currentPage > 1)
						listview.hideloading();
					else
						listview.onRefreshComplete();
 				}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// loadingview.Hide();
					String results = new String(arg2);
					if (currentPage == 1)
						listview.onRefreshComplete();
					else
						listview.hideloading();
					try {
						JSONObject jsonObject2 = new JSONObject(results);
						int retCode= jsonObject2.getInt("res");
	 				    if(retCode==1){
	 				    	myprojectlistAdapter.setJsonDatas(results,currentPage>1);
	 				    	int size=myprojectlistAdapter.getCurpageSize();
							if(size>0){  
								myprojectlistAdapter.notifyDataSetChanged();
  							} else {
 								CommonUtil.showToast(String.format("暂无%1$s信息~~", currentPage == 1 ? "": "更多") );
							}
						} else
							CommonUtil.showToast(jsonObject2.getString("msg"));
					} catch (Exception e) {
						CommonUtil.showToast(User_OwnProjectFragment.this.getString(R.string.error_serverdata_loadfail));
					}
				}
			});
 		}
		mZrbRestClient.post("userStatisticDetail/"+(OwnsType==Constants.MatchingFund ? "getMyProjects":"getMyFunds"), param);
	}

	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
  		   if(OwnsType==Constants.MatchingFund){
			  OwnProjectDto tmpDto =(OwnProjectDto)myprojectlistAdapter.getItem(position - 1);
			  Intent intent = new Intent(User_OwnProjectFragment.this.getActivity(), ProjectDetailActivity.class);
			  intent.putExtra("id", tmpDto.id);
			  intent.putExtra("title", tmpDto.title);
			  startActivity(intent); 
			}
			else{
				
				MyFindMoney tmpDto =(MyFindMoney)myprojectlistAdapter.getItem(position - 1);
 				Intent intent = new Intent(User_OwnProjectFragment.this.getActivity(), MoneyDetailActivity.class);
 				intent.putExtra("id", tmpDto.id);
				intent.putExtra("title", tmpDto.title);
 				startActivity(intent);
			}
		
	} 
 	 
	
	@Override
	public void onPause() {
		super.onPause();
 
	}

	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		currentPage++;
		postDelayed(new Runnable() {
			@Override
			public void run() {
				
				 if(OwnsType==Constants.MatchingFund){
					 
					 LoadData(((OwnProjectDto)myprojectlistAdapter.getItem(myprojectlistAdapter.getCount()-1)).pushTime, false);
				 }
				 else
					 LoadData(((MyFindMoney)myprojectlistAdapter.getItem(myprojectlistAdapter.getCount()-1)).pushTime, false);
				//LoadData(myprojectlistAdapter.getItem(myprojectlistAdapter.getCount()-1).getMinDate(), false);
			}
		}, 300);
	}


	 
	 
 
}
