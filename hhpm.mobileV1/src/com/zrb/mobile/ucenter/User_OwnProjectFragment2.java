package com.zrb.mobile.ucenter;

import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.hhpm.lib.support.inject.ViewInject;
import com.zrb.mobile.ProjectDetailActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.OwnProjectDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.ui.fragment.ABaseAdapter.AbstractItemView;
import com.zrb.mobile.ui.fragment.ARefreshFragment;
import com.zrb.mobile.utility.CommonUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class User_OwnProjectFragment2 extends ARefreshFragment<OwnProjectDto> implements onLoadMoreListener   {
	 
 
    int OwnsType=-1;
	boolean isFirstIn = true;
	TypeToken<List<OwnProjectDto>> curType=new TypeToken<List<OwnProjectDto>>(){};
 	
    @ViewInject(idStr = "discover_list_view")
    PullToRefreshListViewEx mListView;
    String baseurl="userStatisticDetail/getMyProjects?limit=10";
    
	public static User_OwnProjectFragment2 getNewFrament(int type) {

		User_OwnProjectFragment2 tmp = new User_OwnProjectFragment2();
		tmp.OwnsType = type;
		return tmp;
	}
  
	@Override
	  protected void layoutInit(LayoutInflater inflater, Bundle savedInstanceSate) {
		mListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(CommonUtil.getcurTime());
  				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
 						LoadData(baseurl,RefreshMode.refresh,curType);
					}
				}, 1000);
			}
		});
		
		mListView.setOnLoadMoreListener(this);
	 }
	
	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}

	private void firstLoad() {
		if (isFirstIn) {
			isFirstIn = false;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					mListView.setRefreshing(true);
				}
			}, 200); 
		}
	}
	

	 
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
 		  OwnProjectDto tmpDto =getAdapterItems().get(position-1);;
		  Intent intent = new Intent(User_OwnProjectFragment2.this.getActivity(), ProjectDetailActivity.class);
		  intent.putExtra("id", tmpDto.id);
		  intent.putExtra("title", tmpDto.title);
		  startActivity(intent); 
 	} 
 	 
 
	@Override
	protected AbstractItemView<OwnProjectDto> newItemView() {
		// TODO Auto-generated method stub
		return new TimelineItemView(this,true);
	}

	@Override
	public PullToRefreshListViewEx getRefreshView() {
		// TODO Auto-generated method stub
		return mListView;
	}

	@Override
	public void onRefreshViewComplete() {
		// TODO Auto-generated method stub
		//CommonUtil.showToast("onRefreshViewComplete");
	}

	@Override
	public void onHandlerResult(String resStr,RefreshMode curMode) {
		// TODO Auto-generated method stub
 	}

	@Override
	protected int inflateContentView() {
		// TODO Auto-generated method stub
		return R.layout.common_refreshlist_layout;
	}

	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				 
				// Type listType = new TypeToken<List<OwnProjectDto>>(){}.getType();
				LoadData(baseurl+"&pushTime="+getLastItem().pushTime,RefreshMode.loadmore,curType);
				//LoadData(myprojectlistAdapter.getItem(myprojectlistAdapter.getCount()-1).getMinDate(), false);
			}
		}, 300);
	}


	 
	 
 
}
