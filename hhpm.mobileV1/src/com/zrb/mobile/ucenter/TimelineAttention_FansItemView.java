package com.zrb.mobile.ucenter;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
 

import com.hhpm.lib.support.inject.ViewInject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
 
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.MoneyDetailActivity;
import com.zrb.mobile.ProjectDetailActivity;
import com.zrb.mobile.R;
  
import com.zrb.mobile.adapter.model.AttentionUserDto;
import com.zrb.mobile.adapter.model.ChoiceGenera_CardDto;
import com.zrb.mobile.adapter.model.FollowUserDto;
 
import com.zrb.mobile.ui.fragment.ABaseAdapter;
import com.zrb.mobile.ui.fragment.ABaseFragment;
import com.zrb.mobile.utility.FollowRelationUtil;

/**
 * Created by wangdan on 15/4/15.
 */
public class TimelineAttention_FansItemView extends ABaseAdapter.AbstractItemView<AttentionUserDto>
                implements View.OnClickListener  {

    protected ABaseFragment fragment;
    
    private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");//yyyy.
    int FundColor=Color.parseColor("#b68f85");
	int ProjectColor=Color.parseColor("#8ea0be");
	  DisplayImageOptions options;
	
	
    public TimelineAttention_FansItemView(ABaseFragment fragment ) {
         this.fragment = fragment;
	     options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.portrait)
		.showImageForEmptyUri(R.drawable.portrait)
		.showImageOnFail(R.drawable.portrait).cacheInMemory(true)
	//	.displayer(new FadeInBitmapDisplayer(300))
		.cacheOnDisc(true)
		.build();
  
    }

  
    
    @ViewInject(id = R.id.iv_avatar)
    ImageView useravatar;
    
    
    @ViewInject(id = R.id.txt_touser)
    TextView toUser; 
     
    @ViewInject(id = R.id.img_relation)
    ImageView imgRelation;
    
    @ViewInject(id = R.id.txt_relation)
    TextView mrelationdes;

    @ViewInject(id = R.id.txt_attention_content)
    TextView mcontent;
    
   
    

    @Override
    public int inflateViewId() {
        return R.layout.uc_myattention_item;
    }
   
    @Override
    public void bindingData(View convertView, AttentionUserDto curDto) {
     
    	FollowUserDto toUserDto=curDto.fromUser;
       	if(!TextUtils.isEmpty(toUserDto.avatar))
	 		ImageLoader.getInstance().displayImage(AppSetting.mediaServer+toUserDto.avatar, useravatar, options);  
	 	else
	 		useravatar.setImageResource(R.drawable.portrait); 
     	 
       	toUser.setText(toUserDto.realname); 
        
       	imgRelation.setImageResource(FollowRelationUtil.getRelationIcon(curDto.relation));
       	
       	mrelationdes.setText(FollowRelationUtil.getfansRelationdes(curDto.relation));
    	// 	dynamic_content.setText(curDto.title); 
       
        mcontent.setText(FollowRelationUtil.getFormatText(curDto.relation,toUserDto.umobile,toUserDto.realname,true));
        
    }
 
    @Override
    public void onClick(View v) {
    	switch(v.getId() ){
    	case R.id.project_match_txt:
     		 final ChoiceGenera_CardDto status = (ChoiceGenera_CardDto) v.getTag();
     		  if(status.choiceType==1)
				  ProjectDetailActivity.launch(fragment.getActivity(), status.bizId, status.title);
				else
				  MoneyDetailActivity.launch(fragment.getActivity(), status.bizId, status.title);
     		 break;
     	}
      
    }

    /*@Override
    public void onLikeRefreshUI() {
        if (fragment != null && fragment instanceof ARefreshFragment)
            ((ARefreshFragment) fragment).refreshUI();
    }

    @Override
    public void onLikeRefreshView(StatusContent data, final View likeView) {
        if (fragment.getActivity() == null)
            return;

        if (likeView.getTag() == data) {
            animScale(likeView);
        }
    }*/

 
	

}
