package com.zrb.mobile.common;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.zrb.mobile.R;
import com.zrb.mobile.adapter.ShareListAdapter;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.DialogBuilder;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ShareDialog {
	
	Context mContext;
	Dialog currentDialog;
	ShareListAdapter currentAdapter;
	CustomloadingView loadingview;
	private String mtilte,murl;
	
	
	public ShareDialog(Context context)
	{
		mContext=context;
 		
	}
 	
	public  void share(String title,String url)
	{
		mtilte=title;
		murl=url;
		DialogBuilder tmpbuild=new	DialogBuilder(mContext);
	 
		tmpbuild.setTitle("分享");
		tmpbuild.setParentlayout(R.layout.custom_dialog2);
		tmpbuild.setContentView(R.layout.share_dialog);
		tmpbuild.setCloseOnclick(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(currentDialog!=null) currentDialog.dismiss();
 			}
 		});
	  	View container=tmpbuild.getCurContentView();
	  
	  	ListView tmplist=	(ListView)container.findViewById(R.id.listView1);
	  	loadingview=(CustomloadingView)container.findViewById(R.id.list_emptyview);
	  	
	    currentAdapter=new ShareListAdapter(mContext);
	  	tmplist.setAdapter(currentAdapter);
	  	tmplist.setOnItemClickListener(new OnItemClickListener() {  
	  	  
            @Override  
            public void onItemClick(AdapterView<?> parent, View view,  
                    int position, long id) {  
                // TODO Auto-generated method stub  
  
                Intent shareIntent = new Intent(Intent.ACTION_SEND);  
                AppInfo appInfo = (AppInfo) currentAdapter.CurrentDatas.get(position);  
                shareIntent.setComponent(new ComponentName(appInfo.appPkgName, appInfo.appLauncherClassName));  //.getAppPkgName()  .getAppLauncherClassName()
                shareIntent.setType("text/plain");    //"image/*"
//              shareIntent.setType("*/*");  
                shareIntent.putExtra(Intent.EXTRA_SUBJECT,"分享："); 
                shareIntent.putExtra(Intent.EXTRA_TEXT, mtilte+murl); 
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  
                mContext.startActivity(shareIntent);  
            }  
        });  
	  	currentDialog=	tmpbuild.create();
	  	currentDialog.show();
	  	new GetDataTask().execute();
	  	
	}
	
	
	private class GetDataTask extends AsyncTask<Void, Void, List<AppInfo>> {

		@Override
		protected List<AppInfo> doInBackground(Void... params) {
			return getShareAppList( mContext);
			//return mStrings;
			//return null;
		}

		@Override
		protected void onPostExecute(List<AppInfo> result) {
			
			
			loadingview.Hide();
			currentAdapter.CurrentDatas.addAll(result);
			currentAdapter.notifyDataSetChanged();  
			super.onPostExecute(result);
		}
	}
	
	
	
	public List<ResolveInfo> getShareApps(Context context) {    
        List<ResolveInfo> mApps = new ArrayList<ResolveInfo>();  
        Intent intent = new Intent(Intent.ACTION_SEND, null);  
        intent.addCategory(Intent.CATEGORY_DEFAULT);  
        intent.setType("text/plain");  
//      intent.setType("*/*");  
        PackageManager pManager = context.getPackageManager();  
        mApps = pManager.queryIntentActivities(intent,   PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);  
        return mApps;  
    } 
	
	private List<AppInfo> getShareAppList(Context mContext) { 
		
        List<AppInfo> shareAppInfos = new ArrayList<AppInfo>();  
        PackageManager packageManager = mContext.getPackageManager();//getPackageManager();  
        List<ResolveInfo> resolveInfos = getShareApps(mContext);  
        if (null == resolveInfos) {  
            return null;  
        } else {  
            for (ResolveInfo resolveInfo : resolveInfos) {  
                AppInfo appInfo = new AppInfo();  
                appInfo.appPkgName=(resolveInfo.activityInfo.packageName);        //AppPkgNam
//              showLog_I(TAG, "pkg>" + resolveInfo.activityInfo.packageName + ";name>" + resolveInfo.activityInfo.name);  
                
               
                appInfo.appLauncherClassName=(resolveInfo.activityInfo.name);        //setAppLauncherClassNam
                appInfo.appName=(resolveInfo.loadLabel(packageManager).toString());  //setAppName
                appInfo.appIcon=(resolveInfo.loadIcon(packageManager));  
                shareAppInfos.add(appInfo);                                          //setAppIcon
                
                
                Log.e("pkg", appInfo.appPkgName+"||"+appInfo.appLauncherClassName);
            }  
        }         
        return shareAppInfos;  
    }  
	
	
	private void shareToFriend(File file) {
        Intent intent = new Intent();
        ComponentName comp = new ComponentName("com.tencent.mm",
                        "com.tencent.mm.ui.tools.ShareImgUI");
        intent.setComponent(comp);
        intent.setAction("android.intent.action.SEND");
        intent.setType("image/*");                  
        intent.putExtra(Intent.EXTRA_TEXT,"我是文字");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));                
        mContext.startActivity(intent);
     }
	
      private void shareToTimeLine(File file) {
        Intent intent = new Intent();
        ComponentName comp = new ComponentName("com.tencent.mm",
                        "com.tencent.mm.ui.tools.ShareToTimeLineUI");
        intent.setComponent(comp);
        intent.setAction("android.intent.action.SEND");
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_TEXT,"我是文字");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        mContext.startActivity(intent);
   } 
	
      //File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
     // File file = new File(dir, "1.png");
}
