package com.zrb.mobile.common;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper
{
	private static final String DB_NAME = "zrb_app_demo";
	private static final int DATABASE_VERSION = 3;
	
	private static final String QUERYHISTORY_TABLE_CREATE = "CREATE TABLE "
			+ QueryhistoryDao.TABLE_NAME + " ("
			+ QueryhistoryDao.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ QueryhistoryDao.COLUMN_NAME_WORDS + " TEXT, "
			+ QueryhistoryDao.COLUMN_NAME_TYPE + " INTEGER, "
	        + QueryhistoryDao.COLUMN_NAME_TIME + " TEXT)";
	
	
	private static final String FAVORITEHISTORY_TABLE_CREATE = "CREATE TABLE "
			+ FavoritehistoryDao.TABLE_NAME + " ("
			+ FavoritehistoryDao.COLUMN_NAME_ID + " INTEGER, "
			+ FavoritehistoryDao.COLUMN_NAME_TITLE + " TEXT, "
			+ FavoritehistoryDao.COLUMN_NAME_TYPE + " INTEGER) ";
	
	private static final String PROJECT_FAVORITEHISTORY_TABLE_CREATE = "CREATE TABLE "
			+ ProjectAttentionhistoryDao.TABLE_NAME + " ("
			+ ProjectAttentionhistoryDao.COLUMN_NAME_ID + " INTEGER, "
			+ ProjectAttentionhistoryDao.COLUMN_NAME_TITLE + " TEXT, "
			+ ProjectAttentionhistoryDao.COLUMN_NAME_TYPE + " INTEGER) ";
	      

	public DBHelper(Context context)
	{
		super(context, DB_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		/**
		 * id,title,link,date,imgLink,content,newstype
		 */
		String sql = "create table tb_newsItem( _id integer primary key autoincrement , "
				+ " title text , link text , date text , imgLink text , content text , newstype integer  );";
		db.execSQL(sql);
		db.execSQL(QUERYHISTORY_TABLE_CREATE);
		db.execSQL(FAVORITEHISTORY_TABLE_CREATE);
		db.execSQL(PROJECT_FAVORITEHISTORY_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// TODO Auto-generated method stub
		if(oldVersion < 3){
		    db.execSQL(PROJECT_FAVORITEHISTORY_TABLE_CREATE);
        }
	}

}
