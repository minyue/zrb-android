package com.zrb.mobile.common;

public class CommonException2 extends Exception
{
  public CommonException2()
  {
  }

  public CommonException2(String message, Throwable cause)
  {
    super(message, cause);
  }

  public CommonException2(String message)
  {
    super(message);
  }

  public CommonException2(Throwable cause)
  {
    super(cause);
  }
}