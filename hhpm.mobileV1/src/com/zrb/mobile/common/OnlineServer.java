package com.zrb.mobile.common;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.User_change_Password;
import com.zrb.mobile.adapter.model.RegisterDto;
 
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import com.zrb.applib.utils.AppSetting;
 
public class OnlineServer extends Service {  
    int mStartMode;       // indicates how to behave if the service is killed  
    IBinder mBinder;      // interface for clients that bind  
    boolean mAllowRebind; // indicates whether onRebind should be used  
   // private int delay=60*60; //1hour;
    private int delay=60*3; //1hour;
    Timer timer;
	final Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				Refleshtoken();
				break;
			}
			super.handleMessage(msg);
		}
	};
    
	TimerTask task = new TimerTask() {
		public void run() {
			Message message = new Message();
			message.what = 1;
			handler.sendMessage(message);
		}
	};
    
	private void Refleshtoken(){
		CommonUtil.InfoLog("Refleshtoken", "Refleshtoken");
		//http://192.168.1.21:8090/doRefreshLogin?uid=1000000224&password=111111
		if(AppSetting.curUser==null) return;
 		
		CommonUtil.InfoLog("Refleshtoken", "Refleshtoken2");
		
		RequestParams param = new RequestParams();
		param.put("uid", AppSetting.curUser.uid);
		param.put("password", AppSetting.curUser.pwd);
		ZrbRestClient.post("doRefreshLogin", param,
				new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						// TODO Auto-generated method stub
						//Log.e("0doRefreshLogin", "fail") ;
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						    String results = new String(arg2);
	 						//Log.e("1doRefreshLogin", "ok") ;
	 						JSONObject jsonObject2;
							try {
								jsonObject2 = new JSONObject(results);
								int retCode= jsonObject2.getInt("res");
								if(retCode==1){
									JSONObject tmpObj= jsonObject2.getJSONObject("data");
									if(AppSetting.curUser!=null)  
									 AppSetting.curUser.zrb_front_ut=tmpObj.getString("zrb_front_ut");
								}
							   
							} catch (JSONException e) {
  								//e.printStackTrace();
								CommonUtil.InfoLog("Refleshtoken:error", e.getMessage());
							}
					}
				});
		
	}
	
    @Override  
    public void onCreate() {  
        // The service is being created  
    	
    	CommonUtil.InfoLog("onCreate", "ononCreate");
    	 timer = new Timer(true);
    	 timer.schedule(task,1000*delay, 1000*delay); //��ʱ1000ms��ִ�У�1000msִ��һ��
    	
    }  
    @Override  
    public int onStartCommand(Intent intent, int flags, int startId) {  
        // The service is starting, due to a call to startService()  
    	
    	CommonUtil.InfoLog("tagonStartCommand", "onStartCommand");
    	return START_STICKY;  
    }  
 
    @Override  
    public boolean onUnbind(Intent intent) {  
        // All clients have unbound with unbindService()  
        return mAllowRebind;  
    }  
    @Override  
    public void onRebind(Intent intent) {  
        // A client is binding to the service with bindService(),  
        // after onUnbind() has already been called  
    }  
    @Override  
    public void onDestroy() {  
        // The service is no longer used and is being destroyed  
    	if(timer!=null) timer.cancel();
    }
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return myBinder;
	}  
 	
	 public class MyBinder extends Binder{
	        
	        public OnlineServer getService(){
	            return OnlineServer.this;
	        }
	    }
	 private MyBinder myBinder = new MyBinder();
}