package com.zrb.mobile.common;

import java.io.File;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zrb.applib.utils.FileUtil;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.NewsDtos;

public class NewsCache {

	private static final String CACHEDIR="/recentNews";
	
	public static  NewsDtos getRecentNews(int newType){
		
		File tmpfile=new File(FileUtil.filePath+CACHEDIR,String.valueOf(newType));
		if(tmpfile.exists()){
			try{
 			byte[] results=FileUtil.getBytesFromFile(tmpfile);
				if(results==null) return null;
 		        Gson gson =new Gson();  
		        NewsDtos tmpDto=	gson.fromJson( new String(results), NewsDtos.class);
				return tmpDto;
			}
			 catch (Exception e){
				
				return null;
			}
			
		}
		return null;
	}
	
	public static void saveRecentNews(int newType,String message){
		
		FileUtil.writeFileSdcardhasDir(CACHEDIR,String.valueOf(newType),message);
	}
	
}
