package com.zrb.mobile.common;

import android.graphics.drawable.Drawable;

public class AppInfo {
	
	public String appPkgName;
	public String appLauncherClassName;
	public String appName;
	public Drawable appIcon;
	
    
}

/*appInfo.setAppPkgName(resolveInfo.activityInfo.packageName);  
//showLog_I(TAG, "pkg>" + resolveInfo.activityInfo.packageName + ";name>" + resolveInfo.activityInfo.name);  
appInfo.setAppLauncherClassName(resolveInfo.activityInfo.name);  
appInfo.setAppName(resolveInfo.loadLabel(packageManager).toString());  
appInfo.setAppIcon(resolveInfo.loadIcon(packageManager));  
shareAppInfos.add(appInfo);  */
