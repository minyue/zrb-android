package com.zrb.mobile.common;

import java.util.ArrayList;
import java.util.List;
import com.zrb.mobile.adapter.News_tablistItemAdapter;
import com.zrb.mobile.adapter.model.CollectionDto;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class FavoritehistoryDao
{
    public final static String TABLE_NAME="favoritehistory";
 	public static final String COLUMN_NAME_ID = "id";
 	public static final String COLUMN_NAME_TITLE = "title";
 	public static final String COLUMN_NAME_TYPE = "type";
 	
	private DBHelper dbHelper;
	public FavoritehistoryDao(Context context)
	{
		dbHelper = new DBHelper(context);
	}
 	
	/**
	 * 获取 
	 * 
	 * @return
	 */
	private List<FavoritehistoryItem> getSearchList() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		List<FavoritehistoryItem> tmpdtos = new ArrayList<FavoritehistoryItem>();
		if (db.isOpen()) {
			Cursor cursor = db.rawQuery("select * from " + TABLE_NAME , null);
			while (cursor.moveToNext()) {
				int idx = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_ID));
				String title = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TITLE));
				int type = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_TYPE));
				FavoritehistoryItem record = new FavoritehistoryItem(idx,title,type);
			 
				tmpdtos.add(record);
			}
			cursor.close();
		}
		db.close();
		return tmpdtos;
	}
	
	
	public void initNewsids(){
		
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		//.rawQuery("select name from *** where id=?", new String[]{"1"});
		if (db.isOpen()) {
			Cursor cursor = db.rawQuery("select * from " + TABLE_NAME+" where "+COLUMN_NAME_TYPE+"=1" , null);
			while (cursor.moveToNext()) {
				int idx = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_ID));
				News_tablistItemAdapter.FavoriteMap.put(idx, true);
 			}
			cursor.close();
		}
		db.close();
	 
	}
  
	/**
	 * 更新message
	 * @param msgId
	 * @param values
	 */
	public void delete(int curId,boolean isNews){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if(db.isOpen()){
			//db.update(TABLE_NAME, values, COLUMN_NAME_ID + " = ?", new String[]{String.valueOf(msgId)});
		//	db.delete(TABLE_NAME, COLUMN_NAME_ID + " = ?", new String[]{String.valueOf(newsId)});
			db.delete(TABLE_NAME,COLUMN_NAME_ID+"="+curId+" AND "+COLUMN_NAME_TYPE+"="+(isNews ? 1:2),null);
		}
		db.close();
		 if(isNews) News_tablistItemAdapter.FavoriteMap.remove(curId);
	}
	
	private boolean isExist(int newsId){
		boolean isfind=false;
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		if (db.isOpen()){
	       Cursor cursor = db.query(TABLE_NAME, null, COLUMN_NAME_ID + "=?", new String[]{String.valueOf(newsId)}, null, null, null); 
	       if(cursor != null && cursor.moveToFirst()){  
               isfind=true;
	       }  
	       cursor.close();
	    }
	    db.close();
		return isfind;
	}
	
	// 1为news ,2为projects
	public boolean isExistId(int curid,boolean isNews){
		
		return isExistType(curid,isNews? 1:2);
		
	}
	
	private boolean isExistType(int curId,int type){
		
		 boolean isfind=false;
 		 SQLiteDatabase db = dbHelper.getReadableDatabase();
		 if (db.isOpen()){
 			 Cursor cursor= db.query(TABLE_NAME,null,COLUMN_NAME_TYPE+"="+type+" AND "+COLUMN_NAME_ID+"="+curId,null,null,null,null);
 		     if(cursor != null && cursor.moveToFirst()){  
                isfind=true;
	         }  
	         cursor.close();
	     }
	     db.close();
		return isfind;
    }
	
	
	/**
	 * 删除
	 * @param username
	 */
	public void deleteAll(){
 	
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if(db.isOpen()){
			db.delete(TABLE_NAME, null, null);
		}
		db.close();
		News_tablistItemAdapter.FavoriteMap.clear();
	}
  
	/**
	 * 保存
	 * @param user   1为news ,2为projects
	 */
	public void save(FavoritehistoryItem curitem,boolean isNews){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		
	 
		if(curitem.title != null)
			values.put(COLUMN_NAME_TITLE, curitem.title);
		
		values.put(COLUMN_NAME_ID, curitem.id);
		values.put(COLUMN_NAME_TYPE,isNews? 1:2);// curitem.type
		if(db.isOpen()){
			db.replace(TABLE_NAME, null, values);
		}
		db.close();
		if(isNews)  
			News_tablistItemAdapter.FavoriteMap.put(curitem.id, true);
	}
   
	// 1为news ,2为projects
	//type 0 是咨询 1是项目
	public void resetIds(List<CollectionDto> collectionIds){
 		deleteAll();
 		if(collectionIds==null||collectionIds.size()<=0) return;
  		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.beginTransaction(); // 手动设置开始事务
		for (CollectionDto tmpDto : collectionIds) {
 		    ContentValues values = new ContentValues();
			values.put(COLUMN_NAME_ID, tmpDto.infoId);
			values.put(COLUMN_NAME_TYPE, tmpDto.type==0 ?1 :2);
		    db.insert(TABLE_NAME, null, values);
		    
		   if(tmpDto.type==0) News_tablistItemAdapter.FavoriteMap.put(tmpDto.infoId, true);
		}
		db.setTransactionSuccessful(); // 设置事务处理成功，不设置会自动回滚不提交
		db.endTransaction(); // 处理完成
		db.close();
	}
	 

}
