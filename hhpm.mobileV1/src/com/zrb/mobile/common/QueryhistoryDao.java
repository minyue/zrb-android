package com.zrb.mobile.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

 
import com.zrb.mobile.utility.CommonUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

 

public class QueryhistoryDao
{
    public final static String TABLE_NAME="queryhistory";
 	public static final String COLUMN_NAME_ID = "id";
	public static final String COLUMN_NAME_WORDS = "words";
	public static final String COLUMN_NAME_TYPE = "type";
	public static final String COLUMN_NAME_TIME = "time";
	
	private DBHelper dbHelper;
	public QueryhistoryDao(Context context)
	{
		dbHelper = new DBHelper(context);
	}

	
	/**
	 * 获取 
	 * 
	 * @return
	 */
	public List<QueryhistoryItem> getSearchList() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		List<QueryhistoryItem> tmpdtos = new ArrayList<QueryhistoryItem>();
		if (db.isOpen()) {
			Cursor cursor = db.rawQuery("select * from " + TABLE_NAME +" order by " +COLUMN_NAME_TIME+ " desc", null);
			while (cursor.moveToNext()) {
				int idx = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_ID));
				String words = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_WORDS));
				int type = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_TYPE));
				QueryhistoryItem record = new QueryhistoryItem(idx,words,type);
			 
				tmpdtos.add(record);
			}
			cursor.close();
		}
		db.close();
		return tmpdtos;
	}
	
	/**
	 * 删除
	 * @param username
	 */
	public void delete(String words){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if(db.isOpen()){
			db.delete(TABLE_NAME, COLUMN_NAME_WORDS + " = ?", new String[]{words});
		}
		db.close();
	}
 
	/**
	 * 更新message
	 * @param msgId
	 * @param values
	 */
	public void delete(int msgId){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if(db.isOpen()){
			//db.update(TABLE_NAME, values, COLUMN_NAME_ID + " = ?", new String[]{String.valueOf(msgId)});
			db.delete(TABLE_NAME, COLUMN_NAME_ID + " = ?", new String[]{String.valueOf(msgId)});
		}
		db.close();
	}
	/**
	 * 删除
	 * @param username
	 */
	public void deleteAll(){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if(db.isOpen()){
			db.delete(TABLE_NAME, null, null);
		}
		db.close();
	}
 
 	/**
	 * 更新
	 * @param user
	 */
	public void update(QueryhistoryItem curitem){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		values.put(COLUMN_NAME_TIME, CommonUtil.getlongDate());
 		if(db.isOpen()){
			//db.replace(TABLE_NAME, null, values);
 			db.update(TABLE_NAME, values, COLUMN_NAME_WORDS + " = ?", new String[]{curitem.keywords});
		}
 		db.close();
	}
	
	/**
	 * 保存
	 * @param user
	 */
	public void save(QueryhistoryItem curitem){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		values.put(COLUMN_NAME_TIME, CommonUtil.getlongDate());
		if(curitem.keywords != null)
			values.put(COLUMN_NAME_WORDS, curitem.keywords);
		 
		values.put(COLUMN_NAME_TYPE, curitem.type);
		if(db.isOpen()){
			db.replace(TABLE_NAME, null, values);
		}
		db.close();
	}
	
 
	public void deleteAll(int newsType)
	{
		String sql = "delete from tb_newsItem where newstype = ?";
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.execSQL(sql, new Object[] { newsType });
		db.close();
	}

	 

	 

}
