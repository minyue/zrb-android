package com.zrb.mobile.common;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 保存应用信息
 *
 * @author Administrator
 */
public class NightConfig {


	private static NightConfig mconfig;
 
	
	public static NightConfig getInstance(final Context context){
 		if(mconfig==null) mconfig=new NightConfig(context);
		return mconfig;
	}
	
    private SharedPreferences innerConfig;

    private static final String KEY_NIGHT_MODE_SWITCH="night_mode_switch";
    private static final String KEY_NIGHT_MODE_FontSize="night_mode_fontsize";

    private NightConfig(final Context context) {
        innerConfig = context.getSharedPreferences("app_config", Application.MODE_PRIVATE);
    }

    //夜间模式
    public boolean getNightModeSwitch() {
        return innerConfig.getBoolean(KEY_NIGHT_MODE_SWITCH, false);
    }

    public void setNightModeSwitch(boolean on) {
        Editor editor = innerConfig.edit();
        editor.putBoolean(KEY_NIGHT_MODE_SWITCH, on);
        editor.commit();
    }

    public float getFontSize(){
    	 return innerConfig.getFloat(KEY_NIGHT_MODE_FontSize, 9);
    }
    
    public void setFontSize(float fontsize) {
        Editor editor = innerConfig.edit();
        editor.putFloat(KEY_NIGHT_MODE_FontSize, fontsize);
        editor.commit();
    }
    /**
     * 清空
     */
    public void clear() {
        Editor editor = innerConfig.edit();
        editor.clear();
        editor.commit();
    }
}
