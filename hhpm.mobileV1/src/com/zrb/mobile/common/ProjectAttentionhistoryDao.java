package com.zrb.mobile.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
 
import com.zrb.mobile.utility.CommonUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;


public class ProjectAttentionhistoryDao
{

    public final static String TABLE_NAME="projecthistory";
 	public static final String COLUMN_NAME_ID = "id";
 	public static final String COLUMN_NAME_TITLE = "title";
 	public static final String COLUMN_NAME_TYPE = "type";
 	
	private DBHelper dbHelper;
	public ProjectAttentionhistoryDao(Context context)
	{
		dbHelper = new DBHelper(context);
	}
 	
	/**
	 * 获取 
	 * 
	 * @return
	 */
	public List<FavoritehistoryItem> getSearchList() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		List<FavoritehistoryItem> tmpdtos = new ArrayList<FavoritehistoryItem>();
		if (db.isOpen()) {
			Cursor cursor = db.rawQuery("select * from " + TABLE_NAME , null);
			while (cursor.moveToNext()) {
				int idx = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_ID));
				String title = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TITLE));
				int type = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_TYPE));
				FavoritehistoryItem record = new FavoritehistoryItem(idx,title,type);
			 
				tmpdtos.add(record);
			}
			cursor.close();
		}
		db.close();
		return tmpdtos;
	}
	
  
	/**
	 * 更新message
	 * @param msgId
	 * @param values
	 */
	public void delete(int newsId){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if(db.isOpen()){
			//db.update(TABLE_NAME, values, COLUMN_NAME_ID + " = ?", new String[]{String.valueOf(msgId)});
			db.delete(TABLE_NAME, COLUMN_NAME_ID + " = ?", new String[]{String.valueOf(newsId)});
			 
		}
		db.close();
 	}
	
	public boolean isExist(int newsId){
		boolean isfind=false;
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		if (db.isOpen()){
	       Cursor cursor = db.query(TABLE_NAME, null, COLUMN_NAME_ID + "=?", new String[]{String.valueOf(newsId)}, null, null, null); 
	       if(cursor != null && cursor.moveToFirst()){  
               isfind=true;
	       }  
	       cursor.close();
	    }
	    db.close();
		return isfind;
	}
	
	/**
	 * 删除
	 * @param username
	 */
	public void deleteAll(){
 	
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		if(db.isOpen()){
			db.delete(TABLE_NAME, null, null);
		}
		db.close();
  	}
  
	/**
	 * 保存
	 * @param user
	 */
	public void save(FavoritehistoryItem curitem){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		
	 
		if(curitem.title != null)
			values.put(COLUMN_NAME_TITLE, curitem.title);
		
		values.put(COLUMN_NAME_ID, curitem.id);
		values.put(COLUMN_NAME_TYPE, curitem.type);
		if(db.isOpen()){
			db.replace(TABLE_NAME, null, values);
		}
		db.close();
 	}
   
	public void resetFavorites(int[] favoriteIds){
		if(favoriteIds==null||favoriteIds.length<=0) return;
 	
		deleteAll();
  		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.beginTransaction(); // 手动设置开始事务
		for (int newsid : favoriteIds) {
 		    ContentValues values = new ContentValues();
			values.put(COLUMN_NAME_ID, newsid);
			values.put(COLUMN_NAME_TYPE, 1);
		    db.insert(TABLE_NAME, null, values);
  		}
		db.setTransactionSuccessful(); // 设置事务处理成功，不设置会自动回滚不提交
		db.endTransaction(); // 处理完成
		db.close();
		 
	}

}
