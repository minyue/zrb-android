package com.zrb.mobile;

import java.util.HashMap;
import java.util.Map;

import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.ViewPagerIndicator;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.TextView;

public class TestApp_HomeFragment extends Fragment {

	CenterProgressWebView mWebView;
	 

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
 	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
		View view = inflater.inflate(R.layout.activity_home, container, false);
		mWebView=(CenterProgressWebView)view.findViewById(R.id.webView1);
		 
 		return view;
	}
 	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
 		 super.onActivityCreated(savedInstanceState);
 		//if (this.getUserVisibleHint()) firstLoad();
 		String url="file:///android_asset/www/trend.html";
		mWebView.loadUrl(url);
		
	}

}
