package com.zrb.mobile;
 

import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeConfig;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners.SnsPostListener;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.QZoneShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.TencentWBSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.sso.UMSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.ClipboardManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressWarnings("deprecation")
public class BaseShareFragment  extends Fragment    {

   UMSocialService mController = UMServiceFactory.getUMSocialService("myshare");
   public  String mtitle,murl;
   public Context mcontext;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
 		
	}
	
	public void resetContent(){
		
		String mcontent=mtitle+":"+murl;
		// 设置分享内容
		mController.setShareContent(mcontent);
		
	}
	
	public void init() {
		
		String mcontent=mtitle+":"+murl;
		// 设置分享内容ss
		mController
				.setShareContent(mcontent);
		// 设置分享图片, 参数2为图片的url地址
		mController.setShareMedia(new UMImage(this.getActivity(), R.drawable.logo_icon));
//--------------------------------------------------------------------------------------------------------
		// 参数1为当前Activity，参数2为开发者在QQ互联申请的APP ID，参数3为开发者在QQ互联申请的APP kEY.（自己申请）
		UMQQSsoHandler qqSsoHandler = new UMQQSsoHandler(this.getActivity(), "1104814343", "OuwjSfXtmF5n0NOf");
		qqSsoHandler.addToSocialSDK();
		QQShareContent qqShareContent = new QQShareContent();
		// 设置分享文字
		qqShareContent.setShareContent(mcontent);
		// 设置分享title
		qqShareContent.setTitle(mtitle);
		// 设置分享图片
		qqShareContent.setShareImage(new UMImage(this.getActivity(), R.drawable.logo_icon));
		// 设置点击分享内容的跳转链接
		qqShareContent.setTargetUrl(murl);
		mController.setShareMedia(qqShareContent);
		
	 
//--------------------------------------------------------------------------------------------------------
		// 参数1为当前Activity，参数2为开发者在QQ互联申请的APP ID，参数3为开发者在QQ互联申请的APP kEY.（自己申请）
 		QZoneSsoHandler qZoneSsoHandler = new QZoneSsoHandler( this.getActivity(),"1104814343", "OuwjSfXtmF5n0NOf");
		qZoneSsoHandler.addToSocialSDK();
		QZoneShareContent qzone = new QZoneShareContent();
		// 设置分享文字
		qzone.setShareContent(mcontent);
		// 设置点击消息的跳转URL
		qzone.setTargetUrl(murl);
		// 设置分享内容的标题
		qzone.setTitle("招融宝-分享");
		// 设置分享图片
		qzone.setShareImage(new UMImage(this.getActivity(), R.drawable.logo_icon));
		mController.setShareMedia(qzone);
//--------------------------------------------------------------------------------------------------------
		// 设置腾讯微博SSO handler
		mController.getConfig().setSsoHandler(new TencentWBSsoHandler());
//--------------------------------------------------------------------------------------------------------
		// 设置新浪SSO handler
		mController.getConfig().setSsoHandler(new SinaSsoHandler());
//--------------------------------------------------------------------------------------------------------
		// 添加微信的appID appSecret要自己申请
	 
		// 添加微信的appID appSecret要自己申请
		String appID = "wx003842e0cce47451";
		String appSecret = "d3ebb809779cc675b792150e53c80222";
				if (BuildConfig.DEBUG) {
					 appID = "wxa713663a0313532d";
					 appSecret = "721e6e536943a53433b5dc02a596d528";
				}
		
		//AppID：wxb90fccf78c959cb5  release
		//AppSecret：95265b750c1b17897f598e64bc27a5e9
		// 添加微信平台
		UMWXHandler wxHandler = new UMWXHandler(this.getActivity(), appID, appSecret);
		wxHandler.addToSocialSDK();
		// 设置微信好友分享内容
		WeiXinShareContent weixinContent = new WeiXinShareContent();
		// 设置分享文字
		weixinContent.setShareContent(mcontent);
		// 设置title
		weixinContent.setTitle("招融宝-分享");
		// 设置分享内容跳转URL
		weixinContent.setTargetUrl(murl);
		// 设置分享图片
		weixinContent.setShareImage(new UMImage(this.getActivity(),
				R.drawable.logo_icon));
		mController.setShareMedia(weixinContent);
//--------------------------------------------------------------------------------------------------------
		// 添加微信朋友圈(自会显示title，不会显示内容，官网这样说的)
		UMWXHandler wxCircleHandler = new UMWXHandler(this.getActivity(), appID, appSecret);
		// 设置微信朋友圈分享内容
		CircleShareContent circleMedia = new CircleShareContent();
		circleMedia.setShareContent(mcontent);
		// 设置朋友圈title
		circleMedia.setTitle("招融宝-分享");
		circleMedia.setShareImage(new UMImage(this.getActivity(),
				R.drawable.logo_icon));
		circleMedia.setTargetUrl(murl);
		mController.setShareMedia(circleMedia);
		wxCircleHandler.setToCircle(true);
		wxCircleHandler.addToSocialSDK();
//--------------------------------------------------------------------------------------------------------		
/*			// 添加短信
		SmsHandler smsHandler = new SmsHandler();
		smsHandler.addToSocialSDK();
//--------------------------------------------------------------------------------------------------------
		button = (Button) findViewById(R.id.btn_share);
		button.setOnClickListener(this);*/
		SocializeConfig tmpconfig=mController.getConfig();
		tmpconfig.closeToast();
	} 
	
	
	/**
	 * 分享监听器
	 */
	SnsPostListener mShareListener = new SnsPostListener() {

		@Override
		public void onStart() {

		}

		@Override
		public void onComplete(SHARE_MEDIA platform, int stCode, SocializeEntity entity) {
		 	if (stCode == 200) {
				Toast.makeText(mcontext, "分享成功", Toast.LENGTH_SHORT).show();
			} else {
			   if(stCode!=40000)
				Toast.makeText(mcontext,"分享失败 : error code : " + stCode, Toast.LENGTH_SHORT).show();
			} 
		}
	};
	
	SnsPostListener mwxShareListener = new SnsPostListener() {
         @Override
         public void onStart() {
           //  Toast.makeText(mcontext, "开始分享.", Toast.LENGTH_SHORT).show();
         }
         @Override
         public void onComplete(SHARE_MEDIA platform, int eCode,SocializeEntity entity) {
               if (eCode == 200) {
                  Toast.makeText(mcontext, "分享成功.", Toast.LENGTH_SHORT).show();
              } else {
                   String eMsg = "";
                   if (eCode == -101){
                       eMsg = "没有授权";
                   }
                   if(eCode!=40000)
                   Toast.makeText(mcontext, "分享失败[" + eCode + "] " + eMsg,Toast.LENGTH_SHORT).show();
              } 
         }
       };
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		UMSsoHandler ssoHandler = mController.getConfig().getSsoHandler(resultCode);
		if (ssoHandler != null) {
			ssoHandler.authorizeCallBack(requestCode, resultCode, data);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	    * 发送邮件
	    * @param emailBody
	    */
	   private void sendMail(String emailUrl){
			Intent email = new Intent(android.content.Intent.ACTION_SEND);
			email.setType("plain/text");
			
			String emailBody = "我正在浏览这个,觉得真不错,推荐给你哦~ 地址:" + emailUrl;
			//邮件主题
			email.putExtra(android.content.Intent.EXTRA_SUBJECT, "享");//subjectStr
			//邮件内容
			email.putExtra(android.content.Intent.EXTRA_TEXT, emailBody);  
			startActivityForResult(Intent.createChooser(email,  "请选择邮件发送内容" ), 1001 ); 
		}
	   
	   
	   /**
	    * 发短信
	    */
	   private   void  sendSMS(String webUrl){  
		  String smsBody = "我正在浏览这个,觉得真不错,推荐给你哦~ 地址:" + webUrl;
		  Uri smsToUri = Uri.parse( "smsto:" );  
		  Intent sendIntent =  new  Intent(Intent.ACTION_VIEW, smsToUri);  
		   //sendIntent.putExtra("address", "123456"); // 电话号码，这行去掉的话，默认就没有电话   
		  //短信内容
		  sendIntent.putExtra( "sms_body", smsBody);  
		  sendIntent.setType( "vnd.android-dir/mms-sms" );  
		  startActivityForResult(sendIntent, 1002 );  
	   }  

	public OnItemClickListener mItemclickListener=new OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			String mcontent=mtitle+":"+murl;
			// 设置分享内容
			mController.setShareContent(mcontent);
			//CommonUtil.showToast("fasdfasdf", arg1.getContext());
			// TODO Auto-generated method stub
			switch(arg2){
			  case 0:
				  
//				  ShareDialog tmpshare=new ShareDialog(this.getActivity());
//				  tmpshare.share("sdgdfgsd", "http://www.zhaorongbao.com/");
				  mController.postShare(mcontext,SHARE_MEDIA.WEIXIN_CIRCLE, 
					         mwxShareListener) ;
				  //shareToTimeLine();
				break;
			  case 1:
				  //shareToFriend() ;
				  mController.postShare(mcontext,SHARE_MEDIA.WEIXIN, 
					         mwxShareListener) ;
				break; 
			  case 2:
					mController.postShare(mcontext, SHARE_MEDIA.QQ,
							mShareListener);
				break;
			  case 3:
	 			 // 调用直接分享, 但是在分享前用户可以编辑要分享的内容
					mController.postShare(mcontext, SHARE_MEDIA.QZONE,
							mShareListener);
			    break;
			  case 4:
				  mController.postShare(mcontext, SHARE_MEDIA.SINA,
							mShareListener);
				break; 
			  case 5:
				 /* mController.postShare(this.getActivity(), SHARE_MEDIA.TENCENT,
							mShareListener);*/
				  mController.postShare(mcontext, SHARE_MEDIA.TENCENT,
							null);
				break;
		/*	  case 6:
				  sendSMS(mtitle+":"+murl);
				    break;*/
			  case 6:
				  sendMail(mtitle+":"+murl);
					break; 
				case 7:
					  ClipboardManager cmb = (ClipboardManager)mcontext.getSystemService(Context.CLIPBOARD_SERVICE);
					  cmb.setText(mtitle+":"+murl); // 复制
					  Toast.makeText(mcontext, "复制成功", Toast.LENGTH_SHORT)
						.show();
					break;
			}
			
		}
		
		
	};
	
 
}
