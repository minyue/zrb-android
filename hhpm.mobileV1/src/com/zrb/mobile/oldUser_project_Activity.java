package com.zrb.mobile;

import java.util.ArrayList;
import java.util.List;
 
import com.zrb.mobile.adapter.ProjectListchkAdapter;
import com.zrb.mobile.adapter.model.UserInfoDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.QMToggleView;
import com.zrb.mobile.ui.QMToggleView.IOnToggleListener;
import com.zrb.mobile.utility.CommonUtil;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class oldUser_project_Activity extends Activity implements OnClickListener, IOnToggleListener {

	 
	String mtitle, mUrl;
	TextView top_title;
	ProjectListchkAdapter checklistAdapter;
//	ListView mlistview;
	QMToggleView  qmtoggleview;
	int ReadStatus=0;
	PullToRefreshListView mPullRefreshListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//setContentView(R.layout.preferences_activity);
		setContentView(R.layout.u_center_project);
		//mUrl = this.getIntent().getStringExtra("url");
		//mtitle = this.getIntent().getStringExtra("title");
		 initView();
		//this.findViewById(R.id.project_container).setOnClickListener(this);
		//this.findViewById(R.id.login_icon).setOnClickListener(this);

	}
	
	private void initView() {
		this.findViewById(R.id.topbar_left).setOnClickListener(this);
		this.findViewById(R.id.new_btn).setOnClickListener(this);
		
		qmtoggleview=(QMToggleView)this.findViewById(R.id.note_qmtoggleview);	
		qmtoggleview.InitMenuView("项目");
		qmtoggleview.setOnToggleListener(this);
		
		top_title = (TextView) this.findViewById(R.id.tv_top_title);
		top_title.setText("待发布项目");
		//top_title.setClickable(true);
		top_title.setOnClickListener(this);
		
		//mlistview=(ListView)this.findViewById(R.id.myproject_listview)	;
		
		 mPullRefreshListView = (PullToRefreshListView) 	this.findViewById(R.id.myproject_listview);
		 
	 
		
		CheckBox proCheck=(CheckBox)this.findViewById(R.id.project_select_cb);
		
		 ImageView tmpImg = (ImageView)this.findViewById(R.id.topbar_title_arrow);
         tmpImg.setVisibility(View.VISIBLE);//.Visibility = ViewStates.Visible;
         tmpImg.setImageLevel(1);
         tmpImg.setOnClickListener(this);
 		
		proCheck.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				
			}});
		
		 List<UserInfoDto> dtos=new  ArrayList<UserInfoDto>();
		 for(int i=0;i<15;i++)
		 {
			 UserInfoDto tmpdto=new UserInfoDto();
			 tmpdto.name="test"+i;
			 tmpdto.phone="phone"+i;
			 
			 dtos.add(tmpdto);
			 
		 }
		checklistAdapter=new ProjectListchkAdapter(this,dtos);
		//mlistview.setAdapter(checklistAdapter);
		mPullRefreshListView.setAdapter(checklistAdapter);
		mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				// Do work to refresh the list here.
				//new GetDataTask().execute();
//				pageindex++;
//				LoadData();
			}
		});
	
		
		mPullRefreshListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				CommonUtil.showToast(arg2+"_dddddd", oldUser_project_Activity.this);
			}});
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId())
		{
		   case R.id.topbar_left:
 			   this.finish();
			break;
		   case R.id.login_icon:
			    Intent it = new Intent(this,oldUser_info_modifyActivity.class);
				startActivity(it); 
			   break;
		   case R.id.tv_top_title:
			   
			 //  CommonUtil.showToast("sdfsdfsd", this);
			   qmtoggleview.ShowmoreMenu();
			   break;
           case R.id.topbar_title_arrow:
        	   qmtoggleview.ShowmoreMenu();
               break;
           case R.id.new_btn:
        	    Intent it2 = new Intent(this,User_projectnew_Activity.class);
        	    it2.putExtra("title", "项目招商");
        		startActivity(it2); 
                break;
		
		}
		
	}

	  //0 未读  ,1已读，2全部
    private void ShowTitle()
    {
        if (ReadStatus==0)
        	top_title.setText("待发布项目");
        if(ReadStatus==1)
        	top_title.setText("已发布项目");
    }

   private void LoadData()
   {
	   //	mPullRefreshListView.onRefreshComplete();
	   ShowTitle();
   }
	
	@Override
	public void OnToggleFinished(int status) {
		// TODO Auto-generated method stub
		ReadStatus=status;
		LoadData();
	}
}
