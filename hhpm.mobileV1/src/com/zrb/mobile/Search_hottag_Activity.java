package com.zrb.mobile;



import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

 
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.mobile.adapter.HotRangeResultAdapter;
import com.zrb.mobile.adapter.RadioItem;
import com.zrb.mobile.adapter.SearchResultAdapter;
import com.zrb.mobile.adapter.model.HotRankDto;
import com.zrb.mobile.adapter.model.HotRankDtos;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.ProjectDtos;
import com.zrb.mobile.adapter.model.TagDtos;
import com.zrb.mobile.adapter.model.TagsDto;
import com.zrb.mobile.ui.MultipleTextView;
import com.zrb.mobile.ui.MultipleTextView.OnMultipleTVItemClickListener;
import com.zrb.mobile.ui.WordWrapView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.applib.utils.FileUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class Search_hottag_Activity extends BaseActivity implements OnMultipleTVItemClickListener, OnClickListener, OnTouchListener{
 
	ListView mlistview;
	HotRangeResultAdapter mResultAdapter;
	ImageView refleshimg;
	Animation roundanim ;
	WordWrapView	 wordWrapView ;
	List<TagsDto> curTags;
	Handler mhander=new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
      //  requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.search_hottag_activity);
        initview();
        loadKeywords();
       
    }
    
    private void initview(){
    	
    	this.findViewById(R.id.title_back).setOnClickListener(this);
    	this.findViewById(R.id.searchButton).setOnClickListener(this);
    	this.findViewById(R.id.SearchText).setOnTouchListener(this);//edittext
    	
    	this.findViewById(R.id.reflesh_btn).setOnClickListener(this);
    	refleshimg=	(ImageView)this.findViewById(R.id.reflesh_img);
    	refleshimg.setOnClickListener(this);
      	roundanim = AnimationUtils.loadAnimation(this, R.anim.round_loading);
      	
    	wordWrapView = (WordWrapView) this.findViewById(R.id.view_wordwrap);
	  
	  
        mlistview=(ListView)this.findViewById(R.id.search_app_list_view);
        mlistview.setLayoutAnimation(getListAnim());
        mResultAdapter=new HotRangeResultAdapter(this); 
       
        mlistview.setAdapter(mResultAdapter);
        mlistview.setOnItemClickListener(new OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			 //CommonUtil.showToast(arg2+"_mlist",Search_hottag_Activity.this);
			Intent intent = new Intent(Search_hottag_Activity.this, News_WebContentActivity.class);
 		    HotRankDto tmpDto=	mResultAdapter.CurrentDatas.get(arg2);
		 
			intent.putExtra("title", tmpDto.title);
			intent.putExtra("newid",tmpDto.id);
			 
			startActivity(intent);
		}}); 
        
    }
    
    private LayoutAnimationController getListAnim() {  
        AnimationSet set = new AnimationSet(true);  
        Animation animation = new AlphaAnimation(0.0f, 1.0f);  
        animation.setDuration(300);  
        set.addAnimation(animation);  
  
        animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,  
        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,  
        -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);  
        animation.setDuration(300);  
        set.addAnimation(animation);  
        LayoutAnimationController controller = new LayoutAnimationController( set, 0.5f);  
        controller.setInterpolator(this, android.R.anim.accelerate_interpolator);
        return controller;  
   }  
    
    private void loadAnimation() {
    	 
         // ������ת����
        AnimationSet set = new AnimationSet(false);
        Animation animation ;   
      
        animation = new RotateAnimation(180, 10);
        animation.setDuration(1000);
        set.addAnimation(animation);
         
        LayoutAnimationController controller = new LayoutAnimationController(set, 1);
        controller.setInterpolator(this, android.R.anim.accelerate_interpolator);
        controller.setAnimation(set);
     
        mlistview.setLayoutAnimation(controller);
        mlistview.startLayoutAnimation();
    }
     
    @Override
	public void onMultipleTVItemClick(View view, int position) {
		// TODO Auto-generated method stub
		//Toast.makeText(getApplicationContext(), "sssss"+position, Toast.LENGTH_SHORT).show();
    	  Intent it = new Intent(this, Search_resultActivity.class);
    	  it.putExtra("tag", curTags.get(position).labelName+",");
    	  it.putExtra("value", position);
	 	  startActivity(it);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		  case R.id.title_back:
				
			  this.finish();
		      break;
		  case R.id.searchButton:
			  Intent it = new Intent(this, Search_resultActivity.class);
		 	  startActivity(it);
		 	 // this.finish();
			  break;
		  case R.id.reflesh_img:
		  case R.id.reflesh_btn:
			  refleshRank();
			  break;
		
		}
	}

	private void loadKeywords()
	{
  		ZrbRestClient.get("news/querySearchLabel", null, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
 			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				mhander.postDelayed(new Runnable(){
 					@Override
					public void run() {
 						refleshimg.clearAnimation();
					}
	 			}, 1500);
				
				String results = new String(arg2);	
				FileUtil.writeFileSdcard(FileUtil.tagName, results);
				
				Gson gson =new Gson();  
		            
				TagDtos tmpDto=	gson.fromJson(results, TagDtos.class); 
				curTags=tmpDto.data;
			    int fontcolor=	wordWrapView.getContext().getResources().getColor(R.color.hot_rank_fontcolor);
				  for (int i = 0; i < tmpDto.data.size(); i++) {
				      TextView textview = new TextView(Search_hottag_Activity.this);
				      textview.setText(tmpDto.data.get(i).labelName);
				      textview.setTag(i);
				      textview.setGravity(Gravity.CENTER);
				      textview.setTextColor(fontcolor); 
				      textview.setTextSize(11);
				      textview.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
									onMultipleTVItemClick(v, (Integer) v.getTag());
							}
						});
				      wordWrapView.addView(textview);
				      refleshRank();
				     //wordWrapView.invalidate();
				    } 
			}
		});
	    	
	}
	
 
	private void refleshRank()
	{
	    refleshimg.startAnimation(roundanim) ;
 		ZrbRestClient.get("hotPoular/querHotPoularAPI", null, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
 			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				mhander.postDelayed(new Runnable(){
 					@Override
					public void run() {
 						refleshimg.clearAnimation();
					}
	 			}, 1500);
				
				String results = new String(arg2);				 
				Gson gson =new Gson();  
		            
				HotRankDtos tmpDto=	gson.fromJson(results, HotRankDtos.class); 
				mResultAdapter.setDatalist(tmpDto.data);
				mResultAdapter.notifyDataSetChanged();
				mlistview.startLayoutAnimation();
			}
		});
	    	
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
		Intent it = new Intent(this, Search_resultActivity.class);
 		startActivity(it);
 		//this.finish();
		return true;
	}
   
     
  
}
