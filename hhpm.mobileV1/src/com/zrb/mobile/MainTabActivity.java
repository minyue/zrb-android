package com.zrb.mobile;

import java.util.ArrayList;
import java.util.List;
 
import com.hhpm.lib.support.GlobalContext;
import com.nostra13.universalimageloader.core.ImageLoader;

import com.umeng.update.UmengUpdateAgent;
import com.zrb.applib.utils.LocalUserInfo;
import com.zrb.discover.Discover_pubishmoneyActivity;
import com.zrb.discover.Discover_publishprojectActivity;
import com.zrb.mobile.common.OnlineServer;
import com.zrb.mobile.expandmenu.ExpandableButtonMenu;
import com.zrb.mobile.expandmenu.ExpandableMenuOverlay;
import com.zrb.mobile.utility.AlertDialog;
import com.zrb.mobile.utility.FragmentTabAdapter2;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainTabActivity extends BaseFragmentActivity {

	private RelativeLayout tab_icon_home, tab_icon_news, tab_icon_project,
			tab_icon_user;
	private View seltextView;
	FragmentTabAdapter2 tabAdapter;
	public List<Fragment> fragments = new ArrayList<Fragment>();
	Dialog PopDialog;
	TextView msgunread;

	Message_MainFragment messgaeFragment;
	checkListener checkradio;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
 		
		setContentView(R.layout.main_tab_activity2);
		UmengUpdateAgent.update(this);
		initView();
		bindService();
	}

	
	 
	private void initView() {
		LocalUserInfo.getInstance(MainTabActivity.this).setHaveGuide();
		msgunread = (TextView) findViewById(R.id.bottom_tag_update3);
		tab_icon_home = (RelativeLayout) findViewById(R.id.tab_home);
		tab_icon_news = (RelativeLayout) findViewById(R.id.tab_news);
		tab_icon_project = (RelativeLayout) findViewById(R.id.tab_project);
		tab_icon_user = (RelativeLayout) findViewById(R.id.tab_user);
		tab_icon_home.setSelected(true);
		seltextView = tab_icon_home;

		 checkradio = new checkListener();

		tab_icon_home.setOnClickListener(checkradio);
		tab_icon_news.setOnClickListener(checkradio);
		tab_icon_project.setOnClickListener(checkradio);
		tab_icon_user.setOnClickListener(checkradio);
		// fragments.add(new News_Main2Fragment());
		fragments.add(new Discover_MainFragment());

		fragments.add(new Choiceness_MainFragment());
		messgaeFragment = new Message_MainFragment();
		fragments.add(messgaeFragment);

		fragments.add(new User_centerfragment());
		tabAdapter = new FragmentTabAdapter2(this, fragments, R.id.tab_content);
		
		 final   ExpandableMenuOverlay  menuOverlay = (ExpandableMenuOverlay) findViewById(R.id.button_menus);

	     menuOverlay.setOnMenuButtonClickListener(new ExpandableButtonMenu.OnMenuButtonClick() {
	         @Override
	         public void onClick(ExpandableButtonMenu.MenuButton action) {
	             switch (action) {
	                 case MID:
	                   //  Toast.makeText(Splash_Activity.this, "Mid pressed and dismissing...", Toast.LENGTH_SHORT).show();
	                     menuOverlay.getButtonMenu().toggle();
	                     GlobalContext.getInstance().getHandler().postDelayed(new Runnable(){
	 							@Override
								public void run() {
	 								 Discover_publishprojectActivity.launch(MainTabActivity.this);
								}
	 	                	 }, 320);
	                    
	                     break;
	                 case LEFT:
	                	 menuOverlay.getButtonMenu().toggle();
 	                	 GlobalContext.getInstance().getHandler().postDelayed(new Runnable(){
 							@Override
							public void run() {
	 							 Discover_pubishmoneyActivity.launch(MainTabActivity.this);
							}
 	                	 }, 320);
	                	
	                    // Toast.makeText(Splash_Activity.this, "Left pressed", Toast.LENGTH_SHORT).show();
	                     break;
	                 case RIGHT:
	                    // Toast.makeText(Splash_Activity.this, "Right pressed", Toast.LENGTH_SHORT).show();
	                     break;
	             }
	         }
	     }); 
	}


	 
	
	
	@Override
	protected void onResume() {

		super.onResume();
	}
 
	@Override
	protected void onNewIntent(Intent intent) {
	 
		super.onNewIntent(intent);
	    setIntent(intent);//must store the new intent unless getIntent() will return the old one
	    int curShowIndex=  intent.getIntExtra("showtab", -1);
	    if(curShowIndex!=-1&&null!=checkradio){
	    	checkradio.onClick(curShowIndex==1 ? tab_icon_news:tab_icon_project);
	    }
 	}
	
	// ������
	public class checkListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (seltextView.getId() == v.getId())
				return;
			if (v.getId() != -1) {
				v.setSelected(true);
				seltextView.setSelected(false);
				seltextView = v;
			}
			switch (v.getId()) {
			case R.id.tab_home:
				tabAdapter.setCheckChanged(0);
  				break;
			case R.id.tab_news:
				tabAdapter.setCheckChanged(1);
				break;
			case R.id.tab_project:
				tabAdapter.setCheckChanged(2);
				break;
			case R.id.tab_user:
				tabAdapter.setCheckChanged(3);
  				break;
			}
		}
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
 
			 new AlertDialog(MainTabActivity.this).builder().setTitle("操作提示")
			   .setMsg("确定要退出系统吗?")
			   .setPositiveButton("确认退出", new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(MainTabActivity.this, OnlineServer.class);
					MainTabActivity.this.stopService(intent);
					MainTabActivity.this.finish();
					ImageLoader.getInstance().destroy();
					System.exit(0);
				}
			    }).setNegativeButton("取消").show();
			 return false;
 		}
		return super.dispatchKeyEvent(event);
	}

 
	
	private void bindService() {
		Intent intent = new Intent(MainTabActivity.this, OnlineServer.class);
		// bindService(intent, conn, Context.BIND_AUTO_CREATE);
		this.startService(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		/*
		 * // ������淢�͹�ȥ������������� if (resultCode ==
		 * android.app.Activity.RESULT_OK) { switch (requestCode) { case 234:
		 * isConflict=false; initListener(); break; case 235: isConflict=false;
		 * initListener(); if(messgaeFragment!=null)
		 * messgaeFragment.refreshNotice(); break; default: break; } }
		 */
		super.onActivityResult(requestCode, resultCode, data);
	}

 
	
	
}
