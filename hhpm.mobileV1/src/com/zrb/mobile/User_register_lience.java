package com.zrb.mobile;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.adapter.model.ProjectDtos;
import com.zrb.mobile.adapter.model.UserInfoResult;
import com.zrb.mobile.adapter.model.UserRegisterDto;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class User_register_lience extends BaseActivity implements OnClickListener {

	CenterProgressWebView mWebView;
    String mtitle="注册协议";
    String murl="agreement.html";
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		 
		setContentView(R.layout.u_center_register_lience);
		// setContentView(R.layout.user_register_activity);
		
		if (bundle != null)
			restoreSelf(bundle);
		else {
			String tmptitle = this.getIntent().getStringExtra("pagetitle");
			String turl = this.getIntent().getStringExtra("pageurl");

			mtitle = TextUtils.isEmpty(tmptitle) ? mtitle : tmptitle;
			murl = TextUtils.isEmpty(turl) ? murl : turl;
		}
		initView();
	}
 	 
	@Override
	 protected  void onSaveInstanceState(Bundle outState)
    {
       super.onSaveInstanceState(outState);
       outState.putString("pagetitle", mtitle);
       outState.putString("pageurl", murl);
    }

    private void restoreSelf(Bundle savedInstanceState)
    {
    	mtitle = savedInstanceState.getString("pagetitle");
    	murl = savedInstanceState.getString("pageurl");
    }
	
	private void initView() {
	 
 		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText(mtitle);
		
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
 		
		mWebView = (CenterProgressWebView) this.findViewById(R.id.webView1);

		// http://backend.zhaorongbao.com:8090/newsapp/newsDetails?id=26
 		String url =AppSetting.BASE_URL+"page/"+murl;
		mWebView.loadUrl(url);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;

	 
		}

	}

}
