package com.zrb.mobile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

 
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.applib.utils.AppSetting;
import com.zrb.applib.utils.LocalUserInfo;
import com.zrb.mobile.common.LoadUserAvatar;
import com.zrb.mobile.common.OnlineServer;
import com.zrb.mobile.common.LoadUserAvatar.ImageDownloadedCallBack;
import com.zrb.mobile.register.UserRegister;
import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.ui.CircleImageView;
import com.zrb.mobile.utility.AlertDialog;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.mobile.utility.DownloadManager;

public class User_app_setting extends BaseActivity implements OnClickListener {
 
	Dialog AuthDialog;
	TextView tmpname;
	TextView tmplogin;
	TextView tmpregister;
	 View tmplogout; 
	 LoadUserAvatar avatarLoader;
	 CircleImageView mavatar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  
		setContentView(R.layout.activity_setting);
		initView();
	}

//http://backend.zhaorongbao.test:8090/aboutZRB.html
	private void initView() {
	 
		this.findViewById(R.id.title_left_root).setOnClickListener(this);;
  	    TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("设  置");
 	    
	    tmpname=	(TextView)this.findViewById(R.id.tv_name);
 	    tmplogin=(TextView)this.findViewById(R.id.tv_login);
	    tmpregister=(TextView)this.findViewById(R.id.tv_register);
 	   
	    mavatar= (CircleImageView)this.findViewById(R.id.iv_avatar);
	    
	    tmplogin.setOnClickListener(this);
	    tmpregister.setOnClickListener(this);
 	   
	    
	    
 	    this.findViewById(R.id.appsetting_update).setOnClickListener(this);
 	    this.findViewById(R.id.appsetting_item_aboutus).setOnClickListener(this);
 	    this.findViewById(R.id.appsetting_item_share).setOnClickListener(this);
 	    this.findViewById(R.id.appsetting_item_suggest).setOnClickListener(this);
 	   this.findViewById(R.id.appsetting_item_help).setOnClickListener(this);
 	   
 	    avatarLoader = new LoadUserAvatar(this);
 	    tmplogout=this.findViewById(R.id.logout_btn);
  	    tmplogout.setOnClickListener(this);
  	}

	@Override
	public void onResume(){
 		super.onResume();
		showRelateview();
	}
	
	private void showRelateview(){
		  boolean isLogin=AppSetting.curUser==null  ? false:true;
		  tmpname.setText(!isLogin? "":AppSetting.curUser.uname);
		  tmplogin.setVisibility(!isLogin ?View.VISIBLE:View.GONE);
		  tmpregister.setVisibility(!isLogin ?View.VISIBLE:View.GONE);
 		  tmplogout.setVisibility(AppSetting.curUser==null ? View.GONE:View.VISIBLE);
 		  
 		  if(isLogin)  showUserAvatar(mavatar, AppSetting.curUser.avatar);
	}
	
	private void showUserAvatar(ImageView iamgeView, String avatar) {
		
	    if(TextUtils.isEmpty(avatar)) return;
        final String url_avatar =AppSetting.BASE_URL + avatar;
        iamgeView.setTag(url_avatar);
        if (url_avatar != null && !url_avatar.equals("")) {
            Bitmap bitmap = avatarLoader.loadImage(iamgeView, url_avatar,
                    new ImageDownloadedCallBack() {

                        @Override
                        public void onImageDownloaded(ImageView imageView,
                                Bitmap bitmap) {
                            if (imageView.getTag() == url_avatar) {
                                imageView.setImageBitmap(bitmap);
	                            }
                        }
                   });
            if (bitmap != null)
                iamgeView.setImageBitmap(bitmap);

        }
    }
	
	void logout() {
		AppSetting.curUser=null;
		LocalUserInfo.getInstance(User_app_setting.this).clearUserInfo();
    	User_app_setting.this.finish();
	}
	
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		 case R.id.title_left_root:
			  this.finish();
			  break;
		  case R.id.tv_login:
			  User_phone_login.launch(this, false);
  			  break;
 		    case R.id.tv_register:
 		    	UserRegister.launch(this);
  		        break;
		    case R.id.appsetting_update:
		    	DownloadManager downManger = new DownloadManager(this,true);  
		    	downManger.checkDownload();  
 		    	break;
 		    	
		    case R.id.appsetting_item_help:
//		    	  Intent it6 = new Intent(this, User_register_lience.class);
//	 		      it6.putExtra("pageurl","help.html");//
//	 		      it6.putExtra("pagetitle","使用帮助");
//	 		      startActivity(it6);
		    	CommonUtil.callServerPhone(this);
		    
 		    	break;
		    case R.id.appsetting_item_aboutus:
 		        Intent it = new Intent(this, User_register_lience.class);
 		        it.putExtra("pageurl","aboutZRB.html");//help.html
 		        it.putExtra("pagetitle","关于招融宝");
 				 startActivity(it);
 				break;
 				
		    case R.id.appsetting_item_share:
		    	 Intent it2 = new Intent(this, App_shareActivity.class);
	 			 startActivity(it2);
		    	break;
		    case R.id.appsetting_item_suggest:
		    	 Intent it3 = new Intent(this, App_suggestActivity.class);
	 			 startActivity(it3);
		    	break; 	
		    case R.id.logout_btn:
		    	logout();
		    	break;
		    	
		}

	}

	 
	
}

