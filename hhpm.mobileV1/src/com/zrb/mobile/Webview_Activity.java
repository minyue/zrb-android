package com.zrb.mobile;

import com.zrb.mobile.ui.CenterProgressWebView;

import android.os.Bundle;
import android.app.Activity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.TextView;

public class Webview_Activity extends Activity implements OnClickListener {

	CenterProgressWebView mWebView;
	String mtitle, mUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.webview_activity);

		mUrl = this.getIntent().getStringExtra("url");
		mtitle = this.getIntent().getStringExtra("title");
		initView();

	}
	
	private void initView() {
		this.findViewById(R.id.title_left_root).setOnClickListener(this);

		TextView titleview = (TextView) this.findViewById(R.id.titleText);
		titleview.setText(mtitle);

		mWebView = (CenterProgressWebView) this.findViewById(R.id.webView1);

		//mUrl="file:///android_asset/www/index.html";
		
		if (!TextUtils.isEmpty(mUrl))	mWebView.loadUrl(mUrl);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId())
		{
		   case R.id.title_left_root:
			   
			   if(mWebView.canGoBack()) mWebView.goBack();
			   else
				   this.finish();
			break;
		
		
		}
		
	}
}
