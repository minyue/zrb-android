package com.zrb.mobile;

import org.apache.http.Header;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.mobile.register.UserRegister;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

public class User_forget_Password extends BaseActivity implements OnClickListener  {

	ImageView btnAccountclear;
  	 
	Tip progTip;
   
    private EditText mobileText;
    private String mobile;
    private Button send_btn;
    private RelativeLayout notify_view;
  	private TextView notify_view_text;
    Handler mhandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		 
		setContentView(R.layout.u_center_forgetpwd);
	 	initView();
		 
	}

	private void initView() {
		mobileText = (EditText) findViewById(R.id.edit_phone);
		mobileText.addTextChangedListener(onPhoneTextChanged);
		notify_view = (RelativeLayout)this.findViewById(R.id.notify_view);
		notify_view_text = (TextView)this.findViewById(R.id.notify_view_text);
		send_btn = (Button) findViewById(R.id.send_btn);
		
		btnAccountclear = (ImageView) this.findViewById(R.id.phone_btn_clean);
		btnAccountclear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mobileText.setText("");
			}
		});
		
		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("找回密码");
		this.findViewById(R.id.send_btn).setOnClickListener(this);
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		
		this.findViewById(R.id.right_title).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserRegister.launch(User_forget_Password.this);
			}
		});
		
	}
	
 
 	 
	private TextWatcher onPhoneTextChanged = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {
			 
			//CommonUtil.showToast("afterTextChanged", User_forget_Password.this);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			//CommonUtil.showToast("beforeTextChanged", User_forget_Password.this);
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		/*	if (editTextphone.getText().length() == 0)
				btnAccountclear.setVisibility(View.INVISIBLE);
			else
				btnAccountclear.setVisibility(View.VISIBLE);*/
			
			//CommonUtil.showToast("onTextChanged", User_forget_Password.this);
			if( s.length() != 0){
				btnAccountclear.setVisibility(View.VISIBLE);
			}else{
				btnAccountclear.setVisibility(View.GONE);
			}
			if( s.length() == 13){
				send_btn.setEnabled(true);
				send_btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_blue));
			}else{
				send_btn.setEnabled(false);
				send_btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_blue2));
			}
			
		    String contents = s.toString();
		    int length = contents.length();
  		 
			if (length == 4) {
				if (contents.substring(3).equals(" ")) { // -
					contents = contents.substring(0, 3);
					mobileText.setText(contents);
					mobileText.setSelection(contents.length());
				} else { // +
					contents = contents.substring(0, 3) + " "+ contents.substring(3);
					mobileText.setText(contents);
					mobileText.setSelection(contents.length());
				}
			} else if (length== 9) {
				if (contents.substring(8).equals(" ")) { // -
					contents = contents.substring(0, 8);
					mobileText.setText(contents);
					mobileText.setSelection(contents.length());
				} else {// +
					contents = contents.substring(0, 8) + " "+ contents.substring(8);
					mobileText.setText(contents);
					mobileText.setSelection(contents.length());
				}
			}
			 else if(length== 11){
				if(!contents.contains(" ")){
					
					contents = contents.substring(0, 3) + " "+ contents.substring(3,7)+ " "+contents.substring(7);
					mobileText.setText(contents);
					mobileText.setSelection(contents.length());
				}
				
			} 
		/*	else{
				
				if(length>9){
                   if (!contents.substring(8).equals(" ")){
			    		contents = contents.substring(0, 8) + " "+ contents.substring(8).replace(" ", "");
						mobileText.setText(contents);
						mobileText.setSelection(contents.length());
			    	}
 				}
				else if(length>4){
                    if (!contents.substring(3).equals(" ")){
			    		
			    		contents = contents.substring(0, 3) + " "+ contents.substring(3).replace(" ", "");
						mobileText.setText(contents);
						mobileText.setSelection(contents.length());
			    	}
				}
				
				
			}*/
			  
		}
	};

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.send_btn:
			checkPhone();
			break;
		}

	}

 
	private void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在提交中...", true,
					true);
		else
			progTip.Show("正在提交中...");
	}

	private void checkPhone() {
		mobile = this.mobileText.getText()==null ? "":this.mobileText.getText().toString().replace(" ", "");
		if (TextUtils.isEmpty(mobile)) {
			initNotify("手机号不能为空");
			return ;
 		}
 		if(!CommonUtil.isMobileNO(mobile))//CommonUtil.isMobileNO(this.editTextphone.getText()
		{
 			initNotify("手机号不正确");
  			return ;
		}
		
		showloading();
		String loginurlparms = "?mobile=" + mobile;
		ZrbRestClient.get("register/checkMobileRegister" + loginurlparms, null,
				new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
 						progTip.Dismiss();
 						initNotify(User_forget_Password.this.getString(R.string.error_net_loadfail));
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						progTip.Dismiss();
						String results = new String(arg2);
 						try {
							JSONObject object = new JSONObject(results);
							int res = object.getInt("res");
							String msg = object.getString("msg");//{"res":1,"msg":"手机号未注册！","data":null}
							if( res == 0 ){
	 							sendSMS();
							}else{
								initNotify(msg);
							}
 
						} catch (Exception e) {
							initNotify(User_forget_Password.this.getString(R.string.error_serverdata_loadfail));
							//CommonUtil.showToast( "fail:"+e.getMessage(),User_forget_Password.this);
 						}
 					}
				});

	}

	private void sendSMS(){
		String loginurlparms = "?mobile=" + mobile;// register/sendSMSFindPW
		ZrbRestClient.get("register/sendFindPasswordAuthCode" + loginurlparms, null,
				new AsyncHttpResponseHandler() {
					
					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						progTip.Dismiss();
						String results = new String(arg2);
						
						try {
							JSONObject object = new JSONObject(results);
							int res = object.getInt("res");
							String msg = object.getString("msg");
							if( res == 1){
								showLogin();
							}else{
								initNotify(msg);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
						// TODO Auto-generated method stub
						CommonUtil.showToast("fail", User_forget_Password.this);
					}
				});
	}
	
	
	private void showLogin()
	{
 		//CommonUtil.showToast("ok",	User_forget_Password.this);
 		Intent it = new Intent(this, User_change_Password.class);
 		it.putExtra("mobile", mobile);
		startActivity(it);
		this.finish();
 	}
	
 
	private void initNotify(final String msg) {
		mhandler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				notify_view_text.setText(msg);
				notify_view.setVisibility(View.VISIBLE);
				mhandler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						notify_view.setVisibility(View.GONE);
					}
				}, 2000);
			}
		}, 1000);
	}
	 
 	 
}
