package com.zrb.mobile;

import org.apache.http.Header;
 
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.ui.NumEditText;
import com.zrb.mobile.ui.NumEditText.OnNumslistener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class User_my_surveyback extends BaseImgActivity implements OnClickListener,OnNumslistener{

	int questionType = 0;
	NumEditText numEditText;
	private int selectId=-1;
	TextView right_title;
	private TextView txtNum;
	int curSurveyStatus=-1;
	Dialog AuthDialog;
	int inspectId;
	Tip progTip;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
 		setContentView(R.layout.u_surveyback);
 		if (bundle != null)
			restoreSelf(bundle);
		else {
			inspectId=this.getIntent().getIntExtra("inspectId", -1);
		}
 		
 		initView();
	}
	
	 private void restoreSelf(Bundle savedInstanceState)
	    {
	    	inspectId = savedInstanceState.getInt("inspectId",-1);
	    }
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.right_title:
			cancelSurvey();
			break;
		}
 	}
 
	 private void initView(){
		 
		 	View lefticon= this.findViewById(R.id.title_left_root);
			lefticon.setOnClickListener(this);
	    	right_title = (TextView) this.findViewById(R.id.right_title);
	    	right_title.setVisibility(View.VISIBLE);
	    	right_title.setText("提交");
			right_title.setOnClickListener(this);
			TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
			txtTitle.setText("取消勘察");
		 
		 	numEditText = (NumEditText) this.findViewById(R.id.editText1t);
			numEditText.setMaxNums(30);
			numEditText.setOnNumslistener(this);
			txtNum = (TextView) this.findViewById(R.id.txtNum2);
			
			
			LinearLayout tmpContainer1=(LinearLayout)this.findViewById(R.id.check1_option1);
	    	for(int i=0;i<tmpContainer1.getChildCount();i++)
	    		tmpContainer1.getChildAt(i).setOnClickListener(checkboxListener);
	    	
	    	LinearLayout tmpContainer2=(LinearLayout)this.findViewById(R.id.check1_option2);
	    	for(int i=0;i<tmpContainer2.getChildCount();i++)
	    		tmpContainer2.getChildAt(i).setOnClickListener(checkboxListener);
	 }
	
	
	private OnClickListener checkboxListener=new OnClickListener(){
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
 			if(v.getId()!=selectId){
 				v.setSelected(true);
				if(selectId!=-1)
					User_my_surveyback.this.findViewById(selectId).setSelected(false);;
				selectId=v.getId();
			}
 			
 			switch (v.getId()) {
 			case R.id.tv_temp1_name1:
 				questionType = 101;//我不想看了
 				break;
 			case R.id.tv_temp1_name2:
 				questionType = 102;//行程有变
 				break;
 			case R.id.tv_temp1_name3:
 				questionType = 103;//信息错误重新提交
 				break;
 			case R.id.tv_temp1_name7:
 				questionType = 104; //其它
 				break;
 			}
 			
		}
	}
	;

	@Override
	public void showtextNum(int number) {
		txtNum.setText(number+"");
	}
	
	private void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在取消中...", true,
					true);
		else
			progTip.Show("正在取消中...");
	}
	
	 private void cancelSurvey(){
		if( 0==questionType){
			CommonUtil.showToast("请选择类型", User_my_surveyback.this);
			return;
		}
		 String reason = "";
		 if( !TextUtils.isEmpty(numEditText.getText())){
			 reason = numEditText.getText().toString();
		 }
		 showloading() ;
		 	RequestParams param = new RequestParams();
			param.put("inspectId",inspectId);
			param.put("zrb_front_ut", AppSetting.curUser.zrb_front_ut);
			param.put("reason", reason);
			param.put("reasonType", questionType);
		   ZrbRestClient.post("inspect/userInspectCancel", param,  new AsyncHttpResponseHandler()
	       {
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
 					 progTip.Dismiss();
					  CommonUtil.showToast("操作异常", User_my_surveyback.this);
				}
			   @Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
 				    progTip.Dismiss();
					String results=new String(arg2);
					CommonUtil.InfoLog("cancelSurvey", results);
					try 
					{
				       Gson gson = new Gson();  
				       RegisterDto tmpDto=	gson.fromJson(results, RegisterDto.class);//{"res":1,"msg":null,"data":null,"bannerData":null}
	 			       if(tmpDto.res==1) {
	 			    	  CommonUtil.showToast("取消成功", User_my_surveyback.this);
	 			    	  new Handler().postDelayed(new Runnable() {
		                        @Override
		                        public void run() {
		                        	 User_my_surveyback.this.setResult(RESULT_OK);
		    	 			    	 User_my_surveyback.this.finish();
	 	                        }
		                }, 2000);
 				     }
				      else
				    	  CommonUtil.showToast(tmpDto.msg, User_my_surveyback.this);
	 			    	 
	    			}
					catch(Exception e){
						  CommonUtil.showToast(e.getMessage(), User_my_surveyback.this);
	 				}
	 			}});
	   }
 	 
}
