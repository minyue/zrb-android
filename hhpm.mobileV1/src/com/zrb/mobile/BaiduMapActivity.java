/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zrb.mobile;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.BDNotifyListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMapOptions;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.CoordinateConverter;
import com.baidu.mapapi.utils.route.BaiduMapRoutePlan;
import com.baidu.mapapi.utils.route.RouteParaOption;
import com.zrb.mobile.R;
 

public class BaiduMapActivity extends BaseFragmentActivity implements OnClickListener {

	private final static String TAG = "map";
	static MapView mMapView = null;
	FrameLayout mMapViewContainer = null;
	// ��λ���
	LocationClient mLocClient;
	public MyLocationListenner myListener = new MyLocationListenner();
	public NotifyLister mNotifyer = null;

	Button sendButton = null;

	EditText indexText = null;
	int index = 0;
	// LocationData locData = null;
	static BDLocation lastLocation = null;
	//public static BaiduMapActivity instance = null;
	ProgressDialog progressDialog;
	private BaiduMap mBaiduMap;
	
	private LocationMode mCurrentMode;
	private InfoWindow mInfoWindow;
	TextView mtxtTitle;
	double latitude=0;
	double longtitude=0;
	String address="";
	/**
	 * ����㲥�����࣬���� SDK key ��֤�Լ������쳣�㲥
	 */
	public class BaiduSDKReceiver extends BroadcastReceiver {
		public void onReceive(Context context, Intent intent) {
			String s = intent.getAction();
			String st1 = getResources().getString(R.string.Network_error);
			if (s.equals(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_ERROR)) {
				
				String st2 = getResources().getString(R.string.please_check);
				Toast.makeText(BaiduMapActivity.this, st2, Toast.LENGTH_SHORT).show();
			} else if (s.equals(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR)) {
				Toast.makeText(BaiduMapActivity.this, st1, Toast.LENGTH_SHORT).show();
			}
		}
	}

	private BaiduSDKReceiver mBaiduReceiver;
	
 	@Override
	protected  void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putDouble("latitude", latitude);
        outState.putDouble("longitude", longtitude);
        outState.putString("address", address);
    }

    private void restoreSelf(Bundle savedInstanceState){
    	latitude = savedInstanceState.getDouble("latitude",0);
    	longtitude= savedInstanceState.getDouble("longitude",0);
    	address = savedInstanceState.getString("address");
    } 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//instance = this;
		//��ʹ��SDK�����֮ǰ��ʼ��context��Ϣ������ApplicationContext  
        //ע��÷���Ҫ��setContentView����֮ǰʵ��  
        SDKInitializer.initialize(getApplicationContext());  
		setContentView(R.layout.activity_baidumap);
 	
		if (savedInstanceState != null)
			restoreSelf(savedInstanceState);
		else {
			 Intent intent = getIntent();
			 latitude = intent.getDoubleExtra("latitude", 0);
			 longtitude = intent.getDoubleExtra("longitude", 0);
			 address = intent.getStringExtra("address");
		}
		initView();
		mCurrentMode = LocationMode.NORMAL;
		mBaiduMap = mMapView.getMap();
		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(15.0f);
		mBaiduMap.setMapStatus(msu);
		initMapView();
		if (latitude == 0) {
			mMapView = new MapView(this, new BaiduMapOptions());
			mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(mCurrentMode, true, null));
			showMapWithLocationClient();
		} else {
			
			LatLng p = new LatLng(latitude, longtitude);
			mMapView = new MapView(this,new BaiduMapOptions().mapStatus(new MapStatus.Builder().target(p).build()));
			showMap(latitude, longtitude, address);
		}
		// ע�� SDK �㲥������
		IntentFilter iFilter = new IntentFilter();
		iFilter.addAction(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_ERROR);
		iFilter.addAction(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR);
		mBaiduReceiver = new BaiduSDKReceiver();
		registerReceiver(mBaiduReceiver, iFilter);
	}

	private void initView(){
		
		mtxtTitle=(TextView)this.findViewById(R.id.txt_title);
		mtxtTitle.setText("地理位置");
		
		this.findViewById(R.id.title_left_root).setOnClickListener(this);
		mMapView = (MapView) findViewById(R.id.bmapView);
		sendButton = (Button) findViewById(R.id.btn_location_send);
		
	}
	
	private void showMap(double latitude, double longtitude, String address) {
		sendButton.setVisibility(View.GONE);
		LatLng llA = new LatLng(latitude, longtitude);
		CoordinateConverter converter= new CoordinateConverter();
		converter.coord(llA);
		converter.from(CoordinateConverter.CoordType.COMMON);
		LatLng convertLatLng = converter.convert();
		OverlayOptions ooA = new MarkerOptions().position(convertLatLng).icon(BitmapDescriptorFactory
				.fromResource(R.drawable.icon_marka))
				.zIndex(4).draggable(true);
		mBaiduMap.addOverlay(ooA);
		MapStatusUpdate u = MapStatusUpdateFactory.newLatLngZoom(convertLatLng, 17.0f);
		addPopWindow(address);
		mBaiduMap.animateMapStatus(u);
	}
	
	private boolean isNavClick=false;
	
	private void addPopWindow(String endAddress){
		
		mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			public boolean onMarkerClick(final Marker marker) {
				Button button = new Button(getApplicationContext());
				button.setBackgroundResource(R.drawable.popup_bg1);
				OnInfoWindowClickListener listener = null;
			 
				   button.setText("点击查看地图导航");
					button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
					button.setTextColor(Color.parseColor("#5f5f5f"));
 				   listener = new OnInfoWindowClickListener() {
						public void onInfoWindowClick() {
						 
							mBaiduMap.hideInfoWindow();
							isNavClick=true;
							showMapWithLocationClient() ;
							mLocClient.start();
						}
					};
					LatLng ll = marker.getPosition();
					mInfoWindow = new InfoWindow(BitmapDescriptorFactory.fromView(button), ll, -47, listener);
					mBaiduMap.showInfoWindow(mInfoWindow);
				  
				return true;
			}
		});
 	}
	

	private void showMapWithLocationClient() {
		String str1 = getResources().getString(R.string.Making_sure_your_location);
		progressDialog = new ProgressDialog(this);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(str1);

		progressDialog.setOnCancelListener(new OnCancelListener() {

			public void onCancel(DialogInterface arg0) {
				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				Log.d("map", "cancel retrieve location");
				finish();
			}
		});

		progressDialog.show();

		mLocClient = new LocationClient(this);
		mLocClient.registerLocationListener(myListener);

		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// ��gps
		// option.setCoorType("bd09ll"); //设置坐标类型
		// Johnson change to use gcj02 coordination. chinese national standard
		// so need to conver to bd09 everytime when draw on baidu map
		option.setCoorType("gcj02");
		option.setScanSpan(30000);
		option.setAddrType("all");
		mLocClient.setLocOption(option);
	}

	@Override
	protected void onPause() {
		mMapView.onPause();
		if (mLocClient != null) {
			mLocClient.stop();
		}
		super.onPause();
		lastLocation = null;
	}

	@Override
	protected void onResume() {
		mMapView.onResume();
		if (mLocClient != null) {
			mLocClient.start();
		}
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		if (mLocClient != null)
			mLocClient.stop();
		mMapView.onDestroy();
		unregisterReceiver(mBaiduReceiver);
		BaiduMapRoutePlan.finish(this);
		//if(instance!=null) instance=null;
		super.onDestroy();
	}
	private void initMapView() {
		mMapView.setLongClickable(true);
	}

	/**
	 * ����������λ�õ�ʱ�򣬸�ʽ�����ַ��������Ļ��
	 */
	public class MyLocationListenner implements BDLocationListener {
		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null) {
				return;
			}
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
 			if(isNavClick) {
				isNavClick=false;
				startRoutePlanDriving(location);
 				return;
			}
 			Log.d("map", "On location change received:" + location);
			Log.d("map", "addr:" + location.getAddrStr());
		    sendButton.setEnabled(true);
 
			if (lastLocation != null) {
				if (lastLocation.getLatitude() == location.getLatitude() && lastLocation.getLongitude() == location.getLongitude()) {
					Log.d("map", "same location, skip refresh");
					// mMapView.refresh(); //need this refresh?
					return;
				}
			}
			lastLocation = location;
			mBaiduMap.clear();
			LatLng llA = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
			CoordinateConverter converter= new CoordinateConverter();
			converter.coord(llA);
			converter.from(CoordinateConverter.CoordType.COMMON);
			LatLng convertLatLng = converter.convert();
			OverlayOptions ooA = new MarkerOptions().position(convertLatLng).icon(BitmapDescriptorFactory
					.fromResource(R.drawable.icon_marka))
					.zIndex(4).draggable(true);
			mBaiduMap.addOverlay(ooA);
			MapStatusUpdate u = MapStatusUpdateFactory.newLatLngZoom(convertLatLng, 17.0f);
			mBaiduMap.animateMapStatus(u);
		}

		public void onReceivePoi(BDLocation poiLocation) {
			if (poiLocation == null) {
				return;
			}
		}
	}

	public class NotifyLister extends BDNotifyListener {
		public void onNotify(BDLocation mlocation, float distance) {
		}
	}

	public void back(View v) {
		finish();
	}

	public void sendLocation(View view) {
		Intent intent = this.getIntent();
		intent.putExtra("latitude", lastLocation.getLatitude());
		intent.putExtra("longitude", lastLocation.getLongitude());
		intent.putExtra("address", lastLocation.getAddrStr());
		this.setResult(RESULT_OK, intent);
		finish();
		//overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
	}

	
	/**
	 * �����ٶȵ�ͼ�ݳ�·�߹滮
	 * 
	 */
	public void startRoutePlanDriving(BDLocation location) {
/*		LatLng pt_start = new LatLng(34.264642646862, 108.95108518068);
		LatLng pt_end = new LatLng(34.264642646862, 109.95108518068);//mLat2, mLon2
*/		
		double longtitude =this.getIntent().getDoubleExtra("longitude", 0);
		double latitude = this.getIntent().getDoubleExtra("latitude", 0);
		LatLng endPoint=new LatLng(latitude,longtitude);
 		
		LatLng llA = new LatLng(location.getLatitude(), location.getLongitude());
		CoordinateConverter converter= new CoordinateConverter();
		converter.coord(llA);
		converter.from(CoordinateConverter.CoordType.COMMON);
		LatLng convertLatLng = converter.convert();
		LatLng curPos=convertLatLng;
 		// ���� route��������
 		RouteParaOption para = new RouteParaOption()
			.startPoint(curPos)//pt_start
 			.startName(location.getAddrStr())
 			.endPoint(endPoint);
		/*	.endName("������")
			.cityName("����"); */
		
//		RouteParaOption para = new RouteParaOption()
//				.startName("�찲��").endName("�ٶȴ���");
					//new LatLng(latitude, longtitude);
	
		
		
/* 		RouteParaOption para = new RouteParaOption()
 		.startPoint(curPos).endPoint( new LatLng(latitude,longtitude));*/
		
		try {
			BaiduMapRoutePlan.openBaiduMapDrivingRoute(para, this);	
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this,"未安装百度地图，或百度地图版本太低",Toast.LENGTH_SHORT).show();
			//showDialog();
		}
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.title_left_root:
 		    this.finish();
			break;
	 
		default:
			break;
		}
	}

	
}
