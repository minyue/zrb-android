package com.zrb.mobile;

 

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

 
import com.google.gson.Gson;
import com.zrb.mobile.adapter.model.MsgNoticeDto;
import com.zrb.mobile.adapter.model.MyMessageDto;
import com.zrb.mobile.utility.MsgformatUtil;
import com.zrb.mobile.utility.Tip;

public class System_message_details  extends BaseActivity  implements OnClickListener  {

	private TextView right_title,topbartitle,txtContent,txtTitle,txtTime;
    Handler mhandler = new Handler();
    Tip progTip;
    Dialog AuthDialog;
   // String content,typeName,bizType,ctime,bizKey;
    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");//
  //Toast��ʾ��
  	private RelativeLayout notify_view;
  	private TextView notify_view_text;
    private String jsonObj;
    MyMessageDto curDto;    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
	    	setContentView(R.layout.system_message_details);
		/*	content  = this.getIntent().getStringExtra("content");
			typeName = this.getIntent().getStringExtra("typeName");
			bizType  = this.getIntent().getStringExtra("bizType");
			ctime  =  this.getIntent().getStringExtra("ctime");
			bizKey  =  this.getIntent().getStringExtra("bizKey");*/
	    	jsonObj=this.getIntent().getStringExtra("jsonObj");
	    	Gson gson = new Gson();  
	    	curDto= gson.fromJson(jsonObj, MyMessageDto.class);  
	 		InitView();
	}
	
	private void InitView(){
				
		//Toast��ʾ��
		notify_view = (RelativeLayout)this.findViewById(R.id.notify_view);
		notify_view_text = (TextView)this.findViewById(R.id.notify_view_text);
				
		topbartitle = (TextView) this.findViewById(R.id.txt_title);
		topbartitle.setText("系统通知");
		
		txtContent = (TextView) this.findViewById(R.id.sysnotice_content);
 		txtTitle = (TextView) this.findViewById(R.id.sysnotice_title);
	 	
		txtTime = (TextView) this.findViewById(R.id.sysnotice_time);
		
		txtTime.setText(sdf.format(curDto.messageInfo.createTime));
	 
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		
		MsgformatUtil.formatSummary(txtContent,txtTitle ,curDto);
 		
		right_title = (TextView) this.findViewById(R.id.right_title);
		right_title.setOnClickListener(this);
		right_title.setText("详情");
		boolean isHaveAction=MsgformatUtil.isNeedshow(curDto.messageInfo.bizType);
		right_title.setVisibility(isHaveAction? View.VISIBLE:View.GONE);
		 
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.right_title:
			/*Intent tmpAction=MsgformatUtil.getAction(this, curDto);
			if(tmpAction!=null) this.startActivity(tmpAction);*/
			break;
		}
 	}


	/* ��ʼ��֪ͨ��Ŀ*/
	private void initNotify(final String msg) {
		mhandler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				notify_view_text.setText(msg);
				notify_view.setVisibility(View.VISIBLE);
				mhandler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						notify_view.setVisibility(View.GONE);
					}
				}, 2000);
			}
		}, 1000);
	}
	
	
	
	
	/**
	 * ��Ҫ����վ����Ϣ��ҵ������ö��
	 * 
	 * @author leiruiqi@hanhai.com
	 *
	 */
	public enum MessageBizType {

		/**
		 * ע��ɹ�
		 */
		user_regiester_succes("��ϲ��ע��ɹ���"),//2
		
		/**
		 * Ͷ����ע��ɹ���û����д������ҵ
		 */
		investor_regiester_succes_without_org("��ϲ��ע��ɹ�������Ե��������֤"),//1
		
		/**
		 * ��֤ͨ��
		 */
		user_auth_succes("��������֤ͨ��"),//2
		
		/**
		 * �û���֤��Ϣ�޸�
		 */
		user_auth_modify("��������֤��Ϣ�Ѿ����"),//2
		
		/**
		 * �û�������仯
		 */
		user_org_modify("����������Ѿ����"),//2
		
		/**
		 * ��Ŀ��չ�仯
		 * bizKey ��ʽ pid
		 */
		proj_prog_change("�����Ŀ��չ�б仯"),//3
		
		/**
		 * ��Ŀ�½�
		 * bizKey ��ʽ pid
		 */
		proj_prog_new("��������Ŀ����,����Ե�������鿴"), //1
		
		/**
		 * ��Ŀ���ۻظ�
		 * bizKey��ʽ pid(����id):cid(����id):uid(�ظ���id)��uname(�������û���)
		 */
		proj_cment_relay("����������µĻظ�"),//3
		
		/**
		 * ��Ŀ���µ�����
		 * bizKey��ʽ pid(��Ŀid):cid(����id):uid(������id)��uname(�������û���)
		 */
		proj_cment_new("�����Ŀ���µ�����"),//3
		
		
		/**
		 * �û�������Ŀ����
		 *  bizKey��ʽ insepctid
		 */
		user_apply_inspect("���ʵ�ؿ����������ύ,����鿴������"),//1
		
		/**
		 * �ɹ�ԤԼ��Ŀ����
		 *  bizKey��ʽ insepctid
		 */
		user_confirm_inspect("��ɹ�ԤԼ����Ŀ����"),//3
		
		/**
		 * �ɹ�ԤԼ��Ŀ����
		 *  bizKey��ʽ insepctid
		 */
		user_complete_inspect("��ɹ��������Ŀ����"),//3
		
		
		;
		private String contentTemplate;
		
		private MessageBizType(String contentTemplate) {
			this.contentTemplate = contentTemplate;
		}

		public String getContentTemplate() {
			return contentTemplate;
		}
		
		
	}
	
	
	
	
	
	
	
	
}
