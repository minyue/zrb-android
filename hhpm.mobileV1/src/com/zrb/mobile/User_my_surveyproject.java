package com.zrb.mobile;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
 
 
import com.zrb.mobile.adapter.Project_mysurvey_listAdapter;
import com.zrb.mobile.adapter.Project_mysurvey_listAdapter.OnDelListener;
 
import com.zrb.mobile.adapter.model.ProjectSurveyDto;
import com.zrb.mobile.adapter.model.ProjectSurveyDtos;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;
/*import com.zrb.chat.listviewanim.AnimateDismissAdapter;
import com.zrb.chat.listviewanim.OnDismissCallback;*/

import android.os.Bundle;
import android.content.Intent;
import android.text.format.DateUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class User_my_surveyproject extends BaseImgActivity implements OnClickListener, OnDelListener  {

	PullToRefreshListView msearchResults;
	LinearLayout layout_container;
	Project_mysurvey_listAdapter mynewslistAdapter;
	CustomloadingView mloadingView;
	int curPageindex=0;
	// AnimateDismissAdapter animateDismissAdapter ;
    List<Integer>  mSelectedPositions = new ArrayList<Integer>();
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.u_center_mynews);
		// setContentView(R.layout.user_register_activity);fragment_container
		 //initView();
		initView();
		loadData(1);
	}
  	
	private void initView(){
 	
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("我的勘察");
		
		mloadingView=(CustomloadingView)this.findViewById(R.id.list_emptyview);
		mloadingView.SetOnRefleshListener(this);
		
 		msearchResults=(PullToRefreshListView)this.findViewById(R.id.mylist_news);
 		ListView actualListView= msearchResults.getRefreshableView();
 		
		mynewslistAdapter=new Project_mysurvey_listAdapter(this,new ArrayList<ProjectSurveyDto>());
		mynewslistAdapter.setImgLoader(imageLoader);
		mynewslistAdapter.setOnDelListener(this);
		 
		
	/*    animateDismissAdapter = new AnimateDismissAdapter(mynewslistAdapter, new MyOnDismissCallback());
        animateDismissAdapter.setAbsListView(actualListView);
        actualListView.setAdapter(animateDismissAdapter);*/
		msearchResults.setAdapter(mynewslistAdapter);
        actualListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
  				ProjectSurveyDto tmpDto=	(ProjectSurveyDto)mynewslistAdapter.getItem(arg2-1);
				Intent intent = new Intent(User_my_surveyproject.this, User_my_surveyinfo.class);
				intent.putExtra("inspectId", tmpDto.autoId);
				startActivity(intent);
				
			}});
		
		// Set a listener to be invoked when the list should be refreshed.
		msearchResults.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(PullToRefreshBase<ListView> refreshView) {
						String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						//new GetDataTask().execute();
						curPageindex++;
 						onLoadMore(); 
					}
				});
		
		mloadingView.ShowLoading();
 	}
/*
    private class MyOnDismissCallback implements OnDismissCallback {

        @Override
        public void onDismiss(final AbsListView listView, final int[] reverseSortedPositions) {
            for (int position : reverseSortedPositions) {
              //  mAdapter.remove(position);
            	  mynewslistAdapter.removeAtIndex(position);
			      mynewslistAdapter.notifyDataSetChanged();
			 	  CommonUtil.showToast("删除成功", User_my_surveyproject.this);
            }
        }
    }*/
	
	public void onLoadMore()
	{
	    loadData(mynewslistAdapter.getMinId());
	}
	
	void test(){
		 mloadingView.Hide();
		List<ProjectSurveyDto> tmps=new ArrayList<ProjectSurveyDto>();
		for(int i=0;i<10;i++){
			
			ProjectSurveyDto tmp=new ProjectSurveyDto();
			tmp.autoId=100+i;
			tmp.projectName="我发现了招融宝可以看很多资讯，你也来下载试试test"+i;
			tmp.inspectStatus=(int) (Math.random() * (3 - 0) + 0);
			tmp.insertTime=new java.util.Date().getTime();
			tmps.add(tmp);
			
		}
		  mynewslistAdapter.addAll(tmps);
     	  mynewslistAdapter.notifyDataSetChanged();
	}
	
	private void loadData(int  InspectId){
		
	 	/*if(true) {test();return;} ; */
		//http://172.17.2.13:8090/inspect/queryUserInspectPage?pageSize=10&maxInspectId=1&zrb_front_ut=27e527e6a5e22013d98dffa4f6eee266
		String url=String.format("inspect/queryUserInspectPage?pageSize=10&maxInspectId=%1$s&zrb_front_ut=%2$s",
				               InspectId==1 ?"":InspectId, AppSetting.curUser.zrb_front_ut);
		
 		CommonUtil.InfoLog("queryUserInspectPage", url);
	    ZrbRestClient.get(url, null,  new AsyncHttpResponseHandler() {
			@Override
		    public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
  				  if(curPageindex==0)
					  mloadingView.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
				  else
		   	         CommonUtil.showToast("加载数据出错！", User_my_surveyproject.this);
		   }

		   @Override
		   public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
			   // TODO Auto-generated method stub
  			    String results=new String(arg2);
 			    msearchResults.onRefreshComplete();
 			    if(curPageindex==0)   mloadingView.Hide();
	 		    try{
		            Gson gson = new Gson();  
		            ProjectSurveyDtos tmpDto=	gson.fromJson(results, ProjectSurveyDtos.class);
	                if(tmpDto.res==1) {
	                	if(tmpDto.data!=null&&tmpDto.data.size()>0){
 	                	  mynewslistAdapter.addAll(tmpDto.data);
 	                	  mynewslistAdapter.notifyDataSetChanged();
 	                	}
	                	else{
	                		if(curPageindex==0) mloadingView.HideText("暂无勘察项目").ShowNull();
	                		else
	                			CommonUtil.showToast("暂无更多信息~~", User_my_surveyproject.this);	
	                	}
	                }
	                else{
 	            	  CommonUtil.showToast(tmpDto.msg, User_my_surveyproject.this);
	                }
 			    }
			     catch(Exception e) {
				   CommonUtil.showToast("error:加载数据出错！", User_my_surveyproject.this);
 			    } 
 		}});
	}
  
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
      	case R.id.right_title:
     		//changeBaseInfo() ;
     		break;
    	case R.id.btn_reflesh:
     		//changeBaseInfo() ;
    		mloadingView.HideText("努力加载中～～").ShowLoading();
    		onLoadMore();
     		break;
  		}
 	}

	@Override
	public void onDeleteEvent(final int position) {
 	
	    int inspectId=mynewslistAdapter.getCurItem(position).autoId;
		RequestParams param = new RequestParams();
		param.put("inspectId",inspectId);
		param.put("zrb_front_ut",AppSetting.curUser.zrb_front_ut);
		
	    ZrbRestClient.post("inspect/deleteByInspectId", param,  new AsyncHttpResponseHandler()
        {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				  CommonUtil.showToast("操作异常", User_my_surveyproject.this);
			}
		   @Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String results=new String(arg2);
				CommonUtil.InfoLog("cancelSurvey", results);
				try 
				{
			       Gson gson = new Gson();  
			       RegisterDto tmpDto=	gson.fromJson(results, RegisterDto.class);//{"res":1,"msg":null,"data":null,"bannerData":null}
 			       if(tmpDto.res==1) {
 			    	   mSelectedPositions.add( Integer.valueOf(position));
 			   	      // animateDismissAdapter.animateDismiss(mSelectedPositions);
 			           mSelectedPositions.clear();
 			       }
			      else
			    	  CommonUtil.showToast(tmpDto.msg, User_my_surveyproject.this);
     			}
				catch(Exception e){
					  CommonUtil.showToast(User_my_surveyproject.this.getString(R.string.error_serverdata_loadfail), User_my_surveyproject.this);
 				}
 			}});
	}
 

}
