package com.zrb.mobile;

import java.util.ArrayList;

import org.apache.http.Header;
import org.w3c.dom.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zrb.mobile.XList.IXListViewRefreshListener;
import com.zrb.mobile.XList.XListView;
import com.zrb.mobile.adapter.News_tablistItemAdapter;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.NewsDtos;
import com.zrb.mobile.common.NewsCache;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;

 

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NewsFragment extends Fragment implements  onLoadMoreListener {
	private final static String TAG = "NewsFragment";
	Activity activity;
	ArrayList<NewsDto> newsList = new ArrayList<NewsDto>();
	PullToRefreshListViewEx mXListView;
	//NewsAdapter mAdapter;
	private News_tablistItemAdapter mAdapter;
	String text;
	int channel_id;
	 TextView detail_loading;
	public final static int SET_NEWSLIST = 0;
	public final static int SET_NEWSLIST2 = 1;
	//Toast提示框
	private RelativeLayout notify_view;
	private TextView notify_view_text;
	
	
	private int currentPage = 1;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Bundle args = getArguments();
		text = args != null ? args.getString("text") : "";
		channel_id = args != null ? args.getInt("id", -1) : -1;
		initData();
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		this.activity = activity;
		super.onAttach(activity);
	}
	/** 此方法意思为fragment是否可见 ,可见时候加载数据 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		if (isVisibleToUser) {
			//fragment可见时加载数据
			if(newsList !=null && newsList.size() !=0){
				mhandler.obtainMessage(SET_NEWSLIST2).sendToTarget();
			}else{
				new Thread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							Thread.sleep(2);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						mhandler.obtainMessage(SET_NEWSLIST).sendToTarget();
					}
				}).start();
			}
		}else{
			//fragment不可见时不执行操作
		}
		super.setUserVisibleHint(isVisibleToUser);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
 		
		View view= inflater.inflate(R.layout.news_tab_item_fragment2, null);
 		//Toast提示框
		notify_view = (RelativeLayout)view.findViewById(R.id.notify_view);
		notify_view_text = (TextView)view.findViewById(R.id.notify_view_text);
		detail_loading= (TextView)view.findViewById(R.id.loading_txt);
		
		
		mXListView = (PullToRefreshListViewEx) view.findViewById(R.id.id_xlistView);
		mXListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(NewsFragment.this.getActivity().getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
 
				  refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
 				  onRefreshData();
				}
		});
		mXListView.setOnLoadMoreListener(this);
		//mXListView.setPullLoadEnable(this);
		//mXListView.setRefreshTime(CommonUtil.getRefreashTime(getActivity(), channel_id));
  
		mXListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(getActivity(), News_WebContentActivity.class);
				NewsDto tmpDto=mAdapter.getCurItem(position-1);
				intent.putExtra("title", tmpDto.title);
				intent.putExtra("newid",tmpDto.id);
				startActivity(intent);
			}
 		});
		return view;
  	}

	private void initData() {
	//	newsList = Constants.getNewsList();
	}
	
	Handler mhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch (msg.what) {
			case SET_NEWSLIST:
				 detail_loading.setVisibility(View.GONE);
		   	   /*	if(mAdapter == null){
					mAdapter = new NewsAdapter(activity, newsList);
					 
				}*/
				//mListView.setAdapter(mAdapter);
				//mXListView.startRefresh();
				 mXListView.setRefreshing(true);
 				break;
			case SET_NEWSLIST2:
				 detail_loading.setVisibility(View.GONE);
				// mXListView.NotRefreshAtBegin();
				 if(mAdapter == null)  {
		        		mAdapter = new News_tablistItemAdapter(getActivity(), newsList);
		        		mAdapter.setNewType(channel_id);
 		        		mAdapter.setImgLoader( ImageLoader.getInstance());
						mXListView.setAdapter(mAdapter);
		        	}
				
				break;
			default:
				break;
			}
			super.handleMessage(msg);
		}
	};
 	
	 
	/* 摧毁视图 */
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
 		mAdapter = null;
	}
	/* 摧毁该Fragment，一般是FragmentActivity 被摧毁的时候伴随着摧毁 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.d(TAG, "channel_id = " + channel_id);
	}

	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		currentPage++;
		LoadDate(mAdapter.getMinId()+"",false,10);
	}

 
	public void onRefreshData() {
		// TODO Auto-generated method stub
		currentPage=1;
		mhandler.postDelayed(new Runnable(){
	 		   @Override
				public void run() {
	 			   LoadDate("",false,10);
				}
	    }, 1000);
  	}
	
	private void LoadDate(String idx,final boolean isforward,int pagesize)
	{
     	String url=String.format("news/query?id=%1$s&forward=%2$b&limit=%3$d", idx,isforward,pagesize);
 		if(channel_id!=-1) url+="&category="+channel_id;
		ZrbRestClient.get(url, null,  new AsyncHttpResponseHandler() {
  			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
 
 				initNotify(-1,NewsFragment.this.getString(R.string.error_net_loadfail));
 				handerError();
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
 				String results=new String(arg2);
  				try {
					GsonBuilder gsonb = new GsonBuilder();  
			        Gson gson = gsonb.create();  
			        NewsDtos tmpDto=	gson.fromJson(results, NewsDtos.class);
			        if(tmpDto.res==1){
 			        	 if(currentPage==1) {
 					        	//NewsCache.saveRecentNews(channel_id, results);
 			        		    mXListView.onRefreshComplete();//.stopRefresh();
  					        	if(mAdapter == null)  {
  					        		mAdapter = new News_tablistItemAdapter(getActivity(), newsList);
  					        		mAdapter.setDatas(tmpDto.data);
  					        		mAdapter.setNewType(channel_id);
  					        		mAdapter.setImgLoader( ImageLoader.getInstance());
  									mXListView.setAdapter(mAdapter);
  					        	}
  					        	initNotify(tmpDto.data.size(),"");
 					      }
		 			      else{
		 			        	mXListView.hideloading();
					        	mAdapter.addAll(tmpDto.data);
					            mAdapter.notifyDataSetChanged();
		 			      }
						 
			        }
			        else{
			        	 initNotify(-1,tmpDto.msg);
			        	 handerError();
			        }
    			}
				catch(Exception e){
 				   //loadingview.HideText(e.getMessage()).ShowRefleshBtn(true).ShowError();
					//System.out.println("eeee!!!!"+e.getMessage());
 				}
				CommonUtil.setRefreashTime(getActivity(), channel_id);
   			}});
 	}
	
	  private void handerError(){
		if(currentPage==1)  {
 			mXListView.onRefreshComplete();;
		    if(mAdapter.isEmpty()){
		    	NewsDtos tmpDto=NewsCache.getRecentNews(channel_id);
		    	if(tmpDto!=null){
		    		mAdapter.setDatas(tmpDto.data);
			      	mAdapter.notifyDataSetChanged();
		    	}
		    }
		}
		else
			mXListView.hideloading();
	 }
	  /* 初始化通知栏目*/
		private void initNotify(final int nums,final String errmsg) {
			mhandler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					 if(TextUtils.isEmpty(errmsg))
					   notify_view_text.setText(String.format(getString(currentPage==1 ? R.string.ss_pattern_update
									                                           : R.string.ss_pattern_loadmore), nums));
					 else
						 notify_view_text.setText(errmsg);
					notify_view.setVisibility(View.VISIBLE);
					mhandler.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							notify_view.setVisibility(View.GONE);
						}
					}, 2000);
				}
			}, 1000);
		}
}
