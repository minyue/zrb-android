package com.zrb.mobile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
 
import com.zrb.mobile.adapter.News_mylistItemAdapter;
import com.zrb.mobile.adapter.Projectlist_attentionAdapter;
 
import com.zrb.mobile.adapter.model.AttentionProjectDto;
import com.zrb.mobile.adapter.model.AttentionProjectDtos;
import com.zrb.mobile.adapter.model.NewsmyDto;
import com.zrb.mobile.adapter.model.NewsmyDtos;
import com.zrb.mobile.adapter.model.ProjectDtos;
import com.zrb.mobile.adapter.model.ProjectsDto;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.adapter.model.UserInfoResult;
import com.zrb.mobile.adapter.model.UserRegisterDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class User_attention_projects extends BaseImgActivity implements OnClickListener  {

	PullToRefreshListView msearchResults;
	LinearLayout layout_container;
	Projectlist_attentionAdapter myAttentionlistAdapter;
	CustomloadingView mloadingView;
	int curPageindex=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.u_center_mynews);
		initView();
		loadData(CommonUtil.formateDate(new java.util.Date()));
	}
  	
	private void initView(){
 	
		View lefticon= this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		TextView txtTitle=	(TextView)this.findViewById(R.id.txt_title);
		txtTitle.setText("关注项目");
		
		mloadingView=(CustomloadingView)this.findViewById(R.id.list_emptyview);
		mloadingView.SetOnRefleshListener(this);
		
 		msearchResults=(PullToRefreshListView)this.findViewById(R.id.mylist_news);
		
 		myAttentionlistAdapter=new Projectlist_attentionAdapter(this);
 		myAttentionlistAdapter.setImgLoader(imageLoader);
		msearchResults.setAdapter(myAttentionlistAdapter);
 		msearchResults.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
			 	Intent intent = new Intent(User_attention_projects.this, Project_info_lience.class);
			 	AttentionProjectDto tmpDto=	(AttentionProjectDto)myAttentionlistAdapter.CurrentDatas.get(arg2-1);
			 
				intent.putExtra("title", tmpDto.projectName);
				intent.putExtra("projectId",(int)tmpDto.autoId);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent); 
	 		}});
		
		// Set a listener to be invoked when the list should be refreshed.
		msearchResults.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(PullToRefreshBase<ListView> refreshView) {
						String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);
 						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
						//new GetDataTask().execute();
						curPageindex++;
 						onLoadMore(); 
					}
				});
		
		mloadingView.ShowLoading();
 	}

	public void onLoadMore() {
 	    String time= myAttentionlistAdapter.getMinCollection();//CommonUtil.formateDate(new java.util.Date()); 
	    loadData(time);
	}
	
	private void loadData(String time){
 		///collection/query/project?createTime=2015-08-01%2000:00:00&limit=20&zrb_front_ut=9198112ff75b700e2d419a9c407b2202
		String url=String.format("collection/query/project?createTime=%1$s&limit=10&zrb_front_ut=%2$s",
			                                         	time,AppSetting.curUser.zrb_front_ut);
 		Log.e("tag", url);
	    ZrbRestClient.get(url, null,  new AsyncHttpResponseHandler()
	   {
			@Override
		    public void onFailure(int arg0, Header[] arg1, byte[] arg2,
				Throwable arg3) {
			// TODO Auto-generated method stub
				// progTip.Dismiss();
				  if(curPageindex==0)
					  mloadingView.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
				  else
		   	         CommonUtil.showToast("加载数据出错！", User_attention_projects.this);
		   }

		   @Override
		   public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
			// TODO Auto-generated method stub
			   //progTip.Dismiss();
			    String results=new String(arg2);
			    msearchResults.onRefreshComplete();
 			    if(curPageindex==0)     mloadingView.Hide();
	 		    try 
			    {
 	                Gson gson = new Gson();  
 	                AttentionProjectDtos tmpDto=	gson.fromJson(results, AttentionProjectDtos.class);
	                if(tmpDto.res==1) {
	                	if(tmpDto.data!=null&&tmpDto.data.size()>0){
	                		myAttentionlistAdapter.addAll(tmpDto.data);
	                		myAttentionlistAdapter.notifyDataSetChanged();
 	                	}
	                	else{
	                		if(curPageindex==0) mloadingView.HideText("暂无关注项目").ShowNull();
	                		else
	                			CommonUtil.showToast("暂无更多信息~~", User_attention_projects.this);	
	                	}
	                }
	                else{
 	            	  CommonUtil.showToast(tmpDto.msg, User_attention_projects.this);
	                } 
			     }
			     catch(Exception e) {
				 CommonUtil.showToast("error:加载数据出错！", User_attention_projects.this);
				//System.out.println("eeee!!!!"+e.getMessage());
			   } 
			
		}});
	}
  
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
    
     	case R.id.right_title:
     		//changeBaseInfo() ;
     		break;
    	case R.id.btn_reflesh:
     		//changeBaseInfo() ;
    		mloadingView.HideText("努力加载中～～").ShowLoading();
    		loadData(CommonUtil.formateDate(new java.util.Date()));
     		break;
     	 
 		}
 	}
 

}
