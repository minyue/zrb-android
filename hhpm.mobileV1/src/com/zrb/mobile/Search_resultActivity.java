package com.zrb.mobile;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

 
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
 
import com.zrb.mobile.adapter.News_searchResultAdapter;
import com.zrb.mobile.adapter.RadioItem;
import com.zrb.mobile.adapter.SearchResultAdapter;
import com.zrb.mobile.adapter.SearchResultAdapter.onDeleteListener;
import com.zrb.mobile.adapter.model.Article2Dto;
import com.zrb.mobile.adapter.model.Article2Dtos;
import com.zrb.mobile.adapter.model.HotRankDto;
import com.zrb.mobile.adapter.model.NewsDto;
import com.zrb.mobile.adapter.model.SearchResultDtos;
import com.zrb.mobile.adapter.model.TagDtos;
import com.zrb.mobile.adapter.model.TagsDto;
import com.zrb.mobile.chipsedittextlib.ChipsAdapter;
import com.zrb.mobile.chipsedittextlib.ChipsItem;
import com.zrb.mobile.chipsedittextlib.ChipsMultiAutoCompleteTextview;
import com.zrb.mobile.common.QueryhistoryDao;
import com.zrb.mobile.common.QueryhistoryItem;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.pulltorefresh.PullToRefreshListView;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.mobile.ui.MultipleTextView;
import com.zrb.mobile.ui.MultipleTextView.OnMultipleTVItemClickListener;
import com.zrb.mobile.ui.WordWrapView;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.applib.utils.FileUtil;
import com.zrb.mobile.utility.ZrbRestClient;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class Search_resultActivity extends BaseActivity implements OnClickListener, onDeleteListener{

  	
	ListView mhistroylists ;
	PullToRefreshListView msearchResults;
	SearchResultAdapter mhistoryAdapter;
	//SearchResultAdapter mInputsAdapter;
	
	ChipsMultiAutoCompleteTextview mSearchText;
	ImageView mclearBtn;
	int tagvalue;
	String tagwords;
	QueryhistoryDao mhistoryItemDao;
	int currentPage =1;
	CustomloadingView loadingview;
	News_searchResultAdapter msearchResultAdapter;
	List<TagsDto> curTags;
	boolean isSearch=true;
	private String tagId;
	private int SearchType;//1 tag,2 keyword
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
      //  requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.search_result_activity);
        tagvalue= this.getIntent().getIntExtra("value", -1);
        tagwords= this.getIntent().getStringExtra("tag");
        initview();
     }
     
	private TextView.OnEditorActionListener mActionlistener = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			// TODO Auto-generated method stub
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				seachkeywords(true);
				return true;
			}
			return false;
		}
	};
    
    private void initview(){
    	
    	mhistoryItemDao = new QueryhistoryDao(this);
    	this.findViewById(R.id.title_cancel).setOnClickListener(this);
    	this.findViewById(R.id.searchButton).setOnClickListener(this);
    	mclearBtn=(ImageView)this.findViewById(R.id.go_to_hot_image_view);
    	mclearBtn.setOnClickListener(this);
    	
    	loadingview=(CustomloadingView)this.findViewById(R.id.list_emptyview);
     	mSearchText=(ChipsMultiAutoCompleteTextview)this.findViewById(R.id.SearchText);//edittext
    	mSearchText.addTextChangedListener(inputTxtWatcher);
    	mSearchText.setOnEditorActionListener(mActionlistener);
    
		try {
			String results = CommonUtil.loadFileAsString(FileUtil.tagName);
 			Gson gson =new Gson();  
 			 
	     	TagDtos tmpDto=	gson.fromJson(results, TagDtos.class); 
			curTags=tmpDto.data;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 	
		//Log.e("curTags", curTags==null ? "0":curTags.size()+"");
 		ArrayList<ChipsItem> arrCountry = new ArrayList<ChipsItem>();
		if (curTags != null) {
			for (int i = 0; i < curTags.size(); i++) {
				arrCountry.add(new ChipsItem(curTags.get(i).labelName,
						R.drawable.android));
			}
		}

		//Log.i("MainActivity", "Array :" + arrCountry.size());
		
		ChipsAdapter chipsAdapter = new ChipsAdapter(this, arrCountry);
		mSearchText.setAdapter(chipsAdapter);
     	
		mhistroylists=(ListView)this.findViewById(R.id.search_history_list_view);
		msearchResults=(PullToRefreshListView)this.findViewById(R.id.search_result_list_view);
		msearchResults.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Search_resultActivity.this, News_WebContentActivity.class);
				NewsDto tmpDto=	(NewsDto)msearchResultAdapter.getItem(arg2-1);
			 
				intent.putExtra("title", tmpDto.title);
				intent.putExtra("newid",tmpDto.id);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}});
		
		// Set a listener to be invoked when the list should be refreshed.
		msearchResults.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(PullToRefreshBase<ListView> refreshView) {
						String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						//new GetDataTask().execute();
						isSearch=false;
						currentPage++;
						LoadData();
					}
				});
		
     
		mhistoryAdapter=new SearchResultAdapter(this); 
        View footerview=  LayoutInflater.from(this).inflate(R.layout.search_clear_history, null);
        mhistroylists.addFooterView(footerview);
         
        List<QueryhistoryItem>  tmplists=  mhistoryItemDao.getSearchList();
        mhistoryAdapter.CurrentDatas.addAll(tmplists);
        mhistoryAdapter.setDelListener(this);
        
        mhistroylists.setAdapter(mhistoryAdapter);
        mhistroylists.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				 // TODO Auto-generated method stub
  				 if(arg2==mhistoryAdapter.getCount()){
 					 mhistoryItemDao.deleteAll();
					 mhistoryAdapter.CurrentDatas.clear();
					 mhistoryAdapter.notifyDataSetChanged();
				 }
				 else{
					 tagwords= mhistoryAdapter.CurrentDatas.get(arg2).keywords;
  					 setkeywords();
 				 }
				
			}});
        setkeywords();
     }
    
    private  void setkeywords()
    {
    	if(!TextUtils.isEmpty(tagwords))
    	{
    	  mSearchText.setText(tagwords);
    	  mSearchText.setSelection(mSearchText.length());
    	  mSearchText.setChips();
     	  seachkeywords(false);
     	}
    }
   // search_input_list_view
    
    TextWatcher inputTxtWatcher=new TextWatcher(){

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if (mSearchText.getText().length() == 0){
				mclearBtn.setVisibility(View.INVISIBLE);
				showResultview(true);
			}
			else
				mclearBtn.setVisibility(View.VISIBLE);
			
			if(count >=1&&s.charAt(0)!=','){
				Log.e("msg",s.charAt(start)+"rrr"+ "msg");
				
				if(s.charAt(start) == ',')
					 mSearchText.setChips(); // generate chips
			}
		}
     };
     
     private void getkey(){
     	 SearchType=2;
     	 String curstr=mSearchText.getText().toString();
    	 tagId=curstr.replace(",", "");
    	 if(curstr.endsWith(",")){
    		 for(int i=0;i<curTags.size();i++) 
    			 if((curTags.get(i).labelName+",").equals(curstr)){
    				 SearchType=1;
    				 tagId=curTags.get(i).id+"";
    				 break;
    			 }
    	 }
      } 
     
    private void seachkeywords(boolean isHite){
    	
    	currentPage=1;
    	isSearch=true;
    	if(mSearchText.getText()!=null&&!TextUtils.isEmpty(mSearchText.getText().toString()))
        {
    		String keyword=mSearchText.getText().toString(); 
    		int isfind=-1;
    		for(int i=0;i<mhistoryAdapter.CurrentDatas.size();i++) {
    			if(mhistoryAdapter.CurrentDatas.get(i).keywords.equals(keyword))  {
    				isfind=i;
    				break;
    			}
    		}
    		if(isfind==-1){
    		    QueryhistoryItem item=new QueryhistoryItem(-1,keyword,1);
      		    mhistoryItemDao.save(item);
    		}
    		else{
    			QueryhistoryItem item=mhistoryAdapter.CurrentDatas.get(isfind);
    			mhistoryItemDao.update(item);
    		}
    		List<QueryhistoryItem>  tmplists=  mhistoryItemDao.getSearchList();
            mhistoryAdapter.setAll(tmplists);
            mhistoryAdapter.notifyDataSetChanged();
            getkey();
     		LoadData();
    	}
    	else
    		if(isHite)  CommonUtil.showToast("请输入关键字", this);
     }
    
    private void showResultview(boolean isshowHistory)
    {
    	if(isshowHistory){
    	   mhistroylists.setVisibility(View.VISIBLE);
    	   msearchResults.setVisibility(View.GONE);
    	   loadingview.setVisibility(View.GONE);
    	}
    	else{
    		mhistroylists.setVisibility(View.GONE);
     	    msearchResults.setVisibility(View.VISIBLE);
    	}
   }
    
 
    
    private void LoadData()
	{
    	if(currentPage==1) loadingview.HideText("加载数据中").ShowLoading();
       
    	//String url=String.format("news/search?searchStr=%1$s&searchType=%2$d&pageSize=10&pageNum=%3$d", tagId,SearchType,currentPage);
        RequestParams param = new RequestParams();
        param.put("searchStr",tagId + "");  
        param.put("searchType",SearchType);
        param.put("pageSize",10);
        param.put("pageNum",currentPage);
        
		ZrbRestClient.post("news/search", param,  new AsyncHttpResponseHandler()
    	{
 			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
 				msearchResults.onRefreshComplete();
				loadingview.HideText("加载数据出错！").ShowRefleshBtn(true).ShowError();
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				loadingview.Hide();
 				String results=new String(arg2);
 				msearchResults.onRefreshComplete();
				try 
				{
					GsonBuilder gsonb = new GsonBuilder();  
			        Gson gson = gsonb.create();  
 			        SearchResultDtos tmpDto=	gson.fromJson(results, SearchResultDtos.class); 
					 
			        showResultview(false);
			        if(tmpDto.data.list==null ||tmpDto.data.list.size()==0) {
			        	
 			        	if(currentPage==1)   loadingview.HideText("暂无相关数据～～").ShowNull();
 			        	else
			        		CommonUtil.showToast("暂无更多相关记录", Search_resultActivity.this);
			        	return;
			        }
                   
			        if(msearchResultAdapter==null) 
			        {
			        	msearchResultAdapter=new News_searchResultAdapter(Search_resultActivity.this);
			        	msearchResultAdapter.SetTag(SearchType, tagId);
			        	msearchResultAdapter.addAll(tmpDto.data.list);
 		            	msearchResults.setAdapter(msearchResultAdapter);
			        }
  			        else {
  			        	msearchResultAdapter.SetTag(SearchType, tagId);
 			           if(isSearch) 
			        	   msearchResultAdapter.setDatas(tmpDto.data.list);
			           else
			        	   msearchResultAdapter.addAll(tmpDto.data.list);
 			           msearchResultAdapter.notifyDataSetChanged();
			        }
     			}
				catch(Exception e){
					loadingview.HideText(e.getMessage()).ShowRefleshBtn(true).ShowError();
				}
			  
 			}});
  	
	}
	
    
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		  case R.id.title_cancel:
 			  this.finish();
 			  this.overridePendingTransition(0, 0);
		      break;
		  case R.id.searchButton:
//			  Intent it = new Intent(this, Search_resultActivity.class);
//		 	  startActivity(it);
//		 	  this.finish();
			  seachkeywords(true);
			  break;
		  case R.id.go_to_hot_image_view:
			  mSearchText.setText("");
			  break;
		}
	}

	@Override
	public void onDelclick(int postion) {
		// TODO Auto-generated method stub
		//delete
		int msgId=mhistoryAdapter.CurrentDatas.get(postion).id;
		mhistoryAdapter.CurrentDatas.remove(postion);
		mhistoryAdapter.notifyDataSetChanged();
		mhistoryItemDao.delete(msgId);
	}
   
    
	
  
}
