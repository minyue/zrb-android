package com.zrb.mobile;

import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.adapter.model.FavorEvent;
import com.zrb.mobile.adapter.model.MessageEvent;
import com.zrb.mobile.ui.CenterProgressWebView;
import com.zrb.mobile.utility.CollectionHelper;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.CollectionHelper.checkCollection;
import com.zrb.mobile.utility.CollectionHelper.onCollectionListener;
import com.zrb.mobile.utility.Constants;

import de.greenrobot.event.EventBus;
import android.os.Bundle;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DiscoverDetailOneActivity extends BaseCheckloginActivity implements
		OnClickListener, onCollectionListener, checkCollection {
	private TextView txt_title;
	private LinearLayout title_left_root;
	private CenterProgressWebView mWebView;;
	private int position;
	private long id, type;
	private LinearLayout ll_discover_liuyan_ll;
	private long messageAmount, attentionTimes;
	private TextView tv_discover_focus_tv;
	private TextView tv_discover_content_tv;
	private LinearLayout ll_discover_share_ll;
	private LinearLayout ll_discover_focus_ll;
	private Dialog PopDialog;
	private String from,title;
	private Drawable isFocus, NoFocus;
	private boolean hasAttention;
	// public String ACTION_NAME_ONE = "discover_content";
	// public String ACTION_NAME_FOUR = "discover_detail_content";
	// public String ACTION_NAME_THREE = "my_publish";
	// public String ACTION_NAME_EIGHT = "attention_discover";
	CollectionHelper mCollectionHelper;

	int oldcurStatus = -1;
	boolean isneedSend = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_discover_detail_one);
		 
		oldcurStatus = AppSetting.curUser == null ? 0 : 1;
		registerBoradcastReceiver1();
		// registerBoradcastReceiver();
		title=getIntent().getStringExtra("title");
		from = getIntent().getStringExtra("from");
		id = getIntent().getLongExtra("id", 0);
		position = getIntent().getIntExtra("position", 0);
		attentionTimes = getIntent().getLongExtra("attentionTimes", 0);
		messageAmount = getIntent().getIntExtra("messageAmount", 0);
		type = getIntent().getIntExtra("type", 0);
		hasAttention = getIntent().getIntExtra("attention", -1) == 1 ? true
				: false;
		init();
		setListener();
	}

	private void setListener() {
		// TODO Auto-generated method stub
		title_left_root.setOnClickListener(this);
		ll_discover_liuyan_ll.setOnClickListener(this);
		ll_discover_share_ll.setOnClickListener(this);
		ll_discover_focus_ll.setOnClickListener(this);
	}

	private void init() {
		NoFocus = getResources().getDrawable(R.drawable.found_like_off);
		isFocus = getResources().getDrawable(R.drawable.found_like_on);

		// TODO Auto-generated method stub
		txt_title = (TextView) findViewById(R.id.txt_title);
		title_left_root = (LinearLayout) findViewById(R.id.title_left_root);
		if (Constants.DiscoverProjectIntent == type) {
			txt_title.setText("项目详情");
		} else {
			txt_title.setText("资金详情");
		}

		mWebView = (CenterProgressWebView) this.findViewById(R.id.webView1);
		ll_discover_liuyan_ll = (LinearLayout) findViewById(R.id.ll_discover_liuyan_ll);
		tv_discover_focus_tv = (TextView) findViewById(R.id.tv_discover_focus_tv);
		tv_discover_content_tv = (TextView) findViewById(R.id.tv_discover_content_tv);
		ll_discover_share_ll = (LinearLayout) findViewById(R.id.ll_discover_share_ll);
		ll_discover_focus_ll = (LinearLayout) findViewById(R.id.ll_discover_focus_ll);

		tv_discover_focus_tv.setCompoundDrawablesWithIntrinsicBounds(
				hasAttention ? isFocus : NoFocus, null, null, null);

		tv_discover_focus_tv.setText(attentionTimes + "关注");
		tv_discover_content_tv.setText(messageAmount + "留言");
		mWebView.setOnPageListener(null);
		// mWebView.addJavascriptInterface(this, "localApi");

		String url = AppSetting.BASE_URL + "intention/details?id=" + id;
		mWebView.loadUrl(url);

	}

	@Override
	public void onClick(View v) {
		Intent intent;
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.title_left_root:
			onBackPressed();

			break;
		case R.id.ll_discover_liuyan_ll:
			if (AppSetting.curUser == null) {
				super.showLoginDialog();
			} else {
				intent = new Intent(DiscoverDetailOneActivity.this,
						PushLiuYanActivity.class);
				intent.putExtra("id", id);
				intent.putExtra("position", position);
				intent.putExtra("from", from);
				intent.putExtra("needsend", isneedSend);
				startActivity(intent);
			}

			break;
		case R.id.ll_discover_share_ll:
			if (AppSetting.curUser == null) {
				super.showLoginDialog();
			} else {
				intent = new Intent(DiscoverDetailOneActivity.this,
						News_ShareActivity.class);
				intent.putExtra("url", AppSetting.BASE_URL
						+ "intention/details?id=" + id);
				intent.putExtra("title", title);
				startActivity(intent);
			}

			break;
		case R.id.ll_discover_focus_ll:
			if (AppSetting.curUser == null) {
				super.showLoginDialog();
			} else {
				if (!hasAttention) {
					// Focus(id, position);
					onclickAttention(true);
				} else {
					// CanFocus(id, position);
					onclickAttention(false);
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(mBroadcastReceiver1);
 
		// unregisterReceiver(mBroadcastReceiver);
	}

	public void registerBoradcastReceiver1() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(Constants.FocusNum_FUNDETIAL_mainlist);
		// 注册广播
		registerReceiver(mBroadcastReceiver1, myIntentFilter);
	}

	private BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(Constants.FocusNum_FUNDETIAL_mainlist)) {
				messageAmount = messageAmount + 1;
				tv_discover_content_tv.setText(messageAmount + "留言");
				 EventBus.getDefault().post(new MessageEvent(position));
			}
		}
	};

	private void checkCollectionHelper() {
		if (mCollectionHelper == null) {
			mCollectionHelper = new CollectionHelper(this,
					Constants.FocusIntent);
			mCollectionHelper.setOnCollectionListener(this);
			mCollectionHelper.setOncheckCollection(this);
		}
	}

	private void onclickAttention(boolean isAdd) {
		checkCollectionHelper();
		mCollectionHelper.onClickCollect(id, isAdd);
	}

	@Override
	public void onCollectionEvent() {
		// TODO Auto-generated method stub

		hasAttention = !hasAttention;
		// 关注成功，关注数加1
		if (hasAttention)
			attentionTimes++;
		else
			attentionTimes = attentionTimes - 1;
		tv_discover_focus_tv.setCompoundDrawablesWithIntrinsicBounds(
				hasAttention ? isFocus : NoFocus, null, null, null);
		tv_discover_focus_tv.setText(attentionTimes + "关注");

		if ("1".equals(from) && isneedSend) {

			Intent intent1 = new Intent(Constants.FocusNum_Find_mainlist);// ACTION_NAME_ONE// "discover_content";
			intent1.putExtra("position", position);
			intent1.putExtra("from", "1");
			intent1.putExtra("attention", hasAttention ? 1 : 0);
			sendBroadcast(intent1);
		} else if ("2".equals(from)) {
 			 EventBus.getDefault().post(new FavorEvent(position,hasAttention));
			 
		} else if ("3".equals(from)) {
 			
			Intent intent1 = new Intent(Constants.FocusNum_ATTENTION_mainlist);
			intent1.putExtra("position", position);
			intent1.putExtra("from", "1");
			intent1.putExtra("attention", hasAttention ? 1 : 0);
			sendBroadcast(intent1);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) { // resultCode为回传的标记，我在B中回传的是RESULT_OK
			case 500:
				if (AppSetting.curUser != null) {
					checkCollectionHelper();

					int curStatus = AppSetting.curUser == null ? 0 : 1;
					if (curStatus != oldcurStatus)
						isneedSend = false;
					mCollectionHelper.checkCollect(id);
				}
				break;
			default:
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onhasCollection(boolean ishas) {
		// TODO Auto-generated method stub
		hasAttention = ishas;
		tv_discover_focus_tv.setCompoundDrawablesWithIntrinsicBounds(
				ishas ? isFocus : NoFocus, null, null, null);
	}

}
