package com.zrb.mobile;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.polites.android.GestureImageView;
import com.zrb.mobile.common.NightConfig;
import com.zrb.mobile.ui.WrapContentViewPager;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.DialogBuilder;
import com.zrb.applib.utils.AppSetting;
import com.zrb.applib.utils.FileUtil;
import com.zrb.mobile.utility.Http;
 

public class News_ShareActivity extends FragmentActivity implements OnTouchListener
{
	private boolean isNight=false;
	private String url;
	 View cancelBtn,Settingmask;
	private String title;
	 
	public static void launch (Context  mActivity,int id,String title ){
	 
		Intent intent = new Intent(mActivity, News_ShareActivity.class);
		intent.putExtra("url", AppSetting.BASE_URL+ "intention/details?id="+ id);
		intent.putExtra("title", title);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	 
	     if(NightConfig.getInstance(this).getNightModeSwitch()){
	         isNight=true;
	     }
		setContentView(R.layout.detail_more_dialog);
 		  /*intent.putExtra("title", title);
		  intent.putExtra("url", imageLink);*/
		
		url=this.getIntent().getStringExtra("url");
		title=this.getIntent().getStringExtra("title");
		initView();
	}
  
	 FragmentPagerAdapter mShareAdapter = new FragmentPagerAdapter(this.getSupportFragmentManager())  
	 {
			@Override
			public int getCount(){
				//return mDatas.size();
				return 1;
			}

			@Override
			public Fragment getItem(int position)
			{
				News_ShareFragment fragment = new News_ShareFragment();
				fragment.mtitle=title;
				fragment.murl=url;
				//return new FragmentPage1();
			    return fragment;
			}
		};
	
/*		 <item name="android:windowEnterAnimation">@anim/push_bottom_in</item>
		    <item name="android:windowExitAnimation">@anim/push_bottom_out</item>
		  public  bool OnTouchEvent(MotionEvent events)
	        {
		     	Finish();
	            OverridePendingTransition(Resource.Animation.slide_down_enter, Resource.Animation.slide_down_exit);
			    return true;
		    }*/
		
	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
			// TODO Auto-generated method stub
		   this.finish();
          //overridePendingTransition(Resource.Animation.slide_down_enter, Resource.Animation.slide_down_exit);
		   overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
		   return true;
		   
		  
	}
	
	@Override
	public void onBackPressed(){
		
		 this.finish();
		  overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
	}
		
	private void initView(){
 	
		
		this.findViewById(R.id.empty_overlay).setOnTouchListener(this);
		Settingmask=	this.findViewById(R.id.detail_bg);
		Settingmask.setBackgroundColor(this.getResources().getColor(
						              isNight ? R.color.detail_more_bg_night
								              : R.color.detail_more_bg));
		
		cancelBtn =this.findViewById(R.id.cancel_btn);
 		cancelBtn.setBackgroundDrawable(this.getResources().getDrawable(
				                        isNight ? R.drawable.detail_more_cancel_btn_night
						                        : R.drawable.detail_more_cancel_btn));
 		this.findViewById(R.id.cancel_btn).setOnClickListener(
				new OnClickListener() {
 					@Override
					public void onClick(View v) {
 						
						// TODO Auto-generated method stub
						News_ShareActivity.this.finish();
						News_ShareActivity.this.overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
					}
				});
 		
		WrapContentViewPager tmplist2 = (WrapContentViewPager) this.findViewById(com.zrb.mobile.R.id.myview_pager);
		tmplist2.setAdapter(mShareAdapter);
     }

 
	 
 
	
}
