package com.zrb.mobile;

import java.util.ArrayList;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.adapter.UserMyMessageAdapter;
import com.zrb.mobile.adapter.model.MyMessageDto;
import com.zrb.mobile.adapter.model.UserMyMessageDto;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.MsgformatUtil;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class Message_MainFragment  extends Fragment implements OnItemClickListener, onLoadMoreListener {
	int currentPage;
	Handler myhandler = new Handler();
	View RootView;
	boolean isFirstIn = true;
	PullToRefreshListViewEx listview;
	UserMyMessageAdapter userMyFindMoneyAdapter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// return inflater.inflate(R.layout.u_center_registerstep1, null);

		View view = inflater.inflate(R.layout.message_home_activity1, container, false);
		RootView=view;
 		initView();
		return view;
	}
	
	/**
	 * 初始化UI
	 */
	private void initView() {
 		((TextView)RootView.findViewById(R.id.txt_title)).setText("消息");
		View lefticon = RootView.findViewById(R.id.title_left_root);
		lefticon.setVisibility(View.GONE);
  		
	  
		
		listview = (PullToRefreshListViewEx)RootView.findViewById(R.id.discover_list_view);
		listview.setPagesize(14);
		
		listview.setOnItemClickListener(this);// 点击事件
		listview.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(CommonUtil.getcurTime());
				currentPage = 1;
				myhandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						LoadData(-1, true);
					}
				}, 1000);
			}
		});

		userMyFindMoneyAdapter = new UserMyMessageAdapter(this.getActivity(), new ArrayList<MyMessageDto>());
		listview.initAdapter(userMyFindMoneyAdapter);
		listview.setOnLoadMoreListener(this);
		// listview.setAdapter(usermyPublishAdapter);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		MyMessageDto intention = userMyFindMoneyAdapter.list1.get(position - 1);
		// 当消息状态未读的时候，去执行信息状态栏改变
		if (intention.read == 0) {
			//MakeRead(intention.id, position - 1);
		}
		
		/*Intent intent = MsgformatUtil.getQuickAction(User_my_message.this,intention);
		startActivity(intent);  
		*/
		 

		// Toast.makeText(this, "点击事件", 0).show();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}

	private void firstLoad() {
		if (isFirstIn) {
			isFirstIn = false;
			myhandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					listview.setRefreshing(true);
				}
			}, 200);
		}
	}
 	
	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		currentPage++;
		myhandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				LoadData(userMyFindMoneyAdapter.getMinId(), false);
			}
		}, 300);
	}
	
	ZrbRestClient mZrbRestClient;
	private void LoadData(long maxId, final boolean isreflesh) {
		RequestParams param = new RequestParams();
		//param.put("zrb_front_ut", AppSetting.curUser.zrb_front_ut);
		param.put("limit", 14);
		if (-1 != maxId) {
			param.put("maxId", maxId);
		}

		
		CommonUtil.InfoLog("message/queryMessage", param.toString());
		if(null==mZrbRestClient){
			
			mZrbRestClient=new ZrbRestClient(this.getActivity());
			mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
					CommonUtil.showToast(arg0, Message_MainFragment.this.getActivity());
					if (currentPage > 1)
						listview.hideloading();
					else
						listview.onRefreshComplete();
				}

				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// loadingview.Hide();
					String results = new String(arg2);
					// CommonUtil.InfoLog("intention/list", results);
					if (currentPage == 1)
						listview.onRefreshComplete();
					else
						listview.hideloading();
					try {
						Gson gson = new Gson();
						UserMyMessageDto tmpDto = gson.fromJson(results, UserMyMessageDto.class);
						if (tmpDto.res == 1) {

							if (tmpDto.data != null && tmpDto.data.size() > 0) {
								if (currentPage == 1) {
									userMyFindMoneyAdapter.setCurDatas(tmpDto.data);
								} else {
									userMyFindMoneyAdapter.addAll(tmpDto.data);
								}
								userMyFindMoneyAdapter.notifyDataSetChanged();
							} else {
								CommonUtil.showToast(String.format("暂无%1$s信息~~", currentPage == 1 ? "": "更多"));
	 						}
						} else
							CommonUtil.showToast(tmpDto.msg);
					} catch (Exception e) {
						CommonUtil.showToast(R.string.error_serverdata_loadfail,Message_MainFragment.this.getActivity());
					}
				}
			});
		}
		mZrbRestClient.post("message/queryMessage", param);
	}
}
