package com.zrb.mobile;
import com.zrb.mobile.ui.HackyViewPager;
import com.zrb.mobile.utility.AnimationManager;
import com.zrb.mobile.utility.CommonUtil;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ImagePagerActivity extends FragmentActivity {
	private static final String STATE_POSITION = "STATE_POSITION";
	public static final String EXTRA_IMAGE_INDEX = "image_index";
	public static final String EXTRA_IMAGE_URLS = "image_urls";
	public static final String EXTRA_IMAGE_INFOS = "image_infos";
	private HackyViewPager mPager;
	private int pagerPosition;
	private TextView indicator,indContent;
	Handler mhandler=new Handler();
	RelativeLayout mBottomBar;
	boolean isshow=true;
	String[] picInfos;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_detail_pager);

		pagerPosition = getIntent().getIntExtra(EXTRA_IMAGE_INDEX, 0);
		String[] urls = getIntent().getStringArrayExtra(EXTRA_IMAGE_URLS);
		picInfos= getIntent().getStringArrayExtra(EXTRA_IMAGE_INFOS);
		
		
		mBottomBar= (RelativeLayout) findViewById(R.id.bottom_layout);
		mPager = (HackyViewPager) findViewById(R.id.pager);
		ImagePagerAdapter mAdapter = new ImagePagerAdapter(
				getSupportFragmentManager(), urls);
		mPager.setAdapter(mAdapter);
		indicator = (TextView) findViewById(R.id.indicator);
		
		indContent= (TextView) findViewById(R.id.ind_content);

		CharSequence text = getString(R.string.viewpager_indicator, 1, mPager
				.getAdapter().getCount());
		indicator.setText(text);
		indContent.setText((picInfos==null||picInfos.length<=0) ?"":picInfos[0]);
		// 更新下标
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageSelected(int arg0) {
				CharSequence text = getString(R.string.viewpager_indicator, arg0 + 1, mPager.getAdapter().getCount());
				indicator.setText(text);
				indContent.setText(picInfos[arg0]);
			}

		});
		if (savedInstanceState != null) {
			pagerPosition = savedInstanceState.getInt(STATE_POSITION);
		}

		mPager.setCurrentItem(pagerPosition);
	}

	public void showtoolbar(){
		isshow=!isshow;
 		if (isshow) {
 			mhandler.postDelayed(new Runnable(){
 				@Override
				public void run() {
					// TODO Auto-generated method stub
					  mBottomBar.startAnimation(AnimationManager.getInstance().getBottomBarShowAnimation());
			 		  mBottomBar.setVisibility(View.VISIBLE);
				}
	  		}, 100);
 		}
		else{
			mhandler.postDelayed(new Runnable(){
 				@Override
				public void run() {
		          mBottomBar.startAnimation(AnimationManager.getInstance().getBottomBarHideAnimation());  
		          mBottomBar.setVisibility(View.GONE);
 				}
	  		}, 100);
 		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_POSITION, mPager.getCurrentItem());
	}

	private class ImagePagerAdapter extends FragmentStatePagerAdapter {

		public String[] fileList;

		public ImagePagerAdapter(FragmentManager fm, String[] fileList) {
			super(fm);
			this.fileList = fileList;
		}

		@Override
		public int getCount() {
			return fileList == null ? 0 : fileList.length;
		}

		@Override
		public Fragment getItem(int position) {
			String url = fileList[position];
			return ImageDetailFragment.newInstance(url);
		}

	}
}