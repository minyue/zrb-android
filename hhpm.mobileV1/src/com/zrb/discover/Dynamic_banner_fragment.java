package com.zrb.discover;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

import com.google.gson.Gson;
import com.hhpm.lib.PageIndicator.RingPagerIndicator.onPageSelectListener;
import com.hhpm.lib.banner.BGAViewPager;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.RecImgpagerAdapter;
import com.zrb.mobile.adapter.model.AdsDto;
import com.zrb.mobile.adapter.model.FavoriteDto;
import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.ui.AutoScrollViewPager;
import com.zrb.mobile.ui.CustomRelativeLayout;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

 
public class Dynamic_banner_fragment extends Fragment implements OnClickListener, onPageSelectListener
{
 
	AutoScrollViewPager	mTopViewPager;
	//RingPagerIndicator  PageIndicator;
  	List<AdsDto> mCurrentData;
    RecImgpagerAdapter mbannerAdapter;
  
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	 
		//initPopWindow();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dynamic_ads_banner, container, false);

		mTopViewPager = (AutoScrollViewPager) view.findViewById(R.id.viewpager1);
		mTopViewPager.setOffscreenPageLimit(3);

		ArrayList<AdsDto> tmps = new ArrayList<AdsDto>();
		// tmps.add(new AdsDto(1,"",""));
		mbannerAdapter = new RecImgpagerAdapter(this.getActivity(), tmps).setInfiniteLoop(false);
		// mTopViewPager.setPageTransformer(true, new
		// ZoomOutSlideTransformer());
		// PageIndicator= (RingPagerIndicator)
		// view.findViewById(R.id.id_indicator);
		// PageIndicator.setOnPageChangeListener(curPageChangeListener);
		mTopViewPager.setAdapter(mbannerAdapter);
		// mTopViewPager.setPageChangeDuration(1000);
		mTopViewPager.setInterval(5000);
		mTopViewPager.setSlideBorderMode(AutoScrollViewPager.SLIDE_BORDER_MODE_TO_PARENT);
 		// PageIndicator.setViewPager(mTopViewPager);
		// PageIndicator.setPageSelectListener(this);
		// PageIndicator.redraw();
		CustomRelativeLayout customlayout= (CustomRelativeLayout)view.findViewById(R.id.custom_indicator);
		customlayout.setAutoViewPager(mTopViewPager);
		
		return view;
	}
 	
	public void refleshUI(List<AdsDto> banners){
		
		if(banners!=null&&banners.size()>0){
 			
			mbannerAdapter.setDatas(banners);
			mbannerAdapter.setInfiniteLoop(false);
 			mbannerAdapter.notifyDataSetChanged();
 			mTopViewPager.stopAutoScroll();
			mTopViewPager.startAutoScroll();
			mTopViewPager.setCurrentItem(0);
	 	 	//	mTopViewPager.startAutoScroll(3000);
 	 		
 	 		//mTopViewPager.setCurrentItem(Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % CommonUtil.getSize(tmplist));
	    	// PageIndicator.setViewPager(mTopViewPager, 0);
 	 		//handler.removeCallbacks(ScrollTask);
 	 		//handler.postDelayed(ScrollTask, DELYED);
  		}
  	}
  
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
 		 super.onActivityCreated(savedInstanceState);
 		 //LoadAds();
 	}
	
 
/*	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		CommonUtil.showToast("onHiddenChanged:"+hidden);
  		if(hidden) 
 			 if(mTopViewPager!=null)  mTopViewPager.stopAutoScroll();
 		else
			 if(mTopViewPager!=null)  mTopViewPager.startAutoScroll();
	}*/
	/*
	@Override
	public void onResume() {
 		super.onResume();
 	//	CommonUtil.showToast("onResume");
 		 if(mTopViewPager!=null)  mTopViewPager.startAutoScroll();
   	}
 
	@Override
	public void onPause() {
 		super.onPause() ;
 	//	CommonUtil.showToast("onPause");
 		 if(mTopViewPager!=null)  mTopViewPager.stopAutoScroll();
  	}*/
 
	@Override
	public void onStop() {
  		super.onStop();
	}
 	
    public void LoadData() {
    	//LoadAds();
     }

     
    private ViewPager.OnPageChangeListener curPageChangeListener=new ViewPager.OnPageChangeListener()
    {

    	/**
		 * This method will be invoked when a new page becomes selected.
		 * position: Position index of the new selected page.
		 */
		public void onPageSelected(int position) {
			//currentItem = position;
			/*if(mCurrentData!=null)
			tv_title.setText(mCurrentData.get(position).title);*/
			//dots.get(oldPosition).setBackgroundResource(R.drawable.dot_normal);
			//dots.get(position).setBackgroundResource(R.drawable.dot_focused);
			//oldPosition = position;
		}

		public void onPageScrollStateChanged(int arg0) {

		}

		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}
    	
    }; 
  	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		   case R.id.img_share_btn:
 		    
			 break;
		   case R.id.img_attention_btn:
				break;
		   case R.id.img_comment_btn:
		 
 			   break;
		}
	}
 
	@Override
	public void onPageSelected(int postion) {
		// TODO Auto-generated method stub
		
	}
	 
}
	
    
