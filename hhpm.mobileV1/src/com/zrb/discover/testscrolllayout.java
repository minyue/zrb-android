/*
 * Copyright (C) 2011 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zrb.discover;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
 
 
import com.hhpm.lib.ui.ScrollLayout;
import com.hhpm.lib.ui.ScrollLayout.OnViewChangeListener;
import com.zrb.discover.widget.ArrowlineView;
import com.zrb.mobile.R;
import com.zrb.mobile.ui.ViewPagerIndicator;

public class testscrolllayout extends Activity implements OnClickListener {
	
	ScrollLayout mlayout;
	
	//ViewPagerIndicator mpindicator;
	ArrowlineView mpindicator2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
      //  setTheme(SampleList.THEME); //Used for theme switching in samples
       // requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_register);

      //  mpindicator=(ViewPagerIndicator)this.findViewById(R.id.viewIndicator);
        
        mpindicator2=(ArrowlineView)this.findViewById(R.id.viewIndicator2);
        
        List<String> tmps=new ArrayList<String>();
        tmps.add("验证手机号");
        tmps.add("找回密码");
       // mpindicator.setTabItemTitles(tmps);
     
        mlayout= (ScrollLayout)this.findViewById(R.id.viewpager);
        mlayout.setIsScroll(false);
        mlayout.SetOnViewChangeListener(new OnViewChangeListener(){

			@Override
			public void OnViewChange(int view) {
				// TODO Auto-generated method stub
				 if(view==0) mpindicator2.scroll(0, 0);
				 else if(view==1)
					 mpindicator2.scroll(1, 0);
				 else if(view==2)
					 mpindicator2.scroll(2, 0);
				 
			}});;
        
    /*    this.findViewById(R.id.test2).setOnClickListener(this);
        this.findViewById(R.id.test1).setOnClickListener(this);
        this.findViewById(R.id.test3).setOnClickListener(this);
        this.findViewById(R.id.test2go).setOnClickListener(this);*/
        
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		/*switch(v.getId())
		{
		case R.id.test1:
			mlayout.scrollToScreen(1);
			break;
		case R.id.test2:
			mlayout.scrollToScreen(0);
			break;
		case R.id.test2go:
			mlayout.scrollToScreen(2);
			break;
		case R.id.test3:
			mlayout.scrollToScreen(1);
			break;
		}*/
	}

   }
