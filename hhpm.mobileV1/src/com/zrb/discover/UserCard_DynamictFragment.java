package com.zrb.discover;

import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.hhpm.lib.support.inject.ViewInject;
import com.loopj.android.http.RequestParams;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.DiscoverDtos;
import com.zrb.mobile.adapter.model.DynamicDto;
import com.zrb.mobile.adapter.model.Dynamic_CardDto;
import com.zrb.mobile.ui.CustomloadingView;
import com.zrb.mobile.ui.ListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.ui.fragment.ABaseAdapter.AbstractItemView;
import com.zrb.mobile.ui.fragment.ABaseRefreshFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.TimeUtil;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;
 
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.widget.ListView;

public class UserCard_DynamictFragment extends ABaseRefreshFragment<Dynamic_CardDto> implements onLoadMoreListener     {
	 
	private long mTempTime = 0;
	private String mUrlId;
	boolean isFirstIn = true;
	List<Dynamic_CardDto> curDtos;
 	
    @ViewInject(idStr = "id_stickynavlayout_innerscrollview")
    ListViewEx mListView;
     
    @ViewInject(idStr = "commonloading")
    CustomloadingView mloadingview;
  
	public static UserCard_DynamictFragment getNewFrament(List<Dynamic_CardDto> dynamicList,String userid) {

		UserCard_DynamictFragment tmp = new UserCard_DynamictFragment();
		tmp.curDtos = dynamicList;
		tmp.mUrlId=userid;
		return tmp;
	}
  

	@Override
	protected int inflateContentView() {
		// TODO Auto-generated method stub
		return R.layout.usercard_fragment_tab;
	}

 
	
	@Override
	 protected void layoutInit(LayoutInflater inflater, Bundle savedInstanceSate) {
 
		 mListView.checkloadMore(1);
		 mListView.setOnLoadMoreListener(this);
	 }
	
	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}

	private void firstLoad() {
		if (isFirstIn) {
			isFirstIn = false;
		/*	Message message = handler.obtainMessage(1);  
			handler.sendMessageDelayed(message, 2500);*/
 			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					//loadUserFundData(-1,true);
					if(CommonUtil.isEmpty(curDtos)){
						mloadingview.HideText(UserCard_DynamictFragment.this.getString(R.string.empty_result_tips)).ShowNull();
						return;
					}
					mloadingview.Hide();
				  	addTimeLine();
	 				addItems(curDtos,true);
					notifyDataSetChanged();
				}
			}, 1000);  
		}
	}
  	
	private void addTimeLine() {
		for (int index = 0; index < curDtos.size(); index++) {
			Dynamic_CardDto tempDiscoverDto = curDtos.get(index);
  			long time = tempDiscoverDto.createTime;

			if (!TimeUtil.isSameDayOfMillis(time, mTempTime)) {
				tempDiscoverDto.showTime=true;
				mTempTime = time;
			} else {
				tempDiscoverDto.showTime = false;
			}
 		}
	}
	
	@Override
	protected AbstractItemView<Dynamic_CardDto> newItemView() {
		// TODO Auto-generated method stub
		return new TimelineDynamicItemView(this);
	}

	@Override
	public ListView getRefreshView() {
		// TODO Auto-generated method stub
		return mListView;
	}

	@Override
	public void onRefreshViewComplete() {
		// TODO Auto-generated method stub
		//CommonUtil.showToast("onRefreshViewComplete");
	}

	@Override
	public void onHandlerResult(String resStr,RefreshMode curMode) {
		// TODO Auto-generated method stub
 	}


	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		
	}



	 
	 
 
}
