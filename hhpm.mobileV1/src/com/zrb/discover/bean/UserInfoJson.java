package com.zrb.discover.bean;

public class UserInfoJson {
	  public int res;
	  public String msg;
	  public UserInfo data;
	  
	  public class UserInfo {
			public long uid;
			public String industryName;
			public long industryId;
			public String org;
			public String umobile;
			public String position;
			public String avatar;
			public String realName;
			public int intentionCount;
			public int projectCount;
			public int fundCount;
			
		}
}
