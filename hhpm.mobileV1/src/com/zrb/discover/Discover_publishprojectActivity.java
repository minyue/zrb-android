package com.zrb.discover;

import java.math.BigDecimal;
import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.R;
import com.zrb.mobile.User_locationActivity;
import com.zrb.mobile.User_registerIndustry;
import com.zrb.mobile.adapter.Cityindustry_listAdapter;
import com.zrb.mobile.adapter.model.IndustrydistributDto;
import com.zrb.mobile.adapter.model.IntentionDto;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Discover_publishprojectActivity extends DiscoverPubishActivity
		implements OnClickListener {
	private int selectId = -1;
	 
	ExpandableListView mindustrylist;
	Cityindustry_listAdapter mindustryAdapter;
	int postCode = -1;
	private String district, industry;

 

	private String name;
	private TextView tv_industry_value, tv_publisharea_value;
	private EditText edit_publishmoney_name;
	private String publishmoney_name, investment_amount, content;
	private EditText edit_investment_amount, edit_content;
	private LinearLayout tmpContainer1;
	private LinearLayout tmpContainer2;
	private int mode;
	private TextView tv_industry_name, tv_publisharea_name, tv_Investment_name;
	ZrbRestClient mZrbRestClient;

	String curObj;
	IntentionDto curDto;
	private TextView txtTitle = null;
	private TextView mDel = null;
	private TextView mTxtrightTitle;

	public static void launch (Context mActivity){
		
		 if(mActivity==null) return;
		 Intent it = new Intent(mActivity, Discover_publishprojectActivity.class);
		 mActivity.startActivity(it);
	}
	
	// public String ACTION_NAME = "discover_main";
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		this.setContentView(R.layout.project_publishproj);
		isforModify = this.getIntent().getBooleanExtra("isModify", false);
		initView();
		if (isforModify) {
			curObj = this.getIntent().getStringExtra("jsonObj");
			Gson gson = new Gson();

			curDto = gson.fromJson(curObj, IntentionDto.class);
			loadIntetionData(curDto.id);
		}
	}

	protected void initView() {
		super.initView();
		mProgressBar = (ProgressBar) this.findViewById(R.id.topbar_loading);
		mDel = (TextView) findViewById(R.id.delete);
		mDel.setVisibility(View.INVISIBLE);
		  
	 
		this.findViewById(R.id.title_left_root).setOnClickListener(this);
		txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setText(getResources().getString(R.string.public_project));

		 
		mTxtrightTitle = (TextView) this.findViewById(R.id.right_title);
		mTxtrightTitle.setText(isforModify ? "完成" : "发布");
		if (isforModify) {
			txtTitle.setText(getResources().getString(R.string.discover_project));
			mDel.setVisibility(View.VISIBLE);
			setEditButtonState(false);
		}
	 
	 
		tv_industry_value = (TextView) findViewById(R.id.tv_industry_value);
		tv_publisharea_value = (TextView) findViewById(R.id.tv_publisharea_value);
		
		
		edit_publishmoney_name = (EditText) findViewById(R.id.edit_publishmoney_name);
		edit_investment_amount = (EditText) findViewById(R.id.edit_investment_amount);
		tv_industry_name = (TextView) findViewById(R.id.tv_industry_name);
		tv_publisharea_name = (TextView) findViewById(R.id.tv_publisharea_name);
		tv_Investment_name = (TextView) findViewById(R.id.tv_Investment_name);
	 
	 
		edit_content = (EditText) findViewById(R.id.edit_content_text);
		mTxtrightTitle.setVisibility(View.VISIBLE);
		mTxtrightTitle.setOnClickListener(this);
	 
		 findViewById(R.id.re_publish_area).setOnClickListener(this);;
		 findViewById(R.id.re_industry).setOnClickListener(this);;
		
		tv_industry_name.setText("所属行业");
		tv_publisharea_name.setText("所在地区");
		tv_Investment_name.setText("融资金额");
 
		((TextView) this.findViewById(R.id.tv_publishmoney_name)).setText(this.getString(R.string.project_publish_prjdes));
		edit_publishmoney_name.setHint(this.getString(R.string.project_publish_prjdeshite));
		
		
		// 设置控件
		tmpContainer1 = (LinearLayout) this.findViewById(R.id.check_option1);
		for (int i = 0; i < tmpContainer1.getChildCount(); i++) {
			tmpContainer1.getChildAt(i).setOnClickListener(checkboxListener);
		}
		tmpContainer2 = (LinearLayout) this.findViewById(R.id.check_option2);
		for (int i = 0; i < tmpContainer2.getChildCount(); i++) {
			tmpContainer2.getChildAt(i).setOnClickListener(checkboxListener);
		}
		mDel.setOnClickListener(this);
	}

	@Override
	protected void setValue(IntentionDto dto) {
		if (dto != null) {
			curDto = dto;
		}
		mDel.setVisibility(View.VISIBLE);
		edit_publishmoney_name.setText(curDto.intro + "");
		tv_industry_value.setText(curDto.industryName);
		industry = curDto.industry;
		tv_publisharea_value.setText(curDto.districtName);
		district = curDto.district;
		edit_investment_amount.setText(curDto.amount + "");
		int index = 0;
		mode = curDto.mode;
		switch (curDto.mode) {
		case Constants.ProjectStockRight:
			index = 0;
			break;
		case Constants.ProjectCreditorRight:
			index = 1;
			break;
		case Constants.ProjectBULK_ACQUISITION:
			index = 2;
			break;
		case Constants.ProjectPROJECT_COOPERATION:
			index = 3;
		case Constants.ProjectOtherRight:
			index = 4;
			break;
		}

		if (index <= 2) {
			selectId = tmpContainer1.getChildAt(index).getId();
			tmpContainer1.getChildAt(index).setSelected(true);
		} else {
			selectId = tmpContainer2.getChildAt(index-3).getId();
			tmpContainer2.getChildAt(index-3).setSelected(true);
		}
		edit_content.setText(curDto.content);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.right_title:
			saveData();
			break;
		case R.id.re_publish_area:
			intent = new Intent(this, User_locationActivity.class);
			startActivityForResult(intent, 200);
			break;

		case R.id.re_industry:
			intent = new Intent(this, User_registerIndustry.class);
			intent.putExtra("isProject", true);
			startActivityForResult(intent, 100);
			break;
		case R.id.delete:
			delete();
			break;
		}
	}

	private void saveData() {

		publishmoney_name = edit_publishmoney_name.getText().toString();
		investment_amount = edit_investment_amount.getText().toString();
		content = edit_content.getText().toString();
		// 发布项目进行非空的判断
		if ("".equals(publishmoney_name)) {
			initNotify("请填写项目简介");
			return;
		}
		if (publishmoney_name.length() > 20) {
			initNotify("项目简介字数大于20");
			return;
		}
		if (TextUtils.isEmpty(industry)) {
			initNotify("请选择所属行业");
			return;
		}
		if (TextUtils.isEmpty(district)) {
			initNotify("请选择所在地区");
			return;
		}
		if (TextUtils.isEmpty(investment_amount)) {
			initNotify("请填写融资金融");
			return;
		}
		if (mode == 0) {
			initNotify("请选择融资方式");
			return;
		}
		if (TextUtils.isEmpty(content)) {
			initNotify("请填写项目详情");
			return;
		}
		if (null == AppSetting.curUser) {
			CommonUtil.showToast(R.string.user_no_account,
					Discover_publishprojectActivity.this);
			return;
		}
		publicMessage(Constants.DiscoverProjectIntent, curDto);
	}

	@Override
	protected void setforResult() {
		curDto.intro = publishmoney_name;
		curDto.district = district;
		curDto.districtName = tv_publisharea_value.getText().toString();
		curDto.industry = industry;
		curDto.industryName = tv_industry_value.getText().toString();
		curDto.mode = mode;
		curDto.content = content;
		curDto.amount = new BigDecimal(investment_amount);
		public_success(curDto,RESULT_CODE_MODIFY);
	}

	private OnClickListener checkboxListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.getId() != selectId) {
				v.setSelected(true);
				if (selectId != -1)
					Discover_publishprojectActivity.this.findViewById(selectId)
							.setSelected(false);
				;
				selectId = v.getId();
			}

			switch (v.getId()) {
			case R.id.tv_temp_name1:
				// 股权
				mode = Constants.ProjectStockRight;
				break;
			case R.id.tv_temp_name2:
				// 债权
				mode = Constants.ProjectCreditorRight;
				break;
			case R.id.tv_temp_name3:
				mode = Constants.ProjectBULK_ACQUISITION;
				break;
			case R.id.tv_temp_name4:
				mode = Constants.ProjectPROJECT_COOPERATION;
				break;
			case R.id.tv_temp_name5:
				mode = Constants.ProjectOtherRight;
				break;
			}

		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
 			case 100:
				if (data != null) {
					name = data.getStringExtra("name");
					industry = data.getIntExtra("code", 0) + "";
					tv_industry_value.setText(name);
				}
				break;
			case 200:
				if (data != null) {
					name = data.getStringExtra("area");
					district = data.getStringExtra("code");
					if (name.contains(" ")) {
						name = name.split(" ")[0];
						district = district.substring(0, 2) + "0000";
					}
 					tv_publisharea_value.setText(name);
 				}
				break;
			default:
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);

	}

	private void delete() {
		showloading(getResources().getString(R.string.deleting));
		RequestParams param = new RequestParams();
		param.put("id", curDto.id);

		CommonUtil.InfoLog("param", param.toString());
		if (null == mZrbRestClient) {
			mZrbRestClient = new ZrbRestClient(this);
		}
		mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				hideloading();
				initNotify(Discover_publishprojectActivity.this
						.getString(R.string.error_net_loadfail));
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String results = new String(arg2);
				hideloading();
				try {
					Gson gson = new Gson();
					IndustrydistributDto tmpDto = gson.fromJson(results,
							IndustrydistributDto.class);
					if (tmpDto.res == 1) {
						CommonUtil.showToast(
								getResources().getString(
										R.string.delete_success),
								Discover_publishprojectActivity.this);
						setDelForResult();
					} else {
						initNotify(tmpDto.msg);
					}
				} catch (Exception e) {
					initNotify(Discover_publishprojectActivity.this
							.getString(R.string.error_serverdata_loadfail));
				}

			}
		});
		mZrbRestClient.post("intention/delete", param);
	}

	@Override
	protected void setDelForResult() {
		public_success(curDto,RESULT_CODE_DEL);
	}

	@Override
	protected void setEditButtonState(boolean flag) {
		mTxtrightTitle.setEnabled(flag);
		mDel.setEnabled(flag);
	}

	@Override
	protected RequestParams getParams() {
		RequestParams param = new RequestParams();
		param.put("intro", publishmoney_name);
		param.put("district", district);
		param.put("industry", industry);
		param.put("amount", investment_amount);
		param.put("mode", mode);
		param.put("type", Constants.DiscoverProjectIntent);
		param.put("content", content);
		return param;
	}

}
