package com.zrb.discover;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
 

import com.hhpm.lib.support.inject.ViewInject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
 
import com.zrb.applib.utils.AppSetting;
import com.zrb.discover.widget.TimelineView2;
import com.zrb.mobile.MoneyDetailActivity;
import com.zrb.mobile.ProjectDetailActivity;
import com.zrb.mobile.R;
  
import com.zrb.mobile.adapter.model.ChoiceGenera_CardDto;
import com.zrb.mobile.adapter.model.Dynamic_CardDto;
 
import com.zrb.mobile.ui.fragment.ABaseAdapter;
import com.zrb.mobile.ui.fragment.ABaseFragment;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;

/**
 * Created by wangdan on 15/4/15.
 */
public class TimelineChoicenessItemView extends ABaseAdapter.AbstractItemView<ChoiceGenera_CardDto>
                implements View.OnClickListener  {

    protected ABaseFragment fragment;
    
    private SimpleDateFormat sdf= new SimpleDateFormat("yyyy.MM.dd");//yyyy.
    int FundColor=Color.parseColor("#b68f85");
	int ProjectColor=Color.parseColor("#8ea0be");
	  DisplayImageOptions options;
	
	
    public TimelineChoicenessItemView(ABaseFragment fragment ) {
         this.fragment = fragment;
	     options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.loadpic)
		.showImageForEmptyUri(R.drawable.loadpic)
		.showImageOnFail(R.drawable.loadpic).cacheInMemory(false)
	//	.displayer(new FadeInBitmapDisplayer(300))
		.cacheOnDisc(true)
		.build();
  
    }

  
    
   /*holder = new ViewHolder();
			holder.title = (TextView) view.findViewById(R.id.id_project_title);
			holder.image = (ImageView) view.findViewById(R.id.id_projectsImg);
			//holder.projectType = (ImageView) view.findViewById(R.id.id_projectstype);
			//holder.txt_project_pppType = (TextView) view.findViewById(R.id.txt_project_pppType);
			//holder.project_date = (TextView) view.findViewById(R.id.project_date);
			holder.project_address = (TextView) view.findViewById(R.id.project_address);
			holder.project_auth_status = (TextView) view.findViewById(R.id.project_auth_status);
			holder.project_price = (TextView) view.findViewById(R.id.project_price);
			holder.mlefticon = (ImageView) view.findViewById(R.id.left_icon);
 
			holder.pppType = (TextView) view.findViewById(R.id.txt_project_pppType);
			//holder.price = (TextView) view.findViewById(R.id.project_price);
			holder.publishDate = (TextView) view.findViewById(R.id.project_date);*/

    
    @ViewInject(id = R.id.timelayout)
    View timelayout;
    
    @ViewInject(id = R.id.timelines)
    TimelineView2 lineView; 
    
    @ViewInject(id = R.id.timelinetxt)
    TextView timelinetxt; 
 
    
    @ViewInject(id = R.id.id_projectsImg)
    ImageView mpic;
    
    @ViewInject(id = R.id.txt_choiceness_title)
    TextView mtitle;

    @ViewInject(id = R.id.attention_txt)
    TextView mattention;
    
    @ViewInject(id = R.id.comment_txt)
    TextView mcomment;
    
     
    @ViewInject(id = R.id.project_match_txt)
    TextView mactiontxt;
    

    @Override
    public int inflateViewId() {
        return R.layout.usercard_choiceness_list_item;
    }
   
    @Override
    public void bindingData(View convertView, ChoiceGenera_CardDto curDto) {
  		
    	
    	mtitle.setText(curDto.title);
     	if(!TextUtils.isEmpty(curDto.pic))
	 		ImageLoader.getInstance().displayImage(AppSetting.mediaServer+curDto.pic, mpic, options);  
	 	else
	 		mpic.setImageResource(R.drawable.loadpic); 
     	if(curDto.showTime) 
    		timelayout.setVisibility(View.VISIBLE);
    	else
    	    timelayout.setVisibility(View.GONE); 
    	 
    	 timelinetxt.setText(CommonUtil.formatlongDate(curDto.publishTime, sdf)); 
    	// 	dynamic_content.setText(curDto.title); 
		switch(curDto.choiceType){
 		case 2:
			mactiontxt.setText("资金详情");
			//dynamic_title.setTextColor(FundColor);
			break;
		case 1:
			mactiontxt.setText("项目详情");
			//dynamic_title.setTextColor(ProjectColor);
			break;
 		}
 
		mattention.setText(curDto.collectionCount+"");
		mcomment.setText(curDto.advisoryCount+"");
		mactiontxt.setOnClickListener(this);
		mactiontxt.setTag(curDto);
    }
 
    @Override
    public void onClick(View v) {
    	switch(v.getId() ){
    	case R.id.project_match_txt:
     		 final ChoiceGenera_CardDto status = (ChoiceGenera_CardDto) v.getTag();
     		  if(status.choiceType==1)
				  ProjectDetailActivity.launch(fragment.getActivity(), status.bizId, status.title);
				else
				  MoneyDetailActivity.launch(fragment.getActivity(), status.bizId, status.title);
     		 break;
     	}
      
    }

    /*@Override
    public void onLikeRefreshUI() {
        if (fragment != null && fragment instanceof ARefreshFragment)
            ((ARefreshFragment) fragment).refreshUI();
    }

    @Override
    public void onLikeRefreshView(StatusContent data, final View likeView) {
        if (fragment.getActivity() == null)
            return;

        if (likeView.getTag() == data) {
            animScale(likeView);
        }
    }*/

 
	

}
