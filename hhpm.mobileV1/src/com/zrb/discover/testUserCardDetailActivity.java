package com.zrb.discover;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.zrb.discover.bean.UserInfoJson;
import com.zrb.mobile.IhandlerWrapper;
import com.zrb.mobile.R;
import com.zrb.mobile.ui.CircleImageView;
import com.zrb.mobile.ui.SimpleViewPagerIndicator;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ProgressDialogEx;
import com.zrb.mobile.utility.Tip;
import com.zrb.mobile.utility.ViewUtil;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class testUserCardDetailActivity extends SherlockFragmentActivity implements OnClickListener,IhandlerWrapper {
	private String[] mTitles = new String[] { "publish", "project", "foud" };
	
 
	View customNav;
	private SimpleViewPagerIndicator mIndicator;
	private ViewPager mViewPager;
	private FragmentPagerAdapter mAdapter;
	private List<Fragment> fragments = new ArrayList<Fragment>();
	private int mCurrentPage = 0;
	ZrbRestClient mZrbRestClient;
	private String mUserName = "";
	private String mAvatarUrl = "";
	private String mOrg = "";
	private String mIndustry = "";
	private String mJob = "";
	private int mUserId = 0;
	UserInfoJson.UserInfo userInfo;
	private CircleImageView mCircleImageView = null;
	private TextView mUserNameView = null;
	private TextView mJobView = null;
	Handler myhandler = new Handler();
	 Tip	progTip ;
	
	 @Override
	 public Handler getHandler(){
		 return myhandler;
	 }
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.project_content_detail);

		mUserName = getIntent().getStringExtra(UserCardDetailActivity.USER_NAME);
		mUserId = getIntent().getIntExtra(UserCardDetailActivity.USER_ID, 0);
		mAvatarUrl = getIntent().getStringExtra(UserCardDetailActivity.USER_AVATAR_URL);
		mOrg = getIntent().getStringExtra(UserCardDetailActivity.USER_ORG);
		mJob = getIntent().getStringExtra(UserCardDetailActivity.USER_JOB);
		mIndustry = getIntent().getStringExtra(UserCardDetailActivity.USER_INDUSTRY);

		initView();
		loadUserDescriptionData();
	}

	

	private void initView() {

		mCircleImageView = (CircleImageView) this.findViewById(R.id.business_user_avatar);
		mUserNameView = (TextView) this.findViewById(R.id.business_user_name);
		mJobView = (TextView) this.findViewById(R.id.business_user_job);
		
/*		View lefticon = this.findViewById(R.id.title_left_root);
		lefticon.setOnClickListener(this);
		TextView txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setVisibility(View.GONE);*/
		mIndicator = (SimpleViewPagerIndicator) findViewById(R.id.id_stickynavlayout_indicator);
		mIndicator.setTitles(mTitles);
		mViewPager = (ViewPager) findViewById(R.id.id_stickynavlayout_viewpager);
		mViewPager.setOffscreenPageLimit(3);

		fragments.add(TestUserCard_IntentTabFragment.newInstance("publish", mUserId+""));
		fragments.add(TestUserCard_FundTabFragment.newInstance("project", mUserId+""));
		fragments.add(TestUserCard_IntentTabFragment.newInstance("foud",  mUserId+""));
		mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
			@Override
			public int getCount() {
				return mTitles.length;
			}

			@Override
			public Fragment getItem(int position) {
				return fragments.get(position);

			}

		};

		mViewPager.setAdapter(mAdapter);
		mViewPager.setCurrentItem(0);

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				mIndicator.scroll(position, positionOffset);
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
	}

	private void loadUserDescriptionData() {
		RequestParams param = new RequestParams();
		param.put("userId", mUserId);
  
		if (mZrbRestClient == null) {
			mZrbRestClient = new ZrbRestClient(this);
		}
		mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {

				CommonUtil.showToast(R.string.error_net_loadfail, testUserCardDetailActivity.this);
				//completeRefresh();
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String results = new String(arg2);
				try {
					Gson gson = new Gson();
					UserInfoJson fundJson = gson.fromJson(results, UserInfoJson.class);
					if (fundJson.res == 1) {

						if (fundJson.data != null) {
							userInfo = fundJson.data;
							mUserName = userInfo.realName;//.realName;
							mAvatarUrl = userInfo.avatar;//.avatar;
							mOrg = userInfo.org;//.org;
							mJob = userInfo.position;
							mIndustry = userInfo.industryName;//.industry;
 
						} else {
							CommonUtil.showToast(String.format("暂无%1$s信息~~", mCurrentPage == 0 ? "" : "更多"), testUserCardDetailActivity.this);
						}
					} else {
						CommonUtil.showToast(fundJson.msg, testUserCardDetailActivity.this);
					}
				} catch (Exception e) {
					CommonUtil.showToast(R.string.error_serverdata_loadfail, testUserCardDetailActivity.this);
				}
				fillData();
				//requestIntentionList();
			}
		});
		mZrbRestClient.post("businessCard/myCard", param);
	}
	
	private void fillData(){
		ViewUtil.showUserAvatar(mCircleImageView,mAvatarUrl);
		if(TextUtils.isEmpty(mUserName)){
			mUserName = getResources().getString(R.string.anonymity);
		}
		mUserNameView.setText(mUserName);
		String str = "";
		if(!TextUtils.isEmpty(mIndustry)){
			str = mIndustry;
		}
		if(!TextUtils.isEmpty(mOrg)){
			if(!TextUtils.isEmpty(mIndustry)){
				str = str + Constants.FLAG;
			}
			str = str + mOrg;
		}
		if(!TextUtils.isEmpty(mJob)){
			if(!TextUtils.isEmpty(mIndustry) || !TextUtils.isEmpty(mOrg)){
				str = str + Constants.FLAG;
			}
			str = str + mJob;
		}
		if(TextUtils.isEmpty(str)){
			str = getResources().getString(R.string.secrecy);
		}
		mJobView.setText(str);   
		
		if(userInfo!=null){
	/*		((TextView) this.findViewById(R.id.tv_publish_number)).setText(userInfo.intentionCount+"");
			((TextView) this.findViewById(R.id.tv_user_project)).setText(userInfo.projectCount+"");
			((TextView) this.findViewById(R.id.tv_user_fund)).setText(userInfo.fundCount+"");*/
		}
	}
	
	public void showloading() {
		if (progTip == null)
			progTip = ProgressDialogEx.Show(this, " title", "正在加载中...", true,
					true);
		else
			progTip.Show("正在加载中...");
	}
	
	public void hideloading(){
		
		if (progTip != null) progTip.Dismiss();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;

		}
	}

}
