package com.zrb.discover;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.zrb.discover.bean.IntentionMsg;
import com.zrb.matching.MatchingActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.RegisterBaseActivity;
import com.zrb.mobile.adapter.model.IndustrydistributDto;
import com.zrb.mobile.adapter.model.IntentionDto;
import com.zrb.mobile.adapter.model.RegisterDto;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public abstract class DiscoverPubishActivity extends RegisterBaseActivity {
	
	protected ProgressBar mProgressBar;
	protected ZrbRestClient mZrbRestClient;
	protected boolean isforModify = false;
	public static final int RESULT_CODE_ADD = 0;
	public static final int RESULT_CODE_MODIFY = 1;
	public static final int RESULT_CODE_DEL = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
	}

	protected void init() {
		mProgressBar = (ProgressBar) this.findViewById(R.id.topbar_loading);
	}

	protected void loadIntetionData(long id) {
		mProgressBar.setVisibility(View.VISIBLE);
		RequestParams param = new RequestParams();
	 
		param.put("id", id);
 		String url = "intention/toEdit?" + param.toString();
		if (mZrbRestClient == null) {
			mZrbRestClient = new ZrbRestClient(this);
		}
		mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				CommonUtil.showToast(arg0, DiscoverPubishActivity.this);
				mProgressBar.setVisibility(View.INVISIBLE);
				setValue(null);
				setEditButtonState(true);
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String results = new String(arg2);
				IntentionDto dto = null;
				try {
					Gson gson = new Gson();
					IntentionMsg json = gson.fromJson(results, IntentionMsg.class);
					if (json.res == 1) {
						if (json.data != null) {
							dto = json.data;
						} else {
							initNotify(json.msg);
						}

					} else {
						initNotify(json.msg);
					}
					mProgressBar.setVisibility(View.INVISIBLE);
					setValue(dto);
					setEditButtonState(true);
				} catch (Exception e) {
					CommonUtil.showToast(R.string.error_serverdata_loadfail, DiscoverPubishActivity.this);
				}
			}
		});
		mZrbRestClient.sessionGet(url);
	}

	protected abstract void setValue(IntentionDto dto);

	protected abstract void setEditButtonState(boolean flag);

	protected abstract void setforResult();

	protected abstract RequestParams getParams();

	protected abstract void setDelForResult();

	protected void publicMessage(final int type, IntentionDto dto) {
		showloading("正在提交中...");
		RequestParams param = getParams();

		CommonUtil.InfoLog("param", param.toString());
		if (null == mZrbRestClient) {
			mZrbRestClient = new ZrbRestClient(this);
		}
		mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				hideloading();
				initNotify(DiscoverPubishActivity.this.getString(R.string.error_net_loadfail));
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String results = new String(arg2);
				CommonUtil.InfoLog("publicMessage", results);
				hideloading();
				try {
					Gson gson = new Gson();
					RegisterDto tmpDto = gson.fromJson(results, RegisterDto.class);
					if (tmpDto.res == 1) {
						CommonUtil.showToast(isforModify ? "修改成功" : "发布成功", DiscoverPubishActivity.this);
						if (isforModify) {
							setforResult();
						} else {
							
							 Intent tmpIntent = new Intent(DiscoverPubishActivity.this, MatchingActivity.class);
							 tmpIntent.putExtra("actiontype", type);
							 tmpIntent.putExtra("curId",Integer.parseInt(tmpDto.data) );
							 
							 startActivity(tmpIntent);
							 DiscoverPubishActivity.this.finish();
							 
						}
					} else {
						initNotify(tmpDto.msg);
					}
				} catch (Exception e) {
					initNotify(DiscoverPubishActivity.this.getString(R.string.error_serverdata_loadfail));
				}

			}
		});

		if (isforModify && dto != null) {
			param.put("id", dto.id);
		}
		mZrbRestClient.post(isforModify ? "intention/edit" : "intention/add", param);
	}

	protected void delete(long id) {
		showloading(getResources().getString(R.string.deleting));
		RequestParams param = new RequestParams();
		param.put("id", id);

		CommonUtil.InfoLog("param", param.toString());
		if (null == mZrbRestClient) {
			mZrbRestClient = new ZrbRestClient(this);
		}
		mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				hideloading();
				initNotify(DiscoverPubishActivity.this.getString(R.string.error_net_loadfail));
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String results = new String(arg2);
				hideloading();
				try {
					Gson gson = new Gson();
					IndustrydistributDto tmpDto = gson.fromJson(results, IndustrydistributDto.class);
					if (tmpDto.res == 1) {
						CommonUtil.showToast(R.string.delete_success, DiscoverPubishActivity.this);
						setDelForResult();
					} else {
						initNotify(tmpDto.msg);
					}
				} catch (Exception e) {
					initNotify(DiscoverPubishActivity.this.getString(R.string.error_serverdata_loadfail));
				}

			}
		});
		mZrbRestClient.post("intention/delete", param);
	}

	protected void public_success(IntentionDto dto, int type) {
		Gson gson = new Gson();
		DiscoverPubishActivity.this.finish();
		Intent intent1 = new Intent(Constants.Refresh_Find_mainlist);
		if (dto != null) {
			intent1.putExtra("jsonStr", gson.toJson(dto));
		}
		intent1.putExtra("type", type);
		sendBroadcast(intent1);
		Intent tmpIntent = new Intent(this, MatchingActivity.class);
	/*	tmpIntent.putExtra("actiontype", value);
		tmpIntent.putExtra("curId", value);
		*/
		startActivity(tmpIntent);
		this.finish();
	}

}
