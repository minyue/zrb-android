/*
 * Copyright 2015 chenupt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zrb.discover;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
 
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

 

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.hhpm.lib.ui.DragTopLayout;
import com.zrb.mobile.IhandlerWrapper;
import com.zrb.mobile.R;
import com.zrb.mobile.ui.SimpleViewPagerIndicator;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.Tip;

 


public class DiscoverMainActivity extends SherlockFragmentActivity implements IhandlerWrapper {

    public static final String TAG = "MainActivity";

	private String[] mTitles = new String[] { "publish", "project", "foud" };
	
	 
	View customNav;
	private SimpleViewPagerIndicator mIndicator;
    private DragTopLayout dragLayout;
  
    private ViewPager viewPager;
 
    private ImageView topImageView;
    private List<Fragment> fragments = new ArrayList<Fragment>();
    
    
	private String mUserName = "";
	private String mAvatarUrl = "";
	private String mOrg = "";
	private String mIndustry = "";
	private String mJob = "";
	private int mUserId = 0;
	Handler myhandler = new Handler();
	 Tip	progTip ;
	
	 @Override
	 public Handler getHandler(){
		 return myhandler;
	 }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dragtoplayout);


		mUserName = getIntent().getStringExtra(UserCardDetailActivity.USER_NAME);
		mUserId = getIntent().getIntExtra(UserCardDetailActivity.USER_ID, 0);
		mAvatarUrl = getIntent().getStringExtra(UserCardDetailActivity.USER_AVATAR_URL);
		mOrg = getIntent().getStringExtra(UserCardDetailActivity.USER_ORG);
		mJob = getIntent().getStringExtra(UserCardDetailActivity.USER_JOB);
		mIndustry = getIntent().getStringExtra(UserCardDetailActivity.USER_INDUSTRY);
        
    	
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dragLayout = (DragTopLayout) findViewById(R.id.drag_layout);
     
    	mIndicator = (SimpleViewPagerIndicator) findViewById(R.id.id_stickynavlayout_indicator);
		mIndicator.setTitles(mTitles);
 

        // init DragTopLayout
        DragTopLayout.from(this)
                .open()
                .setRefreshRadio(1.4f)
                .listener(new DragTopLayout.SimplePanelListener() {
                    @Override
                    public void onSliding(float radio) {
                        Log.d(TAG, "sliding: " + radio);
                    }
                }).setup(dragLayout);

     /*   // init pager
        PagerModelManager factory = new PagerModelManager();
        factory.addCommonFragment(TestListFragment.class, getTitles(), getTitles());
        adapter = new ModelPagerAdapter(getSupportFragmentManager(), factory);*/
        dragLayout.setOverDrag(!dragLayout.isOverDrag());

		fragments.add(TestUserCard_IntentTabFragment.newInstance("publish", mUserId+""));
		fragments.add(TestUserCard_FundTabFragment.newInstance("project", mUserId+""));
		fragments.add(TestUserCard_IntentTabFragment.newInstance("foud",  mUserId+""));
		FragmentPagerAdapter	mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
			@Override
			public int getCount() {
				return mTitles.length;
			}

			@Override
			public Fragment getItem(int position) {
				return fragments.get(position);

			}

		};
        
        viewPager.setAdapter(mAdapter);
        
    }

   

    // Handle scroll event from fragments
    public void onEvent(Boolean b){
        dragLayout.setTouchMode(b);
    }

    @Override
    protected void onResume() {
        super.onResume();
       // EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
       // EventBus.getDefault().unregister(this);
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_menu_icon) {
            if(topImageView.getVisibility() == View.GONE){
                topImageView.setVisibility(View.VISIBLE);
            }else{
                topImageView.setVisibility(View.GONE);
            }
            return true;
        } else if(id == R.id.action_toggle){
            dragLayout.toggleTopView();
            return true;
        } else if(id == R.id.action_over_drag){
            dragLayout.setOverDrag(!dragLayout.isOverDrag());
            return true;
        } else if(id == R.id.action_about){
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
