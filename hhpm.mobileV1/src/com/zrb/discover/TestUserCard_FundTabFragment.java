package com.zrb.discover;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.zrb.discover.adapter.FundAdapter;
import com.zrb.mobile.BaseLazyFragment;
import com.zrb.mobile.FindMoney_MainFragment;
import com.zrb.mobile.IhandlerWrapper;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.FindMoneyAdapter;
import com.zrb.mobile.adapter.model.DiscoverDtos;
import com.zrb.mobile.adapter.model.FindMoneyDtos;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;
 
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;
 

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class TestUserCard_FundTabFragment extends BaseLazyFragment implements onLoadMoreListener
{
	public static final String TITLE = "title";
	public static final String URLID = "urlId";
	private String mTitle = "Defaut Value";
	private String mUrlId;
	private ListView mListView;
	// private TextView mTextView;
	private List<String> mDatas = new ArrayList<String>();
	boolean isFirstIn = true;
/* 
	//private News_ContentAdapter mAdapter;*/
	private boolean isFrist=true;
	FindMoneyAdapter mAdapter2;
 
	private String url;
	int currentPage;
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mTitle = getArguments().getString(TITLE);
			mUrlId= getArguments().getString(URLID);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
	{
		View view;
		//if(mTitle=="11") 
		{
	  	   view = inflater.inflate(R.layout.usercard_fragment_tab2, container, false);
		  mListView = (ListView) view.findViewById(R.id.id_stickynavlayout_innerscrollview);
		  
		// mTextView = (TextView) view.findViewById(R.id.id_info);
		// mTextView.setText(mTitle);
 
		
	   /*	  mAdapter = new News_ContentAdapter(this.getActivity());
		  mListView.setAdapter(mAdapter);*/
		}
	/*	else
			view = inflater.inflate(R.layout.no_content, container, false);*/
		return view;

	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
 		super.onActivityCreated(savedInstanceState);
 	/*	
 		mListView.setPagesize(10);
		// Set a listener to be invoked when the list should be refreshed.
 		mListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {

				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(CommonUtil.getcurTime(getActivity()));
				currentPage = 1;
				postDelayed(new Runnable() {
					@Override
					public void run() {
						LoadData(-1,true);
					}
				} ,500);
			}
		});*/
 		
		mAdapter2 = new FindMoneyAdapter(getActivity());
		mListView.setAdapter(mAdapter2);
 
  	 
	}
	
	private void postDelayed(Runnable curRunnable,long delay){
		if( this.getActivity()==null) return;
		
		IhandlerWrapper tmpFrag = (IhandlerWrapper) this.getActivity();
		if (tmpFrag != null)
			tmpFrag.getHandler().postDelayed(curRunnable,delay);
	}
	
/*	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}*/

	@Override
	public void onFirstUserVisible() {
		if (isFirstIn) {
			isFirstIn = false;
 			postDelayed(new Runnable() {
				@Override
				public void run() {
					LoadData(-1,true);
				}
			} ,50);
		}
	}
	
	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		currentPage++;
		postDelayed(new Runnable() {
			@Override
			public void run() {
				LoadData(mAdapter2.getMinTime(), false);
			}
		}, 400);

	}

	
	ZrbRestClient mZrbRestClient;
	private void LoadData(long minTime, boolean isreflesh) {
		  String url = String.format("fund/getUserFundList?limit=%1$s&userId=%2$s",10,mUrlId);// &tradeDates=%2$s
	 
	     if(minTime!=-1)                                    url+="&lastPushTime="+minTime;
	  
 	    CommonUtil.InfoLog("LoadData", url);
 	    if(mZrbRestClient==null){
 	    	mZrbRestClient=new ZrbRestClient(TestUserCard_FundTabFragment.this.getActivity());
 	    	mZrbRestClient.setOnhttpResponseListener( new HttpResponseListener() {
 				@Override
 				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
 				    CommonUtil.showToast(arg0, TestUserCard_FundTabFragment.this.getActivity());
 				/*	if (currentPage > 1)
 						mListView.hideloading();
 					else
 						mListView.onRefreshComplete();*/
 				}

 				@Override
 				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
 					// loadingview.Hide();
 					String results = new String(arg2);
 				/*	if (currentPage == 1) mListView.onRefreshComplete();
 					else
 						mListView.hideloading();*/
 			 
 					try {
 						Gson gson = new Gson();
 						FindMoneyDtos tmpDto = gson.fromJson(results, FindMoneyDtos.class);
 						if (tmpDto.res == 1) {
 							if (tmpDto.data != null && tmpDto.data.size() > 0) {
 								  //emptylayout.checkhide() ;   
 								  mAdapter2.setDatas(tmpDto.data,currentPage>1);
 								  mAdapter2.notifyDataSetChanged();
 							} else {
 								if(currentPage==1){
 									 mAdapter2.CurrentDatas.clear();
 								     mAdapter2.notifyDataSetChanged();
 								  //   emptylayout.show();
 	 							} 
 								else
 									CommonUtil.showToast("暂无更多信息~~",  getActivity());
 							}
 						} else
 							CommonUtil.showToast(tmpDto.msg, getActivity());
 					} catch (Exception e) {
 						CommonUtil.showToast( R.string.error_serverdata_loadfail ,
 								TestUserCard_FundTabFragment.this.getActivity());
 					}
 				}
 			});
 	    }
		mZrbRestClient.sessionGet(url);
	}
	
	

	public static TestUserCard_FundTabFragment newInstance(String title,String uId)
	{
		TestUserCard_FundTabFragment tabFragment = new TestUserCard_FundTabFragment();
		Bundle bundle = new Bundle();
		bundle.putString(TITLE, title);
		bundle.putString(URLID, uId);
		tabFragment.setArguments(bundle);
		return tabFragment;
	}

}
