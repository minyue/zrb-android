package com.zrb.discover.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.News_ShareActivity;
import com.zrb.mobile.PushLiuYanActivity;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.Discover_listAdapter.onActionlistener;
import com.zrb.mobile.adapter.model.DiscoverDto;
import com.zrb.mobile.adapter.model.Dynamic_CardDto;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.TimeUtil;

public class FundAdapter extends BaseAdapter {
	private static final String LOG_TAG = "FundAdapter";
	private static String sInvestmentIndustry = "";
	private static String sInvestmentArea = "";
	private static String sInvestmentStyle = "";
	private static String sFieldCertification = "";

	final class ViewHolder {
		public TextView title;
		public TextView tag;

		public LinearLayout contentLayout;
		public ImageView lefticon;
		public ImageView righticon;
		public TextView tv_message_title;
		public TextView investmentStyle;
		public TextView investmentIndustry;
		public TextView investmentArea;
		// public LinearLayout attentionLayout;
		// public LinearLayout replyLayout;
		// public LinearLayout shareLayout;
		public TextView attention;
		public TextView reply;
		public View share;

		public TextView tv_find_authtype;
		public ImageView certification;
		public LinearLayout contentbar;
	}

	private int _selIndex = 0;
	private Context mcontext;

	private Drawable senceAuthDrawable, hasAuthDrawable;
	public List<Dynamic_CardDto> mCurrentDatas = new ArrayList<Dynamic_CardDto>();
	private LayoutInflater mInflater;
	private onActionlistener monActionlistener;
	private Drawable NoFocus;
	private Drawable isFocus;

	public FundAdapter(Context context) {
		mcontext = context;
		this.mInflater = LayoutInflater.from(context);

		senceAuthDrawable = context.getResources().getDrawable(R.drawable.cashbubble);
		hasAuthDrawable = context.getResources().getDrawable(R.drawable.cashbubble2);
		sInvestmentIndustry = context.getResources().getString(R.string.investment_industry);
		sInvestmentArea = context.getResources().getString(R.string.investment_area);
		sInvestmentStyle = context.getResources().getString(R.string.investment_style);
		sFieldCertification = context.getResources().getString(R.string.field_certification);
		NoFocus = context.getResources().getDrawable(R.drawable.found_like_off);
		isFocus = context.getResources().getDrawable(R.drawable.found_like_on);
	}

	public int getCount() {

		if (mCurrentDatas != null) {
			return mCurrentDatas.size();
		}
		return 0;

	}

	public void setDatas(List<Dynamic_CardDto> fundList) {
		mCurrentDatas.clear();
		mCurrentDatas.addAll(fundList);
	}

	
	
	public void addAll(List<Dynamic_CardDto> fundList) {
		mCurrentDatas.addAll(fundList);
	}

	public long getMinId() {
		if (mCurrentDatas != null && mCurrentDatas.size() > 0)
			return mCurrentDatas.get(mCurrentDatas.size() - 1).id;
		return -1;
	}

	public long getMinTime() {
		if (mCurrentDatas != null && mCurrentDatas.size() > 0) {
			return mCurrentDatas.get(mCurrentDatas.size() - 1).createTime;
		}
		return -1;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.user_fund_item, null);
			holder = new ViewHolder();
			holder.tv_message_title = (TextView) convertView.findViewById(R.id.tv_message_title);
			holder.investmentStyle = (TextView) convertView.findViewById(R.id.investment_amount);
			holder.investmentIndustry = (TextView) convertView.findViewById(R.id.investment_industry);
			holder.investmentArea = (TextView) convertView.findViewById(R.id.investment_area);

			holder.tv_find_authtype = (TextView) convertView.findViewById(R.id.tv_find_authtype);
			holder.attention = (TextView) convertView.findViewById(R.id.fund_attention);
			holder.reply = (TextView) convertView.findViewById(R.id.fund_reply);
			holder.share =   convertView.findViewById(R.id.share);
			
			
			holder.certification = (ImageView) convertView.findViewById(R.id.certification);
			holder.contentbar = (LinearLayout) convertView.findViewById(R.id.contentbar);
			initClickListener(holder);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Dynamic_CardDto fund = mCurrentDatas.get(position);
		holder.tv_message_title.setText(fund.title);

	/*	holder.investmentStyle.setText(fund.type == 101 ? "融资金额: " + fund.amount + "万元整" : "投资金额: " + fund.amount + "万元整");
		holder.investmentArea.setText(fund.type == 101 ? "所在区域: " + fund.districtName : "投资区域: " + fund.districtName);
		holder.investmentIndustry.setText(fund.type == 101 ? "所属行业: " + fund.industryName : "投资行业: " + fund.industryName);*/

		holder.attention.setText(0 + "");
		holder.reply.setText(0 + "");

	 

		if (fund.type == Constants.DiscoverProjectIntent) {
			holder.contentbar.setBackgroundResource(R.drawable.bg_business_card_project);
		} else {
			holder.contentbar.setBackgroundResource(R.drawable.bg_business_card_fund);
		}
	//	holder.attention.setCompoundDrawablesWithIntrinsicBounds(fund.attention == 0 ? NoFocus : isFocus, null, null, null);
		holder.attention.setTag(position);
		holder.reply.setTag(position);
		holder.share.setTag(position);
		return convertView;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	private void initClickListener(ViewHolder holder) {
		holder.attention.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		 
				if (v.getTag() != null) {
					if (AppSetting.curUser == null) {
						if (null != monActionlistener)
							monActionlistener.showLoginDialog("登录后才可以使用该功能");
					} else {
						int pos = Integer.parseInt(v.getTag().toString());
						if (null != monActionlistener)
							monActionlistener.onAttentionEvent(pos);
					}
				}
			}
		});

		holder.reply.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getTag() != null) {
					if (AppSetting.curUser == null) {
						if (null != monActionlistener) {
							monActionlistener.showLoginDialog("登录后才可以使用该功能");
						}
					} else {
						int pos = Integer.parseInt(v.getTag().toString());
						Intent intent = new Intent(mcontext, PushLiuYanActivity.class);
						intent.putExtra("id", mCurrentDatas.get(pos).id);
						intent.putExtra("needsend", false);
						
						mcontext.startActivity(intent);

					}
				}
			}
		});
		holder.share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getTag() != null) {
					int pos = Integer.parseInt(v.getTag().toString());
/*
					Intent intent = new Intent(mcontext, News_ShareActivity.class);
					intent.putExtra("url", AppSetting.BASE_URL + "intention/details?id=" + mCurrentDatas.get(pos).id);
					intent.putExtra("title", mCurrentDatas.get(pos).intro);
					mcontext.startActivity(intent);*/
					// }
				}
			}
		});
	}

	public void setOnAction(onActionlistener IonActionlistener) {

		monActionlistener = IonActionlistener;
	}

	public List<Dynamic_CardDto> getDataList() {
		return mCurrentDatas;
	}

}
