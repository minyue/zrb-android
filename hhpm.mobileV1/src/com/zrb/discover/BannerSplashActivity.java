package com.zrb.discover;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
 
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import com.hhpm.lib.banner.BGABanner;
import com.zrb.mobile.R;

 

public class BannerSplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.banner_splash);
        BGABanner banner = (BGABanner)findViewById(R.id.banner_splash_pager);
        // 用Java代码方式设置切换动画
        banner.setTransitionEffect(BGABanner.TransitionEffect.Default);
        // banner.setPageTransformer(new RotatePageTransformer());
        // 设置page切换时长
        banner.setPageChangeDuration(1000);
        List<View> views = new ArrayList<View>();
        views.add(getPageView(R.drawable.bg_my_login2));
        views.add(getPageView(R.drawable.registered_begin));
        views.add(getPageView(R.drawable.scrollview_header));

        
        List<String> des = new ArrayList<String>();
        des.add("用Java代码方式设置切换动画");
        des.add("设置page切换时长");
        des.add("用Java代码方式设置切换动画");
   /*     View lastView = getLayoutInflater().inflate(R.layout.view_last, null);
        views.add(lastView);*/
 /*       lastView.findViewById(R.id.btn_last_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        });*/
        banner.setViewsAndTips(views,des);
        // banner.setCurrentItem(1);
    }

    private View getPageView(@DrawableRes int resid) {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(resid);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        return imageView;
    }
}
