package com.zrb.discover.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class PullListView extends ListView {
	public PullListView(Context context) {
		this(context, null, 0);
	}

	public PullListView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public PullListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}
