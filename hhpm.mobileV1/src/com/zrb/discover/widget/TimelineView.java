package com.zrb.discover.widget;

import com.zrb.mobile.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;

public class TimelineView extends View {
	private static final String LOG_TAG = "DotsView";
	private float mRadius = 0;
	private float mLineWidth = 0f;
	private boolean isTopLineShow = true;
	private float mTopLineLen = 0f;
	private boolean isDotsShow = false;
	 Paint p = null;
	public TimelineView(Context context) {
		this(context, null, 0);
	}

	public TimelineView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TimelineView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		
		p=new Paint();
		float defaultRadius = context.getResources().getDimension(
				R.dimen.dots_radius);
		float defaultTopLineLen = context.getResources().getDimension(
				R.dimen.default_top_line_length);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.dots, defStyle, 0);
		mRadius = a.getDimensionPixelSize(R.styleable.dots_timeDotsRadius,
				(int) defaultRadius);
		mTopLineLen = a.getDimensionPixelSize(R.styleable.dots_timeDotsTopLineLength,
				(int) defaultTopLineLen); 
		mLineWidth = context.getResources().getDimension(R.dimen.time_line_with);
		a.recycle();
	}
 
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
 
		p.setAntiAlias(true);
		int width = getWidth();
		int height = getHeight();
		p.setColor(getContext().getResources().getColor(
				R.color.new_ui_color_blue_dialog_block_style));
		float x = 0f;
		float y = 0f;
		x = width / 2;
		y = mTopLineLen;
		if(isDotsShow){
			canvas.drawCircle(x, y, mRadius / 2, p);
		}
		p.setStrokeWidth(mLineWidth);
		if(isTopLineShow){
			canvas.drawLine(x, 0, x, mTopLineLen, p);
		}
	//	Log.d("testDraw","onDraw mTopLineLen:" + mTopLineLen);
		//Log.d("testDraw","onDraw height:" + height);
		canvas.drawLine(x, mTopLineLen, x, height, p);
	}
	
	public void setTopLineVisibility(boolean isShow){
		isTopLineShow = isShow;
	}
	
	public void setDotsVisibility(boolean isShow){
		isDotsShow = isShow;
	}
	
}
