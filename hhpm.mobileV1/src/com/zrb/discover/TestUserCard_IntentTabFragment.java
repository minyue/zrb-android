package com.zrb.discover;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.zrb.discover.adapter.FundAdapter;
import com.zrb.mobile.BaseLazyFragment;
import com.zrb.mobile.IhandlerWrapper;
import com.zrb.mobile.R;
import com.zrb.mobile.adapter.model.DiscoverDtos;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase;
import com.zrb.mobile.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.zrb.mobile.ui.PullToRefreshListViewEx;
import com.zrb.mobile.ui.onLoadMoreListener;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.ZrbRestClient;
import com.zrb.mobile.utility.ZrbRestClient.HttpResponseListener;

 

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class TestUserCard_IntentTabFragment extends BaseLazyFragment implements onLoadMoreListener
{
	public static final String TITLE = "title";
	public static final String URLID = "urlId";
	private String mTitle = "Defaut Value";
	private String mUrlId;
	private ListView mListView;
	// private TextView mTextView;
	private List<String> mDatas = new ArrayList<String>();
	boolean isFirstIn = true;
/* 
	//private News_ContentAdapter mAdapter;*/
	private boolean isFrist=true;
	FundAdapter mFundAdapter;
 
	private String url;
	int currentPage;
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mTitle = getArguments().getString(TITLE);
			mUrlId= getArguments().getString(URLID);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
	{
		View view;
		//if(mTitle=="11") 
		{
	  	   view = inflater.inflate(R.layout.usercard_fragment_tab2, container, false);
		  mListView = (ListView) view.findViewById(R.id.id_stickynavlayout_innerscrollview);
		  
		// mTextView = (TextView) view.findViewById(R.id.id_info);
		// mTextView.setText(mTitle);
 
		
	   /*	  mAdapter = new News_ContentAdapter(this.getActivity());
		  mListView.setAdapter(mAdapter);*/
		}
	/*	else
			view = inflater.inflate(R.layout.no_content, container, false);*/
		return view;

	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
 		super.onActivityCreated(savedInstanceState);
 		
 	 
		 
 		
 		mFundAdapter = new FundAdapter(TestUserCard_IntentTabFragment.this.getActivity());
  		mListView.setAdapter(mFundAdapter) ;
  		//mListView.setOnLoadMoreListener(this);
  	 
	}
	
	private void postDelayed(Runnable curRunnable,long delay){
		if( this.getActivity()==null) return;
		
		IhandlerWrapper tmpFrag = (IhandlerWrapper) this.getActivity();
		if (tmpFrag != null)
			tmpFrag.getHandler().postDelayed(curRunnable,delay);
	}
	
/*	@Override
	public void onResume() {
		super.onResume();
		if (this.getUserVisibleHint())
			firstLoad();
	}*/

	@Override
	public void onFirstUserVisible() {
		if (isFirstIn) {
			isFirstIn = false;
 			postDelayed(new Runnable() {
				@Override
				public void run() {
					//mListView.setRefreshing(true);
					loadUserFundData(-1, true);
				}
			} ,50);
		}
	}
	
	@Override
	public void OnLoadMoreEvent() {
		// TODO Auto-generated method stub
		currentPage++;
		postDelayed(new Runnable() {
			@Override
			public void run() {
				loadUserFundData(mFundAdapter.getMinId(), false);
			}
		}, 400);

	}

	
	ZrbRestClient mZrbRestClient;
	private void loadUserFundData(long maxId, final boolean isreflesh) {
 
		RequestParams param = new RequestParams();
 		param.put("limit", 10);
		param.put("uid", mUrlId);
		if (-1 != maxId) {
			param.put("maxId",maxId);
		}
  
		String url = "intention/listByUser?" + param.toString();
		if(mZrbRestClient==null){
			mZrbRestClient=new ZrbRestClient(this.getActivity());
		}
		mZrbRestClient.setOnhttpResponseListener(new HttpResponseListener() {
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
			 
				CommonUtil.showToast(arg0, TestUserCard_IntentTabFragment.this.getActivity());
		/*		if (currentPage > 1)
					mListView.hideloading();
				else
					mListView.onRefreshComplete();*/
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				String results = new String(arg2);
		/*		if (currentPage > 1)
					mListView.hideloading();
				else
					mListView.onRefreshComplete();
				*/
				 
				try {
					Gson gson = new Gson();
					DiscoverDtos fundJson = gson.fromJson(results,
							DiscoverDtos.class);
					if (fundJson.res == 1) {

						if (fundJson.data != null && fundJson.data.size() > 0) {
							/*if (currentPage == 1) {
								mFundAdapter.setDatas(fundJson.data);
							} else {
								mFundAdapter.addAll(fundJson.data);
								//completeLoad();
							}*/
							mFundAdapter.notifyDataSetChanged();
						} else {
							//completeLoad();
							CommonUtil.showToast(String.format("暂无%1$s信息~~",currentPage == 0 ? "" : "更多"),
									 TestUserCard_IntentTabFragment.this.getActivity());
						}

					} else
						CommonUtil.showToast(fundJson.msg,  TestUserCard_IntentTabFragment.this.getActivity());
				} catch (Exception e) {
					CommonUtil.showToast(R.string.error_serverdata_loadfail, TestUserCard_IntentTabFragment.this.getActivity());
				}
			}
		});
		mZrbRestClient.sessionGet(url);
	}
	
	

	public static TestUserCard_IntentTabFragment newInstance(String title,String uId)
	{
		TestUserCard_IntentTabFragment tabFragment = new TestUserCard_IntentTabFragment();
		Bundle bundle = new Bundle();
		bundle.putString(TITLE, title);
		bundle.putString(URLID, uId);
		tabFragment.setArguments(bundle);
		return tabFragment;
	}

}
