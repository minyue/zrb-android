package com.zrb.discover;

import java.math.BigDecimal;

import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.zrb.applib.utils.AppSetting;
import com.zrb.mobile.R;
import com.zrb.mobile.User_locationsActivity;
import com.zrb.mobile.User_registerIndustry;
import com.zrb.mobile.adapter.Cityindustry_listAdapter;
import com.zrb.mobile.adapter.model.IntentionDto;
import com.zrb.mobile.register.User_phone_login;
import com.zrb.mobile.utility.CommonUtil;
import com.zrb.mobile.utility.Constants;
import com.zrb.mobile.utility.ZrbRestClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Discover_pubishmoneyActivity extends DiscoverPubishActivity implements
		OnClickListener {
	private int selectId = -1;
 
	ExpandableListView mindustrylist;
	Cityindustry_listAdapter mindustryAdapter;
	int postCode = -1;
	private String district, industry;
 	 
 
	private String name;
	// Toast��ʾ��
	private TextView tv_industry_value, tv_publisharea_value;
	private EditText edit_publishmoney_name;
	private String publishmoney_name, investment_amount, content;
	private EditText edit_investment_amount, edit_content;
	private LinearLayout tmpContainer1;
	private LinearLayout tmpContainer2;
	private int mode;
	//public String ACTION_NAME = "discover_main";
	ZrbRestClient mZrbRestClient;

	String curObj;
	IntentionDto curDto;
	
	private TextView txtTitle = null;
	private TextView mDel = null;
	private TextView mTxtrightTitle;
	
	
	public static void launch (Context mActivity){
		
		 if(mActivity==null) return;
		 Intent it = new Intent(mActivity, Discover_pubishmoneyActivity.class);
 		 mActivity.startActivity(it);
	}
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		this.setContentView(R.layout.project_publishmoney);
 		initView();
		
		isforModify=this.getIntent().getBooleanExtra("isModify", false);
		if(isforModify){
 			curObj=this.getIntent().getStringExtra("jsonObj");
	    	Gson gson = new Gson();  
	    	curDto= gson.fromJson(curObj, IntentionDto.class);  
	    	loadIntetionData(curDto.id);
		}

		/* loadData(); */
	}

	protected void initView() {
		super.initView();
 
		mProgressBar = (ProgressBar) this.findViewById(R.id.topbar_loading);
		mDel = (TextView) findViewById(R.id.delete);
		mDel.setVisibility(View.GONE);
		
		this.findViewById(R.id.title_left_root).setOnClickListener(this);
		txtTitle = (TextView) this.findViewById(R.id.txt_title);
		txtTitle.setText(getResources().getString(R.string.public_money));
  
		mTxtrightTitle = (TextView) this.findViewById(R.id.right_title);
		mTxtrightTitle.setText(isforModify ?"完成":"发布");
		mTxtrightTitle.setVisibility(View.VISIBLE);
		mTxtrightTitle.setOnClickListener(this); 
		
		tv_industry_value = (TextView) findViewById(R.id.tv_industry_value);
		tv_publisharea_value = (TextView) findViewById(R.id.tv_publisharea_value);
		edit_publishmoney_name = (EditText) findViewById(R.id.edit_publishmoney_name);
		edit_investment_amount = (EditText) findViewById(R.id.edit_investment_amount);
		edit_content = (EditText) findViewById(R.id.edit_content_text);
	
		
		findViewById(R.id.re_publish_area).setOnClickListener(this);;
 		findViewById(R.id.re_industry).setOnClickListener(this);;
		 
		((TextView) this.findViewById(R.id.tv_publishmoney_name)).setText(this
				.getString(R.string.project_publish_moneydes));
		tmpContainer1 = (LinearLayout) this.findViewById(R.id.check_option1);
		tmpContainer2 = (LinearLayout) this.findViewById(R.id.check_option2);
		for (int i = 0; i < tmpContainer1.getChildCount(); i++){
			tmpContainer1.getChildAt(i).setOnClickListener(checkboxListener);
		}
		for (int i = 0; i < tmpContainer2.getChildCount(); i++){
			tmpContainer2.getChildAt(i).setOnClickListener(checkboxListener);
		}
		mDel.setOnClickListener(this);
		if (isforModify) {
			mDel.setVisibility(View.VISIBLE);
			setEditButtonState(false);
		}
		// 设置控件

	}
 
	@Override
	protected void setEditButtonState(boolean flag){
		mTxtrightTitle.setEnabled(flag);
		mDel.setEnabled(flag);
	}
	
	  @Override
	  protected void setValue(IntentionDto dto){
		  if(dto != null){
			  curDto = dto;
		  }
		   mDel.setVisibility(View.VISIBLE);
		   txtTitle.setText(getResources().getString(R.string.discover_money));
		   TextView txtrightTitle = (TextView) this.findViewById(R.id.right_title);
		   txtrightTitle.setText("完成");
		   edit_publishmoney_name.setText(curDto.intro+"");
	 	   tv_industry_value.setText(curDto.industryName);
		   industry=curDto.industry;
		   tv_publisharea_value.setText(curDto.districtName);
		   district=curDto.district;
		   edit_investment_amount.setText(curDto.amount+"");
		   int index=0;
		   mode=curDto.mode;
		   switch(curDto.mode){
		   case   Constants.FundStockRight:
			   index=0;
			   break;
		   case   Constants.FundCreditorRight:
			   index=1;
			   break;
		   case   Constants.FundBULK_ACQUISITION:
			   index=2;
			   break;
		   case  Constants.FundPROJECT_COOPERATION:
			   index=3;
			   break;
		   case  Constants.FundOtherRight:
			   index=4;
			   break;
			   
		   }
		   if(index <= 2){
			   selectId=tmpContainer1.getChildAt(index).getId();
			   tmpContainer1.getChildAt(index).setSelected(true) ;
		   }else{
			   selectId=tmpContainer2.getChildAt(index-3).getId();
			   tmpContainer2.getChildAt(index-3).setSelected(true) ;
		   }
		   edit_content.setText(curDto.content);
		}
 
	private OnClickListener checkboxListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (v.getId() != selectId) {
				v.setSelected(true);
				if (selectId != -1)
					Discover_pubishmoneyActivity.this.findViewById(selectId).setSelected(false);
 				selectId = v.getId();
			}

			switch (v.getId()) {
			case R.id.tv_temp_name1:
				mode = Constants.FundStockRight;
				break;
			case R.id.tv_temp_name2:
				mode = Constants.FundCreditorRight;
				break;
			case R.id.tv_temp_name3:
				mode = Constants.FundBULK_ACQUISITION;
				break;
			case R.id.tv_temp_name4:
				mode = Constants.FundPROJECT_COOPERATION;
				break;
			case R.id.tv_temp_name5:
				mode = Constants.FundOtherRight;
				break;
			}

		}
	};

	
	private void saveData() {
		// 发布项目进行非空的判断
		if ("".equals(publishmoney_name)) {
			initNotify("请填写项目简介");
			return;
		}
		if (publishmoney_name.length() > 20) {
			initNotify("项目简介字数大于20");
			return;
		}
		if (industry == null) {
			initNotify("请选择投资行业");
			return;
		}
		if (district == null) {
			initNotify("请选择投资区域");
			return;
		}
		if ("".equals(investment_amount)) {
			initNotify("请填写投资金额");
			return;
		}
		if (mode == 0) {
			initNotify("请选择投资方式");
			return;
		}
		if ("".equals(content)) {
			initNotify("请填写资金详情");
			return;
		}
		
		 if(null==AppSetting.curUser){
			  CommonUtil.showToast(R.string.user_no_account, Discover_pubishmoneyActivity.this);
			  return;
		  } 
		 
	 
		publicMessage( Constants.DiscoverMoneyIntent,curDto);
	}
	
	@Override
	protected void setDelForResult() {
		public_success(curDto,RESULT_CODE_DEL);
	}
	
  @Override
  protected void setforResult(){
		
		curDto.intro=publishmoney_name;
		curDto.district=district;
		curDto.districtName=tv_publisharea_value.getText().toString();
		curDto.industry=industry;
		curDto.industryName=tv_industry_value.getText().toString();
 		curDto.mode=mode;
		curDto.content=content;
		curDto.amount=new BigDecimal(investment_amount);  
		public_success(curDto,RESULT_CODE_MODIFY);
	}
	
	@Override
	public void onClick(View v) {
		Intent intent;
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.title_left_root:
			this.finish();
			break;
		case R.id.right_title:
 	 
			publishmoney_name = edit_publishmoney_name.getText().toString();
			investment_amount = edit_investment_amount.getText().toString();
			content = edit_content.getText().toString();
 			saveData();
 			break;
		case R.id.re_publish_area:
			intent = new Intent(this, User_locationsActivity.class);
			startActivityForResult(intent, 200);
			break;

		case R.id.re_industry:
			intent = new Intent(this, User_registerIndustry.class);
			startActivityForResult(intent, 100);
			break;
			
		case R.id.delete:
			delete(curDto.id);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case 105:
				saveData();
				break;
			case 100:
				if (data != null) {
					name = data.getStringExtra("name");
					industry = data.getIntExtra("code", 0) + "";
					tv_industry_value.setText(name);
				}
				break;
			case 200:
				if (data != null) {
					name = data.getStringExtra("area");
					district = data.getStringExtra("code");
					
					//CommonUtil.showToast("district"+ district+"",Discover_pubishmoneyActivity.this);
					tv_publisharea_value.setText(name);
				}
				break;
			default:
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);

	}
	
	@Override
	protected RequestParams getParams() {
		RequestParams param = new RequestParams();
		param.put("intro", publishmoney_name);
		param.put("district", district);
		param.put("industry", industry);
		param.put("amount", investment_amount);
		param.put("mode", mode);
		param.put("type", Constants.DiscoverMoneyIntent);
		param.put("content", content);
		return param;
	}
	
}
