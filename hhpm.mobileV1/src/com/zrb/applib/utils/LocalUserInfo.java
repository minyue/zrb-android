package com.zrb.applib.utils;

 

import com.zrb.mobile.adapter.model.UserDto;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class LocalUserInfo {


    /**
     * 保存Preference的name
     */
    public static final String PREFERENCE_NAME = "local_userinfo";
    private static SharedPreferences mSharedPreferences;
    private static LocalUserInfo mPreferenceUtils;
    private static SharedPreferences.Editor editor;
   

    private LocalUserInfo(Context cxt) {
        mSharedPreferences = cxt.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
    }

    /**
     * 单例模式，获取instance实例
     * 
     * @param cxt
     * @return
     */
    public static LocalUserInfo getInstance(Context cxt) {
        if (mPreferenceUtils == null) {
            mPreferenceUtils = new LocalUserInfo(cxt);
        }
        editor = mSharedPreferences.edit();
        return mPreferenceUtils;
    }

/*  public long uid;
	public String uname;
	public String zrb_front_ut;
	public Boolean auth;
	public String org;
	public String userTypeName;
	public String userType;
	public String avatar;
	public String orgId;*/
    
    public void saveUserInfo(UserDto userinfo){
      
    	   editor.putLong("uid", userinfo.uid);
    	   editor.putString("pwd", userinfo.pwd);
    	   editor.putString("uname", userinfo.uname);
    	   editor.putString("zrb_front_ut", userinfo.zrb_front_ut);
    	   editor.putString("org", userinfo.org);
    	   editor.putString("avatar", userinfo.avatar);
    	   editor.putString("uemail", userinfo.uemail);
    	   editor.putString("position", userinfo.position);
    	   editor.putString("industry", userinfo.industry);
    	   editor.putInt("focusproject", userinfo.collectProject);//.putLong("uid", userinfo.uid);
    	   editor.putInt("focusnum", userinfo.collectedNumber);
    	   editor.putInt("focusfund", userinfo.collectFund);
           editor.commit();
     }
  
    public void clearUserInfo(){
    	
       editor.putLong("uid", -1);
   	   editor.putString("pwd", "");
   	   editor.putString("uname", "");
   	   editor.putString("zrb_front_ut","");
   	   editor.putString("org", "");
   	   editor.putString("avatar", "");
   	   editor.putString("uemail", "");
   	   editor.putString("position","");
   	   editor.putString("industry", "");
   	   editor.putInt("focusproject", 0);//.putLong("uid", userinfo.uid);
   	   editor.putInt("focusnum", 0);
   	   editor.putInt("focusfund", 0);
        editor.commit();
    }
    
    public void restoreUserInfo(){
    	UserDto curDto=new UserDto();
    	
    	curDto.uid=mSharedPreferences.getLong("uid", 0);
    	curDto.pwd=mSharedPreferences.getString("pwd", "");
    	curDto.uname=mSharedPreferences.getString("uname", "");
    	curDto.zrb_front_ut=mSharedPreferences.getString("zrb_front_ut", "");
    	curDto.org=mSharedPreferences.getString("org", "");
    	curDto.avatar=mSharedPreferences.getString("avatar", "");
    	curDto.uemail=mSharedPreferences.getString("uemail", "");
    	curDto.position=mSharedPreferences.getString("position", "");
    	curDto.industry=mSharedPreferences.getString("industry", "");
    	curDto.collectProject=mSharedPreferences.getInt("focusproject", 0);
    	curDto.collectedNumber=mSharedPreferences.getInt("focusnum", 0);
    	curDto.collectFund=mSharedPreferences.getInt("focusfund", 0);
    	
    	AppSetting.curUser=curDto;
    	
    }
    
    //
    public void setUserInfo(String str_name, String str_value) {

        editor.putString(str_name, str_value);
        editor.commit();
    }
  
    public void setUseravatar(String str_value) {
    	 if(AppSetting.curUser!=null) AppSetting.curUser.avatar=str_value;
    	 
    	  editor.putString("avatar", str_value);
          editor.commit();
    }
    
    public String getUseravatar() {
    	 return mSharedPreferences.getString("avatar", "");
    }
    
    public void setUserPosition(String str_value) {
      	if(AppSetting.curUser!=null) AppSetting.curUser.position=str_value;
    	
  	    editor.putString("position", str_value);
        editor.commit();
    }
    
    public void setUserCompany(String str_value) {
      	if(AppSetting.curUser!=null) AppSetting.curUser.org=str_value;
    	
  	    editor.putString("org", str_value);
        editor.commit();
    }
    
    public void setUserIndustry(String str_value) {
      	if(AppSetting.curUser!=null) AppSetting.curUser.industry=str_value;
    	
  	    editor.putString("industry", str_value);
        editor.commit();
    }
     
    public void setUserName(String str_value) {
      	if(AppSetting.curUser!=null) AppSetting.curUser.uname=str_value;
    	
  	    editor.putString("uname", str_value);
        editor.commit();
    }
    
    public String getUserName() {
  	   return mSharedPreferences.getString("uname", "");
    }
   
    public void setUserEmail(String str_value) {
      	if(AppSetting.curUser!=null) AppSetting.curUser.uemail=str_value;
    	
  	    editor.putString("uemail", str_value);
        editor.commit();
    }
    
    
    public String getUserInfo(String str_name) {
         return mSharedPreferences.getString(str_name, "");
     }
    
    public boolean isLogined(){
     	 boolean flag1=  mSharedPreferences.getLong("uid", 0)>0;
     	 boolean flag2= !TextUtils.isEmpty(mSharedPreferences.getString("pwd", ""));
     	 return flag1&&flag2;
    }
    
   public boolean hasGuide(){
 	  return mSharedPreferences.getBoolean("hasguide", false);
   }
   
   public void setHaveGuide(){
	   
	   editor.putBoolean("hasguide", true);
       editor.commit();
   }
}
