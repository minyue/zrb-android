package com.hhpm.lib.swipelistviewex;

public interface SwipeMenuCreator {

    void create(SwipeMenu menu);
}
