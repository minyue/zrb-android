package com.hhpm.lib.utils;

import android.util.Log;

public class ViewUtilsSingleton {
	private static ViewUtilsSingleton instance;
	private boolean isOpen = false;
	private static final String LOG_TAG = "ViewUtilsSingleton";
	private boolean mRefresh = false;
	
	private ViewUtilsSingleton() {

	}

	public static synchronized ViewUtilsSingleton getInstance() {
		if (instance == null) {
			instance = new ViewUtilsSingleton();
		}
		return instance;
	}
	
	public boolean getSwipeMenuLayoutState(){
		return isOpen;
	}
	
	public void setSwipeMenuLayoutState(boolean hasOpen){
		Log.d(LOG_TAG,"setSwipeMenuLayoutState hasOpen:" + hasOpen);
		isOpen = hasOpen;
	}
	
	public boolean getRefresh(){
		return mRefresh;
	}
	
	public void setRefresh(boolean refresh){
		mRefresh = refresh;
	}
	
}
