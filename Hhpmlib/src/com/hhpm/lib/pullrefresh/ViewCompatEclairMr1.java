package com.hhpm.lib.pullrefresh;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ViewCompatEclairMr1
{
  public static final String TAG = "ViewCompat";
  private static Method sChildrenDrawingOrderMethod;

  public static boolean isOpaque(View view)
  {
    return view.isOpaque();
  }

  public static void setChildrenDrawingOrderEnabled(ViewGroup viewGroup, boolean enabled) {
	 int version = Build.VERSION.SDK_INT;
	 if(version<7) return;
    if (sChildrenDrawingOrderMethod == null) {
      try {
        sChildrenDrawingOrderMethod = ViewGroup.class.getDeclaredMethod("setChildrenDrawingOrderEnabled", new Class[] { Boolean.TYPE });
      }
      catch (NoSuchMethodException e) {
        Log.e("ViewCompat", "Unable to find childrenDrawingOrderEnabled", e);
      }
      sChildrenDrawingOrderMethod.setAccessible(true);
    }
    try {
      sChildrenDrawingOrderMethod.invoke(viewGroup, new Object[] { Boolean.valueOf(enabled) });
    } catch (IllegalAccessException e) {
      Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", e);
    } catch (IllegalArgumentException e) {
      Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", e);
    } catch (InvocationTargetException e) {
      Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", e);
    }
  }
}